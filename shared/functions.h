#pragma once
#include <limits>
#include <type_traits>
#include <locale>
#include <algorithm>
#include <cctype>

#ifndef ARKE_DEF_ARITH
#define ARKE_DEF_ARITH(RET) auto
//std::enable_if<std::is_integral<T>::value || std::is_floating_point<T>::value, RET>::type
#endif

namespace arke {
//Now part of NVX
/*#ifndef FUNC_CLAMP
#define FUNC_CLAMP
template <typename TA, typename TB, typename TC>
inline ARKE_DEF_ARITH(T) clamp(TA x, TB a, TC b) {
  return x < a ? a : (x > b ? b : x);
}
#endif

#ifndef FUNC_SATS
#define FUNC_SATS
template<typename T> ARKE_DEF_ARITH(T) satAdd(T first, T second) {
  return first > std::numeric_limits<T>::max() - second ? std::numeric_limits<T>::max() : first + second;
}
template<typename T> ARKE_DEF_ARITH(T) satSub(T first, T second) {
  return second > first ? std::numeric_limits<T>::min() : first - second;
}
#endif

template <typename TA, typename TB>
inline ARKE_DEF_ARITH(T) extreme(TA a, TB b) {
  if(abs(a) > abs(b)) return a;
  else return b;
}

template <typename TA, typename TB>
inline ARKE_DEF_ARITH(T) max(TA a, TB b) {
  return a > b ? a : b;
  //return std::max(a, b);
}

template <typename TA, typename TB>
inline ARKE_DEF_ARITH(T) min(TA a, TB b) {
  return a < b ? a : b;
  //return std::min(a, b);
}

template <typename TA, typename TB, typename TC>
bool between(TA v, TB s, TC b) {
  return (v > s && v < b);
}

template <typename T> ARKE_DEF_ARITH(T) sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

template<typename K>
inline K wrapMP(K in) {
  if(in >= (K)M_PI) return in - (K)M_PI;
  else if(in <= -(K)M_PI) return in + (K)M_PI;
  else return in;
}

template <class T>
inline ARKE_DEF_ARITH(T) lerp(T a, T b, float m) {
  m = clamp(m, 0.0f, 1.0f);
  return a * (1.0f - m) + b * m;
}
*/
template<typename T>
inline T sgnmod(T k, T n) {
    return ((k %= n) < 0) ? k+n : k;
}

// The result (the remainder) has same sign as the divisor.
// Similar to matlab's mod(); Not similar to fmod() -   Mod(-3,4)= 1   fmod(-3,4)= -3
template<typename T>
ARKE_DEF_ARITH(T) sfmod(T x, T y) {
  //if (0.== y) return x;
  T m= x - y * floor(x/y);

  // handle boundary cases resulted from floating-point cut off:
  if (y > 0) {            // modulo range: [0..y)
    if (m >= y) return T(0);// Mod(-1e-16             , 360.    ): m= 360.
    else if (m < 0) {
      if(y+m == y) return T(0); // just in case...
      else return y+m; // Mod(106.81415022205296 , _TWO_PI ): m= -1.421e-14
    }
  } else {                  // modulo range: (y..0]
    if (m<=y) return T(0);// Mod(1e-16              , -360.   ): m= -360.

    if (m>0 ) {
      if (y+m == y) return T(0); // just in case...
      else return y+m; // Mod(-106.81415022205296, -_TWO_PI): m= 1.421e-14
    }
  }

  return m;
}



// wrap [rad] angle to [-PI..PI)
inline double wrapPi(double fAng) {
  return sfmod(fAng + _PI, _TWO_PI) - _PI;
}

inline float fWrapPi(float fAng) {
  return sfmod(fAng + (float)_PI, (float)_TWO_PI) - (float)_PI;
}

// wrap [rad] angle to [0..TWO_PI)
inline double wrapTwoPi(double fAng) {
  return sfmod(fAng, _TWO_PI);
}

// wrap [deg] angle to [-180..180)
inline double wrap180(double fAng) {
  return sfmod(fAng + 180., 360.) - 180.;
}

// wrap [deg] angle to [0..360)
inline double wrap360(double fAng) {
  return sfmod(fAng ,360.);
}

template<typename K>
inline K lerpAngle(K u, K v, K p) {
  return u + p*((K)sfmod((v-u) + (K)_PI, (K)_TWO_PI) - (K)_PI);
}

};

template<typename T>
inline void ensureTrailing(T &str, const char c) {
  if(str[str.length()-1] != c) str += c;
}

template<typename T>
inline void trimTrailing(T &str, const char c) {
  if(str[str.length()-1] == c) str = str.substr(0, str.length()-1);
}

// trim from start (in place)
inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

// trim from start (copying)
inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// trim from end (copying)
inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// trim from both ends (copying)
inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}

template <typename T> inline T _smear(T mask, T val) {
  T ret = (mask & val);
  if(ret != (T)(0x0)) return ~((T)0x0) & mask;
  else return (T)(0x0);
}
template <typename T> inline T _excl(T mask, T ret) {
  ret &= mask; return ret ^= (ret & (ret-1));
}

inline uint32_t _rol(uint32_t x, uint32_t n) {
  return (x<<n) | (x>>(32-n));
}

inline uint64_t _rol(uint64_t x, uint64_t n) {
  return (x<<n) | (x>>(64-n));
}

#define INTRIN_ALIAS_T(STEM, ALIAS) \
inline uint8_t _##ALIAS(uint64_t v) {\
    return __builtin_##STEM##ll(v);\
}\
inline uint8_t _##ALIAS(uint32_t v) {\
    return __builtin_##STEM(v);\
}

#define INTRIN_ALIAS_TF(ALIAS, BODY) \
inline uint8_t _##ALIAS(uint64_t v) {\
    return ({ BODY; });\
}\
inline uint8_t _##ALIAS(uint32_t v) {\
    return ({ BODY; });\
}

#define INTRIN_ALIAS(STEM, ALIAS) \
template<typename... Args>\
inline void ALIAS(Args... args) {\
    __builtin_##STEM(args...);\
}

INTRIN_ALIAS_T(ffs, ffs);
INTRIN_ALIAS_T(clz, clz);
INTRIN_ALIAS_T(ctz, ctz);
INTRIN_ALIAS_T(popcount, ct1);
INTRIN_ALIAS_T(parity, parity);

INTRIN_ALIAS_TF(fsb, v == 0x0? 0x0 : sizeof(v)*8 - _fsb(v) - 1);

INTRIN_ALIAS(prefetch, prefetch);
INTRIN_ALIAS(clear_cache, flush);
INTRIN_ALIAS(LINE, line);
INTRIN_ALIAS(FILE, file);
INTRIN_ALIAS(FUNCTION, function);
INTRIN_ALIAS(trap, trap);
INTRIN_ALIAS(unreachable, unreachable);
INTRIN_ALIAS(expect, expect);
INTRIN_ALIAS(choose_expr, ternary);
INTRIN_ALIAS(types_compatible, like);
INTRIN_ALIAS(alloca, alloca);
INTRIN_ALIAS(alloca_with_align, allocaa);


#ifndef _bit
#define _bit(x) (uint64_t(0x1) << (x))
#define _rbit(x) _ctz(x)
#endif

inline uint16_t _bswap(const uint64_t&& v) {
    return __builtin_bswap64(v);
}
inline uint16_t _bswap(const uint32_t&& v) {
    return __builtin_bswap32(v);
}
inline uint16_t _bswap(const uint16_t&& v) {
    return __builtin_bswap16(v);
}

#ifdef __amd64__
#include <x86intrin.h>
/*inline uint8_t _fsb(uint64_t v) {
    return __lzcnt64(v);
}
inline uint8_t _fsb(uint32_t v) {
    return __lzcnt32(v);
}*/
#elif __arm__
/*inline uint8_t _fsb(uint64_t v) {
    return __builtin_clzll(v);
}
inline uint8_t _fsb(uint32_t v) {
    return __builtin_clz(v);
}*/
#endif

#include "optis.h"
