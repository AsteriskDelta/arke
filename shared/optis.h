#ifndef ARKE_OPTIS_H
#define ARKE_OPTIS_H

inline int64_t ipow(int64_t base, uint8_t exp) {
    static const uint8_t highest_bit_set[] = {
        0, 1, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 255, // anything past 63 is a guaranteed overflow with base > 1
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
    };

    uint64_t result = 1;

    switch (highest_bit_set[exp]) {
    case 255: // we use 255 as an overflow marker and return 0 on overflow/underflow
        if (base == 1) {
            return 1;
        }
        
        if (base == -1) {
            return 1 - 2 * (exp & 1);
        }
        
        return 0;
    case 6:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 5:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 4:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 3:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 2:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 1:
        if (exp & 1) result *= base;
    default:
        return result;
    }
}

inline constexpr uint64_t ilog2(const uint64_t& x) {
    return (static_cast<uint64_t>(64) - __builtin_clzll(x) - 1);
}
inline constexpr int64_t ilog2(const int64_t& x) {//Remove sign bit
    return (static_cast<uint64_t>(64) - __builtin_clzll(x & ~(static_cast<uint64_t>(0x01) << 63)) - 1);
}
inline constexpr uint32_t ilog2(const uint32_t& x) {
    return (static_cast<uint32_t>(32) - __builtin_clzl(x) - 1);
}
inline constexpr int32_t ilog2(const int32_t& x) {//Remove sign bit
    return (static_cast<uint32_t>(32) - __builtin_clzll(x & ~(static_cast<uint32_t>(0x01) << 31)) - 1);
}
inline constexpr uint64_t iconstant64mult(const long double &cons) {
    const uint64_t p2 = 53 - (static_cast<uint64_t>(log2(cons)) + 1);//Minimum power of 2 promised to be representable by constexpr double
    return (static_cast<uint64_t>(0x1) << p2);
}
inline constexpr uint32_t iconstant32mult(const long double &cons) {
    const uint32_t p2 = 30 - (static_cast<uint32_t>(log2(cons)) + 1);//Minimum power of 2 promised to be representable by constexpr double
    return (static_cast<uint32_t>(0x1) << p2);
}
inline constexpr int64_t iconstant64(const long double& cons) {
    return static_cast<int64_t>( round(cons * iconstant64mult(cons)) );
}
inline constexpr int32_t iconstant32(const long double& cons) {
    return static_cast<int32_t>( round(cons * iconstant32mult(cons)) );
}
///inline constexpr int64_t imax64mult(const uint64_t& bits, const uint)

#endif /* OPTIS_H */

