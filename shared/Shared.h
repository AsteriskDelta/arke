#ifndef INC_SHARED
#define INC_SHARED

#define DCDEBUG
#pragma GCC diagnostic ignored "-Wnarrowing"

#ifdef OPTIMIZE
#define OPT_INLINE

#endif

#include "preprocessor.h"
#include "defines.h"
#include "types.h"
#include "functions.h"

//Damnit, GCC. Will be removed when some idiot remembers the * after void >_>
#ifndef __builtin_alloca
#define __builtin_alloca(N) __builtin_alloca_with_align(N, 8)
#endif

#include "structs.h"
#include "metaprog.h"
#include "constexpr.h"
#include <functional>

#endif
