#ifndef METAPROG_H
#define METAPROG_H
#include <type_traits>

#ifndef VARIADIC_SAME_F
#define VARIADIC_SAME_F
template<typename T> void variadic_same_check(const T& v) {
    ((void)v);
}
template<typename T1, typename T2>
void variadic_same_check(const T1& first, const T2& second) {
    static_assert(std::is_same<T1, T2>::value, "Variadic template parameters don't have same type!");
    ((void)first); ((void)second);
}
template<typename T1, typename T2, typename... Args>
void variadic_same_check(const T1& first, const T2& second,  const Args&... args) {
    static_assert(std::is_same<T1, T2>::value, "Variadic template parameters don't have same type!");
    ((void)first); ((void)second);
    variadic_same_check(args...);
}
template<typename... Args>
void variadic_all_same(const Args&... args) {
    variadic_same_check(args...);
}
#endif


template<typename T>
inline constexpr _first_n_bits(const unsigned char& N) {
    return (T(1) << T(N)) - T(1);
}

namespace std {
    template <template <typename...> class C, typename...Ts>
    std::true_type is_base_of_template_impl(const C<Ts...>*);

    template <template <typename...> class C>
    std::false_type is_base_of_template_impl(...);

    template <typename T, template <typename...> class C>
    using is_template_of = decltype(is_base_of_template_impl<C>(std::declval<T*>()));
}

/*#ifndef nth_variadic

template<unsigned n>
struct nth_variadic_get {
    template<class X, class... Xs>
    constexpr auto operator()(X x, Xs... xs) {
        constexpr_if(n > sizeof...(xs)) {
            return;
        } else if constexpr(n > 0) {
            return nth_variadic_get < n - 1 >{}(xs...);
        } else {
            return x;
        }
    }
};
#define nth_variadic(A, n) nth_variadic_get<(N)>(A)
#endif*/

#include <array>
template<class Function, std::size_t... Indices>
constexpr auto make_array_helper(Function f, std::index_sequence<Indices...>) 
-> std::array<typename std::result_of<Function(std::size_t)>::type, sizeof...(Indices)> {
    return {{ f(Indices)... }};
}

template<int N, class Function>
constexpr auto make_array(Function f)
-> std::array<typename std::result_of<Function(std::size_t)>::type, N> {
    return make_array_helper(f, std::make_index_sequence<N>{});    
}

template<typename T, unsigned int N>
inline constexpr T _arrayOf(const T val) {
    return make_array([val](unsigned int idx) -> T { ((void)idx); return val; });
}
template<int N, class Function>
constexpr auto _arrayOf(Function f)
-> std::array<typename std::result_of<Function(std::size_t)>::type, N> {
    return make_array_helper(f, std::make_index_sequence<N>{});    
}

template<typename V, V val>
constexpr V return_given_value(const std::size_t& idx) {
    ((void)idx);
    return val;
}

#endif /* METAPROG_H */

