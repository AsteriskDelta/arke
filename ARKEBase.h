#ifndef ARKEBASE_H
#define ARKEBASE_H
#include <Spinster/Spinster.h>
#include <cstdlib>
#include <unistd.h>

#include "Shared.h"
#include "ProgramStat.h"

//#ifndef ARKEBASE_EXTERNAL
#include <NVX/NVX.h>
namespace arke {
    using namespace nvx;

    typedef NI2<22,10,0> nvxi_t;
    typedef NI2<32, 0,0> ni_t;
    typedef NI2<40,24> time_t;
    typedef NVX2<3, nvxi_t> Vec3;
    typedef NVX2<2, nvxi_t> Vec2;
    typedef NVX2<2, ni_t> iVec2;
    typedef NVX2<3, ni_t> iVec3;

    template<int C, typename T> class Image;
    typedef Image<4, Uint8> ImageRGBA;
    typedef Image<3, Uint8> ImageRGB;

    extern Uint64 GlobalFrameID;

    class Renderer;
    class Window;
    class Swapchain;
    class Window;
    class Surface;
    class RenderQueue;
    class Device;
    class Texture;
};
//#endif

namespace ARKE = arke;

#endif /* ARKEBASE_H */
