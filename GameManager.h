#ifndef ARKE_GAMEMANAGER_H
#define ARKE_GAMEMANAGER_H
#include <string>

namespace arke {
    class Camera; class RenderStage;
    
    namespace GameManager {
    //Universal
    void Init();//Called immediatly on execution
    void PrimaryLoad();//Load required assets (for low-level, ie pre loading screen)
    void SecondaryLoad();//Loads assets(run aschronously with loading screen)
    
    //Low-level client
    void ClientSetup();//Called after SecondaryLoad is completed
    void WindowChanged();//Called at window change
    
    //Client-side stuff, called in order listed
    void Update(float newDeltaTime);
    void OnOpaque3D(Camera *const camera, RenderStage *const stage);
    void OnTransparent3D(Camera *const camera, RenderStage *const stage);
    void OnUI3D(Camera *const camera, RenderStage *const stage);
    void OnOrtho();
    
    void Render();
    
    unsigned short GetDistortionCount();
    void OnDistortion(unsigned short id);
    
    //Server-side stuff
    void OnTick();
    
    void Cleanup();
    
    extern bool isServer, isClient;
    };

};

#endif
