#ifndef ARKE_RETURN_H
#define ARKE_RETURN_H
#include "ARKEBase.h"

namespace arke {
    enum ReturnStatus {
        Success = 0,
        Fatal = 1,
        Error = 2,
        Warning = 3,
        Comment = 4
    };

    template<typename T>
    class Return : public T {
    public:
        inline Return(T&& val) : T(val) {

        }
        inline Return(ReturnStatus staus, const std::string& msg) {

        }
    protected:

    };
};

#endif
