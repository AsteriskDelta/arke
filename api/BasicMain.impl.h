#include "Engine.h"
#include <iostream>
#include "Color.h"
#include "Image.h"
#include "AppData.h"
#include "Console.h"
#include "Application.h"
#include <unistd.h>
#include <V3.h>

#include "Window.h"
#include "Input.h"
#include "Testing.h"
#include "Camera.h"
#include "Shader.h"
#include "Pipeline.h"
#include "rInterface.h"
#include "Model.h"

#include "GameManager.h"
/*
Model *model;
Testing *testing;
Shader *opaque;

REND_DRAW_R renderScene REND_DRAW_D {
  if(stage->hasFlag(RenderStage::DrawOpaque)) {
    model->draw();
    //opaque->activate();
    //testing->DrawSphere();
    //opaque->deactivate();
  }
  return 1;
}*/

int main(int argc, char** argv) {
  _unused(argc); _unused(argv);
  GameManager::Init();
  
  GameManager::PrimaryLoad();
  GameManager::SecondaryLoad();
  
  GameManager::ClientSetup();
  
  int exitCode = 0;
  try {
    while(true) {
      GameManager::Render();

      GameManager::Update(1.f);
      //if(_globalFrame % 60 == 0) std::cout << "FPS: " << (1.f/_deltaTime) << "\n";
      //usleep(std::max(0, int((0.014f-_deltaTime)*1400)));
    }
  } catch(const Application::Exiting &ex) {
    exitCode = ex.code;
  } catch (const std::exception& ex) {
    Console::Fatal(ex.what());
    return 1;
  } catch(...) {
    std::cout << "Caught something...\n";
  }
  /*
  pipeline.Deallocate();
  
  delete model;
  delete camera; delete testing;
  delete input;
  delete window;*/
  
  return exitCode;
}