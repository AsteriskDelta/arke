#include "Transformable.h"
#include "Proxy.h"
//#include <glm/gtx/matrix_transform_2d.hpp>
//#include <glm/gtx/transform.hpp>

namespace arke {
    void Transformable::setParent(Transformable * const& newPar) {
        _setParent<Transformable>(newPar);
    }

    void Transformable::clearParent(Transformable * const& newPar) {
        if (Transformable::parent() != newPar) return;
        this->_parentPtr = nullptr;
        //Transformable::_setParent<Transformable>(nullptr);
    }
};
