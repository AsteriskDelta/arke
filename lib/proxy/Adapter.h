#ifndef ARKE_ADAPTER_H
#define ARKE_ADAPTER_H
#include "ARKE.h"

#define _adapterCastFunc(CN, FN, PTR) inline virtual CN* FN() { return (CN*)PTR; };\
inline virtual const CN* FN##Const() const { return (const CN*)PTR; };

#define _adapterFuncImpl(CN, PTR)  template<> inline CN* Adapter::adapt<CN>() { return (CN*)PTR; };\
template<> inline const CN* Adapter::adaptConst<CN>() const { return (const CN*)PTR; };

#define _adaptable() inline operator Adaptable&() { return *this; };\
inline operator const Adaptable&() const { return *this; };

namespace arke {
    class Adapter {
    public:

        inline Adapter() {
        };

        inline Adapter(const Adapter& o) {
            _unused(o);
        };

        inline virtual ~Adapter() {
        };

        //inline virtual Adapter* self() { return this; };
        //inline virtual const Adapter* selfConst() { return this; };

        template<typename T>
        inline T* adapt() {
            return nullptr;
        }
        template<typename T>
        inline const T* adaptConst() const {
            return nullptr;
        }

        inline virtual Adapter* parent() const { return nullptr; };
        template<typename T>
        inline T* parent_t() const {
            return dynamic_cast<T*>(this->parent());
        }

        /* Returns true if object should be retained after parent deletion, otherwise false promises deletion
         * If you return true, MAKE SURE you do *something* in an overload of this function, lest this object linger eternally in RAM.
         */
        inline virtual bool onOrphaned() { return false; };

        _adapterCastFunc(Adapter, adapter, this);

    };

    template<typename T>
    inline T* _adapt(Adapter *const& ptr) {
        return dynamic_cast<T*>(ptr);
    }

    template<typename T, typename K>
    inline T* _derive(K *const& ptr) {
        return dynamic_cast<T*>(ptr);
    }

    _adapterFuncImpl(Adapter, this);

};

#endif
