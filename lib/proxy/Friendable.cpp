#include "Friendable.h"

namespace arke {
Friendable::~Friendable() {
    this->clearFriends();
}

void Friendable::clearFriends() {
    if(_friends.size() == 0) return;
    //Unfriend everyone, prior to our children (if they were included in friends)
    for(auto it = this->beginFriends<Friendable>(); it != this->endFriends<Friendable>(); ) {
        Friendable* curFriend = &(*it);
        //This may invalidate our iterator, so increment it beforehand
        ++it;
        curFriend->removeFriend(this);
    }
    this->_friends.clear();
}
};
