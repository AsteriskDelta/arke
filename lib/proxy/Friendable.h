#ifndef ARKE_FRIENDABLE_H
#define ARKE_FRIENDABLE_H
#include "ARKE.h"
#include "Adapter.h"
#include <unordered_set>

namespace arke {
class Friendable  : public virtual Adapter {
public:
    inline Friendable() : _friends() {

    }
    virtual ~Friendable();

    template<typename T>
    struct Iterator {
        inline Iterator(std::unordered_set<Adapter*>* h, std::unordered_set<Adapter*>* n = nullptr) : holder(h), nextHolder(n) {
            it = holder->begin();
            if(it == holder->end()) return;
            if(_adapt<T>(*it) == nullptr) ++(*this);
        };
        inline Iterator(const Iterator<T>& o) : holder(o.holder), it(o.it) {};
        inline Iterator(const std::unordered_set<Adapter*>::iterator& newIt) : holder(nullptr), nextHolder(nullptr), it(newIt) {};
        inline ~Iterator() {};
        std::unordered_set<Adapter*> *holder, *nextHolder;
        std::unordered_set<Adapter*>::iterator it;

        inline T& operator*() const { return *_derive<T>(*it); };
        inline T* operator->() const { return _derive<T>(*it); };
        inline Iterator<T>& operator++() {
            while((++it) != holder->end()) {
                //std::cout << "testing " << (*it) << "\n";
                if(_derive<T>(*it) != nullptr) return *this;
            }
            if(nextHolder != nullptr) {
                *this = Iterator<T>(nextHolder);
            }
            return *this;
        }

        inline bool operator!=(const Iterator<T>& o) {
            return it != o.it;
        }
    };
    template<typename T> inline Iterator<T> beginAll() {
        return Iterator<T>(&_friends);
    };
    template<typename T> inline Iterator<T> endAll() {
        return Iterator<T>(_friends.end());
    };
    template<typename T> inline Iterator<T> beginFriends() {
        return Iterator<T>(&_friends);
    };
    template<typename T> inline Iterator<T> endFriends() {
        return Iterator<T>(_friends.end());
    };

    //Both may be called multiple times (ie, try to add an existing friend) without ill effect.
    template<typename T>
    inline void addFriend(T obj) {
        auto frPair = _friends.emplace((Adapter*)obj);
        //We got a new friend, make sure they friended us
        if(frPair.second == true) obj->addFriend(this);
    }
    template<typename T>
    inline void removeFriend(T obj) {
        bool wasFriend = (_friends.erase((Adapter*)obj) > 0);
        if(wasFriend) obj->removeFriend(this);
    }

    inline unsigned int friendCount() const {
        return _friends.size();
    }

    //Unfriend everyone
    virtual void clearFriends();

    std::unordered_set<Adapter*> _friends;//Intersects, but doesn't own
private:

};
};

#endif /* FRIENDABLE_H */
