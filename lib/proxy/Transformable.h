#ifndef ARKE_TRANSFORMABLE_H
#define ARKE_TRANSFORMABLE_H
#include "ARKE.h"
#include "Adapter.h"

namespace arke {
    class Proxy;
class Transformable : public virtual Adapter {
public:
    inline Transformable() : _parentPtr(nullptr) {

    }
    inline Transformable(const Transformable& orig) : Adapter(orig), _parentPtr(orig._parentPtr) {

    }
    inline virtual ~Transformable() {
        this->setParent(nullptr);
    }

    inline virtual Transformable* self() { return this; };

    inline virtual Proxy* proxy() { return nullptr; };

    //Pay no mind to pointer hacking... so long as the vtable lives, so to shall this code's use
    inline virtual Transformable *parent() const override {
        return reinterpret_cast<Transformable*>(_parentPtr);
    };
    template<typename T, typename PRX=Proxy*>
    inline void add(T obj) {
        //std::cout << "This=" << this << "\n";
        //std::cout << "self=" << this->self() << "\n";
        //std::cout << "proxy=" << this->proxy() << "\n";
        if(this->proxy() != nullptr) {
            reinterpret_cast<PRX>(proxy())->add(obj);
        }
    }
    template<typename T, typename PRX=Proxy*>
    inline void remove(T obj) {
        if(this->proxy() != nullptr) {
            reinterpret_cast<PRX>(proxy())->remove(obj);
        }
    }

    virtual void setParent(Transformable *const& newPar);
    inline virtual void setParentDeleted() {
        _parentPtr = nullptr;
    }


    inline virtual void onSetParent() {};

    template<typename T>
    inline void _setParent(T * const& newPar) {
        if (newPar == _parentPtr) return;

        if (_parentPtr != nullptr) this->parent()->remove(this->self());
        _parentPtr = newPar;
        //We're only invoked by add, don't call recursivly
        //if (newPar != nullptr) this->parent()->add(this->self());
        this->onSetParent();
    }

    void clearParent(Transformable *const& newPar);
protected:
    void* _parentPtr;
};
};

#endif /* TRANSFORMABLE_H */
