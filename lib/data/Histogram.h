#ifndef ARKE_HISTOGRAM_H
#define ARKE_HISTOGRAM_H
#include <string>
#include <list>

namespace arke {
    class Histogram {
    public:
        Histogram(unsigned int mPts = 64);
        Histogram(const Histogram& orig);
        virtual ~Histogram();
        
        struct Stats {
            double mean, median;
            double stdDev, stdDev2;
            double min, max;
            unsigned int size;
            std::string toString();
        };
        struct Pt {
            double mean, weight;
            double variance, radius;
        };
        
        void add(double val, double weight = 1.f);
        double get(double val);
        Stats calculateStats();
    private:
        std::list<Pt> pts;
        double min, max;
        double totalVal, totalWeight;
        unsigned int maxPoints, inputPointCount;
    };
};

#endif /* HISTOGRAM_H */

