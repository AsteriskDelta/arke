#ifndef FIXEDPRIORITYQUEUE_H
#define FIXEDPRIORITYQUEUE_H
#include <algorithm>

#include <queue>
namespace std {
    template<typename T>
    class fixed_priority_queue : public priority_queue<T>  {
    public:
        fixed_priority_queue(unsigned int size) : fixed_size(size) {}
        void push(const T& x) 
        { 
        // If we've reached capacity, find the FIRST smallest object and replace
        // it if 'x' is larger
        if(this->size() == fixed_size)
        {
            // 'c' is the container used by priority_queue and is a protected member.
            auto beg = priority_queue<T>::c.begin(); auto end = priority_queue<T>::c.end();
            auto min = std::min_element(beg, end);
            if(x > *min)
            {
                *min = x;
                // Re-make the heap, since we may have just invalidated it.
                std::make_heap(beg, end);
            }
        }
        // Otherwise just push the new item.
        else          
        {
            priority_queue<T>::push(x);
        }
        }
    private:
        fixed_priority_queue() {} // Construct with size only.
        const unsigned int fixed_size;
        // Prevent heap allocation
        void * operator new   (size_t);
        void * operator new[] (size_t);
        void   operator delete   (void *);
        void   operator delete[] (void*);
    };
};

#endif /* FIXEDPRIORITYQUEUE_H */

