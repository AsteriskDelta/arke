#ifndef ARKE_IDMAP_INC
#define ARKE_IDMAP_INC
#include <iostream>
#include <algorithm>

namespace arke {

    template <typename IDMT, typename KT = unsigned short>
    class IDMap {
    public:
    IDMap(unsigned short maxMembers);
    ~IDMap();
    
    unsigned short count();
    
    IDMT* get(KT id, const IDMT &def);
    IDMT* get(KT id);
    IDMT* set(KT id, const IDMT &value);
    
    //Get min and max IDs
    KT max();
    KT min();
    
    bool remove(KT id);
    
    void clear();
    
    private:
    unsigned short maxValues, curValues;
    
    KT *idList;
    IDMT *dataList;
    };


    template <typename IDMT, typename KT>
    IDMap<IDMT, KT>::IDMap(unsigned short maxMembers) {
    maxValues = maxMembers;
    idList = new KT[maxValues];
    dataList = new IDMT[maxValues];
    curValues = 0;
    }

    template <typename IDMT, typename KT>
    IDMap<IDMT, KT>::~IDMap() {
    delete[] idList;
    delete[] dataList;
    }

    template <typename IDMT, typename KT>
    unsigned short IDMap<IDMT, KT>::count() {
    return curValues;
    }

    template <typename IDMT, typename KT>
    IDMT* IDMap<IDMT, KT>::get(KT id) {
    for(unsigned short i = 0; i < curValues; i++) {
        if(idList[i] == id) return &dataList[i];
    }
    return NULL;
    }

    template <typename IDMT, typename KT>
    IDMT* IDMap<IDMT, KT>::get(KT id, const IDMT &def) {
    for(KT i = 0; i < curValues; i++) {
        if(idList[i] == id) return &dataList[i];
    }
    
    return this->set(id, def);
    }

    template <typename IDMT, typename KT>
    IDMT* IDMap<IDMT, KT>::set(KT id, const IDMT &value) {
    unsigned short useID = 0xFFFF;
    for(unsigned short i = 0; i < curValues; i++) {
        if(idList[i] == id) { useID = i; break; };
    }
    
    //No Id in place, must add
    if(useID == 0xFFFF) {
        /*if(curValues == maxValues) {
        //std::cout << "MAX VAL REACHED!!!\n";
        //Overwrite minimum value
        IDMT *min = &dataList[0]; unsigned short minID = idList[0];
        for(unsigned short i = 1; i < curValues; i++) {
        if(dataList[i] < *min) {
        min = &dataList[i];
        minID = i;
        }
        }
        useID = minID;
        } else {*/
        useID = curValues;
        curValues++;
        //}
        
        idList[useID] = id;
    }
    
    dataList[useID] = value;
    return &dataList[useID];
    }

    template <typename IDMT, typename KT>
    KT IDMap<IDMT, KT>::max() {
    return idList[std::distance(dataList, std::max_element(dataList, dataList+curValues))];
    }

    template <typename IDMT, typename KT>
    KT IDMap<IDMT, KT>::min() {
    return idList[std::distance(dataList, std::min_element(dataList, dataList+curValues))];
    }

    template <typename IDMT, typename KT>
    bool IDMap<IDMT, KT>::remove(KT id) {
    for(unsigned short i = 0; i < curValues; i++) {
        if(idList[i] == id) {
        if(i != curValues-1) {
        std::swap(idList[i], idList[curValues-1]);
        std::swap(dataList[i], dataList[curValues-1]);
        }
        curValues--;
        return true;
        }
    }
    
    return false;
    }

    template <typename IDMT, typename KT>
    void IDMap<IDMT, KT>::clear() {
    curValues = 0;
    }

    #ifdef UNITTEST
        SCENARIO( "IDMap general use", "[IDMap]" ) {
        
        GIVEN( "a map of up to 100 items" ) {
            const int qIMax = 100;
            IDMap<int> q(qIMax);
            for(int i = 0; i < qIMax; i++) q.set(i, i*2 + 1);
            
            REQUIRE(q.min() == 0);
            REQUIRE(q.max() == (qIMax-1));
            
            REQUIRE(*q.get(q.min()) == 1);
            REQUIRE(*q.get(q.max()) == (2*qIMax - 1));
            /*
            WHEN( "one item is pushed and two popped, indefinitely" ) {
            int place = 10;
            while(q.count() > 0) {
            int startSize = q.count();
            int a = q.pop();
            q.push(place++);
            int b = q.pop();
            REQUIRE(q.count() == startSize-1);
            REQUIRE( (a+1) == b );
            }
            
            THEN( "the size shall reach zero with all items in order" ) {
            REQUIRE(q.count() == 0);
            }
            }*/
            
        }
        
        }

    #endif

};
#endif
