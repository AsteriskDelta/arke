#include "Histogram.h"
#include <sstream>

namespace arke {
    Histogram::Histogram(unsigned int mPts) : pts() {
        min = 999999999.0; max = -999999999.0;
        totalVal = totalWeight = 0.f;
        maxPoints = mPts;
        inputPointCount = 0;
    }

    Histogram::Histogram(const Histogram& orig) : pts(orig.pts), min(orig.min), max(orig.max), totalVal(orig.totalVal), totalWeight(orig.totalWeight), maxPoints(orig.maxPoints), inputPointCount(orig.inputPointCount) {
    
    }

    Histogram::~Histogram() {
    }

    void Histogram::add(double val, double weight) {
        
        totalVal += val*weight;
        totalWeight += weight;
        min = std::min(min, val);
        max = std::max(max, val);
        inputPointCount++;
        }
        double Histogram::get(double val) {
        double ret = 0.f; double weight = 0.f;
        
        return ret/weight;
    }
    Histogram::Stats Histogram::calculateStats() {
        Stats ret;
        ret.min = min; ret.max = max;
        ret.mean = totalVal/totalWeight;
        ret.size = inputPointCount;
        return ret;
    }

    std::string Histogram::Stats::toString() {
        std::stringstream ss;
        ss << "Hist{mean="<<mean<<", from " << min << " -> " << max << " over " << size << "}";
        return ss.str();
    }

};
