#include "SmallMap.h"

namespace arke {
    template<typename K>
    using SmallSet = SmallMap<K,bool>;
};
