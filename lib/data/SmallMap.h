#ifndef D13_SMALLMAP_H
#define D13_SMALLMAP_H
#include "ARKE.h"
#include <list>

namespace arke {
    template<typename KEY, typename VAL>
    class SmallMap {
    public:
        typedef KEY Key;
        typedef VAL Value;
        inline SmallMap() {

        }
        inline ~SmallMap() {

        }

        inline Value* get(const Key& k) {
            for(auto it = data.begin(); it != data.end(); ++it) {
                if(it->key == k) return &it->value;
            }
            return nullptr;
        }
        inline auto& operator[](const Key& k) {
            return this->get(k);
        }

        inline void erase(const Key& k) {
          for(auto it = data.begin(); it != data.end(); ++it) {
              if(it->key == k) {
                  data.erase(it);
              }
          }
        }
        inline Value* add(const Key& k, const Value& v) {
            data.push_back(Ins(k,v));
            return &data.back().value;
        }
        inline Value* add(const Key& k) {
          data.push_back(Ins(k, Value()));
          return &data.back().value;
        }

        inline bool contains(const Key& k) {
            return this->get(k) != nullptr;
        }
        inline size_t size() const {
          return data.size();
        }

        inline auto begin() {
            return data.begin();
        }
        inline auto end() {
            return data.end();
        }

        struct Ins {
            union {
                struct {
                    Key key;
                    Value value;
                };
                struct {
                    Key first;
                    Value second;
                };
            };
            Ins(const Key& k, const Value& v) : key(k), value(v) {};
            Ins(const Ins& o) : key(o.key), value(o.value) {

            }
            ~Ins() {
                key.~Key();
                value.~Value();
            }
        };
        std::list<Ins> data;
    protected:

    };
}

#endif
