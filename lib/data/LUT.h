#ifndef ARKE_LUT_H
#define ARKE_LUT_H

namespace arke {
    /*
    template<typename IDX_T, typename VAL_T>
    struct LUTGenerator {
        inline constexpr LUTGenerator() = default;
        
        inline constexpr VAL_T operator()(const IDX_T& x) {
            ((void)x);
            //static_assert(false, "LUTGenerator operator() must be overridden by child class!");
            return VAL_T();
        }
        
    };*/

    template<typename IDX_T, typename VAL_T, IDX_T MIN, IDX_T MAX, VAL_T (*GEN_FUNC)(const IDX_T&), bool LOOP = false>
    class LUT {
    public:
        typedef LUT<IDX_T, VAL_T, MIN, MAX, GEN_FUNC> Self;
        typedef IDX_T Idx;
        typedef VAL_T Value;
        static constexpr IDX_T Max = MAX;
        static constexpr IDX_T Min = MIN;
        static constexpr IDX_T Size = Max = Min + 1;
        
        VAL_T table[Size];
        
        inline constexpr LUT<IDX_T, VAL_T, MIN, MAX, GEN_FUNC, LOOP>() {
            for(Idx i = Min; i <= Max; i++) table[i - Min] = GEN_FUNC(i);
        }
        inline constexpr IDX_T index(const IDX_T& idx) const {
            if(LOOP) return (idx - Min) % Size;
            else return idx - Min;
        }
        inline const VAL_T& operator[](const IDX_T& idx) const {
            return table[index(idx)];
        }
        /*inline constexpr ~LUT<IDX_T, VAL_T, SIZE, GEN_FUNC>() { 
        }*/
    private:

    };

};

#endif /* LUT_H */

