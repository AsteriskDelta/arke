#ifndef ARKE_PROBTABLE_H
#define	ARKE_PROBTABLE_H
#include <unordered_map>
#include <map>
#include <algorithm>
#include "shared/Shared.h"

namespace arke {
    template<typename V>
    class ProbTable {
    public:
        struct Entry {
            inline Entry() : weight(0) {};
            inline Entry(unsigned int w) : weight(w) {};
            
            inline operator bool() const { return weight != 0; };
            inline bool operator<(const Entry& o) const { return totalWeight < o.totalWeight; };
            
            Uint32 weight;
            Uint64 totalWeight;
        };
        
        inline ProbTable();
        inline ProbTable(const ProbTable& orig);
        inline virtual ~ProbTable();
        
        inline void vote(const V& v, Uint32 votes = 1);
        inline void construct();
        
        inline V get();
        
        std::map<V, Entry> table;
        unsigned long long totalWeight;
    private:

    };

    template<typename V>
    inline ProbTable<V>::ProbTable() {
        totalWeight = 0;
    }
    template<typename V>
    inline ProbTable<V>::ProbTable(const ProbTable& orig) {
        table = orig.table;
        totalWeight = orig.totalWeight;
    }
    template<typename V>
    inline ProbTable<V>::~ProbTable() {
        
    }

    template<typename V>
    inline void ProbTable<V>::vote(const V& v, Uint32 votes) {
        table[v].weight += votes;
    }
    template<typename V>
    inline void ProbTable<V>::construct() {
        totalWeight = 0;
        for(auto it = table.begin(); it != table.end(); ++it) {
            it->second.totalWeight = totalWeight;
            totalWeight += it->second.weight;
        }
    }

    template<typename V>
    inline V ProbTable<V>::get() {
        if(table.size() == 0) return V();
        Uint64 v = ((Uint64)rand() | ((Uint64)rand() << 32)) % totalWeight;
        auto it = table.begin();
        for(; it != table.end(); ++it) {
            if(it->second.totalWeight > v) break;
        }
        if(it == table.end()) it = std::prev(it);
        
        return it->first;
    }
};

#endif	/* PROBTABLE_H */

