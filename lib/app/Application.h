#ifndef INC_ARKE_APPLICATION
#define INC_ARKE_APPLICATION
#include <string>
#include <exception>

namespace arke {
    extern unsigned int _globalTime, _globalFrame;
    extern float _deltaTime;
    extern double _globalTimef;
    extern bool _graphicsContextExists;
    
    class Version;
    class Directory;

    namespace Application {
    struct Exiting : std::exception {
        int code;
        Exiting(int s) : code(s) {};
        const char* what() const noexcept {return "Application exited normally";}
    };

    
    std::string GetName();
    std::string GetCodename();
    std::string GetCompanyName();
    
    Version GetVersion();
    Version GetEngineVersion();
    
    bool GetDebug();
    void SetDebug(bool state);
    
    void UpdateGlobals();
    void UpdateDelta(bool soft = false);
    
    //Setters (disabled after application initialized
    void SetName(const std::string& newName);
    void SetCodename(const std::string & newName);
    void SetCompanyName(const std::string &newName);
    void SetVersion(const std::string &newV);
    void SetDataDir(std::string dPath);
    
    void BeginInitialization();
    void EndInitialization();
    void WillExit(int signal);
    void WillExitWrapper();
    
    void OnExit(void (*callback)(void));
    
    void Quit(int code = 0);
    
    std::string GetLocalPath(bool forceRefresh = false);
    Directory GetLocal(bool forceRefresh = false);
    
    std::string GetCWDPath(bool forceRefresh = false);
    Directory GetCWD(bool forceRefresh = false);
    };

};

#endif
