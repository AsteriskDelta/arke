#include "shared/Shared.h"

#include "Application.h"
#include "Console.h"
#include "Version.h"
#include <sstream>
#include <vector>
#include <signal.h>
#include <chrono>

#if defined(SYS_WINDOWS)
#include <windows.h>
#elif defined(SYS_MAC) || defined(SYS_NIX)
#include <limits.h>
#include <unistd.h>
#endif

#include "AppData.h"
#include "System.h"
#include <iomanip>

#define APP_NOT_INI if(wasInitialized) return;

namespace arke {
    Uint32 _globalTime;
    Uint32 _globalFrame = 0;
    float _deltaTime = 0.001f;
    double _globalTimef = 0.0;
    bool _graphicsContextExists = false;

    namespace Application {
        bool wasInitialized = false, isDebug = DO_DEBUG;
        std::string appName = "", appCodename = "arkeApp", comName = "";
        Version appVersion;// = Verison("0.0r0X");
        std::string cachedLocalPath = "", cachedCWDPath = "", cachedEXEPath = "";
        
        std::string GetName() { return appName; };
        std::string GetCodename() { return appCodename; };
        std::string GetCompanyName() { return comName; };
        
        Version GetVersion() { return appVersion; };
        Version GetEngineVersion() { return Version(ARKE_VERSION); };
        
        bool GetDebug() { return isDebug; };
        void SetDebug(bool state) { isDebug = state; };
        
        auto startTime = std::chrono::high_resolution_clock::now();
        auto lastTime = std::chrono::high_resolution_clock::now();
        auto currentTime = std::chrono::high_resolution_clock::now();
        void UpdateGlobals() {
        // _globalFrame++;
        }
        void UpdateDelta(bool soft) {
            currentTime = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> timeSpan = std::chrono::duration_cast<std::chrono::duration<double>>(currentTime - startTime);
            double newGlobalTime = timeSpan.count();
            
            _globalTime = newGlobalTime;
            _globalTimef= newGlobalTime;
            //_globalFrame++;
            if(!soft) {
                _deltaTime = std::chrono::duration_cast<std::chrono::duration<double>>(currentTime - lastTime).count();
                lastTime = currentTime;
            }
        }
        
        //Setters (disabled after application initialized
        void SetName(const std::string &newName) {
            APP_NOT_INI
            appName = newName;
        }
        void SetCodename(const std::string &newName) {
            APP_NOT_INI
            appCodename = newName;
            cachedLocalPath = "";
        }
        void SetCompanyName(const std::string &newName) {
            APP_NOT_INI
            comName = newName;
        }
        void SetVersion(const std::string &newV) {
            APP_NOT_INI
            appVersion = Version(newV);
        }
        void SetDataDir(std::string dPath) {
            std::string bPath = GetCWDPath();
            ensureTrailing(dPath, '/');
            bPath.append(dPath);
            AppData::Initialize(bPath);
        }
        
        void BeginInitialization() {
            Console::Init(1024, 0, true, Logging_All);
            srand(time(NULL));
            
            signal(SIGQUIT, WillExit);
            signal(SIGINT, WillExit);
            signal(SIGSEGV, WillExit);
            signal(SIGTERM, WillExit);
            signal(SIGABRT, WillExit);
            signal(SIGILL,  WillExit);
            atexit(WillExitWrapper);
            at_quick_exit(WillExitWrapper);
        }
        
        void EndInitialization() {
            std::stringstream ss;
            ss << appName << " " << GetVersion().str();
            ss << " [" << ARKE_NAME << " " << GetEngineVersion().str() << "] ";
            ss << "Initialized OK";
            Console::Print(ss.str());
            ss.str("");
            ss << "CWD: " << GetCWDPath();
            Console::Print(ss.str());
            ss.str(""); std::string lPath = GetLocalPath();
            ss << "Local: " << lPath;
            Console::Print(ss.str());
            
            std::chrono::duration<double> timeSpan = startTime.time_since_epoch();//std::chrono::duration_cast<std::chrono::duration<double>>(startTime);
            Console::Out("Timestamp offset: +",std::setprecision (17),timeSpan.count(),std::setprecision (8));
            wasInitialized = true;
        }
        
        void WillExitWrapper() {
            WillExit(0);
        }
        
        static sig_atomic_t signaled = 0;
        std::vector<void (*)()> exitCallbacks;
        void WillExit(int signal) {
            if(signaled) return;
            signaled = true;
            std::stringstream ss;
            ss << "Exit with signal #" << signal << " caught, " << exitCallbacks.size() << " callbacks registered.";
            Console::Print(ss.str());
            
            try {
            for (std::vector<void (*)()>::iterator it = exitCallbacks.begin() ; it != exitCallbacks.end(); ++it) {
            (*it)();
            }
            } catch(...) {
            Console::Fatal("Exception on attempting to handle exit callbacks!");
            //abort();
            }
            
            Console::Flush();
            Console::Close();
            //std::exit(signal);
            Quit(signal);
            return;
        }
        
        void OnExit(void (*callback)(void)) {
            exitCallbacks.push_back(callback);
        }
        
        void Quit(int code) {
            //exit(code);
            if(!signaled) WillExit(code);
            else {
                exit(code);
                //throw Exiting(code);
            }
        }
        
        std::string GetLocalPath(bool forceRefresh) {
            if(!forceRefresh && cachedLocalPath != "") return cachedLocalPath;
            Directory dir = System::GetHome(forceRefresh);
            bool success = true; std::string useLP = appCodename;
            #ifdef SYS_WINDOWS
            success &= dir.cd("AppData/Local/");
            #elif defined(SYS_MAC)
            success &= dir.cd("Library/Application Support/");
            #elif defined(SYS_NIX)
            useLP = "." + useLP;
            #endif
            if(!dir.isDir(useLP)) dir.mkdir(useLP);
            success &= dir.cd(useLP);
            
            if(!success) {
            Console::Warning("Error while attempting to resolve or create local path!");
            return "";
            }
            
            cachedLocalPath = dir.resolve() + "/";
            return cachedLocalPath;
        }
        
        Directory GetLocal(bool forceRefresh) {
            std::string path = GetLocalPath(forceRefresh);
            return AppData::GetSys(path);
        }
        
        std::string GetCWDPath(bool forceRefresh) {
            if(!forceRefresh && cachedCWDPath != "") return cachedCWDPath;
        #if defined(SYS_WINDOWS)
            char result[ MAX_PATH ];
            cachedEXEPath = std::string(result, GetModuleFileName( NULL, result, MAX_PATH ));
        #elif defined(SYS_NIX) || defined(SYS_MAC) || defined (SYS_ANDROID)
            char result[ PATH_MAX ];
            ssize_t count = readlink( "/proc/self/exe", result, PATH_MAX );
            cachedEXEPath = std::string(result, (count > 0) ? count : 0);
        #endif
            
            unsigned int lastDirPos = cachedEXEPath.find_last_of(DIR_DELIM);
            return cachedEXEPath.substr(0, lastDirPos+1);
        }
        
        Directory GetCWD(bool forceRefresh) {
            std::string path = GetCWDPath(forceRefresh);
            return AppData::GetSys(path);
        }
    };

};
