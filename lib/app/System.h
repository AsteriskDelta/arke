#ifndef INC_SYSTEM
#define INC_SYSTEM
#include <string>

namespace arke {
    class Directory;

    struct SystemVideoInfo {
    std::string deviceName;
    std::string deviceID;
    std::string deviceVendor;
    unsigned long long memorySize;
    };

    namespace System {
        std::string 		GetName();
        unsigned short 	    GetVolumeHash();
        unsigned short 	    GetCPUHash();
        unsigned short 	    GetMACHash(bool primary = true);
        unsigned long long 	GetTotalRAM();
        unsigned long long 	GetAvailibleRAM();
        unsigned int 		GetTotalCores();
        std::string   	    GetOSName();
        bool                SetrLimit(unsigned long long bytes);
        bool                SetStackLimit(unsigned long long bytes);
        
        //40 Characters that (should) be unique
        std::string 		GetUUID();
        
        std::string         GetHomePath(bool forceRefresh = false);
        Directory           GetHome(bool forceRefresh = false);
        
        size_t              GetMemoryUsage();
        size_t              GetPeakMemoryUsage();
    };

};


#endif
