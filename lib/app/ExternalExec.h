#ifndef ARKE_EXT_EXEC_H
#define ARKE_EXT_EXEC_H
#include <string>
#include <fstream>
#include <iostream>

namespace arke {

    class ExternalExec : public std::streambuf, public std::iostream {
    public:
        ExternalExec(const std::string& cmd, bool blocking = false, bool deferExec = false);
        virtual ~ExternalExec();

        inline static constexpr unsigned int DefaultBufferSize = 1024*4;

        inline operator int() {
            return this->returned();
        }

        bool execute(bool blocking = false);

        int read(void *buffer, std::size_t cnt);
        int write(const void *buffer, std::size_t cnt);
        int errors(void *buffer, std::size_t cnt);

        bool readUntil(char delim, std::string &str);
        inline bool readLine(std::string& line) {
            return this->readUntil('\n', line);
        }

        bool running();

        void signal(int sig);

        int returned();
        void terminate();
        void kill();

        virtual int underflow() override;
        virtual int overflow(int c) override;
        virtual int sync() override;

        unsigned int terminateTimeoutMs;
    protected:
        int execStatus;
        unsigned int pid;
        std::string command;

        int inFD[2], outFD[2], errFD[2];

        int returnStatus;

        char *readBuffer, *writeBuffer;
        unsigned int bufferSize;
        std::string untilBuffer;

        void setupFD(int fd);
        void allocBuffers(unsigned int sz);
    };

};

#endif
