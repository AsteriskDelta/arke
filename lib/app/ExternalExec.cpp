#include "ExternalExec.h"
#include <string>
#include <sstream>
#include <cstdlib>
#include <sys/stat.h>
#include <sys/prctl.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>
#include <iostream>
#include <fcntl.h>
#include <cstring>
#include <sys/prctl.h>
#include <sys/resource.h>
#include "Decode.h"

inline static bool _fexists(const std::string& name) {
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

//Defined via unistd
extern char ** environ;
static const std::string shell = "bash";
static const std::string shellPath = "/bin/"+shell;
static const std::string shellPreCmds = "trap \"pkill -P $$\" SIGINT SIGTERM EXIT; ";
static const std::string shellPostCmds = "";
//static const std::string shellPreCmds = "(";
//static const std::string shellPostCmds = ") & trap \"pkill -P $$\" SIGINT SIGTERM EXIT; wait";

namespace arke {
    ExternalExec::ExternalExec(const std::string& cmd, bool blocking, bool deferExec) : std::streambuf(), std::iostream((std::streambuf*)this), terminateTimeoutMs(0), execStatus(0), pid(0),
    command(shellPreCmds+cmd+shellPostCmds), inFD{-1,-1}, outFD{-1,-1}, errFD{-1,-1}, returnStatus(0),
    readBuffer(nullptr), writeBuffer(nullptr), bufferSize(0) {
        if(!deferExec) this->execute(blocking);
    }
    ExternalExec::~ExternalExec() {
        //std::cout << "EE destruct pid=" << pid << "\n";
        if(this->running()) this->terminate();
        //if(inFD > 0) close(inFD);
        //if(outFD > 0) close(outFD);
        //if(errFD > 0) close(errFD);
    }

    bool ExternalExec::running() {
        if(pid == 0) return false;
        int reapedStatus;//Reap any children first (geez, programming is dark...)
        while(waitpid(-1, &reapedStatus, WNOHANG) > 0);

        int procState = ::kill(pid, 0);
        if(procState != 0) {
            returnStatus = reapedStatus;
            //std::cout << "process exited, state=" << returnStatus << "\n";
        }

        bool processActive = procState == 0;
        //if(!processActive) pid = 0;

        return processActive;
    }

    bool ExternalExec::execute(bool blocking) {
        bool pipesOpen = true;
        pipesOpen &= pipe(inFD) > 0;
        pipesOpen &= pipe(outFD) > 0;
        pipesOpen &= pipe(errFD) > 0;

        setupFD(inFD[0]);
        setupFD(outFD[0]);
        setupFD(errFD[0]);
        setupFD(inFD[1]);
        setupFD(outFD[1]);
        setupFD(errFD[1]);

        struct rlimit fdrlim;
        getrlimit(RLIMIT_NOFILE, &fdrlim);
        for(int i = 0; i < int(fdrlim.rlim_cur); i++) {
            if(i == inFD[0] || i == outFD[0] || i == errFD[0] ||
            i == inFD[1] || i == outFD[1] || i == errFD[1]
            ) continue;
            //close(i);
            fcntl(i, F_SETFD, FD_CLOEXEC);
        }

        this->allocBuffers(DefaultBufferSize);

        //std::cout << "EXEC:" << command << "\n";
        const char *const scriptArgv[] = {shell.c_str(), "-c", command.c_str(), reinterpret_cast<char*>(0x0)};
        if((pid = fork()) == 0) {
            //Prevent inheritance (and possible destruction) of our file/socket handles
            usleep(10);

            prctl(PR_SET_PDEATHSIG, SIGTERM);


            //close(STDIN_FILENO);
            //close(STDOUT_FILENO);
            //close(STDERR_FILENO);

            //Redirect IO to pipes
            dup2(inFD[0], STDIN_FILENO);
            dup2(outFD[1], STDOUT_FILENO);
            dup2(errFD[1], STDERR_FILENO);

            execStatus = execvpe(shellPath.c_str(), const_cast<char *const*>(scriptArgv), environ);
            _Exit(0);
        } else {
            execStatus = 0;
            //dup2(0, STDIN_FILENO);
            //dup2(0, STDOUT_FILENO);
            //dup2(1, STDERR_FILENO);
        }

        if(execStatus != 0) return false;

        if(blocking) {
            while(this->running()) {
                usleep(1000);
            }
        }

        return true;
    }

    int ExternalExec::returned() {
        //std::cout << "getting return value " << returnStatus << " for pid " << pid << "\n";
        if(pid == 0) return -1;
        else return returnStatus;
    }
    void ExternalExec::terminate() {
        //std::cout << "Terminating child pid=" << pid << "\n";
        if(pid == 0) return;
        ::kill(pid, 15);

        if(terminateTimeoutMs != 0) {
            unsigned int passedMs = 0;
            while(this->running()) {
                passedMs++;
                if(passedMs >= terminateTimeoutMs) {
                    this->kill();
                    return;
                }

                usleep(1000);
            }
        }
    }
    void ExternalExec::kill() {
        return this->signal(SIGKILL);
    }

    void ExternalExec::signal(int sig) {
        if(pid == 0) return;
        ::kill(pid, sig);
    }

    int ExternalExec::read(void *buffer, std::size_t cnt) {
        int ret = ::read(outFD[0], buffer, cnt);
        //std::cout << "read got " << ret << "\n";
        if(ret >= 0) return ret;

        if(errno == EAGAIN) {
            return 0;//Don't err on no data availible
        } else {
            return ret;
        }
    }

    bool ExternalExec::readUntil(char delim, std::string &str) {
        char buffer[256];
        memset(buffer, 0x0, 256);
        int ret = ::read(outFD[0], buffer, 255);
        //std::cout << "read got " << ret << "\n";
        if(ret >= 0 || !untilBuffer.empty()) {
            //std::cout << "untilBuffer = \"" << untilBuffer << "\"\n";
            if(ret >= 0) untilBuffer += Decode::Terminal(std::string(buffer));
            size_t end;
            for(end = 0; end < untilBuffer.size(); end++) {
                if(untilBuffer[end] == delim) {
                    //std::cout << "GOT DELIM\n";
                    str = untilBuffer.substr(0, end);
                    untilBuffer.erase(0, end+1);
                    return true;
                }
            }
        }

        return false;
    }

    int ExternalExec::write(const void *buffer, std::size_t cnt) {
        int ret = ::write(inFD[1], buffer, cnt);
        return ret;
    }

    int ExternalExec::errors(void *buffer, std::size_t cnt) {
        int ret = ::read(errFD[0], buffer, cnt);
        if(ret >= 0) return ret;

        if(errno == EAGAIN) {
            return 0;//Don't err on no data availible
        } else {
            return ret;
        }
    }

    void ExternalExec::setupFD(int fd) {
        int flags = fcntl(fd, F_GETFL, 0);
        fcntl(fd, F_SETFL, flags | O_NONBLOCK);
    }

    int ExternalExec::underflow() {
        int bytesRead = this->read(readBuffer, bufferSize);
        setg(readBuffer, readBuffer, readBuffer + bytesRead);

        if(bytesRead == 0) return EOF;

        return std::streambuf::traits_type::to_int_type(readBuffer[0]);
    }
    int ExternalExec::overflow(int c) {
        int bytesWritten = 0;
        if(pptr() - pbase() > 0){
            bytesWritten = this->write(pbase(),(pptr() - pbase()));
            if(bytesWritten <= 0)  return std::streambuf::traits_type::not_eof(c);
        }
        writeBuffer[0] = std::streambuf::traits_type::to_char_type(c);
        setp(writeBuffer + 1, writeBuffer + bufferSize);

        this->pubsync();

        return std::streambuf::traits_type::not_eof(c);
    }
    int ExternalExec::sync() {
        int bytesWritten = 0;
        if(pptr() - pbase() > 0){
            bytesWritten = this->write(pbase(),(pptr() - pbase()));
        }
        setp(writeBuffer, writeBuffer + bufferSize);
        return bytesWritten;
    }

    void ExternalExec::allocBuffers(unsigned int sz) {
        char *newReadBuffer = new char[sz];
        char *newWriteBuffer = new char[sz];

        if(readBuffer != nullptr) {
            memcpy(newReadBuffer, readBuffer, std::min(bufferSize, sz));
            delete readBuffer;
        }
        if(writeBuffer != nullptr) {
            memcpy(newWriteBuffer, writeBuffer, std::min(bufferSize, sz));
            delete writeBuffer;
        }

        readBuffer = newReadBuffer;
        writeBuffer = newWriteBuffer;
        bufferSize = sz;

        setg(readBuffer, readBuffer, readBuffer);
        setp(writeBuffer, writeBuffer + bufferSize);
    }

};
