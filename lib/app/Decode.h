#ifndef ARKE_DECODE_H
#define ARKE_DECODE_H
#include "ARKE.h"

namespace arke {
    namespace Decode {
        std::string Terminal(const std::string &s);
    }
}

#endif
