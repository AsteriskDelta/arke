#include "Decode.h"

namespace arke {
    namespace Decode {
        std::string Terminal(const std::string& s) {
            std::string res;
            std::string::const_iterator it = s.begin();
            while (it != s.end()) {
                char c = *it++;
                if (c == '\\' && it != s.end()) {
                    //std::cout << "got escape sequence " << (*std::next(it)) << " -> ";
                    char cs[3] = {*it++, 0x0, 0x0};
                    switch (cs[0]) {
                        case '\\': c = '\\'; break;
                        case 'a': c = '\a'; break;
                        case 'n': c = '\n'; break;
                        case 't': c = '\t'; break;
                        case 'e': c = '\e'; break;
                        case 'r': continue; break;
                        // all other escapes
                        default:
                        if(isalnum(cs[0])) {
                            int n = atoi(&(*std::prev(it)));
                            c = *reinterpret_cast<char*>(&n);
                            if(it != s.end())++it;
                            if(it != s.end()) ++it;// Expect 3-digit padding
                        }
                        break;
                    }
                    //std::cout << "\"" <<c<< "\"\n";
                } else if(c < 0) continue;
                res += c;
            }
            return res;
        }
    }
}
