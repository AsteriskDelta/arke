#ifndef INC_DAEMON
#define INC_DAEMON
#include <string>

#ifndef STRING
#define STRING(s) #s
#endif

namespace arke {
    namespace Daemon {
        enum class Log : unsigned char {
            Debug = 0, 
            Info,//Informative, but nothing wrong
            Warning, 
            Error,
            Critical,//Daemon termination
            Emergency//Signifies imminent system-wide failure
        };
        
        //Called to daemonize the current process, terminiating it and spawning a new continuation
        bool Daemonize(std::string workingDir = "/");
        inline bool Begin(const std::string &workingDir = "/") {
            return Daemonize(workingDir);
        }
        
        //Redirect std::cout and std::cerr to (presumably) log files
        bool RedirectIO(std::string coutPath, std::string cerrPath = "");
        
        bool OpenSyslog(std::string name);
        bool Syslog(std::string text, Daemon::Log level = Log::Info);
        bool CloseSyslog();
        
        //Safely terminates daemon
        void Terminate(int exitCode = 0);
        inline void End(int exitCode = 0) { Terminate(exitCode); };
        
        //Debugging functions- leave daemon unforked
        bool DaemonizeDBG(std::string workingDir = "/");
        inline bool BeginDBG(const std::string &workingDir = "/") {
            return DaemonizeDBG(workingDir);
        }
    };
};

#endif
