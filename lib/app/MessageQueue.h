#ifndef ARKE_MESSAGEQUEUE_INC
#define ARKE_MESSAGEQUEUE_INC
#include <mutex>
#include "Console.h"

namespace arke {
    //Thread-safe message queue
    template <class MQD>
    class MessageQueue {
    public:
        MessageQueue(unsigned int maxMessages = 256);
        ~MessageQueue();
        
        //Queries
        unsigned int count();
        unsigned int max();
        
        //Insertion and removal
        bool push(MQD d);
        MQD  pop();
    private:
        void lock();
        void unlock();
        std::recursive_mutex mtx;
        
        unsigned int messageCount, messageMax;
        unsigned int head, tail;
        
        MQD *data;
    };

    template <class MQD>
    MessageQueue<MQD>::MessageQueue(unsigned int maxMessages) { 
        std::lock_guard<std::recursive_mutex> mg(mtx);
        messageMax = maxMessages;
        data = new MQD[messageMax];
        messageCount = 0;
        head = tail = 0;
    }

    template <class MQD>
    MessageQueue<MQD>::~MessageQueue() {
        std::lock_guard<std::recursive_mutex> mg(mtx);
        delete[] data;
    }

    template <class MQD>
    unsigned int MessageQueue<MQD>::count() {
        std::lock_guard<std::recursive_mutex> mg(mtx);
        unsigned int ret = messageCount;
        
        return ret;
    }

    template <class MQD>
    unsigned int MessageQueue<MQD>::max() {
        std::lock_guard<std::recursive_mutex> mg(mtx);
        unsigned int ret = messageMax;
        
        return ret;
    }

    template <class MQD>
    bool MessageQueue<MQD>::push(MQD d) {
        std::lock_guard<std::recursive_mutex> mg(mtx);
        if(messageCount >= messageMax) {
            Console::Error("Message Queue overflow!!!");
            return false;
        }
        
        data[tail] = d;
        
        tail++; if(tail >= messageMax) tail %= messageMax;
        messageCount++;
        return true;
    }

    template <class MQD>
    MQD  MessageQueue<MQD>::pop() {
        std::lock_guard<std::recursive_mutex> mg(mtx);
        if(messageCount == 0) return MQD();
        
        MQD ret = data[head];
        
        head++; if(head >= messageMax) head %= messageMax;
        messageCount--;
        
        return ret;
    }

    template <class MQD>
    void MessageQueue<MQD>::lock() {
        //mtx.lock();
    }

    template <class MQD>
    void MessageQueue<MQD>::unlock() {
        //mtx.unlock();
    }

    #ifdef UNITTEST
    SCENARIO( "MessageQueue general use", "[MessageQueue]" ) {
    
    GIVEN( "a MessageQueue with ten items" ) {
        const int qIMax = 327;
        MessageQueue<int> q(qIMax);
        for(int i = 0; i < 10; i++) q.push(i);
        
        REQUIRE(q.count() == 10);
        REQUIRE(q.max() == qIMax );
        
        WHEN( "one item is pushed and two popped, indefinitely" ) {
        int place = 10;
        while(q.count() > 0) {
        int startSize = q.count();
        int a = q.pop();
        q.push(place++);
        int b = q.pop();
        REQUIRE(q.count() == startSize-1);
        REQUIRE( (a+1) == b );
        }
        
        THEN( "the size shall reach zero with all items in order" ) {
        REQUIRE(q.count() == 0);
        }
        }
        WHEN( "the max queue size is reached" ) {
        while(q.count() > 0) q.pop();
        while(q.count() < qIMax) {
        REQUIRE( q.push(0) == true);
        }
        REQUIRE(q.count() == q.max());
        
        THEN( "push fails and no faults occur" ) {
        REQUIRE( q.push(1) == false);
        }
        }
        WHEN( "all items are deleted" ) {
        while(q.count() > 0) q.pop();
        
        THEN( "the size reaches zero" ) {
        REQUIRE(q.count() == 0);
        }
        }
        WHEN( "functionality is tested after reaching zero and adding ten items" ) {
        while(q.count() > 0) q.pop();
        for(int i = 0; i < 10; i++) q.push(i);
        
        REQUIRE(q.count() == 10);
        REQUIRE(q.max() == qIMax );
        int place = 10;
        while(q.count() > 0) {
        int startSize = q.count();
        int a = q.pop();
        q.push(place++);
        int b = q.pop();
        REQUIRE(q.count() == startSize-1);
        REQUIRE( (a+1) == b );
        }
        
        THEN( "the size shall reach zero with all items in order" ) {
        REQUIRE(q.count() == 0);
        }
        }
    }
    
    }
    
    #endif
};

#endif
