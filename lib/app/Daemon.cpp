#include "Daemon.h"

#include <stdio.h>    //printf(3)
#include <stdlib.h>   //exit(3)
#include <unistd.h>   //fork(3), chdir(3), sysconf(3)
#include <signal.h>   //signal(3)
#include <sys/stat.h> //umask(3)
#include <syslog.h>   //syslog(3), openlog(3), closelog(3)
#include <iostream>
#include <fstream>

namespace arke {
    namespace Daemon {
        bool Daemonize(std::string workingDir) {
            pid_t child;
            //fork, detach from process group leader
            if( (child=fork())<0 ) { //failed fork
            fprintf(stderr,"error: failed fork\n");
            exit(EXIT_FAILURE);
            }
            if (child>0) { //parent
            exit(EXIT_SUCCESS);
            }
            if( setsid()<0 ) { //failed to become session leader
            fprintf(stderr,"error: failed setsid\n");
            exit(EXIT_FAILURE);
            }
            
            //catch/ignore signals
            signal(SIGCHLD,SIG_IGN);
            signal(SIGHUP,SIG_IGN);
            
            //fork second time
            if ( (child=fork())<0) { //failed fork
            fprintf(stderr,"error: failed fork\n");
            exit(EXIT_FAILURE);
            }
            if( child>0 ) { //parent
            exit(EXIT_SUCCESS);
            }
            
            //new file permissions
            umask(0);
            chdir(workingDir.c_str());
            
            //Close all open file descriptors
            for(int fd = sysconf(_SC_OPEN_MAX); fd > 0; --fd ) {
            close(fd);
            }
            
            return true;
        }
        
        //Redirect std::cout and std::cerr to (presumably) log files
        std::ofstream out, outErr;
        bool RedirectIO(std::string coutPath, std::string cerrPath) {
            stdout = fopen(coutPath.c_str(), "w+");
            stderr = fopen((cerrPath == "") ? coutPath.c_str() : cerrPath.c_str(), "w+");
            
            out.open(coutPath, std::fstream::trunc);
            //std::streambuf *coutbuf = std::cout.rdbuf();
            std::cout.rdbuf(out.rdbuf()); 
            
            if(cerrPath == coutPath || cerrPath == "") std::cerr.rdbuf(out.rdbuf()); 
            else {
            outErr.open(cerrPath, std::fstream::trunc);
            //std::streambuf *cerrbuf = std::cerr.rdbuf();
            std::cerr.rdbuf(outErr.rdbuf()); 
            }
            
            return true;
        }
        
        bool OpenSyslog(std::string name) {
            //open syslog
            setlogmask (LOG_UPTO (LOG_DEBUG));
            openlog(name.c_str(), LOG_CONS | LOG_PID | LOG_NDELAY, LOG_DAEMON);
            return true;
        }
        
        bool Syslog(std::string text, Daemon::Log level) {
            int sysLevel;
            switch(level) {
            case Log::Info:		sysLevel = LOG_INFO; 	 break;
            case Log::Warning:	sysLevel = LOG_WARNING;  break;
            case Log::Error:		sysLevel = LOG_ERR; 	 break;
            case Log::Critical:	sysLevel = LOG_CRIT;  break;
            case Log::Emergency:	sysLevel = LOG_EMERG;break;
            default:
            case Log::Debug:      sysLevel = LOG_DEBUG; 	 break;
            }
            
            syslog(sysLevel, text.c_str());
            return true;
        }
        
        bool CloseSyslog() {
            closelog();
            return true;
        }
        
        //Safely terminates daemon
        void Terminate(int exitCode) {
            exit(exitCode);
        }
        
        bool DaemonizeDBG(std::string workingDir) {
            umask(0);
            
            chdir(workingDir.c_str());
            
            return true;
        }
    }

};
