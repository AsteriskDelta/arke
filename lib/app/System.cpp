#include "System.h"
#include "shared/Shared.h"
#include "AppData.h"
#include "Console.h"
#include <cstdlib>
#include <thread>

#include "sysHash.cpp"

#if defined(SYS_NIX) || defined(SYS_MAC)
  #include <pwd.h>
  #include <unistd.h>
  #include <sys/types.h>
#elif defined(SYS_WINDOWS)
  #include <windows.h>
#endif

namespace arke {
    namespace System {
    std::string cachedHomePath = "";
    //std::string localPrefix = "arke";
    
    std::string GetHomePath(bool forceRefresh) {
        if(!forceRefresh && cachedHomePath != "") return cachedHomePath;
        
        char *pEnvA = NULL;
        std::string homePath = "./";//Use CWD by defualt
        
    #ifdef SYS_WINDOWS
        char *pEnvB = NULL;
        pEnvA = getenv("USERPROFILE");
        if(pEnvA != NULL) {
        homePath = std::string(pEnvA);
        } else {//Concatanate and assign
        pEnvA = getenv("HOMEDRIVE");
        pEnvB = getenv("HOMEPATH");
        homePath = std::string(pEnvA) + std::string(pEnvB);
        }
    #elif defined(SYS_MAC)
        struct passwd* pwd = getpwuid(getuid());
        if(pwd) pEnvA = pwd->pw_dir;
        else pEnvA = getenv("HOME");
        homePath = std::string(pEnvA);
    #elif defined(SYS_NIX)
        struct passwd* pwd = getpwuid(getuid());
        if(pwd) pEnvA = pwd->pw_dir;
        else pEnvA = getenv("HOME");
        homePath = std::string(pEnvA);
    #endif
        return homePath;
    }
    
    Directory GetHome(bool forceRefresh) {
        std::string hPath = GetHomePath(forceRefresh);
        return AppData::GetSys(hPath);
    }
    
    /*void SetLocalPrefix(std::string newPrefix) {
        //Ensure trailing slash, but trust sane input
        //ensureTrailing(newPrefix, DIR_DELIM);
        localPrefix = newPrefix;
        GetLocalPath(true);
    }
    
    Directory GetHome(bool forceRefresh) {
        std::string hPath = GetHomePath(forceRefresh);
        return AppData::GetSys(hPath);
    }
    
    std::string GetLocalPath(bool forceRefresh) {
        if(!forceRefresh && cachedLocalPath != "") return cachedLocalPath;
        Directory dir = GetHome(forceRefresh);
        bool success = true; std::string useLP = localPrefix;
    #ifdef SYS_WINDOWS
        success &= dir.cd("AppData/Local/");
    #elif defined(SYS_MAC)
        success &= dir.cd("Library/Application Support/");
    #elif defined(SYS_NIX)
        useLP = "." + useLP;
    #endif
        if(!dir.isDir(useLP)) dir.mkdir(useLP);
        success &= dir.cd(useLP);
        
        if(!success) {
        Console::Warning("Error while attempting to resolve or create local path!");
        return "";
        }
        
        cachedLocalPath = dir.resolve();
        return cachedLocalPath;
    }
    
    Directory GetLocal(bool forceRefresh) {
        std::string path = GetLocalPath(forceRefresh);
        return AppData::GetSys(path);
    }*/
    
    unsigned long long sysTotalRAM = 0;
    unsigned long long GetTotalRAM() {
        if(sysTotalRAM != 0) return sysTotalRAM;
        
    #if defined(SYS_NIX) || defined(SYS_MAC)
        unsigned long long pages = sysconf(_SC_PHYS_PAGES);
        unsigned long long page_size = sysconf(_SC_PAGE_SIZE);
        sysTotalRAM = pages * page_size;
        return sysTotalRAM;
    #elif defined(SYS_WINDOWS)
        MEMORYSTATUSEX status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatusEx(&status);
        sysTotalRAM = status.ullTotalPhys;
    #endif
        return sysTotalRAM;
    }
    
    unsigned long long GetAvailibleRAM() {
        #if defined(SYS_NIX) || defined(SYS_MAC)
        unsigned long long pages = sysconf(_SC_AVPHYS_PAGES);
        unsigned long long page_size = sysconf(_SC_PAGE_SIZE);
        return pages * page_size;
        #elif defined(SYS_WINDOWS)
        MEMORYSTATUSEX status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatusEx(&status);
        return status.ullAvailPhys;
        #endif
    }
    
    unsigned int sysTotalCores = 0;
    unsigned int GetTotalCores() {
        if(sysTotalCores != 0) return sysTotalCores;
        sysTotalCores = std::thread::hardware_concurrency();
        sysTotalCores = std::max(sysTotalCores, (unsigned int)1);
        return sysTotalCores;
    }
    
    std::string sysName = "";
    std::string GetName() {
        if(sysName != "") return sysName;
        
        sysName = std::string(getMachineName());
        return sysName;
    }
    
    unsigned short sysVolumeHash = 0;
    unsigned short GetVolumeHash() {
        if(sysVolumeHash != 0) return sysVolumeHash;
        
        sysVolumeHash = getVolumeHash();
        return sysVolumeHash;
    }
    
    unsigned short sysCPUHash = 0;
    unsigned short GetCPUHash() {
        if(sysCPUHash != 0) return sysCPUHash;
        
        sysCPUHash = getCpuHash();
        return sysCPUHash;
    }
    
    bool sysMACHashed = false;
    unsigned short sysMACPrimary = 0, sysMACSecondary = 0;
    unsigned short GetMACHash(bool primary) {
        if(sysMACHashed && primary) return sysMACPrimary;
        else if(sysMACHashed) return sysMACSecondary;
        
        getMacHash(sysMACPrimary, sysMACSecondary);
        sysMACHashed = true;
        
        if(primary) return sysMACPrimary;
        else return sysMACSecondary;
    }
    
    std::string GetOSName() {
    #if defined(SYS_WINDOWS)
        return "Windows";
    #elif defined(SYS_NIX)
        return "Linux";
    #elif defined(SYS_MAC)
        return "Mac";
    #elif defined(SYS_ANDROID)
        return "Android";
    #else
        return "Unknown";
    #endif
    }
    
    /*std::string GetUUID() {
        
    }*/
    
    bool SetrLimit(unsigned long long bytes)
    {
        struct rlimit limit;
        getrlimit(RLIMIT_AS, &limit);
        std::stringstream ss;
        if(bytes <= limit.rlim_max)
        {
            limit.rlim_cur = bytes;
            ss << "New memory limit: " << limit.rlim_cur << " bytes" ;
            Console::Print(ss.str());
        }
        else
        {
            limit.rlim_cur = limit.rlim_max;
            ss << "WARNING: Memory limit couldn't be set to " << bytes << " bytes" << std::endl;
            ss << "New memory limit: " << limit.rlim_cur << " bytes" << std::endl;
            Console::Print(ss.str());
        }

        if(setrlimit(RLIMIT_AS, &limit) != 0) {
        
        ss.str("");
            //std::perror("WARNING: memory limit couldn't be set:");
        ss << "WARNING: memory limit couldn't be set:" << bytes;
        Console::Error(ss.str());
        return false;
        }

        // included for debugging purposes
        struct rlimit tmp;
        getrlimit(RLIMIT_AS, &tmp);
        ss.str("");
        ss << "Tmp limit: " << tmp.rlim_cur << " bytes" ;
        Console::Print(ss.str());
        return true;
    }
    
        bool SetStackLimit(unsigned long long bytes)
    {
        struct rlimit limit;
        getrlimit(RLIMIT_STACK, &limit);
        std::stringstream ss;
        if(bytes <= limit.rlim_max)
        {
            limit.rlim_cur = bytes;
            //ss << "New stack limit: " << limit.rlim_cur << " bytes" ;
            //Console::Print(ss.str());
        }
        else
        {
            limit.rlim_cur = limit.rlim_max;
            //ss << "WARNING: Memory limit couldn't be set to " << bytes << " bytes" << std::endl;
        // ss << "New memory limit: " << limit.rlim_cur << " bytes" << std::endl;
            //Console::Print(ss.str());
        }

        if(setrlimit(RLIMIT_STACK, &limit) != 0) {
        
        ss.str("");
            //std::perror("WARNING: memory limit couldn't be set:");
        ss << "WARNING: memory limit couldn't be set:" << bytes;
        Console::Error(ss.str());
        return false;
        }

        // included for debugging purposes
        //struct rlimit tmp;
        //getrlimit(RLIMIT_STACK, &tmp);
        //ss.str("");
        //ss << "Tmp limit: " << tmp.rlim_cur << " bytes" ;
        //Console::Print(ss.str());
        return true;
    }
        
        #if defined(_WIN32)
    #include <windows.h>
    #include <psapi.h>

    #elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
    #include <unistd.h>
    #include <sys/resource.h>

    #if defined(__APPLE__) && defined(__MACH__)
    #include <mach/mach.h>

    #elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
    #include <fcntl.h>
    #include <procfs.h>

    #elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
    #include <stdio.h>

    #endif

    #else
    #error "Cannot define getPeakRSS( ) or getCurrentRSS( ) for an unknown OS."
    #endif





    /**
    * Returns the peak (maximum so far) resident set size (physical
    * memory use) measured in bytes, or zero if the value cannot be
    * determined on this OS.
    */
    size_t getPeakRSS( )
    {
    #if defined(_WIN32)
        /* Windows -------------------------------------------------- */
        PROCESS_MEMORY_COUNTERS info;
        GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
        return (size_t)info.PeakWorkingSetSize;

    #elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
        /* AIX and Solaris ------------------------------------------ */
        struct psinfo psinfo;
        int fd = -1;
        if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 )
            return (size_t)0L;		/* Can't open? */
        if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
        {
            close( fd );
            return (size_t)0L;		/* Can't read? */
        }
        close( fd );
        return (size_t)(psinfo.pr_rssize * 1024L);

    #elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
        /* BSD, Linux, and OSX -------------------------------------- */
        struct rusage rusage;
        getrusage( RUSAGE_SELF, &rusage );
    #if defined(__APPLE__) && defined(__MACH__)
        return (size_t)rusage.ru_maxrss;
    #else
        return (size_t)(rusage.ru_maxrss * 1024L);
    #endif

    #else
        /* Unknown OS ----------------------------------------------- */
        return (size_t)0L;			/* Unsupported. */
    #endif
    }

    
    
    /**
     * Returns the current resident set size (physical memory use) measured
     * in bytes, or zero if the value cannot be determined on this OS.
     */
    size_t getCurrentRSS( )
    {
        #if defined(_WIN32)
        /* Windows -------------------------------------------------- */
        PROCESS_MEMORY_COUNTERS info;
        GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
        return (size_t)info.WorkingSetSize;
        
        #elif defined(__APPLE__) && defined(__MACH__)
        /* OSX ------------------------------------------------------ */
        struct mach_task_basic_info info;
        mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
        if ( task_info( mach_task_self( ), MACH_TASK_BASIC_INFO,
            (task_info_t)&info, &infoCount ) != KERN_SUCCESS )
            return (size_t)0L;		/* Can't access? */
            return (size_t)info.resident_size;
        
        #elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
        /* Linux ---------------------------------------------------- */
        long rss = 0L;
        FILE* fp = NULL;
        if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL ) return (size_t)0L;		/* Can't open? */
        if ( fscanf( fp, "%*s%ld", &rss ) != 1 ) {
            fclose( fp );
            return (size_t)0L;		/* Can't read? */
        }
        fclose( fp );
        return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);
        
        #else
        /* AIX, BSD, Solaris, and Unknown OS ------------------------ */
        return (size_t)0L;			/* Unsupported. */
        #endif
    }
    
    size_t GetMemoryUsage() {
        return getCurrentRSS();
    }
    size_t GetPeakMemoryUsage() {
        return getPeakRSS();
    }
};
};
