#include "Loadable.h"
#include "AppData.h"
#include <iostream>
#include <algorithm>

namespace arke {
    Loadable::Loadable() : name(), path(), subPath(), loadedState(0) {
        
    }
    Loadable::Loadable(const std::string& p) : name(), path(), subPath(), loadedState(0) {
        this->load(p);
    }

    Loadable::~Loadable() {
        if(this->isLoaded()) this->unload();
    }
    bool Loadable::couldLoad(const std::string& p) const {
        ((void)p);
        return false;
    }
    bool Loadable::load(const std::string& p) {
        if(!this->couldLoad(p)) return false;
        
        path = p;
        name = this->getName();
        return true;
    }
    void Loadable::unload() {
        loadedState = 0;
    }
    bool Loadable::isLoaded() const {
        return loadedState != 0;
    }

    std::string Loadable::getBaseName(const std::string& p, const std::string& delim, const std::string& rep) const {
        std::string baseName = p;
        if(rep.size() >= 2) std::replace( baseName.begin(), baseName.end(), rep[0], rep[1]);
        
        auto slashPos = baseName.find_last_of("/");
        if(slashPos != std::string::npos) baseName = p.substr(slashPos+1, p.size()-slashPos-1);
        
        auto dotPos = baseName.find_last_of(".");
        if(dotPos != std::string::npos) baseName =  baseName.substr(0, dotPos);
        
        auto delimPos = delim.size() > 0? baseName.find_first_of(delim) : std::string::npos;
        if(delimPos != std::string::npos) baseName=  baseName.substr(0, delimPos);
        
        return baseName;
    }

    std::string Loadable::getName() const {
        return this->getBaseName(path);
    }

    void Loadable::LoadAll(Directory *dir, const std::function<Loadable* (const std::string&)>& factory, const std::function<void(Loadable*)>& cb) {
        if(dir == nullptr) return;
        
        dir->listCallback([&](std::string path, std::string fullPath)->bool {
            Loadable* obj = factory(fullPath);
            //std::cout << "loadall got obj=" << obj << "\n";
            if(obj == nullptr) return false;
            obj->subPath = path;
            obj->name = obj->getName();
            //std::cout << "\tinvoking cb...\n";
            cb(obj);
            return true;
        }, true);
    }

    void Loadable::LoadAll(const std::string& dir, const std::function<Loadable* (const std::string&)>& factory, const std::function<void(Loadable*)>& cb) {
        Directory *dirPtr = new Directory(dir);
        LoadAll(dirPtr, factory, cb);
        delete dirPtr;
        return;
    }

};
