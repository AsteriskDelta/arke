#ifndef LOADABLE_H
#define LOADABLE_H
#include <string>
#include <functional>

namespace arke {

    class Directory;

    class Loadable {
    public:
        Loadable();
        Loadable(const std::string& p);
        virtual ~Loadable();
        
        virtual bool couldLoad(const std::string& p) const;
        virtual bool load(const std::string& p);
        virtual void unload();
        
        virtual std::string getBaseName(const std::string& p, const std::string& delim = "", const std::string& rep = "") const;
        virtual std::string getName() const;
        
        inline virtual void* rawInstancePtr() { return reinterpret_cast<void*>(this); };
        
        inline static Loadable* Factory(const std::string p = "") {
            ((void)p);
            return nullptr;
        }
        
        virtual bool isLoaded() const;
        
        static void LoadAll(Directory *dir, const std::function<Loadable* (const std::string&)>& factory, const std::function<void(Loadable*)>& cb);
        
        static void LoadAll(const std::string& dir, const std::function<Loadable* (const std::string&)>& factory, const std::function<void(Loadable*)>& cb);
        
        template<typename T>
        inline static void LoadAll(const std::string& dir, const std::function<T* (const std::string&)>& factory, const std::function<void(T*)>& cb) {
            return LoadAll(dir, 
                    [&](const std::string& p)->Loadable* { return (Loadable*)factory(p); },
                    [&](Loadable* loadedPtr)->void { cb(reinterpret_cast<T*>(loadedPtr->rawInstancePtr())); });
        }
        
        std::string name, path, subPath;
    private:
        unsigned char loadedState;
    };

};

#endif /* LOADABLE_H */

