#ifndef LOADABLEMANAGER_H
#define LOADABLEMANAGER_H
#include "Loadable.h"
#include <unordered_map>
#include <list>
#include <string>
#include <functional>

namespace arke {

template<typename T, typename K = std::string>
class LoadableManager {
public:
    inline LoadableManager() {
        
    }
    inline virtual ~LoadableManager() {
        
    }
    
    std::list<std::string> sourceDirs;
    std::unordered_map<K, T*> loaded;
private:
    
    inline void wasLoadedDispatch(T* ptr) {
        return this->wasLoaded(ptr);
    }
public:
    
    inline virtual void wasLoaded(T* ptr) {
        if(ptr == nullptr) return;
#ifdef ARKE_LOADABLE_V2
        loaded[ptr->name()] = ptr;
#else
        loaded[ptr->name] = ptr;
#endif
    }
    
    inline virtual void loadFromDir(const std::string& dir) {
        sourceDirs.push_back(dir);
        Loadable::LoadAll<T>(dir, &T::Factory, std::bind(LoadableManager::wasLoadedDispatch, this, std::placeholders::_1));
    }
    
    T* get(const K& key) { 
        auto it = loaded.find(key);
        if(it == loaded.end()) return nullptr;
        else return it->second;
    }
    inline T* operator[](const K& key) { return this->get(key); };
};

};


#endif /* LOADABLEMANAGER_H */

