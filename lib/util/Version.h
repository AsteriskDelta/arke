#ifndef INC_VERSION
#define INC_VERSION
#include <string>

namespace arke {

    const char VersionMLookups[] = {
        'u',//Unknown
        'X',//eXperimental
        'D',//Devloper
        'a',//Alpha
        'b',//Beta
        'c',//Canidate
        'R',//Release
        'p',//Production
        'o'//Omega (End of line)
    };

    const unsigned char VersionCountLookups = sizeof(VersionMLookups);

    class Version {
    public:
        Version() : value(0) {};
        Version(unsigned int i);
        Version(std::string s);
        
        inline bool operator<(const Version& o) const { return value < o.value; };
        inline bool operator>(const Version& o) const { return value > o.value; };
        inline bool operator<=(const Version& o) const { return value <= o.value; };
        inline bool operator>=(const Version& o) const { return value >= o.value; };
        inline bool operator==(const Version& o) const { return value == o.value; };
        inline bool operator!=(const Version& o) const { return value != o.value; };
        
        inline unsigned int getValue() const { return value; };
        std::string str() const;
        inline std::string toString() const { return str(); };
    protected:
        //As Uint8s: Major.Minor rRevision char(value[3])
        union {
            unsigned int value;
            struct {
            unsigned char code, rev, minor, major;
            };
        };
    };

}

#endif
