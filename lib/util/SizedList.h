#ifndef SIZEDLIST_INC
#define SIZEDLIST_INC
#include <string>

namespace arke {

    template<typename T, int S>
    class SizedList {
    public:
    inline SizedList() : dataCount(0) {
        
    };
    inline ~SizedList() {
        
    }
    
    inline T& operator[](const unsigned int i) {
        return data[i];
    }
    
    inline int add(const T& item) {
        if(dataCount >= S) return -1;
        data[dataCount] = item;
        return dataCount++;
    }
    inline void remove(unsigned int i) {
        if(i >= dataCount) return;
        else if(i == dataCount-1) dataCount--;
        else {
        data[i] = data[--dataCount];
        }
    }
    inline bool remove(const T& item) {
        for(unsigned int i = 0; i < dataCount; i++) {
        if(data[i] == item) {
        this->remove(i);
        return true;
        }
        }
        return false;
    }
    
    inline int findID(std::string id) {
        for(unsigned int i = 0; i < dataCount; i++) {
        if(data[i].id == id) return i;
        }
        return -1;
    }
    
    inline unsigned int count() const { return dataCount; };
    inline unsigned int max() const { return S; };
    protected:
    unsigned int dataCount;
    T data[S];
    };

};

#endif
