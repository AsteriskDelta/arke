#pragma once
#include <cfloat>

namespace arke {

    const double rdDistMapping = 5.52;

    namespace Random {
    int Range(int min, int max);
    float Range(float min, float max);
    double Range(double min, double max);
    
    template<typename T> T Gaussian (T mean, T vPrior, T vAfter);
    template<typename T> inline T Gaussian(T mean, T variance) {
        return Gaussian(mean, variance, variance);
    }
    
    void Seed(int seed);
    };

};
