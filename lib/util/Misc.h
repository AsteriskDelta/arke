#pragma once
#include <string>

namespace arke {

    namespace Misc {
    std::string ToRoman(unsigned int value);
    };

};
