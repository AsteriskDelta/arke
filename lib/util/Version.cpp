#include "Version.h"
#include <sstream>
#include "Console.h"

namespace arke {

    Version::Version(unsigned int val) {
    value = val;
    if(code > VersionCountLookups) code = VersionCountLookups-1;
    }

    Version::Version(std::string in) {
    unsigned int tmpMajor, tmpMinor, tmpRev; char tmpCode;
    sscanf(in.c_str(), "%u.%ur%u%c", &tmpMajor, &tmpMinor, &tmpRev, &tmpCode);
    if(tmpCode == '-') sscanf(in.c_str(), "%u.%ur%u-%c", &tmpMajor, &tmpMinor, &tmpRev, &tmpCode);
    major = std::min((unsigned int)255, tmpMajor);
    minor = std::min((unsigned int)255, tmpMinor);
    rev   = std::min((unsigned int)255, tmpRev);
    code = 0xFF;
    for(unsigned char i = 0; i < VersionCountLookups; i++) {
        if(tolower(VersionMLookups[i]) == tolower(tmpCode)) { code = i; break; };
    }
    
    if(code == 0xFF) {
        code = 0;
        std::stringstream ss;
        ss << "Unknown version code '"<<tmpCode<<"' [" << int(tmpCode) << "]";
        Console::Warning(ss.str());
    }
    }

    std::string Version::str() const {
    std::stringstream ss;
    ss << int(major) << '.' << int(minor) << 'r' << int(rev) << '-' << VersionMLookups[std::max(code, (unsigned char)0)];
    return ss.str();
    }

};
