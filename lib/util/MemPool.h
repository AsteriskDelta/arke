#ifndef INC_MEMPOOL
#define INC_MEMPOOL
#ifndef NULL
#define NULL 0
#endif

namespace arke {

    class MemPool {
    public:
    MemPool(unsigned int len = 0);
    MemPool(char* d, unsigned int nsize);
    ~MemPool();
    
    inline operator char*() { return data; };
    
    void resize(unsigned int newSize);
    void shift(unsigned int from, unsigned int to);
    
    inline void ensure(unsigned int total, unsigned int nextStep = 0) {
        if(total < size) return; 
        else resize(nextStep != 0? nextStep : total);
    }
    
    inline unsigned int maxSize() { return size; };
    
    char* data;
    protected:
    struct {
        bool extPtr		: 1;
        unsigned int size	: 31;
    };
    };

};

#endif
