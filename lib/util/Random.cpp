#include "Random.h"
#define SFMT_MEXP 19937
#include "ext/sfmt/SFMT.h"
#include "ext/sfmt/SFMT.c"
#include <cmath>
#include <algorithm>

namespace arke {

    namespace Random {
    sfmt_t sfmt;
    
    int Range(int min, int max) {
        int ret = sfmt_genrand_uint32(&sfmt);
        ret %= max - min;
        return ret + min;
    }
    
    float Range(float min, float max) {
        float ret = (float)sfmt_genrand_uint32(&sfmt) * (1.0f/4294967296.0f);
        return (ret * (max - min) + min);
    }
    
    double Range(double min, double max) {
        double ret = sfmt_genrand_res53(&sfmt);
        return (ret * (max - min) + min);
    }
    
    void Seed(int seed) {
        sfmt_init_gen_rand(&sfmt, seed);
    }
    
    template<typename T> T Gaussian (T mean, T vPrior, T vAfter) {
        double u1 = Range(0., 1.0), u2 = Range(0.0, 1.0);
        u1 += DBL_EPSILON;
        //u2 += DBL_EPSILON;
        //Box-Mueller transform
        double ret = cos(8.*atan(1.)*u2)*sqrt(-2.*log(u1)); 
        if(ret >= 0.0) ret = vAfter * std::min(1.0, ret / rdDistMapping);
        else ret = vPrior * std::max(-1.0, ret / rdDistMapping);
        return (ret + mean);
    }
    };

};
