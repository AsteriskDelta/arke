#include "ProgramStat.h"
#include <mutex>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <unistd.h>
#include <atomic>
#include <stack>
#include <iostream>

#include <unordered_map>
#include "Histogram.h"

namespace arke {

    namespace ProgramStat {
    thread_local std::list<QueuedTF> tfq = std::list<QueuedTF>();
    thread_local std::list<QueuedTF> dbq = std::list<QueuedTF>();
    thread_local std::thread *recThread = nullptr;
    thread_local unsigned long long threadAlloc = 0;
    
    struct CallInfo {
        unsigned int callID;
        std::string functionName;
        std::string fileName, fileLine;
        Histogram histogram;
        
        inline std::string toString() {
        std::stringstream ss;
        ss << "#"<<callID << " " << functionName << "["<<fileName << ":"<<fileLine<<"]";
        return ss.str();
        }
    };
    struct CallInstance {
        unsigned int callID;
        unsigned int callSize;
        double start;
    };
    
    std::unordered_map<std::string, CallInfo> callMap;
    std::unordered_map<unsigned int, std::string> revCallMap;
    unsigned int callInfoCount = 0;
    std::mutex callMapMutex;
    
    thread_local std::unordered_map<unsigned int, Histogram> localHistograms;
    thread_local std::stack<CallInstance> callStack;
    
    inline std::string mkCallName(const char* fstr, const char *lstr, const char *pf, unsigned char st) {
        ((void)pf);
        if(st == 0) return std::string(fstr)+"@"+std::string(lstr);
        else return std::string(fstr)+"#"+std::string(lstr);
        //((void)fstr);((void)lstr);
        //std::string ret = std::string(pf);
        //return ret;
    }
    inline unsigned int getCallID(const char* fstr, const char *lstr, const char *prettyFunc, unsigned char st) {
        const std::string key = mkCallName(fstr, lstr, prettyFunc, st);
        auto it = callMap.find(key);
        if(it == callMap.end()) {
        callMapMutex.lock();
        it = callMap.find(key);
        if(it == callMap.end()) {
            const unsigned int newID = callInfoCount++;
            callMap.insert({key, CallInfo{newID, prettyFunc, fstr, lstr, Histogram()}});
            it = callMap.find(key);
            revCallMap[newID] = key;
        }
        callMapMutex.unlock();
        }
        
        return it->second.callID;
    }
    inline Histogram* getLocalHist(unsigned int cid) {
        auto it = localHistograms.find(cid);
        if(it == localHistograms.end()) {
        localHistograms.insert({cid, Histogram()});
        it = localHistograms.find(cid);
        }
        return &(it->second);
    }
    
    void RecorderThread(std::string outDir, bool enabled, unsigned int threadID, std::mutex *nRec, std::mutex *swp, std::atomic<bool> *swpBool, std::list<QueuedTF> *myDataQ) {
        std::stringstream ss; ss << outDir << "/thread" << std::hex << threadID << ".txt";
        std::string threadFileName = ss.str();
        std::ofstream outStream(threadFileName, std::ofstream::out | std::ofstream::trunc);
        swpBool->store(false);
        //char lineBuf[2049];
        bool doEnd = false;
        while(!doEnd && !nRec->try_lock()) {
        //Write all data
        while(!doEnd) {
            if(nRec->try_lock()) {
            doEnd = true;
            break;
            }
            if(swpBool->load() != false) break;
            usleep(100);
        }
        /*while(!doEnd && !swp->try_lock()) {
            if(nRec->try_lock()) {
            doEnd = true;
            break;
            }
            usleep(100);
        }*/
        swp->lock();
        swpBool->store(false);
        
        if(enabled && !doEnd) {
            for(auto it = myDataQ->begin(); it != myDataQ->end(); ++it) {
            //snprintf(lineBuf, 2048, "%f\t%d\t%s @ %s:%s", it->t,it->state, it->pf, it->ftext, it->ltext);
            //outStream << lineBuf;
            const bool isAlloc = (BIT31 & it->state) != 0x0, isFree = (BIT30 & it->state) != 0x0;
            const unsigned int remState = it->state & (~(BIT31 | BIT30));
            unsigned int callID = getCallID(it->ftext, it->ltext, it->pf, (isAlloc||isFree)? 1 : 0);
            
            if(it->state == 0) {//Push callstack
                callStack.push(CallInstance{callID, 0, it->t});
            } else if(it->state == 1) {//Pop
                if(callStack.size() > 0) {
                const CallInstance inst = callStack.top(); callStack.pop();
                if(inst.callID != callID) {
                    std::cerr << "Performance trace stack mismatch for functions " << callMap[revCallMap[callID]].toString() << " and " << callMap[revCallMap[inst.callID]].toString() << "... Did you forget a _prstart() or _prsend() in either function?"<<std::endl;
                    break;
                }
                Histogram *hist = getLocalHist(callID);
                double time = (it->t - inst.start)/1000.0;//Record in MS
                hist->add(time);
                } else {
                std::cerr << "Performance trace stack underflow... Did you forget a _prsend() call?"<<std::endl;
                break;
                }
            } else if(isAlloc && remState != 0) {//Handle allocation stats
                callStack.push(CallInstance{callID, remState, it->t});
                threadAlloc += remState;
            } else if(isAlloc) {
                if(callStack.size() > 0) {
                const CallInstance inst = callStack.top(); callStack.pop();
                if(inst.callID != callID) {
                    std::cerr << "Performance trace stack mismatch for allocs " << callMap[revCallMap[callID]].toString() << " and " << callMap[revCallMap[inst.callID]].toString() << "... Did you forget a _prstart() or _prsend() in either function?"<<std::endl;
                    break;
                }
                Histogram *hist = getLocalHist(callID);
                //double time = (it->t - inst.start)/1000.0;//Record in MS
                hist->add(double(inst.callSize));
                } else {
                std::cerr << "Performance trace stack underflow (prsalloc)... Did you forget a _prsend() call?"<<std::endl;
                break;
                }
            } else if(isFree && remState != 0) {//Handle allocation stats
                callStack.push(CallInstance{callID, remState, it->t});
                threadAlloc -= remState;
            } else if(isFree) {
                if(callStack.size() > 0) {
                const CallInstance inst = callStack.top(); callStack.pop();
                if(inst.callID != callID) {
                    std::cerr << "Performance trace stack mismatch for free " << callMap[revCallMap[callID]].toString() << " and " << callMap[revCallMap[inst.callID]].toString() << "... Did you forget a _prstart() or _prsend() in either function?"<<std::endl;
                    break;
                }
                Histogram *hist = getLocalHist(callID);
                //double time = (it->t - inst.start)/1000.0;//Record in MS
                hist->add(-double(inst.callSize));
                } else {
                std::cerr << "Performance trace stack underflow (prsfree)... Did you forget a _prsend() call?"<<std::endl;
                break;
                }
            }
            }
        }
        
        myDataQ->clear();
        //Allow buffer flip
        swp->unlock();
        }
        nRec->unlock();
        
        //Store and sort by initial metric
        struct EntryDS {
        unsigned int callID;
        Histogram::Stats stats;
        };
        std::list<EntryDS> sortedEntries = std::list<EntryDS>();
        for(auto it = localHistograms.begin(); it != localHistograms.end(); ++it) {
        Histogram::Stats stats = it->second.calculateStats();
        auto info = callMap.find(revCallMap[it->first]);
        if(info == callMap.end()) continue;
        sortedEntries.push_back(EntryDS{it->first, stats});
        }
        
        auto cmpFunc = [=](const EntryDS& a, const EntryDS& b) {
        return a.stats.mean > b.stats.mean;
        };
        sortedEntries.sort(cmpFunc);
        
        //Write local histograms
        std::cout << "writing " << sortedEntries.size() << " pts to " << threadFileName << "\n";
        for(auto it = sortedEntries.begin(); it != sortedEntries.end(); ++it) {
        auto info = callMap.find(revCallMap[it->callID]);
        if(info == callMap.end()) continue;
        outStream << info->second.toString() << "\t\t " << it->stats.toString() << "\n";
        }
        outStream.close();
        
        //Incorporate histogram
        
    }
    
    thread_local std::mutex *swapList;
    thread_local std::mutex *endRec;
    thread_local std::atomic<bool> *qSwapped;
    void SubmitFrame() {
        if(qSwapped->load() == false && swapList->try_lock()) {
        //We locked it, swap the buffers and unlock
        dbq.swap(tfq);
        qSwapped->store(true);
        swapList->unlock();
        } else {
        //Can't lock (we're still writing the previous data), defer the buffer swap
        }
    }
    
    void StartRecorderThread(std::string outDir, bool enabled) {
        unsigned int threadID =  std::hash<std::thread::id>()(std::this_thread::get_id());
        swapList = new std::mutex(); endRec = new std::mutex();
        qSwapped = new std::atomic<bool>();
        endRec->lock();
        recThread = new std::thread(RecorderThread, outDir, enabled, threadID, endRec, swapList, qSwapped, &dbq);
    }
    void StopRecorderThread() {
        //SubmitFrame();
        /*
        if(!endRec->try_lock()) endRec->unlock();
        swapList->lock();
        dbq.swap(tfq);
        //SubmitFrame();
        qSwapped->store(true);
        qSwapped->store(true);
        swapList->unlock();
        */
        //qSwapped->store(true);
        endRec->unlock();
        qSwapped->store(true);
        //swapList->unlock();
        //swapList->lock();
        if(recThread != nullptr) {
        //swapList->unlock();
        //qSwapped->store(true);
        
        //endRec->unlock();
        recThread->join();
        }
    }
    };
};
