#ifndef FLAGS_H
#define	FLAGS_H

namespace arke {

    template<typename T>
    class Flags {
    public:
        T value;
        
        Flags() { value = T(0); };
        Flags(T def) { value = def; };
        //~Flags();
        
        inline void set(T flag) { value |= flag; };
        inline void clr(T flag) { value &= ~flag; };
        
        inline void set(T flag, bool state) {
            return (state? set(flag) : clr(flag));
        }
        
        inline bool get(T flag) const { return (value & flag) != T(0); };
        inline bool all(T flag) const { return (value & flag) == flag; };
        
        inline void reset() { value = T(0); };
    private:
        
    };

};

#endif	/* FLAGS_H */

