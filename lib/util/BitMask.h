#ifndef BITMASK_H
#define BITMASK_H

namespace arke {

    template<typename T>
    struct BitMask {
        static constexpr T All = (1 << (sizeof(T)*8)) - 1;
        static constexpr T None = 0x0;
        
        static constexpr T Bytes(const unsigned char N) {
            return (T(1) << (T(N)*T(8))) - T(1);
        }
        static constexpr T Bytes(const unsigned char min, const unsigned char max) {
            return Bytes(max) & (~Bytes(min));
        }
        static constexpr T Bits(const unsigned short N) {
            return (T(1) << (T(N))) - T(1);
        }
        static constexpr T Bits(const unsigned short min, const unsigned short max) {
            return Bits(max) & (~Bits(min));
        }
    };

};

#endif /* BITMASK_H */

