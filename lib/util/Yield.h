/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Yield.h
 * Author: Galen Swain
 *
 * Created on June 29, 2016, 2:18 PM
 */

#ifndef YIELD_H
#define YIELD_H
#include <future>

#define _XOPEN_SOURCE_EXTENDED 1
#include <stdio.h>
#include <ucontext.h>
/*
namespace Yield {//Thread-safe coroutine management library
    class Coroutine;
    
    void Register(Coroutine *cr);
    void Deregister(Coroutine *cr);
    void Dispatch();
    unsigned int Waiting();
    
    class Coroutine {
    public:
        
        unsigned short depth;
        std::future<void>* future;
        ucontext_t context;
        
        inline Coroutine() : depth(0), future(nullptr), context(nullptr) {
            
        }
        inline void cleanup() {
           //if(future != nullptr) delete future;
           //if(context != nullptr) delete context;
           future = nullptr; context = nullptr;
        }
        inline ~Coroutine() {
            this->cleanup();
        }
        
        inline bool activate(std::future<void>* fut) {//Return true on store, false on load
            if(fut != nullptr) future = fut;
            ucontext_t *tmpContext = new ucontext_t();
            getcontext(tmpContext);
            //bool isSecond = (depth == 1);
            
            if(context != nullptr) {
                if(depth == 1) {
                    delete tmpContext;
                    depth--;
                    setcontext(context);
                } else {
                    Yield::Deregister(this);
                    this->cleanup();
                    return false;
                }
            } else {
                context = tmpContext;
                depth++;
                Yield::Register(this);
                return true;
            }
        }
        
        inline void expectReturn() {
            
        }
        inline void didReturn() {
            if(depth == 0) {
              delete this;  
            }
        }
        
        inline bool ready() {
            if(future->wait_for(std::chrono::seconds(0)) == std::future_status::ready) return true;
            else return false;
        }
    };
    class CoroutineGuard {
    public:
        Coroutine *coroutine;
        inline CoroutineGuard(Coroutine* cr) : coroutine(cr) {
            
        }
        inline ~CoroutineGuard() {
            coroutine->didReturn();
        }
        
    };
};
*/
#define _coroutine()    Yield::Coroutine *_coroutinePtr = new Yield::Coroutine();\
                        Yield::CoroutineGuard _coroutineGuard(_coroutinePtr);

//fut will automatically be delete'd on next dispatch
#define _yield(f, Args...)      ({\
                                _coroutinePtr->expectReturn();\
                                std::future<void>* _fut = new std::future<void>();\
                                (*_fut) = std::async(std::launch::async, f, Args);\
                                if(_coroutinePtr->activate(_fut)) return;\
                                })


#endif /* YIELD_H */

