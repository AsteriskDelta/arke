/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Yield.cpp
 * Author: Galen Swain
 * 
 * Created on June 29, 2016, 2:18 PM
 */

#include "Yield.h"

#include <vector>
#include <algorithm>
/*
namespace Yield {
  std::vector<Coroutine*> coroutines = std::vector<Coroutine*>();
  
  void Register(Coroutine *cr) {
    coroutines.emplace_back(cr);
  }
  void Deregister(Coroutine *cr) {
    //coroutines.remove(cr);
    coroutines.erase(std::remove(coroutines.begin(), coroutines.end(), cr), coroutines.end());
  }
  void Dispatch() {
    for(auto it = coroutines.begin(); it != coroutines.end(); ) {
      auto newIt = std::next(it);
      if((*it)->ready()) {
        (*it)->activate(nullptr);
        coroutines.erase(it);
      }
      it = newIt;
    }
  }
  unsigned int Waiting() {
    return coroutines.size();
  }
};*/
