#ifndef PSEUDO_H
#define PSEUDO_H
#include <type_traits>

namespace arke {

    struct Pseudo_Wrapper_T {};

    template<typename T, typename PAR,  T (*GET)(const PAR*), T (*SET)(PAR*, const T &)>
    class Pseudo : public Pseudo_Wrapper_T {
    public:
        typedef Pseudo<T,PAR,GET,SET> Self;
        typedef T Type;
        
        PAR *const parent;
        inline Pseudo(PAR *obj) : parent(obj) {
        }
        //inline Pseudo(const Pseudo& o) : parent(o.parent) {
        //}
        
        inline const T get() const {
            return GET(parent);//*(reinterpret_cast<T(*)()>(GET))();
        }
        inline const T set(const T& val) const {
            return SET(parent, val);//*(reinterpret_cast<T(*)(const T&& val)>(SET))(val);
        }
        
        inline operator T() const { return this->get(); };
        
        //inline const T operator()(const T& v) const { return this->set(v); };
        //inline const T operator=(const T& v) const { return this->set(v); };
        
        template<typename TARG_T>
        inline std::enable_if<!std::is_base_of<Pseudo_Wrapper_T, TARG_T>::value && std::is_constructible<T, TARG_T>::value, Pseudo> operator()(const TARG_T& v) const { 
            /*const auto ret = */this->set(T(v)); 
            //return ret;
            return *this;
        };
        
        /*template<typename TARG_T>
        inline typename std::enable_if<
        std::is_same<Self, TARG_T>::value
        , Self> 
        operator=(const TARG_T& v) const { 
            this->set(v.get());
            return *this;
        };
        template<typename TARG_T>
        inline typename std::enable_if<
        !std::is_same<Self, TARG_T>::value && 
        !std::is_base_of<Pseudo_Wrapper_T, TARG_T>::value && 
        std::is_constructible<T, TARG_T>::value
        , Self> */
        template<typename TARG_T, typename =
            typename std::enable_if<
                /*( !std::is_same<
                    Self,
                    TARG_T
                >::value )*/true,
                Self
            >::type
        >
        Self operator=(const TARG_T& v) const { 
            /*const auto ret = */this->set(T(v)); 
            //return ret;
            return Self(parent);
        };
        
        template<typename TARG_T>
        inline operator std::enable_if<!std::is_base_of<Pseudo_Wrapper_T, TARG_T>::value && std::is_convertible<T, TARG_T>::value, TARG_T>() const {
            return static_cast<TARG_T>(this->get());
        }
    private:

    };

};

#include <iostream>
template<typename genType>
typename std::enable_if<std::is_base_of<arke::Pseudo_Wrapper_T, genType>::value, std::ostream&>::type 
inline operator<<(std::ostream& out, const genType& g) {
    return out << static_cast<typename genType::Type>(g);
}


#endif /* PSEUDO_H */

