#include "MemPool.h"
#include <cstring>
#include <iostream>

namespace arke {

    MemPool::MemPool(unsigned int len) {
    size = 0;
    data = NULL;
    extPtr = false;
    if(len > 0) resize(len);
    }

    MemPool::MemPool(char* d, unsigned int ns) {
    extPtr = true;
    data = d;
    size = ns;
    }

    MemPool::~MemPool() {
    if(data != NULL&&!extPtr) delete[] data;
    }

    void MemPool::resize(unsigned int newSize) {
    char* newData = new char[newSize];
    if(data != NULL) {
        memcpy(newData, data, size);
        if(extPtr) extPtr = false;
        else delete[] data;
    }
    data = newData;
    size = newSize;
    }
    void MemPool::shift(unsigned int from, unsigned int to) {
    if(from > size || to > size) return;
    //std::cout << "moving " << from <<"->"<<to<<"\n";
    memmove(data + to, data + from, size - (from - to));
    //std::cout << "eMOV\n";
    }

};
