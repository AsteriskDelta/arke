#ifndef PROGRAMSTAT_H
#define PROGRAMSTAT_H
#include <chrono>
#include <thread>
#include <list>

#ifndef BIT31
#define BIT31 (1 << 31)
#define BIT30 (1 << 30)
#endif

namespace arke {

    namespace ProgramStat {
        struct QueuedTF {
            const char *ftext, *ltext, *pf;
            double t; unsigned int state;
        };
        extern thread_local std::list<QueuedTF> tfq;
        
        inline void registerTF(const char* ftext, const char *ltext, const char *pf, unsigned int s) {
            static const auto progStart = std::chrono::high_resolution_clock::now();
            double t = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - progStart).count();
            //Thread ID is handled by SubmitFrame()
            tfq.push_back(QueuedTF{ftext, ltext, pf, t, s});
        }
        
        void SubmitFrame();
        
        void StartRecorderThread(std::string outDir, bool enabled);
        
        inline void StartRecorderThread(std::string outDir) {
    #ifdef PROGSTAT
            StartRecorderThread(outDir, true);
    #else
            ((void)outDir);
            return;
    #endif
        }
        void StopRecorderThread();
    };

    #ifdef PROGSTAT
    #define STRING(s) #s
    #define S(x) #x
    #define S_(x) S(x)
    #define S__LINE__ S_(__LINE__)

    //#define _prstat() ProgramStat::registerTF(__FILE__, S__LINE__, __PRETTY_FUNCTION__, 0);
    //#define _prsend() ProgramStat::registerTF(__FILE__, S__LINE__, __PRETTY_FUNCTION__, 1);

    class prScopeGuard {
    public:
        const char *fp, *lp, *pfp;
        inline prScopeGuard(const char *f, const char *l, const char *pf) : fp(f), lp(l), pfp(pf) {
            ProgramStat::registerTF(fp, lp, pfp,0);
        };
        inline ~prScopeGuard() {
            ProgramStat::registerTF(fp, lp, pfp, 1);
        };
    };
    class prAllocGuard {
    public:
        const char *fp, *lp, *pfp;
        inline prAllocGuard(const char *f, const char *l, const char *pf, unsigned int sz) : fp(f), lp(l), pfp(pf) {
            ProgramStat::registerTF(fp, lp, pfp, BIT31 | sz);
        };
        inline ~prAllocGuard() {
            ProgramStat::registerTF(fp, lp, pfp, BIT31);
        };
    };
    class prFreeGuard {
    public:
        const char *fp, *lp, *pfp;
        inline prFreeGuard(const char *f, const char *l, const char *pf, unsigned int sz) : fp(f), lp(l), pfp(pf) {
            ProgramStat::registerTF(fp, lp, pfp, BIT30 | sz);
        };
        inline ~prFreeGuard() {
            ProgramStat::registerTF(fp, lp, pfp, BIT30);
        };
    };

    #define MERGE_(a,b)  a##b
    #define LABEL_(a) MERGE_(prSTmpGuardIden, a)
    #define PRS_UNIQUE_NAME LABEL_(__LINE__)
    #define LABEL2_(a) MERGE_(prSTmpAllocIden, a)
    #define PRS_UNIQUE_NAME2 LABEL2_(__LINE__)

    #define _prsguard() prScopeGuard PRS_UNIQUE_NAME(__FILE__, S__LINE__, __PRETTY_FUNCTION__); ((void)PRS_UNIQUE_NAME);
    #define _prsguardt(specText) prScopeGuard PRS_UNIQUE_NAME(__FILE__, specText, __PRETTY_FUNCTION__); ((void)PRS_UNIQUE_NAME);

    #define _prsalloc() prAllocGuard PRS_UNIQUE_NAME2(__FILE__, S__LINE__, __PRETTY_FUNCTION__, sizeof(*this)); ((void)PRS_UNIQUE_NAME2);
    #define _prsalloct(specText) prAllocGuard PRS_UNIQUE_NAME2(__FILE__, specText, __PRETTY_FUNCTION__, sizeof(*this)); ((void)PRS_UNIQUE_NAME2);
    #define _prsallocst(sz, specText) { prAllocGuard PRS_UNIQUE_NAME2(__FILE__, specText, __PRETTY_FUNCTION__, sz)); ((void)PRS_UNIQUE_NAME2); };

    #define _prsfree() prFreeGuard PRS_UNIQUE_NAME2(__FILE__, S__LINE__, __PRETTY_FUNCTION__, sizeof(*this)); ((void)PRS_UNIQUE_NAME2);
    #define _prsfreet(specText) prFreeGuard PRS_UNIQUE_NAME2(__FILE__, specText, __PRETTY_FUNCTION__, sizeof(*this)); ((void)PRS_UNIQUE_NAME2);
    #define _prsfreest(sz, specText) { prFreeGuard PRS_UNIQUE_NAME2(__FILE__, specText, __PRETTY_FUNCTION__, sz)); ((void)PRS_UNIQUE_NAME2); };
    #else 
    //#define _prstat() 
    //#define _prsend() 
    #define _prsguard() 
    #define _prsguardt(x) 

    #define _prsalloc() 
    #define _prsalloct(x) 
    #define _prsallocst(s, x) 

    #define _prsfree()
    #define _prsfreet(x) 
    #define _prsfreest(s, x) 
    #endif

};

#endif /* PROGRAMSTAT_H */
