#pragma once
#ifndef TIMER_H_DEF
#define TIMER_H_DEF

#ifdef WIN32   // Windows system specific
#include <windows.h>
#else          // Unix based system specific
#include <sys/time.h>
#endif

namespace arke {

    class Timer {
    public:
        Timer();                                    // default constructor
        ~Timer();                                   // default destructor

        void   start();
        void   stop();
        double getElapsedSeconds();
        double getElapsedMilli();
        double getElapsedMicro();


    protected:
        double startTimeInMicroSec;
        double endTimeInMicroSec;
        int    stopped;
    #ifdef WIN32
        LARGE_INTEGER frequency;
        LARGE_INTEGER startCount;
        LARGE_INTEGER endCount; 
    #else
        timeval startCount;
        timeval endCount;
    #endif
    };

};

#endif // TIMER_H_DEF
