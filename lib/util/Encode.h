#ifndef ENCODE_H
#define ENCODE_H
#include <string>

namespace arke {

    namespace Encode {
        //Standard Base64 encoding
        std::string ToBase64(const unsigned char* ptr, unsigned int len);
        std::string FromBase64(const std::string& s);
        unsigned int FromBase64(const std::string & s, const unsigned char* targetPtr, unsigned int maxLength);
        
        //Filesystem-safe Base64 encoding (a la RFC 3548)
        std::string ToBaseFS(const unsigned char* ptr, unsigned int len);
        std::string FromBaseFS(const std::string& s);
        unsigned int FromBaseFS(const std::string & s, const unsigned char*targetPtr, unsigned int maxLength);
        
        //Encodes a longer filename to a set of subdirectories with prefixes, limiting to maxFileCount files/dirs per directory
        std::string FSFragment(const std::string& name, unsigned int maxFileCount = 4096, unsigned int base = 64);
        
        //Encodes 64b number with default FSFragment parameters
        std::string FSEncode(const unsigned long long& id);
    };

};

#endif /* ENCODE_H */

