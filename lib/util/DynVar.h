#ifndef VARTYPE_INC
#define VARTYPE_INC
#define MAX_VT_SIZE sizeof(double)

class DynVar {
enum Type : unsigned char {
  Bool, Int, Float, String
};
public:
  DynVar();
  ~DynVar();
  
  void SetType(Type);
  
protected:
  Type type;
  unsigned char data[MAX_VT_SIZE];
};

#endif