#include "DataStore.h"
#include <cstring>
#include <cstdlib>
#include <fstream>
#include "Application.h"
#include "AppData.h"
#include <sstream>

namespace arke {

    template<typename T>
    DSVar<T>::DSVar() {
    value = NULL;
    attached = false;
    }

    static DataStorePayload dstpl;
    template<typename T>
    DSVar<T>::~DSVar() {
    detach();
    }

    template<typename T>
    void DSVar<T>::detach() {
    if(value != NULL) {//Hacky pointer-backing below, cry if you must- it works.
        const int ptrOffset = int((unsigned char*)&(dstpl.data) - (unsigned char*)&(dstpl));
        DataStorePayload *const payload = (DataStorePayload*)( ((unsigned char*)value) - ptrOffset);
        payload->instances.remove((DSVarContainer*)this);
        value = NULL; attached = false;
    }
    }

    template<typename T>
    void DSVar<T>::attach(unsigned char *const p) {
    value = p;
    if(value != NULL) {
        const int ptrOffset = int((unsigned char*)&(dstpl.data) - (unsigned char*)&(dstpl));
        DataStorePayload *const payload = (DataStorePayload*)( ((unsigned char*)value) - ptrOffset);
        payload->instances.push_front((DSVarContainer*)this);
        attached = true;
    }
    }

    DataStorePayload::~DataStorePayload() {
    for (auto var : instances) var->detach();
    /*if((ovrDataType & Arr) != 0x00 && data != NULL) */
    this->deleteData();
    //else if(data != NULL) delete data;
    }

    unsigned char DataStore::varBuffer[DATASTORE_TMPBUFFSZ];

    void DataStore::initVars() {
    lastLength = 0;
    loadPath = "";
    }

    DataStore::DataStore() {
    initVars();
    }

    DataStore::DataStore(const std::string& path) {
    initVars();
    this->Load(path);
    }

    DataStore::~DataStore() {
    //Payload destruction is done by unordered_map destructor and ~DataStorePayload()
    }

    bool DataStore::Load(const std::string& file, bool isRelPath) {
    std::fstream in; std::string path; std::stringstream ss;
    if(file == "" && loadPath != "") path = loadPath;
    else if(isRelPath) path = Application::GetLocal().resolve(file);
    else path = file;
    in.open(path.c_str(), std::fstream::in | std::fstream::binary);
    
    static char newKey[256]; memset(newKey, 0x00, sizeof(newKey));
    
    unsigned short keyCount = 0;
    if(in) {
        unsigned char keyLength, flags; unsigned int dataLength;
        unsigned char* newData = NULL; DataStorePayload payload;
        //We want to keep each data array allocated, as they are referenced in the map
        while(in.good()) {
        in.read((char*)&keyLength, 1);
        in.read(newKey, keyLength);
        
        in.read((char*)&flags, sizeof(DataStorePayload::ovrDataType));
        in.read((char*)&dataLength, sizeof(DataStorePayload::length));

        newData = new unsigned char[dataLength];
        in.read((char*)newData, dataLength);
        
        payload.data = newData;
        payload.length = dataLength;
        payload.ovrDataType = flags;
        
        map.insert(map.begin(), {std::string(newKey), payload});
        
        in.peek();
        keyCount++;
        }
        
        std::stringstream s; s << "Save data loaded, " << keyCount << " keys registered.";
        Console::Print(s.str());
    } else {
        ss << "Unable to read save data at \"" << path << "\"! If you appear to be missing data, check permissions and paths!";
        Console::Warning(ss.str());
        return false;
    }
    
    return true;
    }

    bool DataStore::Save(const std::string& file, bool isRelPath) {//Defaults to current load path
    std::fstream out; std::string path;
    if(file == "" && loadPath != "") path = loadPath;
    else if(isRelPath) path = Application::GetLocal().resolve(file);
    else path = file;
    out.open(path.c_str(), std::fstream::out | std::fstream::trunc | std::fstream::binary);
    
    if(!out) {
        std::stringstream ss; ss << "Unable to write save data to \"" << path << "! Check permissions and path!";
        Console::Error(ss.str());
        return false;
    }
    
    unsigned char keyLength;
    for(auto found : map) {
        if(found.second.length > 0) {
        keyLength = (unsigned char)std::min(255, int(((found.first).length()) + 1) );
        out.write((char*)&keyLength, 1);
        out.write((char*) (found.first.c_str()), keyLength);
        out.write((char*)&(found.second.ovrDataType), sizeof(DataStorePayload::ovrDataType));//Flags
        out.write((char*)&(found.second.length), sizeof(DataStorePayload::length));
        out.write((char*) (found.second.data), found.second.length);
        }
    }
    out.close();
    std::stringstream ss;
    ss << "Save data successfully written to \"" << path << "\".";
    Console::Print(ss.str());
    return true;
    }

    template <typename T> DSVar<T> DataStore::var(const std::string& id) {
    DSVar<T> ret;
    auto it = map.find(id);
    if (it == map.end()) {
        ret.detach();
    } else {
        ret.attach(&((*it).second.data) );
    }
    
    return ret;
    }

    void DataStore::deleteKey(const std::string& id) {
    map.erase(id);
    }

    bool DataStore::hasKey(const std::string& id) {
    auto it = map.find(id);
    if (it == map.end()) return false;
    else return true;
    }

    unsigned int DataStore::getRawToBuff(const std::string& id, unsigned int maxLen) {//Stores to static buffer, moves to stack
    auto it = map.find(id);
    DataStorePayload *payload = NULL;
    if(it != map.end() && (payload = &(it->second)) != NULL && payload->length > 0) {
        unsigned int dataLen = std::min(payload->length, std::min(maxLen, (unsigned int)DATASTORE_TMPBUFFSZ));
        memcpy(varBuffer, payload->data, dataLen);
        return dataLen;
    }
    
    memset(varBuffer, 0x00, maxLen);
    return 0;
    }

    unsigned char* DataStore::getRawToPtr(const std::string& id, unsigned int& len) {//Allocates Data
    (void)(id); (void)(len);
    Console::Error("DataStore::getRawToPtr unimplemented call!");
    return NULL;
    }

    void DataStore::setRaw(const std::string& id, unsigned char * data, const unsigned int length, unsigned char flags) {
    auto it = map.find(id);
    DataStorePayload *payload = NULL;
    
    if(it != map.end() && (payload = &(it->second)) != NULL) {
        if(payload->data != NULL) payload->deleteData();
        payload->data = new unsigned char[length];
        memcpy(payload->data, data, length);
        payload->length = length;
        payload->ovrDataType = flags;
    } else {
        DataStorePayload payloadObj;
        payloadObj.length = length;
        payloadObj.ovrDataType = flags;
    
        payloadObj.data = new unsigned char[length];
        memcpy(payloadObj.data, data, length);
        map.insert(map.begin(), {id, payloadObj});
    }
    }

};
