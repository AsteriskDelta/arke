#ifndef GDATA_INC
#define GDATA_INC
#include <string>
#include <algorithm>
#include <functional>
#include "Shared.h"

#define AD_CTX_MAX 32
#define AD_VFS_MAX 128

namespace arke {

class Directory;

namespace AppData {
  enum Base : unsigned char {Rel = 0, Abs = 1, Sys = 2};
  enum class Permissions : unsigned char {
    ReadRoot	  = BIT0,
    WriteRoot 	  = BIT1,
    ReadWriteRoot = BIT0 | BIT1,
    Read	  = BIT2,
    Write	  = BIT3,
    ReadWrite 	  = BIT2 | BIT3,
    Register	  = BIT4,
    Override	  = BIT5,
    Context 	  = BIT6,
    AccessSys	  = BIT7,
    
    Full = 0xFF,
    Program = (ReadWriteRoot | ReadWrite | Register | Override),
    MaxMod  = (ReadRoot | ReadWrite | Register | Override),
    NormMod = (ReadRoot | ReadWrite | Register),
    MinMod  = (ReadRoot | Read),
    None = 0x00
  };
  
  bool Initialize(std::string rootPath);
  bool Register(const std::string& realPath, const std::string& mountPath, bool over = true);
  bool Unregister(const std::string& realPath);
  
  std::string Resolve(const std::string &path, Base b = Rel);
  Directory GetSys(const std::string &path);
  
  void PushContext(const std::string &path, Permissions perm = Permissions::NormMod);
  void PopContext();
  void ClearContext();
  
  bool HasPermissions(Permissions p);
  bool HasPermissions(const std::string& path, Permissions p);
  
  extern Directory root, base, system;
};

class Directory {
public:
  Directory() { rawPath = ""; valid = false; base = AppData::Rel; };
  Directory(const std::string rPath, const AppData::Base b = AppData::Rel) { 
    base = b;
    this->setPath(rPath);
  };
  inline operator std::string() const { return rawPath; };
  inline std::string str() const { return rawPath; };
  inline operator bool() const { 
    return valid; 
  };
  inline const Directory& operator=(const std::string& path) {
     this->setPath(path);
     return *this;
  };
  
  
  bool setPath(const std::string& path) {
    rawPath = path;
    std::replace(rawPath.begin(), rawPath.end(), '\\', '/');
    if(path[0] == '/' && AppData::HasPermissions(path, AppData::Permissions::AccessSys)) base = AppData::Sys;
    valid = this->isDir();
    return valid;
  }
  
  bool exists(const std::string& subPath = "");
  bool isDir(const std::string& subPath = "");
  bool isFile(const std::string& subPath);
  std::string resolve(const std::string& subPath = "", bool checkDirs = false);
  
  bool mkdir(const std::string& subPath);
  bool cd(const std::string& subPath);
  
  unsigned int readFileBuffered(const std::string& subPath, char *const buff, unsigned int& length, bool nullTail = false);
  unsigned char* readFile(const std::string& subPath, unsigned int& length, bool nullTail = false);
  inline char* readFileText(const std::string& subPath, unsigned int& length) { return (char*)readFile(subPath, length, true); };
  bool writeFile(const std::string& subPath, const char* buffer, const unsigned int length);
  bool deleteFile(const std::string& subPath);
  bool touch(const std::string& subPath);
  
  bool list(std::string *& contents, unsigned int& count, const std::string& subPath);
  bool listCallback(bool (*callback)(std::string name, std::string fullPath), bool recurse = false, std::string path = "");
  bool listCallback(std::function<bool(std::string, std::string)> f, bool recursive = false, std::string path = "");
  
  Directory getDir(std::string path);
  Directory getParent();
  
  AppData::Base base;
protected:
  inline std::string getPath(const std::string& subPath);
  
  bool valid;
  std::string rawPath;//without trailing slash
};

};

#endif
