#ifndef D13_SERIALIZABLE_H
#define D13_SERIALIZABLE_H
#include "ARKE.h"

namespace arke {
    class JSONNode;
    class Serializable {
    public:
        Serializable();
        virtual ~Serializable();

        virtual void serializeTo(JSONNode *node, const std::string& pth) const;
        virtual void deserializeFrom(JSONNode *node, const std::string& pth);

        virtual std::string serialize() const;
        virtual void deserialize(const std::string& str);
    protected:

    };
};

#endif
