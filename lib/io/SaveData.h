#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <unordered_map>

namespace arke {

    struct SaveDataKey {
    unsigned short length;
    unsigned char* data;
    };

    #ifndef COLOR_DECLARED
    #define COLOR_DECLARED
    struct Color {
    unsigned char r, g, b, a;
    };
    #endif

    class SaveData {
    public:
    bool Load(std::string file, unsigned short newKeySuggestion = 256);
    bool Save();
    
    static std::string GetPath(const std::string& name);
    static std::string GetDir();
    
    bool			Exists(const std::string& id);

    bool			GetBool(const std::string& id, bool def = false);
    char 			GetChar(const std::string& id, char def = 0);
    unsigned char		GetByte(const std::string& id, unsigned char def = 0);
    int 			GetInt(const std::string& id, int def = 0);
    float			GetFloat(const std::string& id, float def = 0.0f);
    unsigned short 	GetUni(const std::string& id, unsigned short def = 0);
    std::string 		GetString(const std::string& id, std::string def = "");
    unsigned char* 	GetBytes(const std::string& id, unsigned short& length);
    Color			GetColor(const std::string& id, Color def);
    
    bool			SetBool(const std::string& id, bool value);
    bool			SetChar(const std::string& id, char value);
    bool			SetByte(const std::string& id, unsigned char value);
    bool			SetInt(const std::string& id, int value);
    bool			SetFloat(const std::string& id, float value);
    bool			SetUni(const std::string& id, unsigned short value);
    bool			SetString(const std::string& id, const std::string& value);
    bool			SetBytes(const std::string& id, unsigned char* value, unsigned short length);
    bool			SetColor(const std::string& id, Color value);
    private:
    std::unordered_map<std::string, SaveDataKey> map;
    std::unordered_map<std::string, SaveDataKey>::iterator found;
    SaveDataKey toInsert;
    
    unsigned short keyCount;
    unsigned short keySuggestion;
    
    std::string fileName;
    
    static std::string saveDir;
    static unsigned char* buffer;
    };

};
