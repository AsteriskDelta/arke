#include <string>
#include <unordered_map>
extern std::unordered_map<std::string,const std::string*>& _CTMap;

inline const std::string* _ctdata(const std::string& path) {
    auto it = _CTMap.find(path);
    if(it == _CTMap.end()) return nullptr;
    else return it->second;
}
inline bool _cthas(const std::string& path) {
    return _ctdata(path) != nullptr;
}
