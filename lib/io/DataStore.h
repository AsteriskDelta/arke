#ifndef DATASTORE_INC
#define DATASTORE_INC
#include <string>
#include <unordered_map>
#include <utility>
#include <forward_list>
#include "Console.h"

#define DATASTORE_TMPBUFFSZ (1024 * 4)

namespace arke {

    class DSVarContainer {
    public:
        virtual void detach() {};
        virtual void attach(unsigned char *const p) { (void)(p);};
    protected:
        
    };

    template<typename T>
    class DSVar : public DSVarContainer {
    public:
        DSVar();
        ~DSVar();
        void detach();
        void attach(unsigned char *const p);
        
        inline operator T&() {
            if(!attached) this->attach((unsigned char*)value);
            
            if(value == NULL) {
                Console::Warning("Dereference of null DSVar attemtped!");
                return T();
            }
            else return (*value);
        }
    private:
        T* value;
        bool attached;
    };

    class DataStorePayload {
    public:
        enum {
            SigType      = 0x1 << 0,
            Arr          = 0x1 << 1
        };
        DataStorePayload() : data(NULL), length(0), ovrDataType(0x00) {};
        ~DataStorePayload();
        
        unsigned char* data;
        unsigned int length;
        unsigned char ovrDataType;
        std::forward_list<DSVarContainer*> instances;
        
        inline void deleteData() {
            if(data == NULL) return;
            
            if((ovrDataType & SigType) != 0x00) delete data;
            else delete[] data;
            length = 0;
        }
    };

    class DataStore {
    public:
        DataStore();
        DataStore(const std::string& path);
        ~DataStore();

        bool Load(const std::string& path, bool isRelPath = true);
        bool Save(const std::string& file = "", bool isRelPath = true); //Defaults to current load path

        void deleteKey(const std::string& id);
        bool hasKey(const std::string& id);
        
        template <typename T> DSVar<T> var(const std::string& id);
        
        template <typename T> T get(const std::string& id);
        template <typename T> T get(const std::string& id, const T& def);

        template <typename T> void set(const std::string& id, const T& val);

        inline unsigned int getLengthLast() {
            return lastLength;
        };
    protected:
        void initVars();

        unsigned int getRawToBuff(const std::string& id, unsigned int maxLen); //Stores to static buffer, moves to stack
        unsigned char* getRawToPtr(const std::string& id, unsigned int& len); //Allocates Data
        void setRaw(const std::string& id, unsigned char * data, const unsigned int length, unsigned char flags = 0x00);
        template<typename T> void setType(const std::string& id, const T& value);

        unsigned int lastLength;
        static unsigned char varBuffer[DATASTORE_TMPBUFFSZ];
        std::unordered_map<std::string, DataStorePayload> map;
        std::string loadPath;
    };

    template<typename T> T DataStore::get(const std::string& id) {
        if (std::is_pointer<T>::value) {
            unsigned char *const data = getRawToPtr(id, lastLength);
            return (T) data;
        } else {
            lastLength = getRawToBuff(id, sizeof (T));
            return T(*varBuffer);
        }
    }

    template<typename T> T DataStore::get(const std::string& id, const T& def) {
        if (!this->hasKey(id)) return def;
        return this->get<T>(id);
    }

    template <typename T> void DataStore::set(const std::string& id, const T& val) {
        unsigned char flags = 0x00;
        if(std::is_same<T, std::string>::value) {
            return this->setType<T>(id, val);
        }
        
        if (std::is_pointer<T>::value) {
            this->setRaw(id, (unsigned char*) val, sizeof ( decltype(*std::declval<T>())), flags);
        } else {
            this->setRaw(id, (unsigned char*) &val, sizeof (T), flags);
        }
    }

    template <typename T> void DataStore::setType(const std::string& id, const T& value) {
    auto it = map.find(id);
    DataStorePayload *payload = NULL;
    const unsigned int length = sizeof(T);
    const unsigned char flags = DataStorePayload::SigType;
        
    if(it != map.end() && (payload = &(it->second)) != NULL) {
        if(payload->data != NULL) payload->deleteData();
        payload->data = new T(value);
        payload->length = length;
        payload->ovrDataType = flags;
    } else {
        DataStorePayload payloadObj;
        payloadObj.length = length;
        payloadObj.ovrDataType = flags;
        payloadObj.data = new T(value);
        map.insert(map.begin(), {id, payloadObj});
    }
    }

};

#endif
