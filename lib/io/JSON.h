#ifndef ARKE_JSON_H
#define ARKE_JSON_H
#include "ext/picojson.h"
namespace arke {
    namespace json = picojson;
}

namespace picojson {
    class value;
}

namespace arke {
    class JSON;

    struct JSONNode {
        enum Type {
            None, Bool, Double, String, Array, Object, Int64
        };
        JSONNode *json = nullptr;
        mutable json::value *value = nullptr;
        std::string path = "";
        
        ~JSONNode() {
            //Only delete the root JSON node, since it presumably has recursive destructors
            if((json == nullptr || json->value == this->value) && this->value != nullptr) delete this->value;
        }

        inline std::string serialize() const {
            return value->serialize();
        }
        inline bool deserialize(const std::string& str, std::string *errPtr = nullptr) {
            std::string err = "";
            if(value == nullptr) value = new json::value();
            json::parse(*value, str.begin(), str.end(), &err);
            if(err.empty()) {
                if(errPtr != nullptr) (*errPtr) = err;
                return false;
            }

            return true;
        }
        inline void blank() {
            if(value == nullptr) value = new json::value(json::object());
            json = this;
        }

        inline JSONNode create(const std::string& pth) {
            json::value *val = this->getValue(pth, Type::Object);
            JSONNode ret;
            ret.json = this->json;
            ret.value = val;
            return ret;
        }
        inline json::value* getValue(const std::string& subpath, Type ctype = Type::None) const {
            std::string sep = "/.";
            auto et = find_first_of(subpath.begin(), subpath.end(), sep.begin(), sep.end());
            std::string sub = std::string(subpath.begin(), et);
            //std::cout << "Asked " <<this->value << " for " << sub << " from " << subpath << "\n";

            if(this->value == nullptr) {
                this->value = new json::value(json::object());
            }

            bool last = subpath == sub;
            json::value *ret = this->value;

            if(ret->is<json::object>()) {
                auto& map = ret->get<json::object>();
                auto it = map.find(sub);

                if(it == map.end()) {
                    auto nctype = last? ctype : Type::Object;
                    switch(nctype) {
                        case Type::String:
                            map[sub] = json::value(std::string());
                        break;
                        default:
                        case Type::Object:
                            map[sub] = json::value(json::object());
                        break;
                        case Type::Array:
                            map[sub] = json::value(json::array());
                        break;
                        case Type::Bool:
                            map[sub] = json::value(bool(false));
                        break;
                        case Type::Double:
                            map[sub] = json::value(0.0);
                        break;
                        case Type::Int64:
                            map[sub] = json::value(int64_t(0));
                        break;
                    }
                    //std::cout << "\tcreated object at " << &map[sub] << "\n";
                }

                ret = &map[sub];
            }

            if(!last) {
                JSONNode tmp;
                tmp.json = this->json;
                tmp.value = ret;
                return tmp.getValue(std::string(++et, subpath.end()), ctype);
            } else {
                //std::cout << "return last object at " << ret << "\n";
                return ret;
            }
        }
        /*
        inline json::value* getValue(const std::string& subpath, Type ctype = Type::None) const {
            std::cout << "pre getvalue " << subpath << "\n";
            //if(subpath.empty()) return value;
            //else if(this->value == nullptr || !value->is<json::object>()) return nullptr;

            if(this->value == nullptr) {
                this->value = new json::value(json::object());
            }

            std::cout << "JSON::getValue " << subpath << "\n";
            static const std::string sep = "/";
            auto bt = subpath.begin(),
                 ft = find_first_of(subpath.begin(), subpath.end(), sep.begin(), sep.end());//subpath.find_first_of('.');
            json::value *ret = value;
            do {
                std::string sub = (bt > ft)? std::string() : std::string(bt, ft);
                std::cout << "JSON seek to " << sub << "\n";
                usleep(1000*500);
                if(ret != nullptr && !ret->is<json::object>()) break;

                auto nt = find_first_of(ft, subpath.end(), sep.begin(), sep.end());
                auto& map = ret->get<json::object>();
                //ret = map[sub];
                auto it = map.find(sub);
                if(nt != subpath.end() && it == map.end() && ctype != Type::None) {
                    map[sub] = json::value(json::object());
                } else if(it == map.end() && ctype != Type::None) {
                    switch(ctype) {
                        case Type::String:
                            map[sub] = json::value(std::string());
                        break;
                        default:
                        case Type::Object:
                            map[sub] = json::value(json::object());
                        break;
                        case Type::Array:
                            map[sub] = json::value(json::array());
                        break;
                        case Type::Bool:
                            map[sub] = json::value(bool(false));
                        break;
                        case Type::Double:
                            map[sub] = json::value(0.0);
                        break;
                        case Type::Int64:
                            map[sub] = json::value(int64_t(0));
                        break;
                    }
                } else if(it == map.end()) return nullptr;

                ret = &map[sub];

                if(ft == subpath.end()) break;
                //ft = bt;
                ft++;
                auto oldft = ft;
                ft = nt;//find_first_of(ft, subpath.end(), sep.begin(), sep.end());
                bt = oldft;
            } while(true);//ft != subpath.end());

            return ret;
        }*/
        inline JSONNode getNode(const std::string& subpath) const {
            JSONNode ret;
            ret.json = this->json;
            ret.value = this->getValue(subpath);
            ret.path = "";
            return ret;
        }

        inline bool empty() const {
            return this->value == nullptr || (this->value->is<json::object>() && this->value->get<json::object>().empty());
        }

        template<typename T>
        inline T get(std::string subpath = "") const;

        template<typename T>
        inline void set(const T& val, std::string subpath = "");
        template<typename VT>inline std::vector<VT> array(std::string subpath = "") const;
        template<typename VT>inline void array(const std::vector<VT>& val, std::string subpath = "");

        struct iterator {
            JSONNode *current = nullptr;
            json::value::object::const_iterator it = json::value::object::const_iterator();
            bool syncd = false;

            inline ~iterator();

            inline bool operator==(const iterator& o) const {
                return this->it == o.it;
            }
            inline bool operator!=(const iterator& o) const {
                return !(*this == o);
            }

            inline const JSONNode& operator*();
            inline const JSONNode* operator->();
            inline void sync();
            inline iterator& operator++() {
                ++it;
                syncd = false;
                return *this;
                //if(*this != this->parent->end()) this->sync();
            }
        };
        inline iterator begin() {
            iterator ret;
            ret.current = new JSONNode();
            ret.current->json = this;
            if(value != nullptr && value->is<json::object>())  {
                ret.it = value->get<json::object>().begin();
                if(ret.it != value->get<json::object>().end()) ret.sync();
            }
            return ret;
        }
        inline iterator end() {
            iterator ret;
            //ret.current->json = this;
            if(value != nullptr && value->is<json::object>()) ret.it = value->get<json::object>().end();
            return ret;
        }
    };

    inline JSONNode::iterator::~iterator() {
        if(current != nullptr) delete current;
    }

    inline const JSONNode& JSONNode::iterator::operator*() {
        if(!this->syncd) this->sync();
        return *current;
    }
    inline const JSONNode* JSONNode::iterator::operator->() {
        if(!this->syncd) this->sync();
        return current;
    }
    inline void JSONNode::iterator::sync() {
        current->value = const_cast<json::value*>(&it->second);
        current->path = it->first;
        syncd = true;
    }

    template<> inline double JSONNode::get(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        //std::cout << "get got val=" << val << ", valid=" << val->is<double>() << "\n";
        if(val == nullptr || !val->is<double>()) return 0;
        else return val->get<double>();
    }
    template<> inline bool JSONNode::get(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        if(val == nullptr || !val->is<bool>()) return 0;
        else return val->get<bool>();
    }
    template<> inline std::string JSONNode::get(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        if(val == nullptr || !val->is<std::string>()) return "";
        else return val->get<std::string>();
    }
    template<typename VT>
    inline std::vector<VT> JSONNode::array(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        if(val == nullptr || !val->is<json::array>()) return std::vector<VT>();
        else {
            json::array tmp = value->get<json::array>();
            std::vector<VT> ret;
            for(auto it = tmp.begin(); it != tmp.end(); ++it) {
                if(!it->is<VT>()) continue;
                ret.push_back(it->get<VT>());
            }
            return ret;
        }
        /*if(val == nullptr || !val->is<json::object>()) return std::vector<VT>();
        else {
            auto tmp = this->getNode(subpath);//value->get<json::object>();
            unsigned int count = tmp.get<double>("count");
            std::vector<VT> ret;
            for(unsigned int i = 0; i < count; i++) {
                const std::string fname = "i"+std::to_string(i);
                auto it = tmp.getValue(fname);
                if(it == nullptr || !it->is<VT>()) continue;
                ret.push_back(it->get<VT>());
            }
        }*/
            //return ret;
    }
    template<> inline int64_t JSONNode::get(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        //std::cout << "get got val=" << val << ", valid=" << val->is<int64_t>() << "\n";
        if(val == nullptr || !val->is<int64_t>()) return 0;
        else return val->get<int64_t>();
    }
    template<> inline int JSONNode::get(std::string subpath) const {
        json::value *val = value;
        if(!subpath.empty()) val = this->getValue(subpath);
        //std::cout << "get got val=" << val << ", valid=" << val->is<int64_t>() << "\n";
        if(val == nullptr || !val->is<int64_t>()) return 0;
        else return int(val->get<int64_t>());
    }

    template<> inline void JSONNode::set(const double& val, std::string subpath) {
        json::value *v = value;
        if(!subpath.empty()) v = this->getValue(subpath, Type::Double);
        return v->set<double>(val);
    }
    template<> inline void JSONNode::set(const bool& val, std::string subpath) {
        json::value *v = value;
        if(!subpath.empty()) v = this->getValue(subpath, Type::Bool);
        return v->set<bool>(val);
    }
    template<> inline void JSONNode::set(const int64_t& val, std::string subpath) {
        json::value *v = value;
        if(!subpath.empty()) v = this->getValue(subpath, Type::Int64);
        return v->set<int64_t>(val);
    }
    template<> inline void JSONNode::set(const int& val, std::string subpath) {
        json::value *v = value;
        if(!subpath.empty()) v = this->getValue(subpath, Type::Int64);
        return v->set<int64_t>(val);
    }
    template<> inline void JSONNode::set(const std::string& val, std::string subpath) {
        json::value *v = value;
        if(!subpath.empty()) v = this->getValue(subpath, Type::String);
        return v->set<std::string>(val);
    }
    template<typename VT> inline void JSONNode::array(const std::vector<VT>& val, std::string subpath) {
        json::value *v = value;
        //JSONNode tmp = this->create(subpath);
        /*this->create(subpath);
        this->set(double(val.size()), subpath+".count");
        for(unsigned int i = 0; i < val.size(); i++) {
            const std::string fname = subpath+".i"+std::to_string(i);
            this->set<VT>(val[i], fname);
        }*/
        if(!subpath.empty()) v = this->getValue(subpath, Type::Array);
        if(val.empty()) return;

        json::array arr;
        for(auto it = val.begin(); it != val.end(); ++it) {
            arr.push_back(json::value(*it));
        }

        return v->set(arr);
    }

    class JSON : public JSONNode {
    public:
        using JSONNode::JSONNode;
        using JSONNode::get;
        using JSONNode::set;

        bool load(std::fstream& fd);
        bool save(std::fstream& fd);
        bool load(const std::string& fp);
        bool save(const std::string& fp);
    protected:

    };
    
    struct JSONDelegate {
        bool populate(JSON *json) = delete;
        bool store(JSON *json) = delete;
    };
}

#endif
