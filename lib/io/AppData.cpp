#include "AppData.h"

#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include "Console.h"
#include <iostream>

namespace arke {
    
    struct AppDataContext {
        Directory base;
        AppData::Permissions perm;
    };
    
    
    struct AppDataFS {
        std::string realPath;//With trailing slash
        std::string mountPath;//With trailing slash
        bool overrides;
        unsigned short id;
        unsigned char padding;
    };
    
    namespace AppData {
        Directory root, base, system;
        AppDataContext contextStack[AD_CTX_MAX];
        unsigned char contextID = 255;
        bool initialized = false;
        
        unsigned short fsCount = 0;
        AppDataFS fss[AD_VFS_MAX];
        
        AppDataFS getFS(std::string path);
        
        std::string GetDirPortion(std::string in) {
            auto off = in.find_last_of('/');
            if(off == std::string::npos) return "";
            else return in.substr(0, off);
        }
        
        bool Initialize(std::string rootPath) {
            if(contextID != 255)  return false;
            
            AppDataFS *fs = &(fss[0] = AppDataFS());
            fs->realPath = rootPath;
            fs->mountPath = "";
            fs->overrides = false;
            fs->id = 0;
            fsCount++;
            
            contextID = 0;
            AppDataContext *ctx = &contextStack[0];
            ctx->perm = Permissions::Full;
            ctx->base = rootPath;
            
            root.base = Rel;
            root = "";
            if(!root) {
                std::stringstream ss;
                ss << "AppData initialization halted by invalid root, \"" << rootPath << "\" can't be accessed!";
                Console::Fatal(ss.str());
                return false;
            }
            //base = root;
            
            system = "/";
            system.base = Sys;
            initialized = true;
            //std::cout << "SYS: "<< system <<", BP: " <<  base << "\n";
            return true;
        };
        
        bool Register(const std::string& realPath, const std::string& mountPath, bool over) {
            if(fsCount == AD_VFS_MAX) {
                std::stringstream ss; ss << "Unable to register additional filesystem \"" << realPath << "\" -> \"" << mountPath << "\", max VFS count reached!";
                Console::Error(ss.str());
                return false;
            }
            
            AppDataFS *fs = &fss[fsCount];
            fs->realPath = realPath;
            fs->mountPath = mountPath;
            fs->overrides = over && HasPermissions(Permissions::Override);
            fs->id = fsCount;
            
            Directory testDir = fs->realPath;
            if(!testDir) {
                std::stringstream ss; ss << "Unable to register additional filesystem \"" << realPath << "\" -> \"" << mountPath << "\", real directory cannot be reached!";
                Console::Error(ss.str());
                return false;
            }
            
            fsCount++;
            return true;
        }
        
        bool Unregister(const std::string& realPath) {
            unsigned short rmID = 0;
            for(unsigned short i = 1; i < fsCount; i++) {
                if(fss[i].realPath == realPath) {
                    rmID = i; break;
                }
            }
            if(rmID == 0) return false;
            
            if(rmID != fsCount-1) {
                fss[rmID] = fss[fsCount-1];
            }
            fsCount--;
            
            return true;
        }
        
        std::string Resolve(const std::string &path, Base b) {
            if(b == Sys) {
                if(!initialized || HasPermissions(Permissions::AccessSys)) return path;
                else {
                    std::stringstream ss; ss << "System path (starting with slash, "<<path<<") requested without sufficient permissions!";
                    Console::Warning(ss.str());
                    return "";
                }
            } else if(path.length() > 0 && path[0] == '/') {
                std::stringstream ss; ss << "System path (starting with slash, "<<path<<") requested, but not tagged as such!";
                Console::Warning(ss.str());
                return "";
            }
            
            unsigned short maxMatch = 0; unsigned short fsID = 0;
            for(int i = fsCount-1; i > 0; i++) {
                AppDataFS *const fs = &fss[i];
                
                unsigned short dirMatch = 0, strPos = 0, strMax = std::min(path.length(), fs->mountPath.length());
                //Find depth of matching directories
                while(strPos < strMax) {
                    if(path[strPos] != fs->mountPath[strPos]) break;
                    
                    if(path[strPos] == '/' || path[strPos] == '\\') dirMatch++;//we know they already match, so increment
                    strPos++;
                }
                
                if(dirMatch > maxMatch) {//If we match more (Greedy), use the match
                    maxMatch = dirMatch;
                    fsID = i;
                }
            }
            
            return fss[fsID].realPath + path;
        }
        
        Directory GetSys(const std::string& path) {
            return Directory(path, Sys);
        }
        
        void PushContext(const std::string &path, Permissions perm) {
            if(contextID < AD_CTX_MAX) {
                if(!HasPermissions(perm) && !HasPermissions(Permissions::Context)) {
                    Console::Warning("Appdata context override attempted with insufficient permissions!");
                    return;
                }
                contextID++;
                AppDataContext *const ctx = &contextStack[contextID];
                ctx->base.base = Rel;
                ctx->base = path;
                ctx->perm = perm;
            } else {
                Console::Error("AppData context stack overrun- expect a bad day!");
                return;
            }
        }
        void PopContext() {
            if(contextID > 0) contextID--;
            
            const AppDataContext *const ctx = &contextStack[contextID];
            base = ctx->base;
        }
        void ClearContext() {
            //TODO: authorization checks?
            contextID = 0;
            const AppDataContext *const ctx = &contextStack[contextID];
            base = ctx->base;
        }
        
        bool HasPermissions(Permissions p) {
            if(!initialized) return true;
            const AppDataContext *const ctx = &contextStack[contextID];
            return ((unsigned char)p & (unsigned char)ctx->perm) == (unsigned char)p;
        }
        
        bool HasPermissions(const std::string& path, Permissions p) {
            if(!initialized) return true;
            const AppDataContext *const ctx = &contextStack[contextID];
            return ((unsigned char)p & (unsigned char)ctx->perm) == (unsigned char)p;
        }
    };
    
    //Directory class
    inline std::string Directory::getPath(const std::string& subPath) {
        if(subPath == "") return  AppData::Resolve(rawPath, base);
        else if(rawPath == "") return  AppData::Resolve(subPath, base);
        else {
            //std::cout << subPath << ":" << rawPath <<"\n";
            if(subPath.compare(0, std::string::npos, rawPath) == 0) return AppData::Resolve(subPath, base);
            else if(rawPath.size() > 0 && *(rawPath.rbegin()) == '/') return AppData::Resolve(rawPath + subPath, base);
            else return AppData::Resolve(rawPath + '/' + subPath, base);
        }
    };
    
    bool Directory::exists(const std::string& subPath) {
        std::string path = getPath(subPath);
        //std::cout << " exists? : " << path << "\n";
        return boost::filesystem::is_directory(path) || boost::filesystem::is_regular_file(path);
    }
    
    bool Directory::isDir(const std::string& subPath) {
        std::string path = getPath(subPath);
        return boost::filesystem::is_directory(path);
    }
    bool Directory::isFile(const std::string& subPath) {
        std::string path = getPath(subPath);
        return boost::filesystem::is_regular_file(path);
    }
    
    std::string Directory::resolve(const std::string& subPath, bool checkDirs) {
        if(checkDirs) {
            std::string ret = getPath(subPath);
            //Recursivly create directory, if needed
            std::string dirPart = AppData::GetDirPortion(ret);
            if(!boost::filesystem::is_directory(dirPart)) {
                if(!boost::filesystem::create_directories(dirPart)) {
                    std::stringstream s; s << "Unable to open \""<< ret << "\" for writing by mkdir'ing \"" << dirPart << "\"";
                    Console::Warning(s.str());
                    //return false;
                }
            }
            return ret;
        } else return getPath(subPath);
    }
    
    //Read/Write
    bool Directory::mkdir(const std::string& subPath) {
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Write) || path == "") {
            std::stringstream s; s << "Insufficient permissions to create directory [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return false;
        }
        
        return boost::filesystem::create_directories(path);
    }
    
    bool Directory::cd(const std::string &subPath) {
        std::string path = getPath(subPath);
        //std::cout << "cding to " << path << "\n";
        if(!boost::filesystem::is_directory(path)) return false;
        unsigned int strEffLen = subPath.length();
        if(subPath[subPath.length()-1] == '/') strEffLen--;
        if(rawPath != "") rawPath += '/' + subPath.substr(0, strEffLen);
        else rawPath = subPath.substr(0, strEffLen);
        return true;
    }
    
    unsigned int Directory::readFileBuffered(const std::string& subPath, char *const buffer, unsigned int& length, bool nullTail) {
        if(!exists(subPath)) {
            std::stringstream s; s << "Unable to load resource at data path [" << rawPath << "/]" << subPath << "";
            //#ifdef DEBUG
            s << "(" << resolve(subPath) << ")";
            //#endif
            Console::Error(s.str());
            return 0;
        }
        
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Read) || path == "") {
            std::stringstream s; s << "Insufficient permissions to read [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return 0;
        }
        unsigned int fileLength = boost::filesystem::file_size(path);
        if(nullTail) {
            length = std::min(fileLength + 1, length);
        } else length = std::min(fileLength, length);
        
        boost::filesystem::ifstream file(path);
        file.read(buffer, length);
        file.close();
        if(nullTail) {
            buffer[length-1] = 0x00;
        }
        return length;
    }
    
    unsigned char* Directory::readFile(const std::string& subPath, unsigned int& length, bool nullTail) {
        if(!exists(subPath)) {
            std::stringstream s; s << "Unable to load resource at data path [" << rawPath << "/]" << subPath << " ";
            #ifdef DEBUG
            s << "(" << resolve(subPath) << ")";
            #endif
            Console::Error(s.str());
            return NULL;
        }
        
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Read) || path == "") {
            std::stringstream s; s << "Insufficient permissions to read [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return NULL;
        }
        unsigned int fileLength = boost::filesystem::file_size(path);
        if(nullTail) {
            length = fileLength + 1;
        } else length = fileLength;
        
        char* buffer;
        buffer = new char[length];
        memset(buffer, '\0', length);
        boost::filesystem::ifstream file(path);
        file.read(buffer, length);
        file.close();
        if(nullTail) {
            buffer[length-1] = 0x00;
        }
        
        return (unsigned char*)buffer;
    }
    
    bool Directory::writeFile(const std::string& subPath, const char* buffer, const unsigned int length) {
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Write) || path == "") {
            std::stringstream s; s << "Insufficient permissions to write [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return false;
        }
        
        //Recursivly create directory, if needed
        std::string dirPart = AppData::GetDirPortion(path);
        if(!boost::filesystem::is_directory(dirPart)) {
            if(!boost::filesystem::create_directories(dirPart)) {
                std::stringstream s; s << "Unable to open [" << rawPath << "/]" << subPath << " for writing by mkdir'ing \"" << dirPart << "\"";
                Console::Warning(s.str());
                return false;
            }
        }
        
        boost::filesystem::ofstream file(path);
        if(!file.good()) {
            std::stringstream s; s << "Unable to open [" << rawPath << "/]" << subPath << " for writing";
            Console::Warning(s.str());
            return false;
        }
        
        file.write(buffer, length);
        file.close();
        
        return true;
    }
    
    bool Directory::deleteFile(const std::string& subPath) {
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Write) || path == "") {
            std::stringstream s; s << "Insufficient permissions to delete [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return false;
        }
        
        return boost::filesystem::remove(path);
    }
    
    bool Directory::touch(const std::string& subPath) {
        char tContents[2] = {' ', 0x0};
        return this->writeFile(subPath, tContents, sizeof(tContents));
    }
    
    //List functions
    bool Directory::list(std::string *& contents, unsigned int& count, const std::string& subPath) {
        if(!exists(subPath)) return false;
        std::string path = getPath(subPath);
        if(!AppData::HasPermissions(path, AppData::Permissions::Read) || path == "") {
            std::stringstream s; s << "Insufficient permissions to list [" << rawPath << "/]" << subPath << "";
            Console::Warning(s.str());
            return false;
        }
        count = 0;
        
        boost::filesystem::directory_iterator end;
        for (boost::filesystem::directory_iterator pos(path); pos != end; ++pos) {
            if (boost::filesystem::is_regular_file( *pos ) || boost::filesystem::is_directory( *pos )) {
                count++;
            }
        }
        
        contents = new std::string[count];
        
        unsigned int offset = 0;
        for (boost::filesystem::directory_iterator pos(path); pos != end; ++pos) {
            if (boost::filesystem::is_regular_file( *pos ) || boost::filesystem::is_directory( *pos )) {
                contents[offset] = subPath + std::string(pos->path().filename().string());
                offset++;
            }
        }
        
        return true;
    }
    
    bool Directory::listCallback(bool (*callback)(std::string name, std::string fullPath), bool recurse, std::string path) {
        return this->listCallback(std::function<bool(std::string, std::string)>(callback), recurse, path);
    }
    
    bool Directory::listCallback(std::function<bool(std::string, std::string)> f, bool recursive, std::string path) {
        //std::cout << "listCallback on " << path << "\n";
        if(!exists(path)) return false;
        
        const std::string subPath = path + ((path.size() > 0 && path[path.size()-1] != '/')? "/":"");
        path = getPath(path);
        if(!AppData::HasPermissions(path, AppData::Permissions::Read) || path == "") {
            std::stringstream s; s << "Insufficient permissions to list " << path << "";
            Console::Warning(s.str());
            return false;
        }
        //std::cout << "listing...\n";
        boost::filesystem::path dir(path);
        boost::filesystem::directory_iterator end;
        try {
            for (boost::filesystem::directory_iterator pos(path); pos != end; ++pos) {
                std::string itemName = std::string(pos->path().filename().string());
                std::string itemPath = path; itemPath.append("/"); itemPath.append(itemName);
                
                if(itemPath == path || itemPath == dir.root_directory()) continue;
                
                if(boost::filesystem::is_directory( *pos ) && recursive) {
                    listCallback(f, recursive, subPath+itemName);
                } else if(boost::filesystem::is_regular_file( *pos )) {
                    f(subPath+itemName, itemPath);
                }
            }
        } catch(const std::exception &exc) {
            std::stringstream s; s << "Exception while iterating over directory " << path << ": " << exc.what();
            Console::Error(s.str());
            return false;
        }
        
        return true;
    }
    
    Directory Directory::getParent() {
        std::string path = getPath("");
        int maxPath = std::max((int)path.length()-1, 0);
        while(maxPath > 0) {
            if(path[maxPath] == '/') break;//Use character before slash
            maxPath--;
        }
        return Directory(path.substr(0, maxPath));
    }
    
    Directory Directory::getDir(std::string path) {
        path = getPath(path);
        return Directory(path);
    }
    
};
