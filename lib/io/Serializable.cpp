#include "Serializable.h"

namespace arke {
    Serializable::Serializable() {

    }
    Serializable::~Serializable() {

    }

    std::string Serializable::serialize() const {
        return "";
    }

    void Serializable::deserialize(const std::string& str) {
        _unused(str);
    }

    void Serializable::serializeTo(JSONNode *node, const std::string& pth) const {
        _unused(node, pth);
    }
    void Serializable::deserializeFrom(JSONNode *node, const std::string& pth) {
        _unused(node, pth);
    }
}
