#include "JSON.h"
#include <fstream>

namespace arke {
    bool JSON::load(std::fstream& fd) {
        std::string data;
        fd.seekg(0, std::ios::end);
        data.reserve(fd.tellg());
        fd.seekg(0, std::ios::beg);
        data.assign(std::istreambuf_iterator<char>(fd), std::istreambuf_iterator<char>());
        return this->deserialize(data);
    }
    bool JSON::save(std::fstream& fd) {
        fd << this->serialize();
        return fd.good();
    }
    bool JSON::load(const std::string& fp) {
        std::fstream fs(fp, std::fstream::in | std::fstream::binary);
        return this->load(fs);
    }
    bool JSON::save(const std::string& fp) {
        std::fstream fs(fp, std::fstream::out | std::fstream::trunc | std::fstream::binary);
        return this->save(fs);
    }
}
