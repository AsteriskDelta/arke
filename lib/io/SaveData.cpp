#include "SaveData.h"
#include "Console.h"

//Bit of a fix for other compilers
#ifdef _WIN32
  #define WINDOWS
#endif

#ifdef WINDOWS
  #include <direct.h>
  #define GetCurrentDir _getcwd
  #include <shlwapi.h>
  #pragma comment(lib,"shlwapi.lib")
  #include "shlobj.h"
  #define APPDATA_SUBDIR "\\BDHack\\"
  #define DIR_DELIM "\\"
#else
  #include <unistd.h>
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <pwd.h>
  #include <cstdlib>
  #define GetCurrentDir getcwd
  #define APPDATA_SUBDIR "/.BDHack/"
  #define DIR_DELIM "/"
#endif
//Finally, something they agree on.
#define EXT_DELIM "."
#define SAVEFILE_NAME "prefs.dat"

namespace arke {

    std::string SaveData::saveDir;
    unsigned char* SaveData::buffer = NULL;

    static bool printedSavePath;

    bool SaveData::Load(std::string name, unsigned short newKeySuggestion) {
    saveDir = ""; GetDir();
    fileName = name;
    keySuggestion = newKeySuggestion;
    map.reserve(keySuggestion);
    
    Console::Print("Loading Save data...");
    
    GetDir();
    std::fstream in;
    std::string path = saveDir; path.append(fileName);
    in.open(path.c_str(), std::fstream::in | std::fstream::binary);
    unsigned short keyCount = 0;
    if(in) {
        if(buffer == NULL) buffer = new unsigned char[2048 + 4];
        
        unsigned char keyLength; unsigned short dataLength;
        unsigned char* newData = NULL; char* newKey = new char[256];
        //We want to keep each data array allocated, as they are referenced in the map
        while(in.good()) {
        in.read((char*)&keyLength, 1);
        in.read(newKey, keyLength);
        in.read((char*)&dataLength, 2);

        newData = new unsigned char[dataLength];
        in.read((char*)newData, dataLength);
        
        toInsert.length = dataLength;
        toInsert.data = newData;
        
        found = map.insert(map.begin(), {std::string(newKey), toInsert});
        
        in.peek();
        keyCount++;
        }
        
        std::stringstream s; s << "Save data loaded, " << keyCount << " keys registered.";
        Console::Print(s.str());
    } else {
        Console::Warning("Unable to read save data! If you appear to be missing data, check permissions and paths!");
        return false;
    }
    
    return true;
    }

    bool SaveData::Save() {
    GetDir();
    std::fstream out;
    std::string path = saveDir; path.append(fileName);
    
    out.open(path.c_str(), std::fstream::out | std::fstream::trunc | std::fstream::binary);
    if(!out) {
        Console::Error("Unable to write save data! Check permissions and path!");
        return false;
    }
    
    
    if(buffer == NULL) buffer = new unsigned char[2048 + 4];
    
    unsigned char keyLength;
    for(found = map.begin(); found != map.end(); ++found ) {
        if(found->second.length > 0) {
        keyLength = (unsigned char)((found->first).length()) + 1;
        out.write((char*)&keyLength, 1);
        out.write((char*)(found->first.c_str()), keyLength);
        out.write((char*)&(found->second.length), 2);
        out.write((char*)(found->second.data), found->second.length);
        }
    }
    out.close();
    Console::Print("Save data successfully written.");
    return true;
    }

    std::string SaveData::GetDir() {
    if(saveDir != "") return saveDir;
    
    #ifdef WINDOWS
        char homeDirectory[FILENAME_MAX];//FILENAME_MAX is defined in stdio
        SHGetFolderPathA(NULL,CSIDL_COMMON_APPDATA,NULL,0,homeDirectory);
    #else
        char* homeDirectory;
        homeDirectory = getenv("HOME");
        if(homeDirectory == NULL) {//If it's not in getenv, do it the hard way
        //homeDirectory = getpwuid(getuid())->pw_dir;
        }
    #endif
    saveDir = homeDirectory;
    saveDir.append(APPDATA_SUBDIR);

    #ifdef WINDOWS
        _mkdir(saveDir.c_str());
    #else
        mkdir(saveDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    #endif
    if(!printedSavePath) {
        std::stringstream s; s << "Save path recognized as \"" << saveDir << "\".";
        Console::Print(s.str());
        printedSavePath = true;
    }
    
    return saveDir;
    }

    std::string SaveData::GetPath(const std::string& name) {
    std::string path = GetDir();
    path.append(name);
    
    return path;
    }

    bool SaveData::Exists(const std::string& id) {
    found = map.find(id);
    
    if(found == map.end()) return false;
    
    return true;
    }

    bool SaveData::GetBool(const std::string& id, bool def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length <= 0) return def;
    
    return (bool) (*(found->second.data));
    }

    char SaveData::GetChar(const std::string& id, char def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length <= 0) return def;
    
    return *(found->second.data);
    }

    unsigned char SaveData::GetByte(const std::string& id, unsigned char def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length <= 0) return def;
    
    return *(found->second.data);
    }

    int SaveData::GetInt(const std::string& id, int def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length < sizeof(int)) return def;
    
    return *((int*)(found->second.data));
    }

    float SaveData::GetFloat(const std::string& id, float def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length < sizeof(float)) return def;
    
    return *((float*)(found->second.data));
    }

    unsigned short SaveData::GetUni(const std::string& id, unsigned short def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length < 2) return def;
    
    return *((unsigned short*)(found->second.data));
    }

    std::string SaveData::GetString(const std::string& id, std::string def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length <= 0) return def;
    
    return std::string( (char*)(found->second.data));
    }

    unsigned char* SaveData::GetBytes(const std::string& id, unsigned short& length) {
    found = map.find(id);
    
    if(found == map.end() || found->second.length <= 0) {
        length = 0;
        return NULL;
    }
    
    length = found->second.length;
    
    return (found->second.data);
    }

    Color SaveData::GetColor(const std::string& id, Color def) {
    found = map.find(id);
    
    if(found == map.end()) return def;
    else if(found->second.length < sizeof(Color)) return def;
    
    return *((Color*)(found->second.data));
    }

    bool SaveData::SetBool(const std::string& id, bool value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 1;
        toInsert.data = new unsigned char[1];
        found = map.insert(map.begin(), {id, toInsert});
        
        found->second.data[0] = value;
    } else if(found->second.length == 1) {
        found->second.data[0] = value;
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 1;
        (found->second).data = new unsigned char[1];
        (found->second).data[0] = value;
    }
    return true;
    }

    bool SaveData::SetChar(const std::string& id, char value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 1;
        toInsert.data = new unsigned char[1];
        found = map.insert(map.begin(), {id, toInsert});
        
        found->second.data[0] = value;
    } else if(found->second.length == 1) {
        found->second.data[0] = value;
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 1;
        (found->second).data = new unsigned char[1];
        (found->second).data[0] = value;
    }
    return true;
    }

    bool SaveData::SetByte(const std::string& id, unsigned char value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 1;
        toInsert.data = new unsigned char[1];
        found = map.insert(map.begin(), {id, toInsert});
        
        found->second.data[0] = value;
    } else if(found->second.length == 1) {
        found->second.data[0] = value;
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 1;
        (found->second).data = new unsigned char[1];
        (found->second).data[0] = value;
    }
    return true;
    }

    bool SaveData::SetInt(const std::string& id, int value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 4;
        toInsert.data = new unsigned char[4];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, &value, 4);
    } else if(found->second.length == 4) {
        memcpy(found->second.data, &value, 4);
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 4;
        (found->second).data = new unsigned char[4];
        memcpy(found->second.data, &value, 4);
    }
    return true;
    }

    bool SaveData::SetFloat(const std::string& id, float value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = sizeof(float);
        toInsert.data = new unsigned char[sizeof(float)];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, &value, sizeof(float));
    } else if(found->second.length == sizeof(float)) {
        memcpy(found->second.data, &value, sizeof(float));
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = sizeof(float);
        (found->second).data = new unsigned char[sizeof(float)];
        memcpy(found->second.data, &value, sizeof(float));
    }
    return true;
    }

    bool SaveData::SetUni(const std::string& id, unsigned short value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 2;
        toInsert.data = new unsigned char[2];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, &value, 2);
    } else if(found->second.length == 2) {
        memcpy(found->second.data, &value, 2);
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 2;
        (found->second).data = new unsigned char[2];
        memcpy(found->second.data, &value, 2);
    }
    return true;
    }

    bool SaveData::SetString(const std::string& id, const std::string& value) {
    //Copy with null terminator
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = value.length() + 1;
        toInsert.data = new unsigned char[toInsert.length];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, value.c_str(), found->second.length);
    } else if(found->second.length == value.length() + 1) {
        memcpy(found->second.data, value.c_str(), value.length() + 1);
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = value.length() + 1;
        (found->second).data = new unsigned char[(found->second).length];
        memcpy(found->second.data, value.c_str(), (found->second).length);
    }
    return true;
    }

    bool SaveData::SetBytes(const std::string& id, unsigned char* value, unsigned short length) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = length;
        toInsert.data = new unsigned char[length];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, value, found->second.length);
    } else if(found->second.length == length) {
        memcpy(found->second.data, value, length);
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = length;
        (found->second).data = new unsigned char[length];
        memcpy(found->second.data, value, length);
    }
    return true;
    }

    bool SaveData::SetColor(const std::string& id, Color value) {
    found = map.find(id);
    
    if(found == map.end()) {
        toInsert.length = 4;
        toInsert.data = new unsigned char[4];
        found = map.insert(map.begin(), {id, toInsert});
        
        memcpy(found->second.data, &value, 4);
    } else if(found->second.length == 4) {
        memcpy(found->second.data, &value, 4);
    } else {
        if(found->second.length > 0&&found->second.data != NULL) delete[] (found->second).data;
        
        (found->second).length = 4;
        (found->second).data = new unsigned char[4];
        memcpy(found->second.data, &value, 4);
    }
    return true;
    }

};
