#include "Console.h"
#include "SaveData.h"
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstring>
#include <queue>
#include <iomanip>
#include <sstream>
#include <cmath>

#include "Application.h"
#include "Timer.h"

namespace arke {

    static bool consoleExists = false;
    static std::fstream logStream;
    static std::fstream errStream;
    static char* lineBuffer;
    static char* outBuffer;

    static std::queue<ConsoleMessage> consoleBuffer;

    unsigned short Console::lineWidth;
    unsigned short Console::lineHistory;
    unsigned short Console::lineOffset;

    bool Console::ioOutput;

    static const unsigned short maxBufferSize = (unsigned short)65536;

    LoggingState Console::loggingState;
    ConsoleLine* Console::lines;
    ConsoleColor Console::colors [256];
    unsigned int Console::startTime;

    std::string Console::progressStr = "";
    unsigned int Console::progressMax = 0;

    CONSOLEHOOK_RET (*Console::messageHook) CONSOLEHOOK_DEF = NULL;

    static inline bool strReplace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
    }

    bool Console::Init(unsigned short lineCount, unsigned short lineSize, bool nioOutput, LoggingState state, bool utf8) {
    ((void)utf8);
    //A console instance already exists, delete and reallocate all lines
    if(consoleExists&&lineHistory == lineCount) {//We just need to reallocate line widths
        for(unsigned short i = 0; i < lineHistory; i++) {
        delete[] lines[i].text;
        lines[i].text = new char[lineWidth];
        }
        delete[] lineBuffer;
    } else if(consoleExists) {//Delete and reallocate all lines and line widths
        for(unsigned short i = 0; i < lineHistory; i++) {
        delete[] lines[i].text;
        }
        
        delete[] lines;
        lines = new ConsoleLine[lineCount];
        
        for(unsigned short i = 0; i < lineHistory; i++) {
        lines[i].text = new char[lineWidth];
        }
        delete[] lineBuffer;
    } else {//Make everything, for the first time
        lines = new ConsoleLine[lineCount];
        
        if(lineWidth > 0) {
        for(unsigned short i = 0; i < lineHistory; i++) {
        lines[i].text = new char[lineWidth];
        }
        } else {
        for(unsigned short i = 0; i < lineHistory; i++) {
        lines[i].text = NULL;
        }
        }
    }
    
    lineBuffer = new char[lineWidth];
    
    lineWidth = lineSize;
    lineHistory = lineCount;
    lineOffset = 0;
    
    LoadColors(false);
    SetIOOutput(nioOutput);
    
    SetLogging(state);
    
    consoleExists = true;
    outBuffer = new char[maxBufferSize];
    
    startTime = std::time(0);
    
    return true;
    }

    void Console::LoadColors(bool force) {
    if(consoleExists&&!force) return;
    
    
    colors[0  ] = ConsoleColor{255, 255, 255, 0};//White
    colors[1  ] = ConsoleColor{255, 0  , 0  , 0};
    colors[2  ] = ConsoleColor{0  , 255, 0  , 0};
    colors[3  ] = ConsoleColor{0  , 0  , 255, 0};
    
    colors[4  ] = ConsoleColor{255, 255, 0  , 0};//Yellow
    colors[5  ] = ConsoleColor{0  , 255, 255, 0};//Cyan
    colors[6  ] = ConsoleColor{255, 0  , 255, 0};//Purple
    
    colors[7  ] = ConsoleColor{255, 127, 0  , 0};//Orange
    colors[8  ] = ConsoleColor{0  , 127, 255, 0};
    colors[9  ] = ConsoleColor{0  , 127, 0  , 0};
    
    colors[10 ] = ConsoleColor{127, 255, 0  , 0};
    colors[11 ] = ConsoleColor{127, 0  , 255, 0};
    colors[12 ] = ConsoleColor{127, 0  , 0  , 0};
    
    colors[13 ] = ConsoleColor{127, 255, 0  , 0};
    colors[14 ] = ConsoleColor{127, 0  , 255, 0};
    colors[15 ] = ConsoleColor{127, 0  , 0  , 0};
    
    colors[254] = ConsoleColor{255, 255, 255, 0};//White
    colors[255] = ConsoleColor{0  , 0  , 0  , 0};//Black
    }


    void Console::SetIOOutput(bool state) {
    ioOutput = state;
    }

    void Console::SetOutputWidth(unsigned short width) {
    for(unsigned short i = 0; i < lineHistory; i++) {
        delete[] lines[i].text;
        lines[i].text = new char[lineWidth];
    }
    lineWidth = width;
    
    delete[] lineBuffer;
    lineBuffer = new char[lineWidth];
    
    while(!consoleBuffer.empty()) {
        ConsoleMessage msg = consoleBuffer.front();
        AddMessage(msg.text, msg.color, msg.type, ">", msg.time);
        consoleBuffer.pop();
    }
    }

    void Console::SetColor(uint8_t index, const ConsoleColor& color) {
    colors[index] = color;
    }

    bool Console::SetLogging(LoggingState state) {
    loggingState = state;
    if(state == Logging_None) return true;
    /*std::string logPath = SaveData::GetPath("log.txt");
    std::string errPath = SaveData::GetPath("error.txt");
    
    logStream.open(logPath, std::fstream::out | std::fstream::trunc);
    errStream.open(errPath, std::fstream::out | std::fstream::trunc);
    */
    return logStream.good() && errStream.good();
    }

    bool Console::Flush() {
    if(loggingState == Logging_None) return true;
    
    if(logStream.good()) logStream.flush();
    if(errStream.good()) errStream.flush();
    
    if(!logStream.good()||!errStream.good()) return false;
    return true;
    }

    bool Console::Close() {
    if(loggingState == Logging_None) return true;
    
    if(logStream.good()) {
        logStream.flush();
        logStream.close();
    }
    
    if(errStream.good()) {
        errStream.flush();
        errStream.close();
    }
    
    if(!logStream.good()||!errStream.good()) return false;
    return true;
    }

    bool Console::AddMessage(const std::string& otext, uint8_t color, uint8_t type, const std::string& prefix, unsigned int time) {
        std::string text(otext);
        text.insert(0, prefix);
        Application::UpdateDelta(true);
        
        if(progressMax == 0xFFFFFFFF) {
            progressMax = 0;
            //if(ioOutput) std::cout << std::endl;
        }
        
        bool newline = text.size() < 1 || text.at(text.length()-1) != '\r';
        
        unsigned short line = lineOffset;
        unsigned short length = text.length();
        if(time == 0) time = std::time(0);
        
        if(lineWidth > 0) {
            for(unsigned short chars = length; chars > 0; chars -= lineWidth) {
            line++;
            if(line >= lineHistory) line = 0;
            
            lines[line].length = chars;
            lines[line].color = color;
            lines[line].type = type;
            lines[line].time = time;
            
            memcpy(lines[line].text, text.substr(length - chars, chars).c_str(), chars);
            }
        } else {
            ConsoleMessage msg;
            msg.time = time;
            msg.color = color;
            msg.type = type;
            msg.text = text;
            
            consoleBuffer.push(msg);
        }
        
        unsigned short timePrec = std::max(5, ((int)ceil(log10(_globalTimef))));
        std::stringstream ss; ss << std::setw(timePrec) << std::setfill('0') << /*(time - startTime)*/((unsigned long long)round(_globalTimef)) << 
                "." << std::setw(3) << (int)floor((_globalTimef - floor(_globalTimef)) * 1000.0 );
        if(ioOutput) {
            std::cout << "[" << ss.str() << "] : " << text;
            if(newline) std::cout << std::endl;
            else std::cout.flush();
        }
        
        if(loggingState != Logging_None) {
            //Write to error log?
            if(type >= Console_Error || type == Console_ScriptErr) {
            errStream << "[" << time << "] : " << text << std::endl;
            }
            //Write to primary log?
            if(type >= loggingState) {
            logStream << "[" << time << "] : " << text << std::endl;
            }
        }
        
        if(messageHook != NULL) {
            (messageHook)(text);
        }
        
        lineOffset = line + 1;
        if(line >= lineHistory) line = 0;
        
        return true;
    }

    bool Console::Print(std::string text) {
    return AddMessage(text, 0, Console_Message);
    }

    bool Console::Print(std::string text, std::string e) {
    strReplace(text, "%s", e);
    return AddMessage(text, 0, Console_Message);
    }

    bool Console::Print(std::string text, uint8_t color) {
    return AddMessage(text, color, Console_Message);
    }

    bool Console::PrintScript(std::string text) {
    return AddMessage(text, 0, Console_Message);
    }

    bool Console::Warning(std::string text) {
    return AddMessage(text, 7, Console_Warning, "[Warning] ");
    }

    bool Console::Error(std::string text) {
    return AddMessage(text, 7, Console_Error, "[Error] ");
    }

    bool Console::ScriptError(std::string text) {
    return AddMessage(text, 7, Console_ScriptErr, "[ScriptErr] ");
    }

    bool Console::Fatal(std::string text) {
    return AddMessage(text, 5, Console_Fatal, "[Fatal] ");
    }

    static Timer *pTimer = NULL;
    static unsigned int pLast = 0, pLastVal = 0;
    static float timePer = 0.f;
    bool Console::Progress(std::string test, unsigned int max) {
    progressStr = test;
    progressStr.append("\r");
    progressMax = max;
    if(pTimer != NULL) delete pTimer;
    pTimer = new Timer();
    pTimer->start();
    pLast = pLastVal = 0; timePer = 0.f;
    return true;
    }

    bool Console::Progress(unsigned int current) {
    if(progressMax == 0) return false;
    
    float actionsDone = current - pLastVal;
    unsigned int totalTime = pTimer->getElapsedMilli()+1;
    float timeSpent = totalTime - pLast;
    //float tRatio = float(timeSpent / totalTime);
    timePer = /*tRatio * timePer + */(actionsDone/timeSpent);// * (1.f - tRatio);
    //float timeLeft = (float(progressMax - current) * timePer) / 1000.f;
    
    std::string text = progressStr;
    std::stringstream ss; 
    ss.setf(std::ios::fixed,std::ios::floatfield);
    ss.precision(1);
    ss << (round((float)current / (float)progressMax * 1000.f ) / 10.f) << "%";
    strReplace(text, "%p", ss.str());
    ss.str(""); ss << current << "/" << progressMax;
    strReplace(text, "%e", ss.str());
    ss.str("");;// ss << timeLeft << "s remaining " << float(timePer);
    strReplace(text, "%t", ss.str());
    if(current == progressMax) {
        text[text.length()-1] =  ' ';
        progressMax = 0xFFFFFFFF;
    }
    
    pLastVal = current;
    pLast = totalTime;
    
    return AddMessage(text, 0, Console_Message);
    }

    bool Console::GetLines(char* &buffer, unsigned short &length, unsigned short offset, unsigned short lineCount) {
    buffer = outBuffer;
    unsigned short bufferPos = 0;
    offset = (offset + lineOffset) % lineCount;
    length = 0;
    
    for(unsigned int i = offset; i < lineCount + offset; i++) {
        if(bufferPos > maxBufferSize) return false;
        unsigned short line = (i + lineOffset) % lineHistory;
        std::cout << lines[line].time << ":" << lines[line].text << "\n";
        
        memcpy(buffer + bufferPos, lines[line].text, lines[line].length);
        bufferPos += lines[line].length;
        memcpy(buffer + bufferPos, &lineDelimiter, 1);
        bufferPos++;
    }
    
    return true;
    }

};
