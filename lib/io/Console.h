#ifndef INC_CONSOLE
#define INC_CONSOLE
#define CONSOLE_INC
#include <string>
#include <sstream>

#define CONSOLEHOOK_RET void
#define CONSOLEHOOK_DEF (const std::string& message)

namespace arke {

    const char lineDelimiter = 0x0A;

    enum ConsoleMType {
        Console_Message = 0,
        Console_Meta,
        Console_ScriptErr,
        Console_Custom,
        Console_Warning = 127,
        Console_Error,
        Console_Fatal
    };
    enum LoggingState {
        Logging_None = 255,
        Logging_Custom = 254,
        Logging_Fatal = Console_Fatal,
        Logging_Error = Console_Error,
        Logging_Warning = Console_Warning,
        Logging_All = Console_Message
    };

    struct ConsoleLine {
        unsigned int time;
        unsigned short length;
        
        uint8_t type;
        uint8_t color;
        
        char* text;
    };

    struct ConsoleMessage {
        unsigned int time;
        uint8_t type;
        uint8_t color;
        
        std::string text;
    };

    struct ConsoleColor {
        uint8_t r, g, b, a;
    };

    class Console {
    public:
    static bool Init(unsigned short lineCount, unsigned short lineSize, bool nioOutput, LoggingState state, bool utf8 = false);
    static void LoadColors(bool force = true);
    static void SetIOOutput(bool state);
    static bool SetLogging(LoggingState state);
    
    static void SetOutputWidth(unsigned short width);
    static void SetColor(uint8_t index, const ConsoleColor& color);
    
    static bool Print(std::string text);
    static bool Print(std::string text, std::string e);
    static bool Print(std::string text, uint8_t color);
    
    static bool PrintScript(std::string text);
    
    static bool Warning(std::string text);
    static bool Error(std::string text);
    static bool Fatal(std::string text);
    
    #define CONSOLE_MNAME(NM) NM
    #define CONSOLE_STREAM_PF(NM, PFUNC) template<typename ... Args >\
    inline static bool CONSOLE_MNAME(NM) (Args const& ... args ) {\
        std::ostringstream stream;\
        using List= int[];\
        (void)List{0, ( (void)(stream << args), 0 ) ... };\
        return PFUNC(stream.str());\
    }
    CONSOLE_STREAM_PF(Out, Print);
    CONSOLE_STREAM_PF(Warn, Warning);
    CONSOLE_STREAM_PF(Err, Error);
    CONSOLE_STREAM_PF(Crit, Fatal);
    
    static bool Progress(std::string test, unsigned int max);
    static bool Progress(unsigned int current);
    
    static bool ScriptError(std::string text);
    
    static bool GetLines(char* &buffer, unsigned short &length, unsigned short offset, unsigned short lineCount);
    
    template<typename ... Args >
    inline static bool $(const std::string& cat, Args const& ... args ) {
        std::ostringstream stream;
        using List= int[];
        (void)List{0, ( (void)(stream << args), 0 ) ... };
        
        const std::string headerStr = std::string("[")+cat+"] ";
        return AddMessage(stream.str(), 0, Console_Custom, headerStr);
    }
    
    static bool Flush();
    static bool Close();
    
    static ConsoleColor colors[256];
    static ConsoleLine* lines;
    
    static CONSOLEHOOK_RET (*messageHook) CONSOLEHOOK_DEF;
    private:
    static unsigned short lineWidth;
    static unsigned short lineHistory;
    static LoggingState loggingState;
    static bool ioOutput;
    static unsigned short charsetMult;
    
    static unsigned short lineOffset;
    static unsigned int startTime;
    
    static std::string progressStr;
    static unsigned int progressMax;
    
    static bool AddMessage(const std::string& text, uint8_t color, uint8_t type, const std::string& prefix = "", unsigned int time = 0);
    };

};
#endif
