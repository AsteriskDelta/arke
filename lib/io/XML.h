#ifndef INC_XML
#define INC_XML

#include <string>
#include <thread>
#define MAX_XML_LENGTH 65536
#define RAPIDXML_NO_EXCEPTIONS
#include "externals/rapidxml.hpp"
#include <cstring>
#include <sstream>
#include <type_traits>

//#include "Console.h"
#define HLDEBUG
#include "Color.h"
#if defined(DEBUG) || defined(HLDEBUG)
#include "Console.h"
#endif

namespace arke {

    //Path queries support are like so: "child.secChild.attribute"
    class XMLNode {
    public:
        XMLNode();
        XMLNode(rapidxml::xml_node<> *const n);
        ~XMLNode();
        
        inline operator bool() { return (node != NULL); };
        inline int operator ++() {
            if(node == NULL/* || !node->parent()*/) return 0;
            this->node = node->next_sibling(node->name());
            return int(this->node != nullptr);
        }
        
        inline XMLNode operator[](const std::string& p) {
            std::string att;
    #if defined(DEBUG) || defined(HLDEBUG)
            rapidxml::xml_node<>* n = this->getPath(p, att, true);
            /*if(att != "") {
                Console::Warning("xmlNode[path] called with real attribute, but can only return nodes!");
            }*/
            return XMLNode(n);
    #else
            return XMLNode(this->getPath(p, att), true);
    #endif
            
        }
        
        inline XMLNode operator[](const char* cs) {
            return this->operator[](std::string(cs));
        }
        
        XMLNode parent() { return XMLNode(node->parent()); }
        XMLNode child(const std::string& p = "") { 
            if(p == "") return XMLNode(node->first_node());
            else return (*this)[p];
        }
        XMLNode sibling(const std::string& p = "") { 
            if(p == "") return XMLNode(node->next_sibling());
            else return XMLNode(node->next_sibling(p.c_str()));
        }
        
        inline std::string name() const { return std::string(node->name()); };
        
        template<typename T> typename std::enable_if<std::is_floating_point<T>::value, T>::type get(const std::string& p, const T& def);
        template<typename T> typename std::enable_if<std::is_integral<T>::value && !std::is_same<T, bool>::value, T>::type get(const std::string& p, const T& def);
        template<typename T> typename std::enable_if<std::is_same<T, bool>::value, T>::type get(const std::string& p, const T& def);
        
        template<typename T> typename std::enable_if<!(std::is_integral<T>::value || std::is_same<T, bool>::value || std::is_floating_point<T>::value), T>::type get(const std::string& p, const T& def);
        template<typename T> T get(const std::string& p = "");
        
        template<typename T>
        T operator()(const std::string& p = "", const T& def = T()) { return get<T>(p, def); }
        template<typename T>
        T operator()(const std::string& p = "") { return get<T>(p); }
    protected:
        rapidxml::xml_node<>* getPath(const std::string& p, std::string& remainder, bool forceNode = false);
        rapidxml::xml_node<>* node;
        
    };

    class XMLDoc : public XMLNode {
    public:
        XMLDoc();
        XMLDoc(const std::string& file, bool isRel = true);
        ~XMLDoc();
        
        bool load(const std::string& file, bool isRel = true);
        bool save(const std::string& file = NULL, bool isRel = true);
        bool close();
        
        static thread_local std::string lastPath;
    protected:
        void initVars();
        
        std::string path; bool fIsRel;
        rapidxml::xml_document<> doc;
        
        char* buffer;
    };
    
    struct XMLDelegate {
        bool populate(XMLDoc *doc) = delete;
        bool store(XMLDoc *doc) = delete;
    };

    #define SET_attValue (attValue = ((attName == "") ? xn->value() : (xn->first_attribute(attName.c_str()) == NULL? NULL : (char*) xn->first_attribute(attName.c_str())->value()) )) == NULL
    //#define SET_attValue (attValue = (attName == "") ? xn->value() : (char*) xn->first_attribute(attName.c_str()) ) == NULL)

    template<typename T>
    typename std::enable_if<std::is_floating_point<T>::value, T>::type XMLNode::get(const std::string& p, const T& def) {
        std::string attName;
        char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);

        if (xn == NULL || SET_attValue ) return def;

        double tmp;
        try {
            tmp = std::stod(attValue);
        } catch (std::exception const & e) {
            return def;
        }
        return (T) tmp;
    }

    template<typename T>
    typename std::enable_if<std::is_integral<T>::value && !std::is_same<T, bool>::value, T>::type XMLNode::get(const std::string& p, const T& def) {
        std::string attName;
        char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);

        if (xn == NULL || SET_attValue ) return def;

        long long tmp;
        try {
            tmp = std::stoll(attValue);
        } catch (std::exception const & e) {
            return def;
        }
        if (tmp < std::numeric_limits<T>::min()) tmp = std::numeric_limits<T>::min();
        else if (tmp > std::numeric_limits<T>::max()) tmp = std::numeric_limits<T>::max();
        return (T) tmp;
    }

    template<typename T>
    typename std::enable_if<std::is_same<T, bool>::value, T>::type XMLNode::get(const std::string& p, const T& def) {
        std::string attName;
        char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);

        if (xn == NULL || SET_attValue ) return def;


        if (attName == "" && strlen(attValue) <= 1) return true; //Node exists, no attribute requested

        const std::string attVStr = std::string(attValue);
        long long attVInt;
        try {
            attVInt = std::stoll(attValue);
        } catch(...) {
            attVInt = 1;
        }
        return !(attVStr == "false" || attVStr == "no" || attVInt <= 0);
    }

    template<typename T>
    typename std::enable_if<!(std::is_integral<T>::value || std::is_same<T, bool>::value || std::is_floating_point<T>::value), T>::type XMLNode::get(const std::string& p, const T& def) {
        std::string attName; char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);

        if (xn == NULL || SET_attValue ) return def;
        return T(attValue);
    }

    /*template<typename T>
    T XMLNode::get(const std::string& p, const T& def) {
        std::string attName; char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);
        if(attName == "") attValue = xn->value();
        else attValue = (char*)xn->first_attribute(attName.c_str());
        
        if(xn == NULL || attValue == NULL) return def;
        
        if(std::is_integral<T>::value) {
            long long tmp;
            try {
                tmp = std::stoll(attValue);
            } catch(std::exception const & e) {
                return def;
            }
            if(tmp < std::numeric_limits<T>::min()) tmp = std::numeric_limits<T>::min();
            else if(tmp > std::numeric_limits<T>::max()) tmp = std::numeric_limits<T>::max();
            return (T)tmp;
        } else if(std::is_floating_point<T>::value) {
            double tmp;
            try {
                tmp = std::stod(attValue);
            } catch(std::exception const & e) {
                return def;
            }
            return (T)tmp;
        } else if(std::is_same<T, bool>::value) {
            if(attName == "" && strlen(attValue) <= 1) return true;//Node exists, no attribute requested
            
            const std::string attVStr = std::string(attValue);
            const long long attVInt = std::stoll(attValue);
            return !(attVStr == "false" || attVStr == "no" || attVInt < 0);
        }
        
        return T(attValue);
    }*/

    template<typename T>
    T XMLNode::get(const std::string& p) {
        std::string attName;
        char* attValue;
        rapidxml::xml_node<>* xn = this->getPath(p, attName);
        if (xn == NULL || (attValue = (attName == "") ? xn->value() : (char*) xn->first_attribute(attName.c_str())) == NULL) {
#ifdef DEBUG_XML
            std::stringstream ss;
            ss << "Required node at path \"" << p << "\" from " << ((node == NULL) ? "NULL" : node->name()) << "("<<attName<<") does not exist!";
            Console::Warning(ss.str());
#endif
            return T();
        }

        return this->get(p, T());
    }

};

#endif
