#include "XML.h"
#include "AppData.h"
#include <sstream>
#include "externals/rapidxml_print.hpp"
#include <fstream>

void rapidxml::parse_error_handler(const char *what, void *where) {
    _unused(where);
    std::stringstream ss; ss << "XML parsing error \"" << what << "\" in file \"" << arke::XMLDoc::lastPath << "\", caused by \"";
    for(char* p = (char*)where; *p != 0x0A && *p != 0x00; p++) {
        ss << *p;
    }
    ss << "\"";
    arke::Console::Error(ss.str());
    
    //Because rapidXML is a little b1tch, force a stack unwind to our earlier try{} block
    throw -1;
};

namespace arke {

    thread_local std::string XMLDoc::lastPath = "";

    XMLNode::XMLNode() {
    node = NULL;
    }

    XMLNode::XMLNode(rapidxml::xml_node<> *const n) {
    node = n;
    }

    XMLNode::~XMLNode() {
    return;
    }

    #include <iostream>
    rapidxml::xml_node<>* XMLNode::getPath(const std::string& p, std::string& remainder, bool forceNode) {
    const char* pch = p.c_str(), *pchStart;
    pchStart = pch; remainder = "";
    rapidxml::xml_node<> *n = node;
    if(n == NULL) return NULL;
    
    while(*pch != 0x00) {
        if(*pch == '.') {//Add pchStart...pch-1
        std::string childName = std::string(pchStart, pch);
        rapidxml::xml_node<> *childNode = n->first_node(childName.c_str());
        if(childNode == NULL) break;
        
        n = childNode;
        pchStart = pch+1;
        }
        pch++;
    }
    
    {//Try node, then attribute
        std::string childName = std::string(pchStart, pch);
        //std::cout << childName << ": CNAME\n";
        rapidxml::xml_node<> *childNode = n->first_node(childName.c_str());
        if(childNode == NULL) {
        remainder = std::string(pchStart);
        } else {
        n = childNode;
        remainder = "";
        }
    }
    
    if(forceNode && n == this->node) return NULL;
    
    return n;
    }

    inline void XMLDoc::initVars() {
    path = "";
    this->node = NULL;
    }

    XMLDoc::XMLDoc() {
    initVars();
    }

    XMLDoc::XMLDoc(const std::string& file, bool isRel) {
    initVars();
    this->load(file, isRel);
    }

    XMLDoc::~XMLDoc() {
    this->close();
    }

    bool XMLDoc::load(const std::string& file, bool isRel) {
    lastPath = file; unsigned int length;
    path = file; fIsRel = isRel;
    Directory *dir;
    if(isRel) dir = &AppData::base;
    else dir = &AppData::system;
    
    if(path == "" || ( buffer = (char*)dir->readFile(path, length, true)) == NULL || length == 0) {
        std::stringstream ss; ss << "Requested XML file at path \"" << path << "\" could not be read or is empty!";
        Console::Error(ss.str());
        return false;
    }
    
    try {
        doc.parse<rapidxml::parse_declaration_node>((char*)buffer);
    } catch(...) {
        return false;
    }
    
    this->node = &doc;
    
    return true;
    }

    bool XMLDoc::save(const std::string& file, bool isRel) {
    std::string usePath;
    if(file == "") {
        usePath = path; isRel = fIsRel;
    } else if(!isRel) usePath = file;
    else {//Resolve by isRel
        
    }
    
    try {
        std::ofstream of;
        of.open (usePath);
        of << doc;
        bool rState = of.good();
        of.close();
        return rState;
    } catch(...) {
        return false;
    }
    }

    bool XMLDoc::close() {
    doc.clear();
    if(buffer != NULL) delete[] buffer;
    return true;
    }

};
