#include "Time.h"
#include <ctime>

namespace arke {
    time_t Time::Current = 0;
    const std::string& Time::DayName(day_t day) {
        static const std::string days[] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
        return days[day];
    }
        
    const std::string& Time::MonthName(month_t month) {
        static const std::string months[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return months[month];
    }
    
    void Time::set(double epochT) {
        data.seconds = ::floor(::fabs(epochT));
        data.nanoseconds = ::round(::fmod(::fabs(epochT), 1.0)*NanosecondsPerSecond);
        if(epochT < 0.0) {
            data.nanoseconds = NanosecondsPerSecond - data.nanoseconds;
            data.seconds *= -1;
            data.seconds -= 1;
        }
    }
    
    Time::day_t Time::day() const {
        std::time_t secs = this->seconds();
        std::tm *tm = localtime(&secs);
        return tm->tm_mday;
    }
    Time::day_t Time::dayID() const {
        std::time_t secs = this->seconds();
        std::tm *tm = localtime(&secs);
        return tm->tm_wday;
    }
    Time::month_t Time::month() const {
        std::time_t secs = this->seconds();
        std::tm *tm = localtime(&secs);
        return tm->tm_mon;
    }
    uint64_t Time::year() const {
        std::time_t secs = this->seconds();
        std::tm *tm = localtime(&secs);
        return tm->tm_year;
    }
    
    std::string Time::str(const std::string& fmnt) const {
        std::time_t secs = this->seconds();
        std::tm *tm = localtime(&secs);
        
        if(this->data.duration) {
            std::stringstream ss;
            if(tm->tm_year > 0) ss << tm->tm_year << " years, ";
            if(tm->tm_mon > 0) ss << tm->tm_mon << " months, ";
            if(tm->tm_mday > 0) ss << tm->tm_mday << " days, ";
            if(tm->tm_hour > 0) ss << tm->tm_hour << " hours, ";
            if(tm->tm_min > 0) ss << tm->tm_min << " minutes, ";
            if(tm->tm_sec > 0) ss << tm->tm_sec << " seconds, ";
            
            std::string ret = ss.str();
            if(!ret.empty()) return ret.substr(0, ret.size()-2);
            else return ret;
        } else {
            static char buff[256];
            strftime(buff, sizeof(buff), fmnt.c_str(), tm);
            return std::string(buff);
        }
    }
    std::string Time::str() const {
        return this->str("%Y-%m-%d %H:%M:%S");
    }
    std::string Time::date() const {
        return this->str("%Y-%m-%d");
    }
    std::string Time::time() const {
        return this->str("%H:%M:%S");
    }
}
