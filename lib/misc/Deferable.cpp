#include "Deferable.h"
#include "Time.h"

namespace arke {
    Deferable::Deferable() {

    }

    Deferable::~Deferable() {

    }

    void Deferable::update() {
        for(auto it = this->begin<Deferable>(); it != this->end<Deferable>(); ++it) {
            it->update();
        }
        this->lastUpdate = Time::Current;
    }

    time_t Deferable::deltaTime() const {
      return Time::Current - this->lastUpdate;
    }

    void Deferable::refresh() {
        
    }
}
