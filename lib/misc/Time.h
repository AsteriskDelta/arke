#ifndef ARKE_TIME_INC_H
#define ARKE_TIME_INC_H
#include "ARKE.h"

typedef __int128 int128_t;
typedef unsigned __int128 uint128_t;

namespace arke {
    //TODO: Variable Epoch offset implementation
    class Time {
    public:
        typedef uint8_t day_t;
        typedef uint8_t month_t;
        static constexpr const day_t
            Sunday      = 0,
            Monday      = 1,
            Tuesday     = 2,
            Wednesday   = 3,
            Thursday    = 4,
            Friday      = 5,
            Saturday    = 6;
        static const std::string& DayName(day_t day);
        
        static constexpr const month_t
            January     = 0,
            February    = 1,
            March       = 2,
            April       = 3,
            May         = 4,
            June        = 5,
            July        = 6,
            August      = 7,
            September   = 8,
            October     = 9,
            November    = 10,
            December    = 11;
        static const std::string& MonthName(month_t month);
        
        inline Time() : data() {};
        template<typename ...Args>
        inline Time(Args... args) : Time() {
            this->set(args...);
        }
        
        void set(double epochT);
        inline void set(int64_t epochT, uint32_t nanos, uint16_t epochOff = 0) {
            data.seconds = epochT;
            data.nanoseconds = nanos;
            data.epoch = epochOff;
        }
        template<typename V=void>
        inline void set(int epochT) {
            return this->set(epochT, 0, 0);
        }
        template<typename V=void>
        inline void set(const Time& o) {
            data = o.data;
        }
        
        inline void seconds(const int64_t sec) {
            data.seconds = sec;
        }
        inline void milliseconds(const uint32_t ms) {
            data.nanoseconds = ms * (1000*1000);
        }
        inline void microseconds(const uint32_t us) {
            data.nanoseconds = us * (1000);
        }
        inline void nanoseconds(const uint32_t ns) {
            data.nanoseconds = ns;
        }
        
        inline const auto& seconds() const {
            return data.seconds;
        }
        inline const auto& nanoseconds() const {
            return data.nanoseconds;
        }
        inline auto microseconds() const {
            return data.nanoseconds / 1000;
        }
        inline auto milliseconds() const {
            return data.nanoseconds / (1000*1000);
        }
        
        template<typename T>
        inline Time& operator=(const T& v) {
            this->set(v);
            return *this;
        }
        
        static constexpr const uint32_t NanosecondsPerSecond = (1000*1000*1000);
        
        inline uint128_t packed() const {
            //return ((*reinterpret_cast<const uint128_t*>(&data) & ~uint128_t(0xFFFFFFFF)) >> 32) | (uint128_t(data.epoch) << 96);
            union {
                struct {
                    int16_t ep;
                    int64_t se;
                    uint32_t ns;
                } dat __attribute__((packed));
                uint128_t ret = 0x0;
            };
            dat.ep = this->data.epoch;
            dat.se = this->data.seconds;
            dat.ns = this->data.nanoseconds;
            
            return ret;
        }
        inline int128_t full() const {
            return int128_t(data.seconds) * NanosecondsPerSecond + data.nanoseconds;
        }
        inline void fromFull(const int128_t& f) {
            data.nanoseconds = f % NanosecondsPerSecond;
            data.seconds = f / NanosecondsPerSecond;
        }
        
        inline double flt() const {
            double ret = this->data.seconds;
            ret += double(this->data.nanoseconds) / double(NanosecondsPerSecond);
            return ret;
        };
        
        inline explicit operator double() const {
            return this->flt();
        };
        
        inline bool operator==(const Time& t) const {
            return this->packed() == t.packed();
        }
        inline bool operator!=(const Time& t) const {
            return !(*this == t);
        }
        
        inline bool operator<(const Time& t) const {
            return this->packed() < t.packed();
        }
        inline bool operator>(const Time& t) const {
            return this->packed() > t.packed();
        }
        inline bool operator<=(const Time& t) const {
            return this->packed() <= t.packed();
        }
        inline bool operator>=(const Time& t) const {
            return this->packed() >= t.packed();
        }
        
        day_t day() const;
        day_t dayID() const;
        month_t month() const;
        uint64_t year() const;
        
        inline const std::string& dayStr() const {
            return DayName(this->dayID());
        }
        inline const std::string& monthStr() const {
            return MonthName(this->month());
        }
        
        std::string str(const std::string& fmnt) const;
        std::string str() const;
        std::string date() const;
        std::string time() const;
        
        inline Time operator-() const {
            Time ret (*this);
            ret.data.seconds = 0;
            ret.data.nanoseconds = 0;
            ret.data.epoch = 0;
            return ret - (*this);
        }
        
        inline Time operator+(const Time& o) const {
            Time ret(*this);
            ret.data.seconds += o.data.seconds;
            ret.data.nanoseconds += o.data.nanoseconds;
            if(ret.data.nanoseconds > NanosecondsPerSecond) {
                ret.data.seconds++;
                ret.data.nanoseconds -= NanosecondsPerSecond;
            }
            return ret;
        }
        inline Time operator-(const Time& o) const {
            Time ret(*this);
            ret.data.seconds -= o.data.seconds;
            ret.data.nanoseconds -= o.data.nanoseconds;
            if(ret.data.nanoseconds > NanosecondsPerSecond) {
                ret.data.seconds--;
                ret.data.nanoseconds = NanosecondsPerSecond - ret.data.nanoseconds;
            }
            return ret;
        }
        inline Time operator*(const Time& o) const {
            Time ret(*this);
            ret.data.seconds *= o.data.seconds;
            const uint64_t tmp = uint64_t(data.nanoseconds) * o.data.nanoseconds;
            if(tmp > NanosecondsPerSecond) {
                ret.data.seconds += tmp / NanosecondsPerSecond;
                ret.data.nanoseconds = tmp % NanosecondsPerSecond;
            }
            return ret;
        }
        inline Time operator/(const Time& o) const {
            Time ret(*this);
            int128_t f = this->full(), of = o.full();
            ret.fromFull(f / of);
            return ret;
        }
        inline Time operator%(const Time& o) const {
            Time ret(*this);
            int128_t f = this->full(), of = o.full();
            ret.fromFull(f % of);
            return ret;
        }
        
        inline Time& operator+=(const Time& o) {
            (*this) = (*this) + o;
            return *this;
        }
        inline Time& operator-=(const Time& o) {
            (*this) = (*this) - o;
            return *this;
        }
        inline Time& operator*=(const Time& o) {
            (*this) = (*this) * o;
            return *this;
        }
        inline Time& operator/=(const Time& o) {
            (*this) = (*this) / o;
            return *this;
        }
        inline Time& operator%=(const Time& o) {
            (*this) = (*this) % o;
            return *this;
        }
        
        static time_t Current;
        
        inline bool valid() const {
            return data.seconds != 0 || data.nanoseconds != 0 || data.epoch != 0;
        };
        
        inline static Time Max() {
            Time ret(static_cast<int64_t>(0x7FFFFFFFFFFFFFFF), 0);
            return ret;
        }
        inline static Time Min() {
            Time ret(static_cast<int64_t>(0x7FFFFFFFFFFFFFFF) * -1, 0);
            return ret;
        }
    protected:
        struct {
            int64_t seconds = 0;
            uint32_t nanoseconds = 0;
            int16_t epoch = 0;
            struct {
                uint16_t preferredTZ : 6;
                bool duration : 1;
                uint16_t padding: 9;
            };
        } data;
    };
    /*class Time {
    public:
        uint64_t timestamp;
        uint32_t nanoseconds;

        static time_t Current;
    protected:

    };*/
    template<typename genType>
    typename std::enable_if<std::is_base_of<Time, genType>::value, std::string>::type
    inline to_string(const genType& g) {
        return g.str();
    }
    template<typename genType>
    typename std::enable_if<std::is_base_of<Time, genType>::value, std::ostream&>::type 
    inline operator<<(std::ostream& out, const genType& g) {
        return out << g.str();
    }
};

#endif
