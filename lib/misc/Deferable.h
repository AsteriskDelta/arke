#ifndef D13_DEFERABLE_H
#define D13_DEFERABLE_H
#include "ARKE.h"
#include "SmallSet.h"
#include "Serializable.h"
#include "Proxy.h"

namespace arke {
  class Deferable : public Serializable, public virtual Proxy {
  public:
    Deferable();
    virtual ~Deferable();

    virtual void update();
    virtual void refresh();

    time_t deltaTime() const;
  protected:
    time_t lastUpdate;
  };
};

#endif
