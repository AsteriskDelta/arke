#ifndef ARKE_HOLDER_H
#define ARKE_HOLDER_H
#include "Instance.h"
#include <ARKE/Proxy.h>
#include <vector>

namespace arke {
    template<typename T>
    class Holder : public virtual Proxy {
    public:
        typedef Instance<T> Ins;
        
        inline Ins& operator[](T* ptr) {
            for(Ins& ins : holder.instances) {
                if(ins.is(ptr)) return ins;
            }
            return holder.null;
        }
        inline const Ins& operator[](T* ptr) const {
            return const_cast<Holder<T>*>(this)->operator[](ptr);
        }
    protected:
        struct Data {
            std::vector<Ins> instances;
            static Ins null;
        } holder;
    };
}

#endif
