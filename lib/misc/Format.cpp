#include "Format.h"

namespace arke {
  namespace Format {
    std::string SI(nvxi_t value) {
        static char suffix[] = {' ','k','M','B'};
        static nvxi_t mults[] = {1,1000,1000000,1000000000};
        //constexpr uint8_t multCount = 4;

        uint8_t mult = 0;
        if(value > mults[3]) mult = 3;
        else if(value > mults[2]) mult = 2;
        else if(value > mults[1]) mult = 1;

        if(mult == 0) return std::string(value);
        else {
          return std::string(value/mults[mult]) + suffix[mult];
        }

    }
    std::string Time(nvxi_t value) {
        return std::string(value);
    }
    std::string TimeTo(nvxi_t value) {
      return std::string(value);
    }
  }
};
