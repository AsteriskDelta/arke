#ifndef D13_FORMAT_H
#define D13_FORMAT_H
#include "ARKE.h"

namespace arke {
  namespace Format {
    std::string SI(nvxi_t value);
    std::string Time(nvxi_t value);
    std::string TimeTo(nvxi_t value);
  }
};

#endif
