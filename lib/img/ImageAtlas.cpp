#include "ImageAtlas.h"
#include "Console.h"
#include "AppData.h"
#include "Application.h"
#include <sstream>

namespace arke {

    ImageAtlas::PackingNode::PackingNode() {
    imageID = TP_IMGID_NULL;
    children[0] = children[1] = NULL;
    return;
    };

    ImageAtlas::PackingNode::~PackingNode() {
    if(children[0] != NULL) {
        delete children[0];
        delete children[1];
    }
    }

    unsigned int ImageAtlas::PackingNode::giveID(unsigned int s) {
    nodeID = s++;
    if(children[0] != NULL) s = children[0]->giveID(s);
    if(children[1] != NULL) s = children[1]->giveID(s);
    return s;
    }

    std::string ImageAtlas::PackingNode::toString(unsigned int pid) {
    std::stringstream ss;
    ss << "IAPN"<<nodeID<<" " << x<<","<<y<<" "<<w<<"x"<<h<<" #" << *reinterpret_cast<int*>(&imageID);/* << " C";
    if(children[0] == NULL) ss << "-1";
    else ss << children[0]->nodeID;
    ss << ",";
    if(children[1] == NULL) ss << "-1";
    else ss << children[1]->nodeID;
    */
    ss << " P"<<pid;
    return ss.str();
    }

    void ImageAtlas::PackingNode::streamWrite(std::stringstream *ss, unsigned int pid) {
    (*ss) << this->toString(pid) << "\n";
    if(children[0] != NULL) children[0]->streamWrite(ss, nodeID);
    if(children[1] != NULL) children[1]->streamWrite(ss, nodeID);
    }

    char ImageAtlas::PackingNode::Contains(unsigned int width, unsigned int height) {
    //std::cout << "CON(" << width << ", " << height << ") " << w << ", " << h << "\n";
    if((width > w || height > h)/* && (width > h || height > w)*/) return -1;
    else if((width == w && height == h)/* || (width == h && height == w)*/) return 0;
    else return 1;
    }

    ImageAtlas::PackingNode* ImageAtlas::PackingNode::Insert(const TPImage& val) {
    //std::cout << "insert " << val.width << ", " << val.height << "\n";
    if(children[0] != NULL) {
        //try inserting into first child
        PackingNode *newNode = children[0]->Insert(val);
        if(newNode != NULL) return newNode;
        
        //no room, insert into second
        return children[1]->Insert(val);
    } else {
        //if there's already an image here, return)
        if(imageID != TP_IMGID_NULL) {
        //std::cout << "non-null image!\n";
        return NULL;
        }
        
        char packingResult = Contains(val.width, val.height);
        
        if(packingResult == -1) return NULL;//Node size insufficient, not gonna happen
        else if(packingResult == 0) return this;//perfect fit, I am JESUS
        else {//Oversized, split node into children, which kinda makes us Jesus too
        children[0] = new PackingNode(); children[1] = new PackingNode();
        
        //Determine split configuration
        int dWidth = w - val.width;
        int dHeight = h - val.height;
            
        if(dWidth > dHeight) {
        children[0]->SetConf(x, y, val.width, h);
        children[1]->SetConf(x + val.width+1, y, w - val.width-1, h);
        //std::cout << x << ", " << y << ", " << val.width << ", " << h << "\n";
        //children[0]->rect = Rectangle(rc.left, rc.top, rc.left+width-1, rc.bottom)
            //children[1]->rect = Rectangle(rc.left+width, rc.top, rc.right, rc.bottom)
        } else {
        children[0]->SetConf(x, y, w, val.height);
        children[1]->SetConf(x, y + val.height+1, w, h - val.height-1);
        //std::cout << x << ", " << y << ", " << w << ", " << val.height<< "\n";
            //children[0]->rect = Rectangle(rc.left, rc.top, rc.right, rc.top+height-1)
            //children[1]->rect = Rectangle(rc.left, rc.top+height, rc.right, rc.bottom)
        }
        
        //Insert into the perfet fit (first) node created
        return children[0]->Insert(val);
        }
    }
    }

    ImageAtlas::ImageAtlas() : padding(0) {
    width = height = 0; layerMax = 0;
    idCounter = 0;
    atlasName = "";
    cached = false;
    }
    ImageAtlas::ImageAtlas(unsigned int w, unsigned int h, unsigned int l) : padding(0) {
    idCounter = 0;
    atlasName = "";
    cached = false;
    cacheDirty = false;
    this->setDimensions(w, h, l);
    }

    void ImageAtlas::setDimensions(unsigned int w, unsigned int h, unsigned int l) {
    width = w; height = h;
    layerMax = l;
    cacheDirty = false;
    pushLayer();
    }

    ImageAtlas::~ImageAtlas() {
    if(cached && cacheDirty) {
        this->saveCache();
    }
    for(auto it = layers.begin(); it != layers.end(); it++) {
        delete it->img;
        delete it->root;
    }
    }


    ImageAtlas::RS* ImageAtlas::getResource(std::string nam) {
    auto iter = resourceList.find(nam);
    if(iter == resourceList.end()) return nullptr;
    else return &(iter->second);
    }
    bool ImageAtlas::hasResource(std::string nam) {
    return (this->getResource(nam) != nullptr);
    }

    ImageAtlas::IDT ImageAtlas::emplace(std::string resourceName, ImageRGBA* img, bool allowMinimize) {
    RS* res = getResource(resourceName);
    
    if(res != NULL) {
        return (res->list.front());
    } else {
        IDT id = this->emplace(img, allowMinimize);
        RS newRes;
        newRes.hash = 0x0;
        newRes.list.push_back(id);
        resourceList[resourceName] = newRes;
        return id;
    }
    }

    ImageAtlas::IDT ImageAtlas::emplaceMulti(std::string resourceName, ImageRGBA* img, bool allowMinimize) {
    RS* res = getResource(resourceName);
    
    if(res == NULL) {
        RS newRes;
        newRes.hash = 0x0;
        resourceList[resourceName] = newRes;
        res = getResource(resourceName);
        if(res == NULL) return 0;
    }
    
    IDT id = this->emplace(img, allowMinimize);
    res->list.push_back(id);
    return id;
    }

    void ImageAtlas::setName(std::string newName) {
    atlasName = newName;
    }

    void ImageAtlas::useCache() {
    if(atlasName == "") {
        std::stringstream ss; ss << "ImageAtlas::useCache() called with empty name, no cache loaded!";
        Console::Error(ss.str());
        return;
    }
    cached = true;
    cacheDirty = true;
    this->loadCache();
    }

    void ImageAtlas::Lookup::fromString(std::string s) {
    
    }
    std::string ImageAtlas::Lookup::toString() {
    std::stringstream ss;
    ss << x<<","<<y<< " " << w << "x" << h << " + "<<ox<<","<<oy << " " << ow << "x" << oh << " #" << selfID << " @" << layer;
    return ss.str();
    }

    void ImageAtlas::Layer::saveCache(Directory *dir, std::string n) {
    //Write image
    img->Save(dir, n+".png");
    //std::cout << "Writing to " << dir->resolve(n+".png") << "\n";;
    //Write tree
    std::stringstream ss;
    root->giveID(0);
    root->streamWrite(&ss, 0);
    std::string sstr = ss.str();
    
    dir->writeFile(n+".txt", sstr.c_str(), sstr.size());
    }
    void ImageAtlas::Layer::loadCache(Directory *dir, std::string nam) {
    std::cout << "loading cache for " << nam << "\n";
    img->Load(dir, nam+".png");
    
    //Load tree
    std::unordered_map<unsigned int, ImageAtlas::PackingNode*> nodeMap;
    
    unsigned int buffLen;
    char* buff = (char*)dir->readFile(nam+".txt", buffLen, true);
    unsigned int off = 0; bool nextPos = true;
    while(off < buffLen) {
        if(nextPos) {
        nextPos = false;
        int cnt;
        if(buff[off] == 'I') {
            int x,y,w,h, iid, nid, pid;
            cnt = sscanf(&buff[off], "IAPN%i %i,%i %ix%i #%i P%i", &nid, &x,&y,&w,&h, &iid, &pid);
            if(cnt < 7) continue;
            
            ImageAtlas::PackingNode *n = new PackingNode;
            n->x = x; n->y = y; n->w = w; n->h = h;
            n->imageID = *(reinterpret_cast<unsigned int*>(&iid));
            n->nodeID = nid;
            if(nid == 0) root = n;
            else {
            auto ft = nodeMap.find(pid);
            if(ft == nodeMap.end()) {
                std::cerr << "cached nodemap for " << nid << " and p=" << pid << " is invalid!";
                continue;
            }
            ft->second->cacheAddChild(n);
            }
            nodeMap[nid] = n;
            
            //std::cout << "iapn " << nid << " <- " << pid << "\n";
        }
        }
        
        if(buff[off] == '\n') nextPos = true;
        off++;
    }
    
    delete[] buff;
    }

    std::string ImageAtlas::getCachePath() {
    std::string ret = "cache/"+atlasName+"/";
    return ret;
    }

    void ImageAtlas::saveCache() {
    if(!cacheDirty) return;
    std::string path = this->getCachePath();
    Directory dir = Application::GetLocal();
    //Save layers
    unsigned int layerIdx = 0;
    for(auto it = layers.begin(); it != layers.end(); ++it) {
        std::stringstream ss; ss << "l"<<layerIdx;
        std::string lName = path+ss.str();
        it->saveCache(&dir, lName);
        layerIdx++;
    }
    
    //Save resource IDs
    std::stringstream ss;
    for(auto it = resourceList.begin(); it != resourceList.end(); ++it) {
        ss <<"RID " << it->first << ":"<<it->second.hash<<";\n";
        for(auto lt = it->second.list.begin(); lt != it->second.list.end(); ++lt) {
        ss << "FRD " << it->first<<":" << *lt << "\n";
        }
    }
    
    //Lookups
    for(auto it = lookupMap.begin(); it != lookupMap.end(); ++it) {
        ss << "LOK"<<it->first << "=" << it->second.toString() << "\n";
    }
    
    std::string sstr = ss.str();
    std::string mPath = path + "index.txt";
    dir.writeFile(mPath, sstr.c_str(), sstr.size());
    cacheDirty = false;
    }
    bool ImageAtlas::loadCache() {
    std::string path = this->getCachePath();
    Directory dir = Application::GetLocal();
    if(!dir.exists(path+"index.txt")) return false;
    
    unsigned int buffLen;
    char *buff = reinterpret_cast<char*>(dir.readFile(path+"index.txt", buffLen, true));
    //std::cout << "loading atlas cache... "<<buffLen<<"\n";
    unsigned int off = 0; bool nextPos = true;
    char nameBuff[256];
    while(off < buffLen) {
        if(nextPos) {
        nextPos = false;
        
        int cnt;
        if(buff[off] == 'R') {
            memset(nameBuff, 0x0, 256);
            int hash;
            cnt = sscanf(&buff[off], "RID %256[^:]: %i=", nameBuff, &hash);
            //std::cout << "rid cnt=" << cnt << "\n";
            if(cnt < 2) continue;
            
            std::string key = std::string(nameBuff);
            RS nrs;
            nrs.hash = hash;
            
            resourceList[key] = nrs;
            //std::cout << "RS " << key << "\n";
        } else if(buff[off] == 'F') {
            memset(nameBuff, 0x0, 256);
            int resIdx;
            cnt = sscanf(&buff[off], "FRD %256[^:]:%i\n", nameBuff, &resIdx);
            if(cnt < 2) continue;
            
            std::string key = std::string(nameBuff);
            resourceList[key].list.push_back(resIdx);
            //std::cout << "frd " << key << " has " << resIdx << "\n";
        } else if(buff[off] == 'L') {
            int idx,x,y,w,h,ox,oy,ow,oh,slf, lay;
            cnt = sscanf(&buff[off], "LOK%i=%i,%i %ix%i + %i,%i %ix%i #%i @%i", &idx, &x,&y,&w,&h, &ox,&oy,&ow,&oh, &slf, &lay);
            //std::cout << "lok cnt=" << cnt << " on layer " << lay << "\n";
            if(cnt < 11) continue;
            
            Lookup lp;
            lp.x = x; lp.y = y;
            lp.w = w; lp.h = h;
            lp.ox = ox; lp.oy = oy;
            lp.ow = ow; lp.oh = oh;
            lp.selfID = slf; lp.layer = lay;
            lookupMap[lp.selfID] = lp;
            
            while(lp.layer >= layers.size() && lp.layer < 256) this->pushLayer();
        }
        }
        
        if(buff[off] == '\n') nextPos = true;
        off++;
    }
    
    delete[] buff;
    
    //Load layers
    unsigned int layerIdx = 0;
    for(auto it = layers.begin(); it != layers.end(); ++it) {
        std::stringstream ss; ss << "l"<<layerIdx;
        std::string lName = path+ss.str();
        it->loadCache(&dir, lName);
        layerIdx++;
    }
    
    cacheDirty = false;
    
    return true;
    }

    ImageAtlas::IDT ImageAtlas::emplace(ImageRGBA *img, bool allowMinimize) {
    ImageRGBA *tmp = img; bool deleteTmp = false;
    unsigned int fx = 0, fy = 0;
    if(allowMinimize) {
        tmp = img->minimized(&fx, &fy);
        
        
        //std::cout << "minimized image from " << img->width << "x" << img->height << " to "<< tmp->width << "x" << tmp->height << " OFF=" << fx << ", " << fy << "\n";
        deleteTmp = true;
    } 
    bool skipInsert = tmp->height == 0 || tmp->width == 0;
    
    if(padding > 0) {
        ImageRGBA *newTmp = tmp->padded(padding);
        if(deleteTmp) delete tmp;
        tmp = newTmp;
        deleteTmp = true;
    }
    
    if(!skipInsert && (tmp->width == 0 || tmp->height == 0 || tmp->width > width || tmp->height > height)) {
        std::stringstream ss; ss << "Image of " << tmp->width << "x" << tmp->height << " cannot be emplaced in atlas of " << width << "x"<<height;
        Console::Error(ss.str());
        if(deleteTmp) delete tmp;
        return TP_IMGID_NULL;
    }
    //std::cout << "alloc " << w << ", " << h << "\n";
    TPImage use = TPImage{tmp->width, tmp->height};
    auto layerIt = layers.begin(); unsigned int layerID = 0;
    while(true) {
        if(layerIt == layers.end()) {
        pushLayer();
        layerIt = std::prev(layers.end());
        }
        
        PackingNode *ret = NULL;
        if(!skipInsert) ret = layerIt->root->Insert(use);

        if(ret != NULL || skipInsert) {
        if(!skipInsert) {
        if(ret->w == tmp->width) {
            //sX = ret->x; sY = ret->y;
            //eX = ret->x + ret->w - 1;
            //eY = ret->y + ret->h - 1;
        } else {//OpenGL doesn't support uploading of semi-mirrored images ;-;
            std::cout << "TEXTUREPACKER RETURNED MIRRORED IMAGE REQ!\n";
            //sX = sY = 0;
            //eX = eY = 0;
        }
        ret->imageID = 1;
        
        layerIt->img->clone(tmp, ret->x, ret->y);
        }
        if(deleteTmp) delete tmp;
        IDT rid = this->addLookup(ret);
        Lookup* lk = this->find(rid);
        lk->ox = fx; lk->oy = fy;
        lk->ow = img->width; lk->oh = img->height;
        lk->layer = layerID;
        cacheDirty = true;
        return rid;
        }
        layerIt++;
        layerID++;
    }
    }
    ImageAtlas::Lookup* ImageAtlas::find(IDT id) {
    auto it = lookupMap.find(id);
    if(it == lookupMap.end()) return NULL;
    
    return &(it->second);
    }
    ImageAtlas::Lookup* ImageAtlas::find(std::string resourceName) {
    RS* r = this->getResource(resourceName);
    if(r == NULL || r->list.size() == 0) return NULL;
    Uint16 id = *(r->list.begin());
    
    auto it = lookupMap.find(id);
    if(it == lookupMap.end()) return NULL;
    
    return &(it->second);
    }


    ImageAtlas::IDT ImageAtlas::addLookup(PackingNode *n) {
    Lookup toInsert; IDT ret = idCounter++;
        if(n != NULL) {
        toInsert.w = n->w; toInsert.h = n->h;
        toInsert.x = n->x; toInsert.y = n->y;
    } else {
        toInsert.x = toInsert.y = toInsert.w = toInsert.h = 0;
    }
    toInsert.selfID = ret;
    lookupMap[ret] = toInsert;
    return ret;
    }

    void ImageAtlas::pushLayer() {
    layers.push_back(Layer());
    Layer *layer = &(*layers.rbegin());
    layer->root = new PackingNode();
    layer->root->x = 0; layer->root->y = 0;
    layer->root->w = width; layer->root->h = height;
    layer->img = new ImageRGBA(width, height);
    layer->img->zero();
    }

};
