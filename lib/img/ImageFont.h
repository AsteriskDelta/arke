#ifndef IMGFONT_H
#define	IMGFONT_H
#include <ft2build.h>
//#include FT_FREETYPE2_H BROKEN for now, *slow clap* @ freetype
#include <freetype2/freetype/ftglyph.h>
#include "Image.h"

namespace arke {

union UniChar {
  char* c;
  unsigned char* u;
};

struct GlyphData {
  FT_UInt index;
  char character;
  short x, y;
  short w, h;
  short bX, bY;//Bearing, raw offset from origin
  short oX, oY;//True offsets from origin
  short adv;//X distance to advance (pixels)
  
  ImageRGBA *img;
  unsigned short tX, tY, eX, eY;
};

class ImageFont {
public:
  ImageFont(std::string path, unsigned int pt = 12);
  virtual ~ImageFont();
    
  std::string path;
  FT_Face face;
  unsigned short size;
  uint8_t image;
  UniChar data;
  unsigned short x, y;
  unsigned short vertAdv, horiAdv;//Max charcter height+2, for vertical advance
  
  unsigned int length;
  
  GlyphData glyphs[256];
  
  void write(ImageRGBA* img, unsigned short x, unsigned short y, std::string text);
  ImageRGBA* renderGlyph(char c, unsigned int w = 0, unsigned int h = 0, unsigned int x = 0, unsigned int y = 0);
  ImageRGBA* renderGlyphSDF(char c, unsigned int w = 0, unsigned int h = 0, unsigned int x = 0, unsigned int y = 0, unsigned int reach  = 8);
  ImageRGBA* renderGlyphQSDF(char c, unsigned int w = 0, unsigned int h = 0, unsigned int x = 0, unsigned int y = 0, unsigned int reach  = 8);
    
  static bool Initialize();
private:
    static bool initialized;
};

};

#endif	/* IMGFONT_H */

