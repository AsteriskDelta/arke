#include "TexturePacker.h"

PackingNode::PackingNode() {
  imageID = TP_IMGID_NULL;
  children[0] = children[1] = NULL;
  return;
};

PackingNode::~PackingNode() {
  if(children[0] != NULL) {
    delete children[0];
    delete children[1];
  }
}

char PackingNode::Contains(unsigned int width, unsigned int height) {
  //std::cout << "CON(" << width << ", " << height << ") " << w << ", " << h << "\n";
  if((width > w || height > h)/* && (width > h || height > w)*/) return -1;
  else if((width == w && height == h)/* || (width == h && height == w)*/) return 0;
  else return 1;
}

PackingNode* PackingNode::Insert(const TPImage& val) {
  //std::cout << "insert " << val.width << ", " << val.height << "\n";
  if(children[0] != NULL) {
    //try inserting into first child
    PackingNode *newNode = children[0]->Insert(val);
    if(newNode != NULL) return newNode;
    
    //no room, insert into second
    return children[1]->Insert(val);
  } else {
    //if there's already an image here, return)
    if(imageID != TP_IMGID_NULL) {
      //std::cout << "non-null image!\n";
      return NULL;
    }
    
    char packingResult = Contains(val.width, val.height);
    
    if(packingResult == -1) return NULL;//Node size insufficient, not gonna happen
    else if(packingResult == 0) return this;//perfect fit, I am JESUS
    else {//Oversized, split node into children, which kinda makes us Jesus too
      children[0] = new PackingNode(); children[1] = new PackingNode();
      
      //Determine split configuration
      int dWidth = w - val.width;
      int dHeight = h - val.height;
        
      if(dWidth > dHeight) {
	children[0]->SetConf(x, y, val.width, h);
	children[1]->SetConf(x + val.width, y, w - val.width, h);
	//std::cout << x << ", " << y << ", " << val.width << ", " << h << "\n";
	//children[0]->rect = Rectangle(rc.left, rc.top, rc.left+width-1, rc.bottom)
        //children[1]->rect = Rectangle(rc.left+width, rc.top, rc.right, rc.bottom)
      } else {
	children[0]->SetConf(x, y, w, val.height);
	children[1]->SetConf(x, y + val.height, w, h - val.height);
	//std::cout << x << ", " << y << ", " << w << ", " << val.height<< "\n";
        //children[0]->rect = Rectangle(rc.left, rc.top, rc.right, rc.top+height-1)
        //children[1]->rect = Rectangle(rc.left, rc.top+height, rc.right, rc.bottom)
      }
      
      //Insert into the perfet fit (first) node created
      return children[0]->Insert(val);
    }
  }
}

TexturePacker::TexturePacker(unsigned int w, unsigned int h) {
  width = w; height = h;
  root = new PackingNode();
  root->x = 0; root->y = 0;
  root->w = width; root->h = height;
};

bool TexturePacker::Allocate(unsigned int w, unsigned int h, unsigned int &sX, unsigned int &sY, unsigned int &eX, unsigned int &eY) {
  if(w == 0 || h == 0) return true;
  //std::cout << "alloc " << w << ", " << h << "\n";
  TPImage use = TPImage{w, h};
  PackingNode *ret = root->Insert(use);
  
  if(ret == NULL) return false;
  
  if(ret->w == w) {
    sX = ret->x; sY = ret->y;
    eX = ret->x + ret->w - 1;
    eY = ret->y + ret->h - 1;
  } else {//OpenGL doesn't support uploading of semi-mirrored images ;-;
    std::cout << "TEXTUREPACKER RETURNED MIRRORED IMAGE REQ!\n";
    sX = sY = 0;
    eX = eY = 0;
  }
  ret->imageID = 1;
  
  return true;
}