#include <iostream>

class DebugImage {
public:
  DebugImage(unsigned short newWidth, unsigned short newHeight);
  ~DebugImage();
  
  void line(int sx, int sy, int ex, int ey, int color);
  //void BKLine(float x1, float y1, float x2, float y2, const int color );
  void point(int x, int y, int color);
  void pixel(int x, int y, int color);
  
  int getColor(unsigned char r, unsigned char g, unsigned char b);
  
  void write(const std::string& name);
private:
  unsigned short width;
  unsigned short height;
  
  void* image;
};