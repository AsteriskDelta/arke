#ifndef ARKE_IMAGE_INC
#define ARKE_IMAGE_INC
#include "Color.h"
#include <cstring>

namespace arke {

    class Directory;

    template<int C = 4, typename T = unsigned char>
    class Image {
    public:
    unsigned short width, height;
    unsigned char dChannels;
    
    inline Image() : width(0), height(0), dChannels(0), data(NULL) {};
    Image(unsigned short w, unsigned short h);
    Image(std::string path, bool isRelPath = true);
    ~Image();
    
    inline operator bool() const { return width > 0 && height > 0; };
    
    void Allocate();
    void Deallocate();
    
    bool Load(std::string path, bool isRelPath = true);
    bool Save(std::string path);
    
    bool Load(Directory *dir, std::string path);
    bool Save(Directory *dir, std::string path);
    
    
    //Query functions
    inline Color<C, T> getPixel(unsigned short x, unsigned short y) const { return data[x+y*width]; };
    inline Color<C, T>& getPixelRef(unsigned short x, unsigned short y) { return data[x+y*width]; };
    
    inline bool hasPixel(unsigned short x, unsigned short y) const {
        return x < width && y < height;
    }
    
    inline Color<C,T> getPixelSafe(unsigned short x, unsigned short y, const Color<C,T>& def = Color<C,T>()) const {
        if(!this->hasPixel(x,y)) return def;
        else return this->getPixel(x,y);
    }
    inline void setPixelSafe(unsigned short x, unsigned short y, const Color<C,T>& def) {
        if(!this->hasPixel(x,y)) return;
        else return this->setPixel(x,y, def);
    }
    
    inline unsigned char getAlpha(unsigned short x, unsigned short y) const {
        Color<C, T>* pix = &data[x+y*width];
        if(C < 4) {
            for(unsigned char i = 0; i < C; i++) {
                if(pix->v[i] != 0) return 255;
            }
            return 0;
        } else return data[x+y*width].v[3];
    }
    
    inline unsigned char getChannels() const { return (unsigned char)C; };
    inline unsigned char getBPP() const { return sizeof(Color<C, T>) * 8; };
    inline unsigned char getPitch() const { return sizeof(Color<C, T>) * width; };
    
    inline unsigned char* getRawData() const { return (unsigned char*)data; };
    
    //Drawing functions (set=eq, draw=blend)
    void setPixel(unsigned short x, unsigned short y, const Color<C, T>& c) {
        data[x+y*width] = c;
    }
    void drawPixel(unsigned short x, unsigned short y, const Color<C, T>& c) {
        if(x >= width || y >= height) return;
        
        if(C < 4) setPixel(x, y, c);
        else data[x+y*width].overlay(c);
    }
    
    void setAlpha(unsigned short x, unsigned short y, unsigned char a) {
        Color<C, T>* pix = &data[x+y*width];
        if(C < 4) {
            for(unsigned char i = 0; i < C; i++) {
                pix->v[i] = (unsigned char)(int(pix->v[i]) * a / 255);
            }
        } else pix->v[3] = a;
    }
    
    inline void setChannel(unsigned short x, unsigned short y, unsigned char channel, unsigned char v) {
        Color<C, T>* pix = &data[x+y*width];
        pix->v[channel] = v;
    }
    
    void drawPoint(unsigned short x, unsigned short y, const Color<C, T>& c, unsigned char r = 4);
    void drawLine(unsigned short sx, unsigned short sy, unsigned short ex, unsigned short ey, const Color<C, T>& c, unsigned char w = 1);
    
    inline bool clone(const Image<C,T>* o) {
        if(width != o->width || height != o->height) return false;
        memcpy(getRawData(), o->data, width*height*C);
        return true;
    }
    inline bool clone(const Image<C,T>* o, unsigned int x, unsigned int y, int w = 0, int h = 0) {
        if(width < x+o->width || height < y+o->height) return false;
        if(w == 0 && h == 0) {
            w = o->width; h = o->height; 
        }
        
        for(int xp = 0; xp < w; xp++) {
            for(int yp = 0; yp < h; yp++) {
                this->setPixel(x+xp, y+yp, o->getPixel(xp, yp));
            }
        }
        return true;
    }
    inline bool cloneFrom(const Image<C,T>* o, unsigned int x, unsigned int y, int w, int h) {
        if(width < w || height < h) return false;
        
        for(int xp = 0; xp < w; xp++) {
            for(int yp = 0; yp < h; yp++) {
                this->setPixel(xp, yp, o->getPixel(x+xp, y+yp));
            }
        }
        return true;
    }
    
    Image<C,T>* getSlice(unsigned int x, unsigned int y, int w, int h);
    void flip(bool flipX, bool flipY);
    
    inline void ftaFloodFill(const Color<C,T>& to, const unsigned short x, const unsigned short y, const Color<C,T>& border) {
        if(x >= width || y >= height) return;
        const Color<C,T> col = this->getPixel(x,y);
        if(/*(col.v[4] != 255&&col.v[4] != 0)*/col.v[3] == 63 || col == border) return;
        
        Color<C,T> tc = to;
        for(int i = 0; i < 3; i++) tc.v[i] = tc.v[i] - std::min(tc.v[i], T(255 - col.v[i]));
        tc.v[3] = 63;
        this->setPixel(x,y,tc);
        
        ftaFloodFill(to, x+1, y  , border);
        ftaFloodFill(to, x-1, y  , border);
        ftaFloodFill(to, x  , y+1, border);
        ftaFloodFill(to, x  , y-1, border);
    }
    
    inline void repAlpha(T f, T t) {
        const unsigned int tot = width*height;
        for(unsigned int i = 0; i < tot; i++) if(data[i].v[3] == f) data[i].v[3] = t;
    }
    inline void repColor(const Color<C,T>& from, const Color<C,T>& to) {
        const unsigned int tot = width*height;
        for(unsigned int i = 0; i < tot; i++) if(data[i] == from) data[i] = to;
    }
    void debugAlpha();//Renders alpha channel to RGB
    
    inline bool overlay(unsigned short x, unsigned short y, Image<C,T>* o, unsigned short w = 0, unsigned short h = 0) {
        if(w == 0 && h == 0) {
            w = o->width; h = o->height; 
        }
        //std::cout << "overlay " << w<<"," << h <<"\n";
        //std::cout << "Overlay " << x<<","<<y<<" to " << (x+w)<<","<<(y+h) << "\n";
        //std::cout << "WH: " << width << "," << height << "\n";
        if(x+w > width ||y+h > height) return false;
        //std::cout << "Overlay " << x<<","<<y<<" to " << (x+w)<<","<<(y+h) << "\n";
        for(int xp = 0; xp < w; xp++) {
            for(int yp = 0; yp < h; yp++) {
                data[(x+xp)+(y+yp)*width].overlay(o->getPixel(xp,yp));
            }
        }
        return true;
    }
    
    inline void clear(const Color<C, T>& c) {
        unsigned int totalPX = width * height;
        for(unsigned int i = 0; i < totalPX; i++) data[i] = c;
    }
    inline void fill(const Color<C,T>& to) {
        clear(to);
    }
    
    inline void getMinimumBounds(unsigned int &x, unsigned int &y, unsigned int &w, unsigned int &h) const {
        unsigned int hx = 0, hy = 0, lx = width, ly = height;
        for(unsigned int a = 0; a < width; a++) {
            for(unsigned int b = 0; b < height; b++) {
                unsigned char v = this->getAlpha(a,b);
                if(v > 0) {
                    if(a < lx) lx = a;
                    if(a > hx) hx = a;
                    if(b < ly) ly = b;
                    if(b > hy) hy = b;
                }
            }
        }
        
        x = lx; y = ly;
        w = hx - lx+1;
        h = hy - ly+1;
        if(w > width) w = 1;
        if(h > height) h = 1;
    }
    inline Image<C,T>* minimized(unsigned int *oX = NULL, unsigned int *oY = NULL) const {
        unsigned int offX, offY, w, h;
        this->getMinimumBounds(offX, offY, w, h);
        Image<C,T>* ret = new Image<C,T>(w, h);
        ret->cloneFrom(this, offX, offY, w, h);
        //std::cout << "img::minimized got offsets " << offX << ", " << offY << "\n";
        if(oX != NULL) *oX = offX;
        if(oY != NULL) *oY = offY;
        return ret;
    }
    
    inline Image<C,T>* padded(unsigned int padding) const {
        unsigned int offX = padding, offY=padding, w = width + 2*padding, h = height + 2*padding;
        Image<C,T>* ret = new Image<C,T>(w, h);
        ret->clone(this, padding, padding, width, height);
        for(unsigned int x = 0; x < w; x++) {
            for(unsigned int y = 0; y < padding; y++) {
                ret->setPixel(x, y, ret->getPixel(x, padding));
                ret->setPixel(x, h - 1 - y, ret->getPixel(x, height - 1 + padding));
            }
        }
        for(unsigned int y = 0; y < h; y++) {
            for(unsigned int x = 0; x < padding; x++) {
                ret->setPixel(x, y, ret->getPixel(padding, y));
                ret->setPixel(w-1-x, y, ret->getPixel(width - 1 + padding, y));
            }
        }
        
        return ret;
    }
    
    inline void zero() {
        memset(data, 0x0, width*height*sizeof(Color<C,T>));
    }
    
    //Plugin Functions
    #ifdef V2_INC
    template <typename O> inline Color<C, T> getPixel(V2T<O> c) { 
        return getPixel((unsigned short)round(c.x), (unsigned short)round(c.y));
    };
    template <typename O> inline void setPixel(V2T<O> c, const Color<C, T>& p) { 
        setPixel((unsigned short)round(c.x), (unsigned short)round(c.y), p);
    };
    template <typename O> inline void drawPixel(V2T<O> c, const Color<C, T>& p) { 
        drawPixel((unsigned short)round(c.x), (unsigned short)round(c.y), p);
    };
    template <typename O> inline void drawPoint(V2T<O> c, const Color<C, T>& p, unsigned char r = 4) {
    drawPoint((unsigned short)round(c.x), (unsigned short)round(c.y), p, r);
    }
    template <typename O> inline void drawLine(V2T<O> s, V2T<O> e, const Color<C, T>& p, unsigned char w = 1) {
        drawLine((unsigned short)round(s.x), (unsigned short)round(s.y), (unsigned short)round(e.x), (unsigned short)round(e.y), p, w);
    }
    #endif
    Color<C, T> *data;
    
    void   _dla_changebrightness(Color<C,T> *from,  Color<C,T> *to, float br);
    void _dla_plot(int x, int y, Color<C,T> col, float br);
    void draw_line_antialias(
    unsigned int x1, unsigned int y1,
    unsigned int x2, unsigned int y2,
    T r,
    T g,
    T b , T a);
    
    void generateSDF(int reach = 8);
    void generateQSDF(int reach = 12);
    private:
    
    inline void plot(unsigned short x, unsigned short y, float str);
    Color<C, T> useColor;
    };

    typedef Image<3> Image3;
    typedef Image<4> Image4;
    typedef Image<3> ImageRGB;
    typedef Image<4> ImageRGBA;

};
#endif
