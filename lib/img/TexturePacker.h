#pragma once

#include <cstddef>
#include <iostream>
#define TP_IMGID_NULL 0xFFFFFFFF

struct TPImage {
  unsigned int width, height;
};

class PackingNode {
public:
  PackingNode* children[2];
  unsigned int x, y, w, h;
  unsigned int imageID;
  
  PackingNode();
  ~PackingNode();
  
  char Contains(unsigned int width, unsigned int height);
  inline void SetConf(unsigned int nX, unsigned int nY, unsigned int nW, unsigned int nH) {
    x = nX; y = nY; w = nW; h = nH;
  };
  
  PackingNode* Insert(const TPImage &val);
};

class TexturePacker {
public:
  unsigned int width, height;
  
  inline TexturePacker() { width = height = 0; root = NULL; };
  TexturePacker(unsigned int w, unsigned int h);
  
  inline ~TexturePacker() { if(root != NULL) delete root; }
  
  bool Allocate(unsigned int w, unsigned int h, unsigned int &sX, unsigned int &sY, unsigned int &eX, unsigned int &eY);
  
private:
  PackingNode *root;
};