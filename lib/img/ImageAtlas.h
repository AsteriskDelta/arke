#ifndef IMAGEATLAS_H
#define IMAGEATLAS_H
#include "Image.h"
#include <deque>
#include <map>
#include <unordered_map>
#include <list>
#include <cstddef>
#include <sstream>
#include <iostream>
#define TP_IMGID_NULL 0xFFFFFFFF

namespace arke {

    class Directory;

    struct TPImage {
    unsigned int width, height;
    };

    class ImageAtlas {
    public:
        typedef unsigned int IDT;
        
        class PackingNode {
        public:
        PackingNode* children[2];
        unsigned int x, y, w, h;
        unsigned int imageID, nodeID;

        PackingNode();
        ~PackingNode();

        char Contains(unsigned int width, unsigned int height);
        inline void SetConf(unsigned int nX, unsigned int nY, unsigned int nW, unsigned int nH) {
            x = nX; y = nY; w = nW; h = nH;
        };

        PackingNode* Insert(const TPImage &val);
        
        unsigned int giveID(unsigned int s);
        std::string toString(unsigned int pid);
        void streamWrite(std::stringstream *ss, unsigned int pid);
        
        inline void cacheAddChild(PackingNode *n) {
            if(children[0] == NULL) children[0] = n;
            else if(children[1] == NULL) children[1] = n;
            else {
                std::cerr << "cacheAddChild called with 2 children!\n";
            }
        }
        };
        
        struct Layer {
            ImageRGBA *img;
            PackingNode *root;
            
            void saveCache(Directory *dir, std::string n);
            void loadCache(Directory *dir, std::string n);
        };
        
        struct LookupFP {
            float x,y,w,h;
            float ox, oy, ow, oh;
        };
        struct Lookup {
            unsigned short x, y, w, h;
            unsigned short ox, oy, ow, oh;
            unsigned short selfID, layer;
            
            inline LookupFP asFP(unsigned short sx, unsigned short sy) {
                LookupFP ret; float fsx = 1.0/float(sx), fsy = 1.f/float(sy);
                ret.x = x * fsx; ret.y = y * fsy;
                ret.w = w * fsx; ret.h = h * fsy;
                ret.ox = ox*fsx; ret.oy = oy*fsy;
                ret.ow = ow*fsx; ret.oh = oh*fsy;
                return ret;
            }
            
            void fromString(std::string s);
            std::string toString();
        };
        //Resourcelist data
        struct RS {
            unsigned long long hash;
            std::list<IDT> list;
        };
        
        ImageAtlas();
        ImageAtlas(unsigned int w, unsigned int h, unsigned int l = 0);
        virtual void setDimensions(unsigned int w, unsigned int h, unsigned int l = 0);
        virtual ~ImageAtlas();
        
        //One-to-one resource emplace, and one-to-multiple resource
        IDT emplace(ImageRGBA *img, bool allowMinimize = true);
        IDT emplace(std::string resourceName, ImageRGBA* img, bool allowMinimize = true);
        IDT emplaceMulti(std::string resourceName, ImageRGBA* img, bool allowMinimize = true);
        Lookup* find(IDT id);
        Lookup* find(std::string resourceName);
        
        bool hasResource(std::string name);
        
        void setName(std::string newName);
        void useCache();
        void saveCache();
        bool loadCache();
        
        inline void setPadding(unsigned short newPadding) { padding = newPadding; };
        
        //TODO:
        //void optimize();
        
        std::string name;
        
        std::deque<Layer> layers;
        std::map<IDT, Lookup> lookupMap;
        std::unordered_map<std::string, RS> resourceList;
    protected:
        unsigned int width, height, layerMax;
        unsigned int idCounter;
        std::string atlasName;
        bool cached, cacheDirty;
        unsigned short padding;
        
        void pushLayer();
        IDT addLookup(PackingNode *n);
        
        std::string getCachePath();
        RS* getResource(std::string name);
    };

};

#endif /* IMAGEATLAS_H */
