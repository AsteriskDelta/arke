#include "ImageFont.h"
#define POINT_PER_PIX 64
#include "AppData.h"
#include <sstream>
#include "Console.h"

namespace arke {
    static FT_Library library;

    bool ImageFont::initialized = false;

    ImageFont::ImageFont(std::string p, unsigned int pt) {
    if(!initialized) ImageFont::Initialize();
    
    for (unsigned int i = 0; i <= 255; i++) {
        glyphs[i].img = NULL;
    }
    
    path = p;
    if((data.u = AppData::base.readFile(path,length, true)) == NULL) {
        std::stringstream ss; ss<< "can't open font at " << p <<"\n";
        Console::Error(ss.str());
        return;
    }
    
    int error = FT_New_Memory_Face(library, (const unsigned char*)data.u, length, 0, &face );
    if(error) {
        std::stringstream ss; ss<< "FT Face memory allocation err #" << error <<"\n";
        Console::Error(ss.str());
        return;
    }
    
    unsigned short fontSize = size = pt;
    error = FT_Set_Pixel_Sizes(face, fontSize, fontSize );
    /*int imgSize = tex.size = npot((int)fontSize * 12);//Constant representing ASCII chars per row
    
    if(imgSize > fontImgSize) {
        Console::Error("Font size requested is too large to fit on texture!");
        return -1;
    }*/
    
    if (error) {
        std::stringstream ss; ss<<  "Unable to set font size at path \"" << path << "\", error code # " << error <<" for size " << fontSize << "!";
        Console::Error(ss.str());
        return;
    }
    //int err;
    

    //unsigned short maxHeight = 0, maxWidth = 0;
    for (unsigned char i = 32; i <= 126; i++) {
        if (true) {
        glyphs[i].index = FT_Get_Char_Index(face, (char) i);
        glyphs[i].img =NULL;
        unsigned int index = glyphs[i].index;
        //if(index == 4 || index == 10 || index == 17 || index == 67 || index == 6 || index == 7 || index == 5 || index == 66) continue;
        //unsigned int sX, sY, eX, eY;

        FT_Glyph glyph;

        // Load the glyph into the font face.
        FT_Load_Glyph(face, index, FT_LOAD_RENDER);
        FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
        FT_Get_Glyph(face->glyph, &glyph);

        // Small shortcuts
        FT_GlyphSlot slot = face->glyph;
        FT_Bitmap& bitmap = slot->bitmap;
        uint32_t w = bitmap.width;
        uint32_t h = bitmap.rows;

        glyphs[i].w = slot->metrics.width / POINT_PER_PIX;
        glyphs[i].h = slot->metrics.height / POINT_PER_PIX;
        glyphs[i].bX = glyphs[i].oX = slot->metrics.horiBearingX / POINT_PER_PIX;
        glyphs[i].bY = slot->metrics.height / POINT_PER_PIX; //slot->metrics.horiBearingY / POINT_PER_PIX;
        glyphs[i].oY = 0; //(slot->metrics.height - slot->metrics.horiBearingY) / POINT_PER_PIX;//glyphs[i].bY - glyphs[i].h;
        glyphs[i].adv = slot->metrics.horiAdvance / POINT_PER_PIX;

        if (slot->metrics.height > 0) {
            glyphs[i].bY += fontSize - (slot->metrics.horiBearingY / POINT_PER_PIX);
            glyphs[i].oY += fontSize - (slot->metrics.horiBearingY / POINT_PER_PIX);
        }
        
        unsigned char* data = new unsigned char[w * h];
        memset(data, 0x00, w * h *  sizeof(unsigned char));
        memcpy(data, bitmap.buffer, sizeof(unsigned char) * w * h);
        
        ImageRGBA* gimg = glyphs[i].img = new ImageRGBA(bitmap.width, bitmap.rows);
        //std::cout << "glyph: " << i << ", idx: " << index << ", " << bitmap.width << "x" << bitmap.rows<<" adv: " << glyphs[i].adv<< "\n";
        /*for(int h = 0; h < bitmap.width* bitmap.rows; i++) {
            std::cout << "buf: " << data[h] << "\n";
            glyphs[i].img->data[h] = ColorRGBA(0, 0, 0, data[h]);
        }*/
        for(unsigned int f = 0; f < w*h; f++) {
            gimg->setPixel(f%w,f/w, ColorRGBA(0,0,0,data[f]));
        }
        
        FT_Done_Glyph(glyph);
        }
    }
    }

    ImageFont::~ImageFont() {
    for (unsigned int i = 0; i <= 255; i++) {
        if(glyphs[i].img != NULL) delete glyphs[i].img;
    }
    }

    void ImageFont::write(ImageRGBA* img, unsigned short x, unsigned short y, std::string text) {
    unsigned short cx = x, cy = y;
    for(unsigned int i = 0; i < text.size(); i++) {
        GlyphData *g = &glyphs[(unsigned int)text[i]];
        
        //std::cout << "Writing " << text[i] << " at " << cx << ", " << cy <<":" << g->oX << ", " << g->oY << "\n";
        
        if(g->img != NULL) img->overlay(cx+g->oX,cy+g->oY, g->img);
        cx += g->adv;
    }
    }

    ImageRGBA* ImageFont::renderGlyph(char c, unsigned int w, unsigned int h, unsigned int x, unsigned int y) {
    GlyphData *g = &glyphs[(unsigned int)c];
    if(w < g->img->width || h < g->img->height) {
        w = g->img->width;
        h = g->img->height;
    }
    
    if(g->img == NULL) return NULL;
    
    ImageRGBA *ret = new ImageRGBA(w, h);
    ret->clear(ColorRGBA(0,0,0,0));
    this->write(ret, x, y, std::string(1, c));
    return ret;
    }
    ImageRGBA* ImageFont::renderGlyphSDF(char c, unsigned int w, unsigned int h, unsigned int x, unsigned int y, unsigned int reach) {
    ImageRGBA *ret = this->renderGlyph(c,w,h,x,y);
    if(ret == NULL) return ret;
    ret->generateSDF(reach);
    return ret;
    }
    ImageRGBA* ImageFont::renderGlyphQSDF(char c, unsigned int w, unsigned int h, unsigned int x, unsigned int y, unsigned int reach) {
    ImageRGBA *ret = this->renderGlyph(c,w,h,x,y);
    if(ret == NULL) return ret;
    ret->generateQSDF(reach);
    return ret;
    }

    bool ImageFont::Initialize() {
    int error = FT_Init_FreeType(&library);
    initialized = true;
    if(error) return false;
    
    return true;
    }

};
