#ifndef COLOR_INC
#define COLOR_INC
#include "ARKEBase.h"
#include <string>
#include <cmath>
#include <limits>
#include <iostream>
#include <type_traits>
#include <array>
#include <unordered_map>

template <int C = 3, typename T = unsigned char> class Color;

#ifndef FUNC_SATS
#define FUNC_SATS
template<typename T> T satAdd(T first, T second) {
  return first > std::numeric_limits<T>::max() - second ? std::numeric_limits<T>::max() : first + second;
}
template<typename T> T satSub(T first, T second) {
  return second > first ? std::numeric_limits<T>::min() : first - second;
}
#endif

#define CLR_RERG(x, T) ((T)(x))

namespace arke {

    template<int C, typename T = uint8_t>
    class Color {
    public:
        union {
            T v[C];
            struct { T r,g,b; };
        };
        inline constexpr Color();
        inline constexpr Color(T nr, T ng, T nb);
        inline constexpr Color(T nr, T ng, T nb, T na);
        
        inline constexpr Color(T val);
        Color(const std::string& hex);
        
        inline operator Color<3, T>() const {
            return Color<3, T>(v[0], v[1], v[2]);
        }
        
        inline Color operator+(const Color<C, T> &o) const;
        inline Color operator-(const Color<C, T> &o) const;
        inline void operator+=(const Color<C, T> &o)  {
            for(int i = 0; i < C; i++) v[i] = CLR_RERG(satAdd(v[i], o.v[i]), T);
        }
        inline void operator-=(const Color<C, T> &o)  {
            for(int i = 0; i < C; i++) v[i] = CLR_RERG(satSub(v[i], o.v[i]), T);
        }
        
        inline Color operator*(const float& m) const;
        inline Color operator/(const float& m) const;
        inline void operator*=(const float& m)  {
            for(int i = 0; i < C; i++) v[i] = CLR_RERG(v[i] * m, T);
        }
        
        inline bool operator==(const Color<C, T> &o) const {
            for(int i = 0; i < C; i++) { if(v[i] != o.v[i]) return false; };
            return true;
        }
        inline bool operator!=(const Color<C, T> &o) const {
            for(int i = 0; i < C; i++) { if(v[i] != o.v[i]) return true; };
            return false;
        }
        inline Color lerped(const Color& o, float t) const;
        inline void  lerp  (const Color& o, float t);
        
        inline float getAlpha() const {
            if(C < 4) return 1.f;
            else return float(v[3]) / 255.f;
        }
        inline void setAlpha(const float& f) {
            if(C < 4) return;
            v[3] = ::round(f * 255.f);
        }
        inline T alpha() const {
            if(C < 4) return 255;
            else return v[3];
        }
        inline T& alphaRef() {
            if(C < 4) return v[0];
            else return v[3];
        }
        
        inline float flt(unsigned char i) const {
            return float(v[i]) / float(std::numeric_limits<T>::max());
        }
        
        inline Color altA(float m) const {
            Color<C, T> ret = *this;
            if(C > 3) ret.v[3] *= m;
            return ret;
        }
        
        inline void overlay(const Color<C, T>& o) {
            if(C < 4) { for(int i = 0; i < C; i++) v[i] = o.v[i]; return;};
            
            if(this->alpha() == 0) {
                *this = o;
                return;
            }
            
            //const float srcAlpha = o.getAlpha(), thisAlpha = this->getAlpha();
            /*const T newAlpha = CLR_RERG(satAdd(v[3], o.v[3]), T);
            for(int i = 0; i < C; i++) v[i] = (1.f-srcAlpha)*v[i] + srcAlpha*o.v[i];
            v[3] = newAlpha;*/
            //this->lerp(o, srcAlpha / (srcAlpha + thisAlpha));
            this->lerp(o, o.getAlpha());
        }
        
        std::string hex();
        std::string toString();
        std::string prettyName() const;
    };

    template<int C, typename T>
    inline constexpr Color<C, T>::Color(T val) : v{0} {
    for(int i = 0; i < C; i++) v[i] = val;
    }

    template<int C, typename T>
    inline constexpr Color<C, T>::Color() : v{0} {
    for(int i = 0; i < C; i++) v[i] = 0;
    if(C > 3) v[3] = 255;//Max Alpha
    }
    template<int C, typename T>
    inline constexpr Color<C, T>::Color(T nr, T ng, T nb) : v{nr, ng, nb} {
    //v[0] = nr; v[1] = ng; v[2] = nb;
    //for(int i = 3; i < C; i++) v[i] = 0;
    if(C > 3) v[3] = 255;//Max Alpha
    }
    template<int C, typename T>
    inline constexpr Color<C, T>::Color(T nr, T ng, T nb, T na) : v{nr,ng,nb} {
    //static_assert((C > 3), "Four or more channels must be present for an alpha constructor!");
    //v[0] = nr; v[1] = ng; v[2] = nb;
    if(C > 3) v[3] = na;
    }

    template<int C, typename T>
    inline Color<C, T> Color<C, T>::operator+(const Color<C, T> &o) const {
    Color ret;
    for(int i = 0; i < C; i++) ret.v[i] = CLR_RERG(v[i] + o.v[i], T);
    return ret;
    }

    template<int C, typename T>
    inline Color<C, T> Color<C, T>::operator-(const Color<C, T> &o) const {
    Color ret;
    for(int i = 0; i < C; i++) ret.v[i] = CLR_RERG(v[i] - o.v[i], T);
    return ret;
    }

    template<int C, typename T>
    inline Color<C, T> Color<C, T>::operator*(const float& m) const {
    Color ret;
    for(int i = 0; i < C; i++) ret.v[i] = CLR_RERG(v[i] * m, T);
    return ret;
    }
    template<int C, typename T>
    inline Color<C, T> Color<C, T>::operator/(const float& m) const {
    Color ret;
    for(int i = 0; i < C; i++) ret.v[i] = CLR_RERG(v[i] / m, T);
    return ret;
    }

    template<int C, typename T>
    inline Color<C, T> Color<C, T>::lerped(const Color& o, float t) const {
    Color ret; const float m = 1.0f - t;
    for(int i = 0; i < C; i++) ret.v[i] = t * v[i] + o.v[i] * m;
    return ret;
    }

    template<int C, typename T>
    inline void Color<C, T>::lerp(const Color& o, float t) {
    const float m = 1.0f - t;
    for(int i = 0; i < C; i++) v[i] = t * v[i] + o.v[i] * m;
    }

    typedef Color<3> Color3;
    typedef Color<4> Color4;
    typedef Color<3> ColorRGB;
    typedef Color<4> ColorRGBA;
    typedef Color<3> C3;
    typedef Color<4> C4;
    
    
    class NamedColor : public Color<4, uint8_t> {
    public:
        static constexpr const unsigned int NameSize = 16;
        typedef std::array<char, NameSize> Name;
        Name name;
        
        inline constexpr NamedColor(const Name &arr, uint8_t nr, uint8_t nb, uint8_t ng, uint8_t na = 0) : ColorRGBA(nr,ng,nb,na), name(arr) {
        }
        inline constexpr NamedColor(const Name &arr, uint8_t vl) : ColorRGBA(vl), name(arr) {
        }
        
        NamedColor(const std::string& name, const std::string& code);
        NamedColor(const std::string& name);//Create an instance from an existing named-color
        
        static void Register(const NamedColor& col, bool addMutations = true);//mutations = light-col, dark-col, etc.
    private:
        static std::unordered_map<std::string, const NamedColor*> nameMapping;
        static std::unordered_map<ColorRGBA, const NamedColor*> valueMapping;
    };
    
    
    namespace Colors {
        constexpr const char LightPrefix[] = "light", DarkPrefix[] = "dark";
        
        constexpr NamedColor
        Red         = NamedColor(NamedColor::Name{"red"}, 255, 0, 0),
        Lime        = NamedColor(NamedColor::Name{"lime"}, 0,255,0),
        Blue        = NamedColor(NamedColor::Name{"blue"}, 0, 0, 255),
        White       = NamedColor(NamedColor::Name{"white"}, 255),
        Black       = NamedColor(NamedColor::Name{"black"}, 0,0,0,255),
        Magenta     = NamedColor(NamedColor::Name{"magenta"}, 255, 0, 255),
        Yellow      = NamedColor(NamedColor::Name{"yellow"}, 255, 255, 0),
        Cyan        = NamedColor(NamedColor::Name{"cyan"}, 0, 255, 255),
        Silver      = NamedColor(NamedColor::Name{"silver"}, 192,192,192),
        Grey        = NamedColor(NamedColor::Name{"grey"}, 128,128,128),
        Maroon      = NamedColor(NamedColor::Name{"maroon"}, 128,0,0),
        Olive       = NamedColor(NamedColor::Name{"olive"}, 128,128,0),
        Green       = NamedColor(NamedColor::Name{"green"}, 0, 128, 0),
        Purple      = NamedColor(NamedColor::Name{"purple"}, 128,0,128),
        Teal        = NamedColor(NamedColor::Name{"teal"}, 0, 128, 128),
        Navy        = NamedColor(NamedColor::Name{"navy"}, 0,0,128);
        
        NamedColor Resolve(const std::string& text);
        //NamedColor Nearest(const ColorRGBA& col);
    };
};


#ifdef CEREAL_INCLUDED
template<class Archive>
void serialize(Archive& archive, ColorRGBA& m) {
  archive(cereal::make_nvp("r",m.v[0]), 
          cereal::make_nvp("g",m.v[1]),
          cereal::make_nvp("b",m.v[2]),
          cereal::make_nvp("a",m.v[3])
          );
}

template<class Archive>
void serialize(Archive& archive, ColorRGB& m) {
  archive(cereal::make_nvp("r",m.v[0]), 
          cereal::make_nvp("g",m.v[1]),
          cereal::make_nvp("b",m.v[2])
          );
}
#endif

#endif
