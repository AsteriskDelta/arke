#include "Color.h"
#include <string>
#include <cstring>
#include <sstream>
#include <iomanip>
#include <stdio.h>

namespace arke {

template<int C, typename T>
Color<C, T>::Color(const std::string& hex) {
  unsigned short strOff = 0;
  const unsigned short strLen = hex.length();
  const char *const str = hex.c_str();
  if(hex[0] == '0'&&hex[1] == 'x') strOff += 2;
  
  const unsigned char hexLen = sizeof(T)*2;
  char buffer[hexLen];//For hex 2h->1b
  unsigned int val; unsigned short c = 0;
  while(strOff < strLen && strOff + hexLen <= strLen && c < C) {
    memcpy(buffer, str+strOff, hexLen);
    sscanf(buffer, "%x", &val);
    memcpy(&v[c], &val, sizeof(T));//WARNING: Little endian systems ONLY
    strOff += hexLen; c++;
  }
}

template<int C, typename T>
std::string Color<C, T>::hex() {
  std::stringstream ss;
  ss << std::hex << std::setw(2) << std::setfill('0');
  for(unsigned char i = 0; i < C; i++) {
    ss << this->v[i];
  }
  return ss.str(); 
}

template<int C, typename T>
std::string Color<C, T>::toString() { 
  std::stringstream ss;
  ss << "Color(";
  for(unsigned char i = 0; i < C; i++) {
    if(i != 0) ss << ", ";
    ss << ((int)this->v[i]);
  }
  ss << ")";
  return ss.str();
};

//Explcit Definition:
template class Color<3, unsigned char>;
template class Color<4, unsigned char>;
template class Color<3, unsigned short>;
template class Color<4, unsigned short>;

};
