#ifdef __cplusplus
    #define PCAST(type, pointer) reinterpret_cast<type>(pointer)
#else
    #define PCAST(type, pointer) (type)(pointer)
#endif

#include "DebugImage.h"
#include <stdio.h>
/*
#include "gd.h"
#include <cmath>

static gdImagePtr* img;

DebugImage::DebugImage(unsigned short newWidth, unsigned short newHeight) {
  width = newWidth; height = newHeight;
  img = new gdImagePtr;
  *img = gdImageCreateTrueColor(width, height);
  gdImageColorAllocate(*img, 255, 255, 255);
  
  image = PCAST(void*, img);
}

DebugImage::~DebugImage() {
  img = PCAST(gdImagePtr*, image);
  if(img != NULL) {
    gdImageDestroy(*img);
    delete img;
  }
}

void DebugImage::line(int sx, int sy, int ex, int ey, int color) {
  img = PCAST(gdImagePtr*, image);
  gdImageLine(*img, sx, sy, ex, ey, color);
}
void DebugImage::point(int x, int y, int color) {
  img = PCAST(gdImagePtr*, image);
  gdImageFilledArc(*img, x, y, 6, 6, 0, 360, color, gdArc);
}

void DebugImage::pixel(int x, int y, int color) {
  img = PCAST(gdImagePtr*, image);
  gdImageSetPixel(*img, x, y, color);
}*/
/*
void DebugImage::BKLine(float x1, float y1, float x2, float y2, int color )
{
  // Bresenham's line algorithm
  const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
  if(steep)
  {
    std::swap(x1, y1);
    std::swap(x2, y2);
  }
  
  if(x1 > x2)
  {
    std::swap(x1, x2);
    std::swap(y1, y2);
  }
  
  const float dx = x2 - x1;
  const float dy = fabs(y2 - y1);
  
  float error = dx / 2.0f;
  const int ystep = (y1 < y2) ? 1 : -1;
  int y = (int)y1;
  
  const int maxX = (int)x2;
  
  for(int x=(int)x1; x<maxX; x++)
  {
    if(steep)
    {
      Pixel(y,x, color);
    }
    else
    {
      Pixel(x,y, color);
    }
    
    error -= dy;
    if(error < 0)
    {
      y += ystep;
      error += dx;
    }
  }
}*/
/*
int DebugImage::getColor(unsigned char r, unsigned char g, unsigned char b) {
  img = PCAST(gdImagePtr*, image);
  return gdImageColorAllocate(*img, r, g, b);
}

void DebugImage::write(const std::string& name) {
  FILE *pngOut = fopen(name.c_str(), "wb");
  gdImagePng(*img, pngOut);
  fclose(pngOut);
}*/