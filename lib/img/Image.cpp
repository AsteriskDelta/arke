#include "Image.h"
#include "Console.h"
#include "AppData.h"

#include <cstdlib>
#include <sstream>
//#include "ext/stbi.c"
#include "ext/stb/stb_image.h"
#include "ext/stb/stb_image_write.h"
#include "ext/stb/stb_image_resize.h"

namespace arke {
    template<int C, typename T> Image<C, T>::Image(unsigned short w, unsigned short h) {
    width = w; height = h;
    dChannels = C; data = NULL;
    Allocate();
    }

    template<int C, typename T> Image<C, T>::Image(std::string path, bool isRelPath) {
    data = NULL; width = height = 0;
    this->Load(path, isRelPath);
    }

    template<int C, typename T> Image<C, T>::~Image() {
    Deallocate();
    }

    //Allocation block
    template<int C, typename T> void Image<C, T>::Allocate() {
    if(data != NULL) Deallocate();
    
    //data = new Color<C, T>[width * height];
    data = (Color<C, T>*)malloc(width * height * sizeof(Color<C, T>));
    }

    template<int C, typename T> void Image<C, T>::Deallocate() {
    if(data == NULL) return;
    free(data);
    //delete[] data;
    data = NULL;
    }

    //Read/Write block
    template<int C, typename T> bool Image<C, T>::Load(std::string path, bool isRelPath) {
    Directory *dir;
    if(isRelPath) dir = &AppData::base;
    else dir = &AppData::system;
    
    return this->Load(dir, path);
    }

    template<int C, typename T> bool Image<C, T>::Save(std::string path) {
    Directory *dir = &AppData::base;
    return this->Save(dir, path);
    }

    template<int C, typename T> bool Image<C, T>::Load(Directory *dir, std::string path) {
    int x = 0, y = 0, comp = 0;
    unsigned char* buffer = NULL; unsigned int bufferLen = 0;
    
    //std::cout << "existsim: " << AppData::base.exists(path) << " at dir:\"" << AppData::base.str() << "\"\n";
    //std::cout << "loadim " << dir->exists(path) << ", " <<dir->resolve(path) << " : " << dir->str() << "\n";
    
    buffer = dir->readFile(path, bufferLen);
    if(buffer != NULL && bufferLen > 0) {
        data = (Color<C, T>*)stbi_load_from_memory(buffer, bufferLen, &x, &y, &comp, (int)C);
        delete[] buffer;
    }
    if(data == NULL || x == 0 || y == 0) {
        std::stringstream ss; ss << "Unable to load image at \"" << path << "\"";
        Console::Error(ss.str());
        if(data != NULL) stbi_image_free(data);
        return false;
    }
    width = x; height = y; dChannels = comp;
    
    return true;
    }
    template<int C, typename T> bool Image<C, T>::Save(Directory *dir, std::string path) {
    unsigned short lDot = path.find_last_of(".");
    std::string ext = path.substr(lDot+1, path.length() - lDot - 1);
    int ret = 0; int rowStride = width * C;
    
    path = dir->resolve(path, true);
    
    if(ext == "png") {
        ret = stbi_write_png(path.c_str(), width, height, (int)C, data, rowStride);
    } else if(ext == "bmp") {
        ret = stbi_write_bmp(path.c_str(), width, height, (int)C, data);
    } else if(ext == "tga") {
        ret = stbi_write_tga(path.c_str(), width, height, (int)C, data);
    }/* else if(ext == "hdr") {
        ret = stbi_write_hdr(path.c_str(), width, height, (int)C, data);
    } */else {
        std::stringstream ss; ss << "Unknown format (" << ext << ") requested when writing to \"" << path << "\"";
        Console::Error(ss.str());
        return false;
    }
    
    if(ret == 0) {
        std::stringstream ss; ss << "Unable to save image to \"" << path << "\"";
        Console::Error(ss.str());
        return false;
    } else return true;
    }

    template<int C, typename T> void Image<C, T>::generateSDF(int reach) {
    const float reach2 = sqrt(2*reach*reach);
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
        //float startAlpha = this->getAlpha(x, y)/255.f;
        float dist = 9999.f; 
        bool invertDist = false;
        if(this->getAlpha(x, y) == 255) {
            invertDist = true;
        }
        
        //Note we PURPOSEFULLY do this without double-buffer, and divide the distance by 2 (2^2=4). This produces an equivalent result with 1/4 the cost and zero mem overhead
        for(int xp = -reach; xp <= reach; xp++) {
            for(int yp = -reach; yp <= reach; yp++) {
            int ex = x + xp, ey = y + yp;
            if(ey < 0 || ey >= height || ex < 0 || ex >= width || (xp == 0 && yp == 0)) continue;
            
            float pxDist = float(xp*xp + yp*yp);//float(xp*xp) + float(yp*yp);
            if(xp < 0) pxDist /= 4.f;
            
            float alpha = float(this->getAlpha(ex, ey))/ 255.f;
            if(invertDist) alpha = 1.f - alpha;
            
            if(alpha > 0.5f) pxDist *= 1.f / (alpha*alpha);
            else pxDist = 9999.f;
            
            //if(alpha < 0.5f) pxDist = 9999.f;
            
            if(pxDist < dist) dist = pxDist;
            }
        }
        
        //Write the distance
        unsigned char writeDist = 127;
        dist = clamp(float(sqrt(dist) - 1.f) / reach2, 0.f, 1.f);
        if(invertDist) {
            writeDist = 128 + ::round(127.f * dist);
        } else {
            writeDist = 127 - ::round(127.f * dist);
        }
        this->setAlpha(x, y, writeDist);
        }
    }
    
    }

    template<int C, typename T> void Image<C, T>::generateQSDF(int reach) {
    const float reach2 = sqrt(2*reach*reach);
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
        //float startAlpha = this->getAlpha(x, y)/255.f;
        float dist = 9999.f, xDist = 9999.f, yDist = 9999.f; 
        bool invertDist = false;
        if(this->getAlpha(x, y) == 255) {
            invertDist = true;
        }
        
        //Note we PURPOSEFULLY do this without double-buffer, and divide the distance by 2 (2^2=4). This produces an equivalent result with 1/4 the cost and zero mem overhead
        for(int xp = -reach; xp <= reach; xp++) {
            for(int yp = -reach; yp <= reach; yp++) {
            int ex = x + xp, ey = y + yp;
            if(ey < 0 || ey >= height || ex < 0 || ex >= width) continue;
            
            float pxDist = float(xp*xp + yp*yp);//float(xp*xp) + float(yp*yp);
            if(xp < 0) pxDist /= 4.f;
            
            float alpha = float(this->getAlpha(ex, ey))/ 255.f;
            if(invertDist) alpha = 1.f - alpha;
            
            if(alpha > 0.5f) pxDist *= 1.f / (alpha*alpha);
            else pxDist = 9999.f;
            
            //if(alpha < 0.5f) pxDist = 9999.f;
            
            if(pxDist < dist && !(xp == 0 && yp == 0)) {
                dist = pxDist;
                xDist = xp;
                yDist = yp;
            } else if(invertDist) {
                xDist = 0;
                yDist = 0;
            }
            }
        }
        
        //Write the distance
        unsigned char writeDist = 127;
        dist = clamp(float(sqrt(dist) - 1.f) / reach2, 0.f, 1.f);
        if(invertDist) {
            writeDist = 128 + ::round(127.f * dist);
        } else {
            writeDist = 127 - ::round(127.f * dist);
        }
        
        unsigned char wxDist = ::round((127.f + clamp((float)fabs(xDist), -float(reach), float(reach))/reach*127.f));
        unsigned char wyDist = ::round((127.f + clamp((float)fabs(yDist), -float(reach), float(reach))/reach*127.f));
        
        this->setAlpha(x, y, writeDist);
        this->setChannel(x,y,0,wxDist);
        this->setChannel(x,y,1,wyDist);
        }
    }
    
    }

    template<int C, typename T> void Image<C, T>::drawPoint(unsigned short x, unsigned short y, const Color<C, T>& c, unsigned char r) {
        int effX = x, effY = y;
        for(int ox = -r-1; ox <= r+1; ox++) {
            for(int oy = -r-1; oy <= r+1; oy++) {
                Color<C,T> col = c;
                float dist = sqrt(ox*ox + oy*oy);
                int iDist = dist;
                float str;
                
                if(iDist+1 < r) str = 1.0;
                else if(dist <= float(r+1)) str = max(0.f, min(1.f, (dist - float(iDist))/float(sqrt(2.f))));
                else continue;
                
                col.alphaRef() = 255;//str * 255;//setAlpha(str*str);
                this->setPixelSafe(x + ox, y + oy, c);
            }
        }
    }

    template<int C, typename T> void Image<C, T>::debugAlpha() {
    for(int x = 0; x < width; x++) {
        for(int y = 0; y < height; y++) {
        Color<C, T>* pix = &data[x+y*width];
        unsigned char val = this->getAlpha(x, y);
        for(int i = 0; i < C; i++) {
            pix->v[i] = val;
        }
        if(C > 3) pix->v[3] = 255;
        }
    }
    }
    //Query functions

    template<int C, typename T> 
    Image<C,T>* Image<C,T>::getSlice(unsigned int x, unsigned int y, int w, int h) {
    Image<C,T> *ret = new Image<C,T>(w, h);
    ret->cloneFrom(this, x,y,w,h);
    return ret;
    }

    //Drawing functions

    template<int C, typename T>
    void Image<C, T>::flip(bool flipX, bool flipY) {
        Color<C,T>* newData = (Color<C,T>*)malloc(width*height*sizeof(Color<C,T>));
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                const int ex = flipX? (width-x-1) : x, ey = flipY? (height-y-1) : y;
                const unsigned int origIdx = x + y*width;
                const unsigned int pixelIdx = ex + ey * width;
                
                Color<C, T>* fromPix = &data[origIdx];
                Color<C, T>* toPix = &newData[pixelIdx];
                *toPix = *fromPix;
            }
        }
        //free(data);
        this->Deallocate();
        data = newData;
    }

    inline float fpart(float x) {
    if(x < 0.f) return 1.f - (x - ::floor(x));
    else return x - ::floor(x);
    }
    inline float rfpart(float x) {
    return 1.f - fpart(x);
    }

    template<int C, typename T> inline void Image<C, T>::plot(unsigned short x, unsigned short y, float str) {
    drawPixel(x, y, useColor.altA(str));
    }
    template<int C, typename T> void Image<C, T>:: _dla_changebrightness(Color<C,T> *from,
                    Color<C,T> *to, float br)
    {
    if ( br > 1.0 ) br = 1.0;
    //br = 1.0f - br;
    /* linear... Maybe something more complex could give better look */
    to->r = from->r;//br * (float)from->r;
    to->g = from->g;//br * (float)from->g;
    to->b = from->b;//br * (float)from->b;
    to->v[3] = (1.0 - br) * float(from->v[3]);
    }
    
    #define plot_(X,Y,D) do{ Color<C,T> f_;				\
    f_.r = r; f_.g = g; f_.b = b;	f_.v[3] = a;		\
    _dla_plot((X), (Y), f_, (D)) ; }while(0)
    
    template<int C, typename T> void Image<C, T>::_dla_plot(int x, int y, Color<C,T> col, float br) {
    Color<C,T> oc;
    _dla_changebrightness(&col, &oc, br);
    this->setPixelSafe(x, y, Color<C,T>(oc.r, oc.g, oc.b, 255/*oc.v[3]*/));
    }

    
    #define ipart_(X) ((int)(X))
    #define round_(X) ((int)(((double)(X))+0.5))
    #define fpart_(X) (((double)(X))-(double)ipart_(X))
    #define rfpart_(X) (1.0-fpart_(X))
    
    #define swap_(a, b) do{ __typeof__(a) tmp;  tmp = a; a = b; b = tmp; }while(0)
    template<int C, typename T> void Image<C, T>::draw_line_antialias(
    unsigned int x1, unsigned int y1,
    unsigned int x2, unsigned int y2,
    T r,
    T g,
    T b, T a )
    {
    double dx = (double)x2 - (double)x1;
    double dy = (double)y2 - (double)y1;
    if ( fabs(dx) > fabs(dy) ) {
        if ( x2 < x1 ) {
        swap_(x1, x2);
        swap_(y1, y2);
        }
        double gradient = dy / dx;
        double xend = round_(x1);
        double yend = y1 + gradient*(xend - x1);
        double xgap = rfpart_(x1 + 0.5);
        int xpxl1 = xend;
        int ypxl1 = ipart_(yend);
        plot_(xpxl1, ypxl1, rfpart_(yend)*xgap);
        plot_(xpxl1, ypxl1+1, fpart_(yend)*xgap);
        double intery = yend + gradient;
    
        xend = round_(x2);
        yend = y2 + gradient*(xend - x2);
        xgap = fpart_(x2+0.5);
        int xpxl2 = xend;
        int ypxl2 = ipart_(yend);
        plot_(xpxl2, ypxl2, rfpart_(yend) * xgap);
        plot_(xpxl2, ypxl2 + 1, fpart_(yend) * xgap);
    
        int x;
        for(x=xpxl1+1; x <= (xpxl2-1); x++) {
        plot_(x, ipart_(intery), rfpart_(intery));
        plot_(x, ipart_(intery) + 1, fpart_(intery));
        intery += gradient;
        }
    } else {
        if ( y2 < y1 ) {
        swap_(x1, x2);
        swap_(y1, y2);
        }
        double gradient = dx / dy;
        double yend = round_(y1);
        double xend = x1 + gradient*(yend - y1);
        double ygap = rfpart_(y1 + 0.5);
        int ypxl1 = yend;
        int xpxl1 = ipart_(xend);
        plot_(xpxl1, ypxl1, rfpart_(xend)*ygap);
        plot_(xpxl1, ypxl1+1, fpart_(xend)*ygap);
        double interx = xend + gradient;
    
        yend = round_(y2);
        xend = x2 + gradient*(yend - y2);
        ygap = fpart_(y2+0.5);
        int ypxl2 = yend;
        int xpxl2 = ipart_(xend);
        plot_(xpxl2, ypxl2, rfpart_(xend) * ygap);
        plot_(xpxl2, ypxl2 + 1, fpart_(xend) * ygap);
    
        int y;
        for(y=ypxl1+1; y <= (ypxl2-1); y++) {
        plot_(ipart_(interx), y, rfpart_(interx));
        plot_(ipart_(interx) + 1, y, fpart_(interx));
        interx += gradient;
        }
    }
    }
    #undef swap_
    #undef plot_
    #undef ipart_
    #undef fpart_
    #undef round_
    #undef rfpart_
    template<int C, typename T> void Image<C, T>::drawLine(unsigned short x0, unsigned short y0, unsigned short x1, unsigned short y1, const Color<C, T>& col, unsigned char w) {
    this->draw_line_antialias(x0,y0,x1,y1,col.r,col.g,col.b, col.v[3]);
    }
    /*
    template<int C, typename T> void Image<C, T>::drawLine(unsigned short x0, unsigned short y0, unsigned short x1, unsigned short y1, const Color<C, T>& col, unsigned char w) {
    if(x0 > x1) {
        std::swap (x0,x1);
        std::swap(y0,y1);
    }
    
    if(w > 1) {
        for(unsigned char i = 0; i < w; i++) {
        
        }
        return;
    }
    
    useColor = col;
    bool steep = abs(y1 - y0) > abs(x1 - x0);
    if(steep) {
        std::swap(x0, y0); std::swap(x1, y1);
    }
    if(x0 > x1) {
        std::swap(x0, x1); std::swap(y0, y1);
    }
    
    unsigned short dx = x1 - x0, dy = y1 - y0;
    float gradient = (float)dy / (float)dx;
    
    // handle first endpoint
    float xend = x0; float yend = y0 + gradient * (xend - x0);
    float xgap = rfpart(x0 + .5f);
    float xpxl1 = xend, ypxl1 = yend;
    if(steep) {
        plot(ypxl1,   xpxl1, rfpart(yend) * xgap);
        plot(ypxl1+1, xpxl1,  fpart(yend) * xgap);
    } else {
        plot(xpxl1, ypxl1  , rfpart(yend) * xgap);
        plot(xpxl1, ypxl1+1,  fpart(yend) * xgap);
    }
    
    float intery = yend + gradient;//first y-intersection for the main loop
    xend = round(x1); yend = y1 + gradient * (xend - x1);
    xgap = fpart(x1 + .5f);
    float xpxl2 = xend, ypxl2 = int(yend);
    if(steep) {
        plot(clamp((unsigned short)ypxl2    , (U16)0, (U16)(width-1)), xpxl2, rfpart(yend) * xgap);
        plot(clamp((unsigned short)(ypxl2+1), (U16)0, (U16)(width-1)), xpxl2,  fpart(yend) * xgap);
    } else {
        plot(xpxl2, clamp((unsigned short)ypxl2    , (U16)0, (U16)(height-1)),  rfpart(yend) * xgap);
        plot(xpxl2, clamp((unsigned short)(ypxl2+1), (U16)0, (U16)(height-1)), fpart(yend) * xgap);
    }
    
    // main loop
    for(int x = xpxl1 + 1.f; x <  xpxl2 - 1.f; x++) {
        if(intery > height-2 || intery < 0) break;
        if(steep) {
        plot(int(intery)  , x, rfpart(intery));
        plot(int(intery)+1, x,  fpart(intery));
        } else {
        plot(x, int(intery),  rfpart(intery));
        plot(x, int(intery)+1, fpart(intery));
        }
        intery += gradient;
    }
    }*/

    //Explcit Definition:
    //template class Image<3, unsigned char>;
    template class Image<4, unsigned char>;
    //template class Image<3, unsigned short>;
    template class Image<4, unsigned short>;

};
