#ifndef ARKE_HTTP_Session_H
#define ARKE_HTTP_Session_H
#include <iostream>
#include <string>
#include <unordered_map>

namespace arke {
  class Session {
  public:
    Session(const std::string& newHash);
    virtual ~Session();

    static constexpr size_t HashLength = 32;
    inline const std::string& hash() const {
        return hash_;
    }


    static Session* Get(const std::string& hash);
    static Session* Create();
    static void Free(Session *session);

    static thread_local Session *Current;
    static std::unordered_map<std::string, Session*> Index;
  protected:
    std::string hash_;
  };
};

#endif
