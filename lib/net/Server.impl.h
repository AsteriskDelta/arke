#include "Server.h"
#include <algorithm>

#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <pistache/net.h>

#include "Session.h"
#include <deque>
using namespace Pistache;

namespace arke {
    class HttpInternal;
    HttpInternal *Internals = nullptr;
/*
    class XAuthToken : public Http::Header::Header
    {
    public:
    	NAME("Access-Control-Allow-Origin");

    	XAuthToken()
    		: token_("")
    	{}

    	explicit XAuthToken(std::string token)
    		: token_(token)
    	{}

    	void parse(const std::string& data);
    	void write(std::ostream& os) const;

    	std::string token() const { return token_; }

    private:
    	std::string token_;
    };
    void XAuthToken::parse(const std::string& data)
    {
    	token_ = data;
    }

    void XAuthToken::write(std::ostream& os) const
    {
    	os << token_;
    }*/

    HttpReply::HttpReply() : JSON() {
        this->blank();
    }

    void printCookies(const Http::Request& req) {
        auto cookies = req.cookies();
        std::cout << "Cookies: [" << std::endl;
        const std::string indent(4, ' ');
        for (const auto& c: cookies) {
            std::cout << indent << c.name << " = " << c.value << std::endl;
            std::cout << "\tExt:\n";
            for (const auto& co: c.ext) {
                std::cout << indent << "\t" << co.first << " = " << co.second << std::endl;
            }
        }

        std::cout << "]" << std::endl;
    }

    class HttpInternal {
    public:
        friend class HttpServer;
        HttpServer *server;

        void forwardCookies(const Rest::Request& req, HttpRequest *target) {
            auto cookies = req.cookies();

            for (const auto& c: cookies) {
                target->cookies.add(c.name, c.value);
            }
        }
        void forwardSplats(const Rest::Request& req, HttpRequest *target) {
            for(auto& splat : req.splat()) {
                target->splats.push_back(splat.as<std::string>());
            }
        }
        void forward(const Rest::Request& req, HttpRequest *target) {
            this->forwardCookies(req,target);
            this->forwardSplats(req,target);
            target->deserialize(req.body());

            target->flags.post = req.method() == Pistache::Http::Method::Post;

            //printCookies(req);

            std::string *sessionIDPtr = target->cookies.get("session-id");
            //std::cout << "got session-id cookie = " << (sessionIDPtr == nullptr?"null":*sessionIDPtr) << "\n";
            if(sessionIDPtr != nullptr) target->session = Session::Get(*sessionIDPtr);

            else target->session = nullptr;

            if(target->session == nullptr) {
                //std::cout << "no session-id, creating a new one...\n";
                target->session = Session::Create();
                target->flags.newSession = true;
            } else {
                target->flags.newSession = false;
                //std::cout << "found existing section\n";
            }
            Session::Current = target->session;
        }

        void mediate(HttpEndpoint *endpoint, const Rest::Request& req, Http::ResponseWriter &response) {
            //auto allowAll = std::make_shared<Http::Header::Header>();
            //allowAll->parse(std::string("Access-Control-Allow-Origin: *"));
            //response.headers().add(allowAll);
            //auto token_header = response.headers().add<XAuthToken>();
	        //std::string t = token_header->token();
            response.headers().add<Http::Header::AccessControlAllowOrigin>("*");

            HttpRequest *request = new HttpRequest();
            this->forward(req, request);
            HttpReply *reply = (*endpoint)(request);
            if(request->flags.newSession) response.cookies().add(Http::Cookie("session-id", request->session->hash()));
            if(reply != nullptr) {
                for(auto& cookie : reply->newCookies) response.cookies().add(Http::Cookie(cookie.first, cookie.second));
                
                response.send(Http::Code::Ok, reply->serialize());
                
                delete reply;
            } else {
                response.send(Http::Code::Ok, "{\"valid\": 0}");
            }
            delete request;
        }
    protected:
        std::shared_ptr<Http::Endpoint> httpEndpoint;
        Rest::Router router;
    };

    HttpServer::HttpServer(uint16_t prt, uint16_t thr) : internals(new HttpInternal()) {
        internals->server = this;
        if(prt != 0) this->initialize(prt, thr);
    }

    /*void Mediate(HttpEndpoint endpoint, const Rest::Request& req, Http::ResponseWriter response) {

    }*/

    struct fake_t {
        void Mediate(const Rest::Request& req, Http::ResponseWriter response) {
            HttpEndpoint *fnPtr = reinterpret_cast<HttpEndpoint*>(this);
            Internals->mediate(fnPtr, req, response);
        }
    };

    std::list<HttpEndpoint> __attribute__((init_priority(900))) CachedEndpoints;
    void HttpServer::endpoint(const std::string& path, HttpEndpoint fnsrc) {
        auto fnr = [fnsrc](HttpRequest *req) -> HttpReply* {
            return fnsrc(req);
        };
        auto fn = HttpEndpoint(fnr);

        CachedEndpoints.push_back(fn);
        HttpEndpoint* fnPtr = &CachedEndpoints.back();
        fake_t *fakePtr = reinterpret_cast<fake_t*>(fnPtr);
        Rest::Routes::Post(internals->router, path, //Rest::Routes::bind(/*HttpInternal::mediate*/std::bind(&HttpInternal::mediate,std::placeholders::_1, fn),  nullptr));
        Rest::Routes::bind(fake_t::Mediate, fakePtr));
#ifndef PRODUCTION
        Rest::Routes::Get(internals->router, path, //Rest::Routes::bind(/*HttpInternal::mediate*/std::bind(&HttpInternal::mediate,std::placeholders::_1, fn),  nullptr));
        Rest::Routes::bind(fake_t::Mediate, fakePtr));
#endif
        std::cout << "Added endpoint at " << path << "\n";
    }

    bool HttpServer::initialize(uint16_t prt, uint16_t thr) {
    	//Http::Header::Registry::registerHeader<XAuthToken>();
        port = prt;
        threadCount = thr;
        Port portObject(port);
        Address addr(Ipv4::any(), portObject);
        internals->httpEndpoint = std::make_shared<Http::Endpoint>(addr);
        auto opts = Http::Endpoint::options()
            .threads(threadCount)
            .flags(Tcp::Options::InstallSignalHandler | Tcp::Options::ReuseAddr);
        internals->httpEndpoint->init(opts);
        Internals = internals;
        return true;
    }
    void HttpServer::prepare() {
        internals->httpEndpoint->setHandler(internals->router.handler());
        //Internals = internals;

        internals->httpEndpoint->serve();
    }

    void HttpServer::terminate() {
        if(internals->httpEndpoint) internals->httpEndpoint->shutdown();
    }
}
