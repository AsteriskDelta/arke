#ifndef ARKE_SERVER_H
#define ARKE_SERVER_H
#include "SmallMap.h"
#include "JSON.h"
#include <functional>

#include <deque>

namespace arke {
        class Session;
}

namespace arke {

    struct HttpRequest : public JSON {
        SmallMap<std::string, std::string> cookies;
        std::vector<std::string> splats;
        Session *session = nullptr;
        struct {
            bool newSession = false;
            bool post = false;
        } flags;
        
        inline bool post() const {
            return flags.post;
        }
    };
    struct HttpReply : public JSON {
        HttpReply();
        SmallMap<std::string, std::string> newCookies;
    };

    typedef std::function<HttpReply*(HttpRequest* request)> HttpEndpoint;

    class HttpInternal;

    class HttpServer {
    public:
        HttpServer(uint16_t prt = 0, uint16_t thr = 0);

        void endpoint(const std::string &path, HttpEndpoint fn);

        uint16_t port, threadCount;
        HttpInternal *internals;
        bool initialize(uint16_t prt, uint16_t thr);
        void prepare();
        void terminate();

        std::deque<HttpEndpoint> endpoints;
    protected:
    };
}

#endif
