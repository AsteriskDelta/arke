#include "ARKEBase.h"

namespace nvx {
    template class NVX2<3, ::arke::nvxi_t>;
    template class NVX2<2, ::arke::nvxi_t>;
};
