#ifndef ARKE_ENGINECLIENT
#define ARKE_ENGINECLIENT

#ifndef USEVK
#define USEGL
#endif

#include "Engine.h"

#include "Color.h"
#include "Image.h"
#include "AppData.h"
#include "Console.h"
#include "Application.h"
#include "ImageFont.h"
#include "ImageAtlas.h"
#include <unistd.h>
//#include "V3.h"
//#include "Matrix.h"

#include "Window.h"
#include "Input.h"
#include "Testing.h"
#include "Camera.h"
#include "Shader.h"
#include "Pipeline.h"
#include "rInterface.h"
#include "Model.h"
#include "PipelineStage.h"
#include "GameManager.h"
#include "rWindow.h"
#include "Timer.h"

#include "TextureAtlas.h"
#include "Skydome.h"
#include "ParticleSystem.h"
#include "Font.h"
#include "FontIndex.h"
#include "FontWriter.h"

#endif
