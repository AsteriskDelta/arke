#ifndef TESTING_INC
#define TESTING_INC
//#include "V3.h"
//#include "V2.h"
#include "Mesh.h"

template <typename T, typename IT> class Mesh;
struct TestVert {
  V3 pos;
  V3 normal;
  V3 tangent;
  V2 texCoord;
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(float));
  }
};
struct TestVertSimple {
  V3 pos;
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
  }
};

class Testing {
public:
  Testing();
  ~Testing();
  
  void DrawSphere();
  void DrawPlane();
  void DrawTriangle();
private:
  void buildSphere();
  void buildPlane();
  void buildTriangle();
  Mesh<TestVert> *sphere;
  Mesh<TestVertSimple> *plane;
  Mesh<TestVertSimple> *triangle;
};

#endif
