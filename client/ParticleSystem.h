/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   ParticleSystem.h
 * Author: Galen Swain
 *
 * Created on April 20, 2016, 7:14 AM
 */

#ifndef PARTICLESYSTEM_H
#define PARTICLESYSTEM_H
#include "client.h"
#include "rInterface.h"
#include "Mesh.h"
#include "Shader.h"
#include "V3.h"
#include "V2.h"
#include "Renderer.h"
#include "TextureAtlas.h"
#include "Texture.h"
#include <vector>
#include <functional>
#include <bitset>

struct ParticleVert {
    V3 pos;
    V3 normal;
    V2 size;
    
    Uint16 figureID;
    Uint8 detA, detB;
    ColorRGBA color;
    V3 right;
    //detA MSB has billboard flag, 0-6 has lifetime
    
    inline ParticleVert() : pos(V3_NULL), normal(V3(0.f)), size(V2(0.f)), figureID(0), detA(0), detB(0), color(ColorRGBA(255,255,255,255)), right(V3(0.f)) {};
    
    inline bool isValid() {
        return pos != V3_NULL;
    }
    inline void makeInvalid() {
        pos = V3_NULL;
    }
    
    void mapAttributes(MeshContainer *const mesh) {
        Uint8 offset = 0;
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(V2)/2);
        
        mesh->addAttribute(offset, 1, MeshAttribute::Unsigned, sizeof(Uint16), false);
        mesh->addAttribute(offset, 2, MeshAttribute::Unsigned, sizeof(Uint8), true);
        mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint8), true);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
    }
    
    bool operator<(const ParticleVert& o) const;
};
struct ParticleMeta {
    V3 velocity, accel;
    float lifetime, strength, lifetimeMax;
    Uint16 seed; Uint16 group;
    
    ParticleMeta() : velocity(0.f), accel(0.f), lifetime(0.f), strength(0.f), lifetimeMax(0.f), seed(0x0), group(0) {};
};

#define PARTICLESYSTEM_MAX_GROUPS 2048
class ParticleSystem {
public:
    
    ParticleSystem(unsigned int maxCnt = 64, bool allowUpsizing = true);
    virtual ~ParticleSystem();
    
    ParticleVert *addVert();
    unsigned int addVertID();
    void clear();
    void removeVert(unsigned int id);
    void removeVert(ParticleVert *v);
    void minimize();
    
    void update(float dT);
    void upload(bool force = false);
    void draw();
    
    void setShader(Shader *newShader);
    void setSimulation(bool state);
    void setSorted(bool state);
    
    void setAtlas(TextureAtlas *atl);
    
    unsigned int getID(ParticleVert *v);
    
    void resize(unsigned int newCnt);
    Uint16 addGroup();
    void removeGroup(Uint16 group);
    
    ParticleVert *getVert(unsigned int idx);
    ParticleMeta *getMeta(unsigned int idx);
    inline ParticleMeta* getMeta(ParticleVert *v) {
        return this->getMeta(getID(v));
    }
    
    void bindShader();
    void unbindShader();
    
    void setRebirth(std::function<void(ParticleSystem*)>, Uint16 gid = 0);
    void disableRebirth();

    bool simulationEnabled, sortOnRender, rebirth;
    TextureAtlas *atlas;
    
    unsigned int maxParticles, activeParticles, firstFree;
    Shader *shader;
    Mesh<ParticleVert> *mesh;
    std::vector<ParticleMeta> meta;
    std::vector<std::function<void(ParticleSystem*)>> rebirthFuncs;
protected:
    bool meshDirty, allowUpsizing, shaderBound;
    std::bitset<PARTICLESYSTEM_MAX_GROUPS> groupTable;
};

#endif /* PARTICLESYSTEM_H */

