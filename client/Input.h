#ifndef INPUT_INC
#define INPUT_INC
#include "client.h"
#include "SizedList.h"
#include <SDL2/SDL_keycode.h>

#include "rWindow.h"
//Return value determines if input was "absorbed"
#define INPUT_AXISHOOK_D (InputAxis* axis)
#define INPUT_AXISHOOK_R bool
#define INPUT_BTNHOOK_D (InputButton* btn)
#define INPUT_BTNHOOK_R bool
#define INPUT_KEYHOOK_D (InputKey* key)
#define INPUT_KEYHOOK_R bool

#define INPUT_DEV_NULL 0xFFFFFFFF
#define INPUT_BTN_NULL 0xFF

#define INPUT_MAX_INSTANCES 8
#define INPUT_MAX_AXES 32
#define INPUT_MAX_HOOKS 4
#define INPUT_MAX_BUTTONS 128
#define INPUT_MAX_KEYS 768
#define INPUT_MAX_COMBOS 2

#define INPUT_MOUSE_AXES 5
#define INPUT_CONTROLLER_AXES 8
#define INPUT_MOUSE_BUTTONS 16
#define INPUT_CONTROLLER_BUTTONS 16

#define BPK(b) (256 + ((unsigned short)(b)))
#define MPK(b) ((b) - SDL_BUTTON_LEFT + 256)
#define CPK(b) ((b) - SDL_CONTROLLER_BUTTON_A + INPUT_MOUSE_BUTTONS + 256)

#define CTOK(k) ((unsigned short) k)
#define KTOC(k) ((char) k)
#define STK(k) ((unsigned short) k )
#define STC(k) ((char) k)
#include <iostream>
class Input;

struct InputKey {
  inline InputKey() {
    state = previousState = 0;
    pressFrame = 0; pressTime = 0.0;
    for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) { hooks[i] = NULL; };
  }
  
  Uint8 state, previousState;
  Uint16 id;
  Uint32 pressFrame;
  double pressTime;
  INPUT_KEYHOOK_R (*hooks[INPUT_MAX_HOOKS])INPUT_KEYHOOK_D;
  
  inline bool pressed() const { return (state && pressFrame == _globalFrame); };
  inline bool held() const { return (state && pressFrame != _globalFrame); };
  inline bool down() const { return state; };
  inline char typed() const {
    return 0x00;
  }
  inline operator bool() const { return down(); };
  
  inline bool tellHooks();
};

struct InputCombo : Serializable {
  InputCombo() : primaryKey(0), keyMod1(0), keyMod2(0) {};
  InputCombo(Uint16 pk) : primaryKey(pk), keyMod1(0), keyMod2(0) {};
  InputCombo(const InputKey *const k) : primaryKey(k->id), keyMod1(0), keyMod2(0) {};
  ~InputCombo() = default;
  
  void addModifier(Uint16 mk);
  inline void addModifier(const InputKey *const k) { addModifier(k->id); };
  
  void clear();
  void clearModifiers();
  
  Uint16 primaryKey;
  Uint16 keyMod1, keyMod2;
  
  inline bool pressed(const Input *const p) const;
  inline bool held(const Input *const p) const;
  inline bool down(const Input *const p) const;
  
  inline Uint8 getState(const Input *const p) const;
  inline double getPressTime(const Input *const p) const;
  inline Uint32 getPressFrame(const Input *const p) const;
  
  std::string toString();
  Uint32 serialLength();
  void serialize(char *const buff, int length = -1);
};

struct InputButton {
  inline InputButton() {
    state = 0;
    pressFrame = 0; pressTime = 0.0;
    for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) { hooks[i] = NULL; };
  }
  Uint8 state;
  Uint32 pressFrame;
  double pressTime;
  
  InputCombo combos[INPUT_MAX_COMBOS];
  
  INPUT_BTNHOOK_R (*hooks[INPUT_MAX_HOOKS])INPUT_BTNHOOK_D;
  std::string name, id;
  
  inline bool pressed() const { return (state && pressFrame == _globalFrame); };
  inline bool down() const { return state; };
  inline operator bool() const { return down(); };
  
  inline void tellHooks();
  
  inline void update(Input *const ip);
};

struct InputAxis {
  inline InputAxis() {
    value = delta = deadZone = 0;
    sensitivity = 1000; power = 1.f;
    for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) { hooks[i] = NULL; };
    axisPlus = axisMinus = INPUT_BTN_NULL;
  }
  Int16 value;
  Int16 delta;
  Int16 deriv;
  Int16 deadZone;
  Int16 sensitivity;
  float power;
  Uint8 axisPlus, axisMinus;//Stores emulated button IDs
  
  inline operator int() const { return delta; };
  inline void zero() { value = delta = 0; };
  inline void set(Int16 newValue);
  inline void setDelta(Int16 newDelta);
  
  INPUT_AXISHOOK_R (*hooks[INPUT_MAX_HOOKS])INPUT_AXISHOOK_D;
  inline void tellHooks();
  inline void update(Input *const ip, int ms);
  
  std::string name;
};

struct InputAxisTracked {
  Int16 value;
  Int16 min, max;
  InputAxis *axis;
};

#ifndef RWINDOW_INC
struct SDL_KeyboardEvent;
struct SDL_ControllerButtonEvent;
#endif

class Input {
public:
  enum Axis : Uint8 {
    MouseX = 0, MouseY = 1, MouseZ = 2,
    ScrollX = 3, ScrollY = 4,
    GPLeftX = 5, GPLeftY = 6,
    GPRightX = 7, GPRightY = 8,
    GPLeftTrig = 9, GPRightTrig = 10,
    GPOtherX = 11, GPOtherY = 12
  };
  enum Key : Uint16 {
      LShift = STK( SDLK_LSHIFT		),
      RShift = STK( SDLK_RSHIFT		),
      LCtrl =  STK( SDLK_LCTRL		),
      RCtrl =  STK( SDLK_RCTRL		),
      UpArrow = STK( SDLK_UP		),
      DownArrow = STK( SDLK_DOWN	),
      LeftArrow = STK( SDLK_LEFT	),
      RightArrow = STK( SDLK_RIGHT	),
      GPA =             CPK(SDL_CONTROLLER_BUTTON_A		),
      GPB =             CPK(SDL_CONTROLLER_BUTTON_B		),
      GPX =             CPK(SDL_CONTROLLER_BUTTON_X		),
      GPY =             CPK(SDL_CONTROLLER_BUTTON_Y		),
      GPLStick =        CPK(SDL_CONTROLLER_BUTTON_LEFTSTICK     ),
      GPRStick =        CPK(SDL_CONTROLLER_BUTTON_RIGHTSTICK	),
      GPLShoulder =     CPK(SDL_CONTROLLER_BUTTON_LEFTSHOULDER	),
      GPRShoulder =     CPK(SDL_CONTROLLER_BUTTON_RIGHTSHOULDER	),
      GPUp =            CPK(SDL_CONTROLLER_BUTTON_DPAD_UP	),
      GPDown =          CPK(SDL_CONTROLLER_BUTTON_DPAD_DOWN	),
      GPLeft =          CPK(SDL_CONTROLLER_BUTTON_DPAD_LEFT	),
      GPRight =         CPK(SDL_CONTROLLER_BUTTON_DPAD_RIGHT	),
      GPStart =         CPK(SDL_CONTROLLER_BUTTON_START         ),
      GPGuide =         CPK(SDL_CONTROLLER_BUTTON_GUIDE         ),
      GPBack =          CPK(SDL_CONTROLLER_BUTTON_BACK          ), 
      
  };
  static void Initialize();
  
  Input();
  ~Input();
  
  void update(int ms);
  void zero();
  
  inline InputKey* getKey(Uint16 keyID);
  inline const InputKey* getKeyConst(Uint16 keyID) const;
  inline bool getKeyDown(Uint16 keyID) const;
  inline bool getKeyPress(Uint16 keyID) const;
  
  inline InputButton* getButton(Uint16 buttonID);
  inline bool getButtonDown(Uint16 buttonID) const;
  inline bool getButtonPress(Uint16 buttonID) const;
  
  inline InputAxis* getAxis(Uint8 axisID);
  inline Int16 getAxisValue(Uint8 axisID) const;
  inline Int16 getAxisDelta(Uint8 axisID) const;
  
  //void clearHeld();
  
  bool hookButton(Uint16 btnID, INPUT_BTNHOOK_R (*function)INPUT_BTNHOOK_D);
  bool hookKey(Uint16 keyID, INPUT_KEYHOOK_R (*function)INPUT_KEYHOOK_D);
  bool hookAxis(Uint8 axisID, INPUT_AXISHOOK_R (*function)INPUT_AXISHOOK_D);
  
  void unhookButton(Uint16 btnID, INPUT_BTNHOOK_R (*function)INPUT_BTNHOOK_D);
  void unhookKey(Uint16 keyID, INPUT_KEYHOOK_R (*function)INPUT_KEYHOOK_D);
  void unhookAxis(Uint8 axisID, INPUT_AXISHOOK_R (*function)INPUT_AXISHOOK_D);
  
  inline bool hasKeyboard() { return keyboardID != INPUT_DEV_NULL; };
  inline bool hasMouse() { return mouseID != INPUT_DEV_NULL; };
  inline bool hasGamepad() { return gamepadID != INPUT_DEV_NULL; };
  
  void axisEvent(Int8 id , Int32 delta);
  void axisEventAbsolute(Int8 id , Int32 value);
  void keyEvent(Uint16 code, Uint16 scan, Uint8 state, Uint16 mod = 0x0000);
  void btnEvent(Uint8 id, Uint8 state);
  void deviceEvent();
  
  void setGamepad(Uint32 gID);
  
  //Static functions
  static std::string keyNames[INPUT_MAX_KEYS];
  inline static const std::string& getKeyName(Uint16 id) {
    if(id > INPUT_MAX_KEYS) return keyNames[0];
    return keyNames[id];
  }
  static void LoadKeyNames();
  
  static void Register(Input *const src);
  static void Unregister(Input *const src);
  
  static Input* GetByMouse(Uint32 mID);
  static Input* GetByKeyboard(Uint32 kID);
  static Input* GetByGamepad(Uint32 gID);
protected:
  Uint32 gamepadID, keyboardID, mouseID;
  
  Uint16 axisCount, btnCount;
  InputAxis axes[INPUT_MAX_AXES];
  InputButton buttons[INPUT_MAX_BUTTONS];
  InputKey keys[INPUT_MAX_KEYS];
  
  //Static
  static SizedList<Input*, INPUT_MAX_INSTANCES> inputs;
};
#include "Input.inl.h"
#endif
