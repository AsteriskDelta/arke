#ifndef INC_MODEL
#define	INC_MODEL
#include "Mesh.h"
#include "Texture.h"
#include <string>
#include <vector>
#include "AppData.h"
#include "Color.h"

class aiMesh; class aiScene;
#define MODEL_MAT_NULL 0xFFFFFFFF
class RenderStage;
class Shader;

struct ModelVert {
  V3 pos;
  V3 normal;
  V2 texCoord;
  ColorRGBA tint;
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 4, MeshAttribute::Unsigned, sizeof(Uint8), true);
  }
};

#define MODEL_MAX_TEX 8
struct ModelObject {
    Mesh<ModelVert> mesh;
    unsigned int texID;
};

class Model {
public:
    Model();
    Model(const std::string& newPath, bool isRelPath = true);
    virtual ~Model();
    
    bool load(const std::string& newPath, bool isRelPath = true);
    
    void scale(float factor);
    
    void draw(RenderStage *const stage);
    void draw(bool forceTex = false);
//protected:
    std::string path;
    Directory dir;
    Shader *shader;
    
    bool initFromScene(const aiScene* pScene);
    void initMesh(unsigned int Index, const aiMesh* paiMesh);
    bool initMaterials(const aiScene* pScene);
    void clear();
    
    std::vector<ModelObject> meshes;
    std::vector<Texture2D*> textures;
    std::vector<Texture2D*> secs;
};

#endif

