#ifndef PIPELINE_INC
#define PIPELINE_INC
#include "SizedList.h"
#include "DynVar.h"
#include "IDMap.h"
#include <string>
#include "V2.h"
#include "Mesh.h"
#include "Texture.h"

#define PIPELINE_CNT_MAX 32
#define PIPELINE_BUFF_MAX 32
#define PIPELINE_OBJ_MAX 64

#include "PipelineStage.h"
#define REND_DRAW_D (RenderStage *const stage)
#define REND_DRAW_R int

struct RenderBuffer {
    std::string id, typeID;
    unsigned char bpp, sampleCount, layers, mipmaps;
    float sizeMod;
    std::string condition, filterRaw;
    
    Texture2D texture;
};

struct RenderPref {
    std::string id;
    DynVar type;
    
    std::string sectionID, name;
};

struct PTileVert {
  V3 pos;
  Uint16 tileID;//Referenced only for provoking vert
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 1, MeshAttribute::Unsigned, sizeof(Uint16));
  }
};

class RenderPipeline {
public:
  RenderPipeline();
  RenderPipeline(const std::string file, bool isRelPath = true);
  ~RenderPipeline();
  
  bool load(const std::string file, bool isRelPath = true);
  bool reload();
  
  void Allocate();
  void Deallocate();
  void ReallocateWSZ();
  
  void Execute(REND_DRAW_R (*renderCall)REND_DRAW_D);
  void Blit();
  
  inline bool isActive();
  void Activate();
  void Deactivate();
  
  Uint16 tileSize, xTiles, yTiles;
  Mesh<PTileVert> *lightTiles;
  
  SizedList<RenderBuffer, PIPELINE_BUFF_MAX> buffers;
  SizedList<RenderStage, PIPELINE_OBJ_MAX> stages;
  
  static SizedList<RenderPipeline*, PIPELINE_CNT_MAX> globalPipelines;
protected:
  std::string id, name;
  
  void initVars();
  
};

extern thread_local RenderPipeline* activePipe;

inline bool RenderPipeline::isActive() {
     return activePipe == this;
}

#endif
