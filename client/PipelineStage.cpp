#define MESH2D
#include "PipelineStage.h"
#include "rInterface.h"
#include "Renderer.h"
#include "Pipeline.h"
#include "Mesh.h"
#include "Shader.h"
#include "Camera.h"
#include <chrono>

struct PostVert {
  V2 pos;
  V2 texCoord;
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(float));
  }
};
template class Mesh<PostVert>;
static Mesh<PostVert> *postQuad = NULL;

RenderStage::RenderStage() {
  gpuID = 0;
  flags = 0;
  chainID = 0;
  chainRaw = "";
  avgTime = curTime = 0.0;
  for(Uint8 i = 0; i < PIPELINE_OBJ_MAX_T; i++) {
    buffers[i] = RBUFF_NULL;
    sources[i] = NULL;
    sourcesRaw[i] = "";
  }
  postTarget = NULL;
  pTargetRaw = "";
  
  if(postQuad == NULL && renderer != NULL) {
    postQuad = new Mesh<PostVert>(MeshType::Quads, 4);
    
    postQuad->vert(0).pos = V2(-1.f, -1.f);
    postQuad->vert(1).pos = V2( 1.f, -1.f);
    postQuad->vert(2).pos = V2( 1.f,  1.f);
    postQuad->vert(3).pos = V2(-1.f,  1.f);
    
    postQuad->vert(0).texCoord = V2(0.f, 0.f);
    postQuad->vert(1).texCoord = V2(1.f, 0.f);
    postQuad->vert(2).texCoord = V2(1.f, 1.f);
    postQuad->vert(3).texCoord = V2(0.f, 1.f);
    
    postQuad->setVertCount(4);
    postQuad->upload();
  }
}

RenderStage::~RenderStage() {
  this->destroy();
}

#ifdef USEGL

void RenderStage::build() { _prsguard();
  glGenFramebuffers(1, &gpuID);
  glBindFramebuffer(GL_FRAMEBUFFER , gpuID);
  //std::cout << this->id << "\n";
  RenderPipeline *const pipe = activePipe;
  GLenum drawBuffers[8]; GlUint drawBufferCount = 0;
  for(Uint8 i = 0; i < PIPELINE_OBJ_MAX_T; i++) {
    //std::cout << "\t" << int(i) << ": " << int(buffers[i]) << "\n";
    if(buffers[i] == RBUFF_NULL) continue;
    RenderBuffer *const buff = &(pipe->buffers[this->buffers[i]]);
    
    GlUint mountPoint;
    switch(i) {
      default: 
      case 0: mountPoint = GL_COLOR_ATTACHMENT0; break;
      case 1: mountPoint = GL_COLOR_ATTACHMENT1; break;
      case 2: mountPoint = GL_COLOR_ATTACHMENT2; break;
      case 3: mountPoint = GL_COLOR_ATTACHMENT3; break;
      case 4: mountPoint = GL_COLOR_ATTACHMENT4; break;
      case 5: mountPoint = GL_COLOR_ATTACHMENT5; break;
      case 6: mountPoint = GL_COLOR_ATTACHMENT6; break;
      case 7: mountPoint = GL_COLOR_ATTACHMENT7; break;
      case PIPELINE_DEPTH: mountPoint = GL_DEPTH_ATTACHMENT; break;
      case PIPELINE_STENCIL: mountPoint = GL_STENCIL_ATTACHMENT; break;
      case PIPELINE_DEPTHSTENCIL: mountPoint = GL_DEPTH_STENCIL_ATTACHMENT; break;
    }
    
    //std::cout << "Attaching mount " << int(i) << "("<<int(i)<<") with " << buff->id << "\n";
    glFramebufferTexture2D(GL_FRAMEBUFFER, mountPoint, buff->texture.getBindTarget(),  buff->texture, 0);
    if(i <= 7) drawBuffers[drawBufferCount++] = mountPoint;
    
    //Handle sources as well
    if(sourcesRaw[i] != "") {
      
    }
  }
  
  //GLenum blkBuffers[] = {GL_COLOR_ATTACHMENT0};
  glDrawBuffers(drawBufferCount, drawBuffers);
  
  riEnsureState();
}

void RenderStage::destroy() {
  if(gpuID == 0) return;
  glDeleteFramebuffers(1,& gpuID);
  riEnsureState();
  
}

thread_local auto stgStartTime = std::chrono::high_resolution_clock::now();
bool RenderStage::bind() { _prsguard();
  if(gpuID == 0) return false;
  riEnsureState();
  stgStartTime = std::chrono::high_resolution_clock::now();
  glBindFramebuffer(GL_FRAMEBUFFER , gpuID);
  
  if(hasFlag(ClearColor) && hasFlag(ClearDepth)) glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
  else if(hasFlag(ClearColor)) glClear(GL_COLOR_BUFFER_BIT);
  else if(hasFlag(ClearDepth)) glClear(GL_DEPTH_BUFFER_BIT);
  
  if(hasFlag(DepthTest)) {
    glDepthFunc(depthFunc);
    glEnable(GL_DEPTH_TEST);
    //std::cout << "depth test on " << id << "\n";
  } else glDisable(GL_DEPTH_TEST);
  
  if(hasFlag(Blend)) {
    glEnable(GL_BLEND);
  } else glDisable(GL_BLEND);
  riEnsureState();
  
  
  
  return true;
}
bool RenderStage::unbind() { _prsguard();
  riEnsureState();
  if((hasFlag(PostProc) || hasFlag(PostTiled)) && postShader != NULL) {
    postShader->activate();
    
    //Attach inputs
    for(Uint8 i = 0; i < PIPELINE_OBJ_MAX_T; i++) {
      if(sources[i] != NULL) postShader->bindTexture(sources[i], i, i);
    }
    
    if(hasFlag(BindClipInfo)) postShader->bindV3(activeCamera->getClipInfo(), 15);
    if(hasFlag(BindProjInfo)) postShader->bindV4(activeCamera->getProjInfo(), 14);
    if(hasFlag(BindFBInfo))   {
      V4 fbInfo = V4(0.f);
      if(postTarget != NULL) {
        fbInfo.x = postTarget->width;
        fbInfo.y = postTarget->height;
        fbInfo.z = float(renderer->targetSize.x) / fbInfo.x;
        fbInfo.w = float(renderer->targetSize.y) / fbInfo.y;
        //std::cout << "fbi: " << glm::to_string(fbInfo) << "\n";
      } else {
        //std::cout << "NULL posttarget!\n";
      }
      fbInfo.x = fbInfo.y = fbInfo.z = 0.f;
      fbInfo.w = 0.0f;
      
      postShader->bindV4(fbInfo, 13);
    }
    
    if(hasFlag(PostTiled)) activePipe->lightTiles->draw();
    else postQuad->draw();
    postShader->deactivate();
    if(this->hasFlag(Barrier)) glTextureBarrierNV();
  }
  riEnsureState();
  glBindFramebuffer(GL_FRAMEBUFFER , 0);
  
  if(hasFlag(MipMap) && postTarget != NULL) {
    postTarget->mipmap();
  } riEnsureState();
  
  if(hasFlag(Resolve)) {
    glBindFramebuffer(GL_READ_FRAMEBUFFER, gpuID);
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, chainID);
    glBlitFramebuffer(0, 0, renderer->targetSize.x, renderer->targetSize.y, 0, 0, renderer->targetSize.x, renderer->targetSize.y, GL_COLOR_BUFFER_BIT, GL_NEAREST); 
  }
  
  riEnsureState();
  curTime = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now() - stgStartTime).count();
  avgTime = avgTime * (59.f/60.f) + curTime * (1.f / 60.f);
  return true;
}

#include <algorithm>

inline static bool cmpstrtx(const std::string& str, const std::string& sub) {
  auto it = std::search(
    str.begin(), str.end(),
    sub.begin(),   sub.end(),
    [](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
  );
  return (it != str.end() );
}

void RenderStage::setBlending(std::string from, std::string to) {
  
}

void RenderStage::setDepthTest(std::string test) {
  if(cmpstrtx(test, "always")) depthFunc = GL_ALWAYS;
  else if(cmpstrtx(test, "never")) depthFunc = GL_NEVER;
  else if(cmpstrtx(test, "not")) depthFunc = GL_NOTEQUAL;
  else if(cmpstrtx(test, "less")) {
    if(cmpstrtx(test, "equal")) depthFunc = GL_LEQUAL;
    else depthFunc = GL_LESS;
  } else if(cmpstrtx(test, "greater")) {
    if(cmpstrtx(test, "equal")) depthFunc = GL_GEQUAL;
    else depthFunc = GL_GREATER;
    std::cout << "df GREATER\n";
  }
}

void RenderStage::processBinds(Shader *const shader) {
  for(Uint8 i = 4; i < PIPELINE_OBJ_MAX_T; i++) {
      if(sources[i] != NULL) shader->bindTexture(sources[i], i, i);
    }
}

#elif USEVK

#endif