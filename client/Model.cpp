#include "Image.h"
#include "Shader.h"
#include "Model.h"
#include "ext/assimp/include/assimp/Importer.hpp"
#include "ext/assimp/include/assimp/scene.h"         // Output data structure
#include "ext/assimp/include/assimp/postprocess.h"
#include "Camera.h"
#include "PipelineStage.h"


template class Mesh<ModelVert>;

Model::Model() {
  shader = Shader::Load("opaqueTest.prgm");
}

Model::Model(const std::string& newPath, bool isRelPath) {
  shader = Shader::Load("opaqueTest.prgm");
  this->load(newPath, isRelPath);
}

Model::~Model() {
  meshes.clear();
  for (unsigned int i = 0; i < textures.size(); i++) {
    if(textures[i] != NULL) delete textures[i];
  }
  for (unsigned int i = 0; i < secs.size(); i++) {
    if(secs[i] != NULL) delete secs[i];
  }
  delete shader;
}

bool Model::load(const std::string& newPath, bool isRelPath) { _prsguard();
  using namespace Assimp;
  bool ret = false;
  path = newPath;
  Assimp::Importer Importer;
  if(isRelPath) dir = AppData::base;
  else dir = AppData::system;

  const aiScene* pScene = Importer.ReadFile(dir.resolve(path).c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);

  if (pScene) {
    ret = initFromScene(pScene);
  } else {
    std::stringstream ss;
    ss << "Error parsing '" << path << "': " << Importer.GetErrorString() << "'\n";
    Console::Error(ss.str());
    return false;
  }

  return ret;
}

bool Model::initFromScene(const aiScene* pScene) { _prsguard();
  meshes.resize(pScene->mNumMeshes);
  textures.resize(pScene->mNumMaterials);
  secs.resize(pScene->mNumMaterials);
  for(unsigned int i = 0; i < pScene->mNumMaterials; i++)  { textures[i] = NULL; secs[i]=NULL; };

  // Initialize the meshes in the scene one by one
  for (unsigned int i = 0; i < meshes.size(); i++) {
    const aiMesh* paiMesh = pScene->mMeshes[i];
    initMesh(i, paiMesh);
  }

  return initMaterials(pScene);
}

void Model::initMesh(unsigned int idx, const aiMesh* paiMesh) { _prsguard();
  meshes[idx].texID = paiMesh->mMaterialIndex;
  
  const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
  meshes[idx].mesh.allocate(paiMesh->mNumVertices, paiMesh->mNumFaces*3);
  meshes[idx].mesh.setType(MeshType::Tris);

  for (unsigned int i = 0; i < paiMesh->mNumVertices; i++) {
    const aiVector3D* pPos = &(paiMesh->mVertices[i]);
    const aiVector3D* pNormal = &(paiMesh->mNormals[i]) ? &(paiMesh->mNormals[i]) : &Zero3D;
    const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

    ModelVert v;
    v.pos = V3(pPos->x, pPos->y, pPos->z);
    v.normal = V3(pNormal->x, pNormal->y, pNormal->z);
    v.texCoord = V2(pTexCoord->x, pTexCoord->y);
    v.tint = ColorRGBA(255,255,255,255);

    meshes[idx].mesh.setVert(i, v);
  }
  meshes[idx].mesh.setVertCount(paiMesh->mNumVertices);

  for (unsigned int i = 0; i < paiMesh->mNumFaces; i++) {
    const aiFace& Face = paiMesh->mFaces[i];
    assert(Face.mNumIndices == 3);
    meshes[idx].mesh.addInd(Face.mIndices[0]);
    meshes[idx].mesh.addInd(Face.mIndices[1]);
    meshes[idx].mesh.addInd(Face.mIndices[2]);
  }
  //meshes[idx].mesh.setIndCount(paiMesh->mNumFaces*3);
  
  meshes[idx].mesh.upload();
}

#include <algorithm>
bool Model::initMaterials(const aiScene* pScene) { _prsguard();
  for (unsigned int i = 0; i < pScene->mNumMaterials; i++) {
    const aiMaterial* pMaterial = pScene->mMaterials[i];
    textures[i] = secs[i] = NULL;
    if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
      aiString imgPath;
      
      if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &imgPath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
        std::string fullPath = imgPath.data;
        std::replace(fullPath.begin(), fullPath.end(), '\\', '/');
        ImageRGBA image = ImageRGBA();

        if(!image.Load(dir.resolve(fullPath).c_str(), false)) {
          printf("Error loading texture '%s'\n", dir.resolve(fullPath).c_str());
          delete textures[i];
          textures[i] = NULL;
          continue;
        }
        
        textures[i] = new Texture2D(&image);
        //textures[i].
      }
      
      if (pMaterial->GetTexture(aiTextureType_SPECULAR, 0, &imgPath, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) {
        std::string fullPath = imgPath.data;
        std::replace(fullPath.begin(), fullPath.end(), '\\', '/');
        ImageRGBA image = ImageRGBA();

        if(!image.Load(dir.resolve(fullPath).c_str(), false)) {
          printf("Error loading texture '%s'\n", dir.resolve(fullPath).c_str());
          delete textures[i];
          textures[i] = NULL;
          continue;
        }
        
        secs[i] = new Texture2D(&image);
        //textures[i].
      } else secs[i] = NULL;
    }
    
  }
  return true;
}

void Model::clear() {
  
}

void Model::draw(RenderStage *const stage) { _prsguard();
  if(!stage->hasFlag(RenderStage::DepthOnly)) shader->activate();
  for (unsigned int i = 0; i < meshes.size(); i++) {
    if(meshes[i].texID > textures.size()) continue;
    
    if(!stage->hasFlag(RenderStage::DepthOnly))stage->processBinds(shader);
    
    Texture2D *tex = textures[meshes[i].texID];
    if(tex == NULL || tex->getBindTarget() == 0) continue;
    if(!stage->hasFlag(RenderStage::DepthOnly))shader->bindTexture(tex, 0, 0);
    
    tex = secs[meshes[i].texID];
    if(tex != NULL && tex->getBindTarget() != 0 && !stage->hasFlag(RenderStage::DepthOnly)) shader->bindTexture(tex, 1, 1);

    
    meshes[i].mesh.draw();
  }
  if(!stage->hasFlag(RenderStage::DepthOnly)) shader->deactivate();
}

void Model::draw(bool forceTex) { _prsguard();
  if(shader != nullptr) {
    shader->activate();

    shader->bindV3(activeCamera->forward(), 27);
    
    shader->bindV3(activeCamera->transform.position, 28);
  }
  for (unsigned int i = 0; i < meshes.size(); i++) {
    if(meshes[i].texID > textures.size()) continue;
    
    Texture2D *tex = textures[meshes[i].texID];
    if(tex != NULL && tex->getBindTarget() != 0)shader->bindTexture(tex, 0, 0);
    else if(forceTex) continue;
    
    tex = secs[meshes[i].texID];
    if(tex != NULL && tex->getBindTarget() != 0) shader->bindTexture(tex, 1, 1);

    
    meshes[i].mesh.draw();
  }
  if(shader != nullptr) shader->deactivate();
}

void Model::scale(float s) { _prsguard();
  for (unsigned int i = 0; i < meshes.size(); i++) {
    meshes[i].mesh.scale(s);
    meshes[i].mesh.upload();
  }
}
