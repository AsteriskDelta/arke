#include "FontWriter.h"

using namespace ARKE;

namespace FontWriter {
  void Write(FontIndex *font, ParticleSystem *sys, const std::string& text, float sizePx, V3 pos, V3 right, V3 up) { _prsguard();
    unsigned short cx = 0, cy = 0; float sx = sizePx, sy = sizePx;
    Font::Kerning ker;
    
    right = glm::normalize(right);
    up = glm::normalize(up);
    
    V3 norm = glm::normalize(glm::cross(right, up));
    up = glm::normalize(glm::cross(right, norm));
    float rot = (1.f - glm::dot(right, V3(1.f, 0.f, 0.f))) * RAD_TO_DEG / 180.f;
    unsigned char detA = clamp(int(round(rot*128.f + 128.f)), 1, 255);
    
    for(unsigned int i = 0; i < text.size(); i++) {
      ImageAtlas::Lookup lk = font->chr(text[i], &ker);

      if(lk.w > 1 && lk.h > 1) {
        unsigned short pType = lk.selfID;
        ParticleVert *vert = sys->addVert();
        if(vert == NULL) continue;
        vert->size = V2(sx*float(lk.ow), sy*float(lk.oh));
        vert->pos = pos + float(cx + vert->size.x/2.f)*sx*right+float(cy + vert->size.y/2.f)*sy*up;
        vert->normal = norm;//glm::normalize(glm::cross(right, up));
        vert->figureID = pType;
        vert->detA = detA; vert->detB = 0x0;
        //std::cout << "Writing " << text[i] << " at " << cx << ", " << cy <<":" << ker.oX << ", " << ker.oY << " figID = " << vert->figureID << "\n";
        //if(g->img != NULL) img->overlay(cx+g->oX,cy+g->oY, g->img);
      }
      
      cx += ker.adv;
    }
  }
  
  void WriteBlock(FontIndex *font, ParticleSystem *sys, const std::string& text, float sizePx, V3 pos, V2 bounds, V3 right, V3 up) { _prsguard();
    unsigned short cx = 0, cy = 0; float sx = sizePx, sy = sizePx;
    Font::Kerning ker;
    
    right = glm::normalize(right);
    up = glm::normalize(up);
    
    V3 norm = glm::normalize(glm::cross(right, up));
    up = glm::normalize(glm::cross(right, norm));
    float rot = (1.f - glm::dot(right, V3(1.f, 0.f, 0.f))) * RAD_TO_DEG / 180.f;
    unsigned char detA = clamp(int(round(rot*128.f + 128.f)), 1, 255);
    int pLine = 0, lineAdv = 0; std::list<unsigned int> lineIDs = std::list<unsigned int>();
    for(int i = 0; i < int(text.size()); i++) {
      ImageAtlas::Lookup lk = font->chr(text[i], &ker);

      if(lk.w > 0 && lk.h > 0) {
        unsigned short pType = lk.selfID;
        ParticleVert *vert = sys->addVert();
        if(vert == NULL) continue;
        vert->size = V2(sx*float(lk.ow), sy*float(lk.oh));
        vert->pos = pos + float(cx + vert->size.x/2.f)*sx*right+float(cy + vert->size.y/2.f)*sy*up;
        vert->normal = norm;//glm::normalize(glm::cross(right, up));
        vert->figureID = pType;
        vert->detA = detA; vert->detB = 0x0;
        //std::cout << "Writing " << text[i] << " at " << cx << ", " << cy <<":" << ker.oX << ", " << ker.oY << " figID = " << vert->figureID << "\n";
        //if(g->img != NULL) img->overlay(cx+g->oX,cy+g->oY, g->img);
        lineIDs.push_back(sys->getID(vert));
        lineAdv = max(lineAdv, int(font->lineAdv(text[i])));
      }
      
      cx += ker.adv;
      if(cx*sx > bounds.x) {
        cy += lineAdv;
        if(cy*sy > bounds.y) return;
        cx = 0; lineAdv = 0;
        //backtrack to last space
        int pOff = i;
        for(int x = pOff; x > pLine; x--) {
          if(std::isspace(text[x]) || std::ispunct(text[x])) {
            pOff = x;
            break;
          }
        }
        pLine = pOff;
        
        for(auto it = lineIDs.rbegin(); it != lineIDs.rend(); ++it) {
          if(i <= pOff-1) break;
          sys->removeVert(*it);
          i--;
        }
        lineIDs.clear();
        //i = pOff-1;
        continue;
      }
    }
  }
};
