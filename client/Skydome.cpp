/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Skydome.cpp
 * Author: Galen Swain
 * 
 * Created on April 19, 2016, 6:17 PM
 */

#include "Image.h"
#include "Skydome.h"
#include "Mesh.h"
#include "Application.h"

Texture2D *Skydome::noise, *Skydome::gradient;
Mesh<Skydome::cVert> *Skydome::mesh = NULL;  
Shader *Skydome::shader = NULL;
const static int lats = 32, longs = 48;
const static int sphereSize = 4 * (lats+1) * (longs+1);

Skydome::Skydome(float newRadius) {
  sTime = 0.f; radius = newRadius;
  if(shader == NULL) LoadResources();
}

Skydome::Skydome(const Skydome& orig) {
  sTime = orig.sTime;
  radius = orig.radius;
}

Skydome::~Skydome() {
}

void Skydome::setTime(float newTime) {
  this->sTime = newTime;
}
void Skydome::render(V3 center) { _prsguard();
  glCullFace(GL_FRONT);
  shader->activate();
  const float sM = 0.14f;//1f;
  //shader->BindV4(V4{gameTime, gameTime*sM, gameTime*(2.2f*sM), gameTime*(4.1f*sM)}, 4);
  //shader->BindV3(fadeData, 5);
  //shader->bindV2(Renderer::targetSize, 6);
  shader->bindTexture(noise, 1, 43);
  shader->bindTexture(gradient, 2, 44);
  shader->bindV4(V4(_globalTimef), 4);
  shader->bindV3(activeCamera->transform.position, 28);
  shader->bindV3(V3{0.0f, /*fmod(gameTime*0.25, 4) - 1.0*/2.2f, 1.1f}, 7);
  
  mesh->draw();
  
  shader->deactivate();
  glCullFace(GL_BACK);
}

void Skydome::LoadResources() { _prsguard();
  mesh = new Mesh<cVert>(MeshType::Quads, sphereSize);
  int vertIndex = 0;
  cVert* sphereVerts = reinterpret_cast<cVert*>(mesh->getDataPtr());
  
  for(int i = 1; i <= lats; i++) {
    float lat0 = M_PI * (-0.5f + (float) (i - 1) / lats);
    float z0  = sin(lat0);
    float zr0 =  cos(lat0);
    float radial = (float)(i - 1) / (float)lats;
    float radialEnd = (i - 1) / (float)lats;
    
    float lat1 = M_PI * (-0.5f + (float) i / lats);
    float z1 = sin(lat1);
    float zr1 = cos(lat1);
    cVert prevA, prevB; bool begun = false;
    for(int j = 0; j <= longs; j++) {
      float lng = 2.0f * M_PI * (float) (j - 1) / longs;
      float angular = lng;
      float x = cos(lng);
      float y = sin(lng);
      
      if(!begun) {
        prevA.pos = V3{x * zr0, y * zr0, z0}; prevA.normal = glm::normalize(V3{x * zr0, y * zr0, z0});//.normalized(); 
        prevA.polar = V2{radial, angular};
        prevB.pos = V3{x * zr1, y * zr1, z1}; prevB.normal = glm::normalize(V3{x * zr1, y * zr1, z1});//.normalized();
        prevB.polar = V2{radialEnd, angular};
        begun = true;
      } else {
        sphereVerts[vertIndex++] = prevB;
        sphereVerts[vertIndex++] = prevA;

        prevA.pos = sphereVerts[vertIndex].pos = V3{x * zr0, y * zr0, z0}; 
        prevA.normal = sphereVerts[vertIndex++].normal = glm::normalize(V3{x * zr0, y * zr0, z0});//.normalized();
        prevA.polar = V2{radial, angular};
        prevB.pos = sphereVerts[vertIndex].pos = V3{x * zr1, y * zr1, z1};
        prevB.normal = sphereVerts[vertIndex++].normal = glm::normalize(V3{x * zr1, y * zr1, z1});//.normalized(); 
        prevB.polar = V2{radialEnd, angular};
      }
    }
  }
  
  if(!mesh->upload()) {
    Console::Error("Unable to upload skydome mesh!");
  }
  
  shader = Shader::Load("pipeline/skydome.prgm");
  
  ImageRGBA *tmpImg = new ImageRGBA("fx/noise0.png");
  noise = new Texture2D(tmpImg);
  noise->setWrap(TextureWrap::Repeat);
  noise->setFilter(TextureFilter::Linear);
  noise->commit();
  delete tmpImg;
  tmpImg = new ImageRGBA("fx/skyGradient.png");
  gradient = new Texture2D(tmpImg);
  gradient->setWrap(TextureWrap::Clamp);
  gradient->setFilter(TextureFilter::Linear);
  delete tmpImg;
}

void Skydome::UnloadResources() {
  if(mesh != NULL) {
    delete mesh;
    mesh = NULL;
  }
  if(shader != NULL) {
    delete shader;
    shader = NULL;
  }
}
