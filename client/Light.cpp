#include "Light.h"

Light::Light(Type t, ColorRGBA c, float inten) {
  cookie = NULL;
  type = t;
  color = c;
  intensity = inten;
  //renderer->addLight(this);
}

Light::~Light() {
  //renderer->removeLight(this);
}

void Light::setCookie(Texture2D *nc, bool colored) {
  useProjection = colored;
  cookie = nc;
  useCookie = (nc != NULL);
}

bool Light::operator <(const Light& o) {
  return priority < o.priority;
}