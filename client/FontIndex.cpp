#include "FontIndex.h"


using namespace ARKE;

void FontIndex::initVar() {
  charMin = 0xFFFF; charMax = 0;
  charCount = 0;
  nullLookup.x = nullLookup.y = 0;
  nullLookup.w = nullLookup.h = 0;
  nullLookup.ox = nullLookup.oy = 0;
  nullLookup.ow = nullLookup.oh = 0;
  nullLookup.selfID = 0xFFFF;
  ftSize = 12;
  atlas = NULL;
}

FontIndex::FontIndex() {
  initVar();
  useSDF = false;
}

FontIndex::FontIndex(TextureAtlas *at, unsigned short pt) {
  initVar();
  this->setAtlas(at);
  this->setSize(pt);
}

FontIndex::~FontIndex() {
  this->unload();
}

bool FontIndex::loadFont(std::string path, unsigned short rangeOffset, unsigned short firstChar, unsigned short lastChar) { _prsguard();
  if(atlas == NULL) return false;
  
  Font *font = new Font(atlas, path, ftSize, useSDF);
  if(font != NULL) {
    FontRange range;
    range.start = rangeOffset+firstChar;
    range.end = range.start + (lastChar-firstChar);//rangeOffset + lastChar - firstChar;
    if(range.start < charMin) charMin = range.start;
    if(range.end > charMax) charMax = range.end;
    range.offset = firstChar;
    range.active = true;
    range.font = font;
    fonts.push_back(range);
    font->saveCache();
  }
  return true;
}
void FontIndex::unload() {
  for(auto it = fonts.begin(); it != fonts.end(); it++) {
    delete it->font;
  }
}

ImageAtlas::Lookup& FontIndex::chr(const unsigned short idx, Font::Kerning *const k) { _prsguard();
  if(idx >= charMin && idx <= charMax) {
    for(auto it = fonts.begin(); it != fonts.end(); it++) {
      //std::cout << "comparing range " << it->start << " ->" <<it->end << "\n";
      if(it->active && it->contains(idx)) {
        
        //std::cout << "chr(" << (idx - it->start + it->offset) << ")\n";
        return it->font->chr(idx - it->start + it->offset, k);
      }
    }
  }
  std::stringstream ss; ss << "FontIndex::chr(): no character set providing " << idx << " was found!\n";
  Console::Warning(ss.str());
  if(k != NULL) {
    k->adv = k->bX = k->bY = k->oX = k->oY = 0;
  }
  return nullLookup;
}

void FontIndex::setAtlas(TextureAtlas *na) {
  atlas = na;
}

void FontIndex::setSize(unsigned short pt) {
  ftSize = pt;
}


unsigned short FontIndex::lineAdv(const unsigned short idx) { _prsguard();
  if(idx == 0xFFFF) {
    unsigned short ret = 0;
    for(auto it = fonts.begin(); it != fonts.end(); it++) {
      if(it->active) ret = max(ret, ((unsigned short)it->font->lineAdv));
    }
    return ret;
  } else {
    if(idx >= charMin && idx <= charMax) {
      for(auto it = fonts.begin(); it != fonts.end(); it++) {
        if(it->active && it->contains(idx)) return it->font->lineAdv;
      }
    }
  }
  return 0;
}
