#ifndef RENDERER_INC
#define RENDERER_INC
#include "Matrix.h"
#include "Camera.h"
#include "V2.h"
#include "Application.h"
#include <iostream>
#include <vector>
#include "Transform.h"
#include "Matrix.h"
#define _drawcall() renderer->drawcalls++;

class Window;
class MeshContainer;
class RenderPipeline;

class Renderer {
public:
  Renderer(Window *const w);
  ~Renderer();
  
  inline bool isActive() { return active; };
  inline const Mat4& matrix();
  inline const Mat4& mvMatrix();
  inline const Mat4& projMatrix();
  
  inline RenderPipeline* pipe() { return activePipeline; };
  
  void usePipeline(RenderPipeline *const p);
  void setBound(MeshContainer *const mesh);
  
  inline float getAspect() { return (targetSize.x / targetSize.y); };
  bool wasResized() { return (resizeFrame == _globalFrame); };
  void newFrame();
  
  unsigned int drawcalls;
  V2 targetSize;
  
  void resize();
  
  void SetActive();
  Uint32 resizeFrame;
  
  static void PushMatrix(const Mat4& mat);
  static void PopMatrix();
  static void PushTransform(const Transform& trans);
  static void PopTransform();
protected:
  bool active;
  Window* window;
  MeshContainer *boundMesh;
  RenderPipeline *activePipeline;
  
  Mat4 modelMatrix, effectiveMatrix, modelViewMatrix;
  std::vector<Mat4> modelMatrixStack;
};

inline const Mat4& Renderer::matrix() {
    effectiveMatrix = (activeCamera->getMatrix() * this->modelMatrix);
    return effectiveMatrix;
}

inline const Mat4& Renderer::mvMatrix() {
    modelViewMatrix = (activeCamera->getViewMatrix() * this->modelMatrix);
    return modelViewMatrix;
}

inline const Mat4& Renderer::projMatrix() {
    return activeCamera->projMatrix();
}

extern thread_local Renderer* renderer;

#endif
