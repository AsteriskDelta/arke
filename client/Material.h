#ifndef INC_MATERIAL
#include "client.h"

class Material {
public:
    Material();
    ~Material();
    
    bool isOpaque();
    bool isTransparent();
    
    void addShader(Shader *const sh);
protected:
    SizedList<
};

#endif