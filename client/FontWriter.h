/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FontWriter.h
 * Author: Galen Swain
 *
 * Created on April 27, 2016, 7:17 AM
 */

#ifndef FONTWRITER_H
#define FONTWRITER_H
#include "client.h"
#include "Font.h"
#include "FontIndex.h"
#include "ParticleSystem.h"
#include "TextureAtlas.h"


namespace FontWriter {
    //void Write(FontIndex *font, ParticleSystem *sys, const std::string& txt, V3 pos, float sizePx);
    void Write(FontIndex *font, ParticleSystem *sys, const std::string& txt, float sizePx, V3 pos, V3 right = V3(1.f, 0.f, 0.f), V3 up = V3(0.f, 1.f, 0.f));

    void WriteBlock(FontIndex *font, ParticleSystem *sys, const std::string& txt, float sizePx, V3 pos, V2 bounds, V3 right = V3(1.f, 0.f, 0.f), V3 up = V3(0.f, 1.f, 0.f));
};

#endif /* FONTWRITER_H */

