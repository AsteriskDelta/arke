#define INC_MESH_CPP
#ifndef INC_MESH_CPP


#include "Mesh.h"
#include "rInterface.h"
#include "Renderer.h"
#include <cstdarg>
#include <cstring>
#include <iostream>

inline MeshContainer::~MeshContainer() {
  
}

inline void MeshContainer::setBound(bool b) { 
  //_unused(b);
  //Excessive pointer hacking below due to C++ limitations ;-;
  //Template type is dealt with only as pointer, so base functions will work regardles...
  Mesh<int> *const mThis = (Mesh<int>*)this;
  if(mThis != NULL) mThis->bound = b;
};
inline void MeshContainer::addAttribute(Uint8 &offset, Uint8 count, MeshAttribute attr, Uint8 size, bool normalized) {
  _unused(offset); _unused(count); _unused(attr); _unused(size); _unused(normalized);
}

template <typename T, typename IT>
Mesh<T,IT>::Mesh(MeshType newType, Uint32 numVerts, Uint32 numInd, bool dyn) {
  vertCountMax = numVerts;
  indCountMax = numInd;
  isTessellated = false;
  patchCnt = 0;
  
  if(numVerts == 0) data = NULL;
  else data = new T[numVerts];
  if(numInd == 0) indices = NULL;
  else indices = new IT[numInd];
  
  vertCount = indCount = 0;
  gpuID = gpuBufferID = gpuIndID = gpuVertCount = 0;
  bound = false;
  isDynamic = false;
  setType(newType);
  attributeCount = 0;
  isDynamic = dyn;
}

template <typename T, typename IT>
Mesh<T,IT>::~Mesh() {
  deallocateGPU();
  deallocate();
}

#ifdef USEGL
template <typename T, typename IT>
void Mesh<T,IT>::addAttribute(Uint8 &offset, Uint8 count, MeshAttribute attr, Uint8 size, bool normalized) {
  GlUint type;
  switch(attr) {
    case MeshAttribute::Signed:
      if(size == 1) type = GL_BYTE;
      else if(size == 2) type = GL_SHORT;
      else if(size == 3) type = GL_INT;
      else Console::Error("Unknown mesh attribute signed type!");
      break;
    case MeshAttribute::Unsigned:
      if(size == 1) type = GL_UNSIGNED_BYTE;
      else if(size == 2) type = GL_UNSIGNED_SHORT;
      else if(size == 3) type = GL_UNSIGNED_INT;
      else Console::Error("Unknown mesh attribute signed type!");
      break;
    case MeshAttribute::Float:
      type = GL_FLOAT;
      break;
    case MeshAttribute::Double:
      type = GL_DOUBLE;
      break;
    default:
      std::cerr << "Unknown attribute type in Mesh::addAttribute: " << int(attr) << std::endl;
      return;
      break;
  };
  
  glEnableVertexAttribArray(attributeCount);
  riEnsureState();
  //std::cout << "Added attribute: #" << int(attributeCount) << " of " << int(count) <<"x"<<int(size)<< " at sz " << sizeof(T) << "\n";
  if(!normalized && (attr == MeshAttribute::Signed || attr == MeshAttribute::Unsigned)) {
    glVertexAttribIPointer(attributeCount, count, type, normalized, sizeof(T), (void*)Uint64(offset));
  } else {
    glVertexAttribPointer(attributeCount, count, type, normalized, sizeof(T), (void*)Uint64(offset));
  }
  riEnsureState();
  attributeCount++;
  offset += count*size;
}
#endif

template <typename T, typename IT>
void Mesh<T,IT>::allocate(Uint32 numVerts, Uint32 numInd, bool dyn) {
  T *const newData = new T[numVerts];
  IT *const newInd = new IT[numInd];
  isDynamic = dyn;
  
  if(data != NULL) {
    vertCount =  std::min(std::min(numVerts, vertCount), vertCountMax);
    memcpy(newData, data, vertCountMax * sizeof(T) );
    //deallocate();
  }
  if(indices != NULL) {
    indCount =  std::min(std::min(numInd, indCount), indCountMax);
    memcpy(newInd, indices, indCountMax * sizeof(IT) );
    //deallocate();
  }
  deallocate();
  
  vertCountMax = numVerts;
  indCountMax = numInd;
  data = newData;
  indices = newInd;
}

template <typename T, typename IT>
void Mesh<T,IT>::deallocate() {
  if(data != NULL) {
    delete[] data;
    data = NULL;
    vertCountMax = 0;
  }
  if(indices != NULL) {
    delete[] indices;
    indices = NULL;
    indCountMax = 0;
  }
}

template <typename T, typename IT>
void Mesh<T,IT>::deallocateGPU() {
  if(gpuID == 0) return;
  attributeCount = 0;
  if(gpuBufferID != 0) glDeleteBuffers(1, &gpuBufferID);
  if(gpuIndID != 0) glDeleteBuffers(1, &gpuIndID);
  if(gpuID != 0) glDeleteVertexArrays(1, &gpuID);
  riEnsureState();
  gpuID = 0;
  gpuBufferID = gpuIndID = 0;
}

template <typename T, typename IT>
bool Mesh<T,IT>::unbind() {
  if(bound) renderer->setBound(NULL);
  return true;
}

template <typename T, typename IT>
void Mesh<T,IT>::clear() {
  vertCount = 0;
  indCount = 0;
}

template <typename T, typename IT>
void Mesh<T,IT>::scale(float factor) {
  for(unsigned int i = 0; i < vertCountMax; i++) {
    data[i].pos *= factor;
  }
}
#ifdef MESH2D
template <typename T, typename IT>
inline void Mesh<T,IT>::getProjectedRange(const V3& v, float* tmin, float *tmax) const {
  return;
}
#else
template <typename T, typename IT>
inline void Mesh<T,IT>::getProjectedRange(const V3& v, float* tmin, float *tmax) const {
  float minv = 9999999.f, maxv = -9999999.f;
  V3 minp = V3(Maxf), maxp = V3(-Maxf);
  for(unsigned int i = 0; i < vertCount; i++) {
    //const float posLen = glm::length(data[i].pos);
    //minv = min(minv, glm::dot(data[i].pos, v)*posLen);
    //maxv = max(maxv, glm::dot(data[i].pos, v)*posLen);
    minp = glm::min(minp, glm::dot(data[i].pos, v)*v);
    maxp = glm::max(maxp, glm::dot(data[i].pos, v)*v);
  }
  //std::cout << "MINMAXMM: " << glm::to_string(minp) << " -> " << glm::to_string(maxp) << "\n";
  minv = -glm::length(minp);
  maxv = glm::length(maxp);
  
  if(tmin != nullptr) *tmin = minv;
  if(tmax != nullptr) *tmax = maxv;
}
#endif

template <typename T, typename IT>
void Mesh<T,IT>::setBound(bool s) {
  bound = s;
}

#ifdef USEGL
template <typename T, typename IT>
bool Mesh<T,IT>::upload() { _prsguard();
  if(true/*!isDynamic*/) {
    if(gpuID != 0) deallocateGPU();
    
    glGenVertexArrays(1, &(gpuID));
    
    riEnsureState();
    //glBindVertexArray(gpuID);
    this->bind();
    riEnsureState();
    glGenBuffers(1, &gpuBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, gpuBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(T) * vertCountMax, data, this->getMeshStoreFlags());
    gpuVertCount = vertCountMax;
    
    riEnsureState();
    if(indCountMax > 0 && indices != NULL) {
      glGenBuffers(1, &gpuIndID);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuIndID);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IT) * indCountMax, indices, this->getMeshStoreFlags());
      gpuIndCount = indCountMax;
    } else gpuIndCount = 0;
    
    riEnsureState();
    //std::cout << "Upload mesh (" << vertCountMax <<","<< ( sizeof(T) * vertCountMax) <<  ") Tsz:"<<sizeof(T)<<" V3sz: " << sizeof(V3) << "\n";
    
    data[0].mapAttributes((MeshContainer*)this);
  } else {//Dynamic
    Console::Error("Dynamic mesh support not yet implemented!");
  }
  return true;
}

template <typename T, typename IT>
bool Mesh<T,IT>::subVerts(unsigned int offset, unsigned int vertc) { _prsguard();
  if(offset + vertc > gpuVertCount) return upload();//Not enough space, normal upload
  this->bind();
  glBindBuffer(GL_ARRAY_BUFFER, gpuBufferID);
  glBufferSubData(GL_ARRAY_BUFFER, offset*sizeof(T), vertc*sizeof(T), &(data[offset]));
  //glBufferSubData(GL_ARRAY_BUFFER,0, vertCount*sizeof(T), &(data));
  riEnsureState();
  return true;
}
template <typename T, typename IT>
bool Mesh<T,IT>::subInd(unsigned int offset, unsigned int indc) { _prsguard();
  if(offset + indc > gpuIndCount) {//We'd have overrun our ind buffer, throw an error!!! -Realloc and upload-
    Console::Error("Mesh::subInd call overran indCountMax!");
    return false;
    //gpuIndCount = offset + indc;
    //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(IT) * gpuIndCount, indices, this->getMeshStoreFlags());
    //riEnsureState();
    //gpuIndCount = indCountMax;
  } else {
    
  this->bind();
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gpuIndID);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, offset*sizeof(IT), indc*sizeof(IT), &(indices[offset]));
    
    riEnsureState();
  }
  return true;
}

template <typename T, typename IT>
bool Mesh<T,IT>::bind() { _prsguard();
  renderer->setBound((MeshContainer*)this);
  glBindVertexArray(gpuID);
  return true;
}

template <typename T, typename IT>
bool Mesh<T,IT>::draw() { _prsguard();
    if(vertCount == 0) return true;//Fast for empty/cleared mesh
    
  if(!bound) this->bind();
  
  if(isTessellated) {
    glPatchParameteri(GL_PATCH_VERTICES, patchCnt);
  }
  
  //std::cout << gpuType << ", " << gpuVertCount << " ind:"<<gpuIndCount<<"\n";
  if(gpuIndID != 0 && gpuIndCount > 0) {
    //std::cout << "Drawing elements..\n";
    glDrawElements(gpuType, std::min(gpuIndCount, indCount), GL_UNSIGNED_INT, (void*)0 );//last is offset
  } else if(gpuVertCount > 0) {
    //std::cout << "drawing array...\n";
    //if(gpuType == GL_POINTS) std::cout << "Drawing PTS!!!\n";
    glDrawArrays(gpuType, 0, gpuVertCount);
  }
  _drawcall();
  return true;
}
template <typename T, typename IT>
void Mesh<T,IT>::setVertCount(Uint32 c) {
  vertCount = c;
}

template <typename T, typename IT>
void Mesh<T,IT>::setIndCount(Uint32 c) {
  indCount = c;
}

template <typename T, typename IT>
void Mesh<T,IT>::setType(MeshType newType) {
  if(isTessellated) {
    gpuType = GL_PATCHES;
    return;
  }
  
  //std::cout << "Mesh::SETTYPE caleld!\n";
  switch(newType) {
    case MeshType::Points:
      //std::cout << "SEL pts\n";
      gpuType = GL_POINTS;
      //patchCnt = 1;
      break;
    case MeshType::Tris:
      //std::cout << "SEL tris\n";
      gpuType = GL_TRIANGLES;
      //patchCnt = 3;
      break;
    default:
    case MeshType::Quads:
      //std::cout << "SEL quads\n";
      gpuType = GL_QUADS;
      //patchCnt = 4;
      break;
    case MeshType::Lines:
      gpuType = GL_LINES;
      //patchCnt = 2;
      break;
  };
}

template <typename T, typename IT>
void Mesh<T,IT>::setTessellated(unsigned int cnt) {
  isTessellated = (cnt > 0);
  patchCnt = cnt;
  if(isTessellated) {
    gpuType = GL_PATCHES;
  }
}

template <typename T, typename IT>
unsigned int  Mesh<T,IT>::getMeshStoreFlags() {
        return isDynamic? GL_DYNAMIC_DRAW : GL_STATIC_DRAW;
    }
#endif

#endif
