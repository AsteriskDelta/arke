#ifndef TRANSFORM_INC
#define TRANSFORM_INC
#include "client.h"
#include "V3.h"
#include "V4.h"
#include "Quat.h"
#include "DualQuat.h"
#include "Matrix.h"

#define TRANSFORM_DEPTH_LIM 255

class Transform {
public:
  inline Transform(const V3& pos = V3(0.f), const Quat& rot = Quat(), const V3& sc = V3(1.f)) : position(pos), rotation(rot), ratio(sc), parent(NULL) {
    
  };
  
  V3 position;
  Quat rotation;
  V3 ratio;
  Transform *parent;
  
  inline operator Mat4() { _prsguard();
    return glm::translate(-position) * glm::mat4_cast(rotation) *  glm::scale(ratio);
  }
  
  inline Mat4 matrix() const{ _prsguard();
      return glm::translate(position) * glm::mat4_cast(rotation) *  glm::scale(ratio);
  }
  inline Mat4 inverseMatrix() const { _prsguard();
      return glm::inverse(matrix());
  }
  inline Mat4 toMatrix() const{ return matrix(); };
  inline Mat4 fromMatrix() const { return inverseMatrix(); };
  
  inline DualQuat dualQuat() const { _prsguard();
      //glm::mat3x4 m = glm::translate(-position) * glm::mat3x4_cast(rotation) *  glm::scale(ratio);
      //auto m = glm::mat3x4(glm::transpose(glm::translate(position))*2);//glm::mat3x4(glm::transpose(glm::mat3_cast(rotation)));
      //auto m = glm::mat3x4(glm::mat4_cast(rotation));
      //m = m *glm::transpose(glm::translate(position));
      //m[0][3] = position.x;
      //m[1][3] = position.y;
      //m[2][3] = position.z;
      //DualQuat ret;
      //ret.real = glm::normalize(rotation);
      //ret.dual = (Quat(position.x, position.y, position.z, 0.f) * ret.real ) * 0.5f;
      //return //DualQuat(rotation, Quat()) + 
      //       DualQuat(glm::mat3x4(glm::transpose(glm::translate(position))*2.f));//DualQuat(m);//ret;
      //return glm::dualquat_cast(glm::transpose(glm::mat4x3(V3(),V3(),V3(),position)) + glm::mat3x4(glm::mat3_cast(rotation)));
      return DualQuat(glm::mat3x4(glm::mat3_cast(rotation)) + glm::transpose(glm::mat4x3(V3(),V3(),V3(),position)));
  }
  inline DualQuat inverseDualQuat() const {
      return glm::inverse(this->dualQuat());
  }
  
  inline V3 operator*(const V3& v) const { _prsguard();
      return V3(this->matrix() * V4(v, 1.f)); 
  }
  inline V3 mkNormal(const V3& v) const {
      return V3(this->matrix() * V4(v, 0.f)); 
  }
  
  void setRotation(const V3& forward, const V3& up);
  void lookAt(const V3 &target);
  inline Transform resolve();
  
  inline void translate(V3 offset);
  inline void rotate(Quat q);
  inline void rotate(V3 eAngles);
  inline void scale(V3 s);
  
  inline V3 forward() const;
  inline V3 back() const;
  inline V3 up() const;
  inline V3 lockedUp() const;
  inline V3 down() const;
  inline V3 right() const;
  inline V3 left() const;
protected:
  void doResolve(Transform &tr, int limit);
};

inline void Transform::translate(V3 offset) {
  position += offset;
}
inline void Transform::rotate(Quat q) {
  rotation = q * rotation;
}
inline void Transform::rotate(V3 eAngles) {
  rotation = rotation * Quat(eAngles);
    /*rotation *= glm::rotate(eAngles.x, V3_RIGHT);
    rotation *= glm::rotate(eAngles.y, V3_UP);
    rotation *= glm::rotate(eAngles.z, V3_FORWARD);*/
}
inline void Transform::scale(V3 s) {
  ratio *= s;
}

V3 Transform::forward() const {
  return V3_FORWARD * rotation;
}
V3 Transform::back() const {
  return (-V3_FORWARD) * rotation;
}
V3 Transform::up() const {
  return V3_UP * rotation;
}
V3 Transform::lockedUp() const {
  return V3_UP * rotation * V3(0.f, 1.f, 1.f);
}
V3 Transform::down() const {
  return (-V3_UP) * rotation;
}
V3 Transform::right() const {
  return V3_RIGHT * rotation;
}
V3 Transform::left() const {
  return (-V3_RIGHT) * rotation;
}

Transform Transform::resolve() {
  Transform ret;
  doResolve(ret, TRANSFORM_DEPTH_LIM);
  return ret;
}

#ifdef CEREAL_INCLUDED
template<class Archive>
void serialize(Archive& archive, Transform& m) {
  archive(cereal::make_nvp("position",m.position), 
          cereal::make_nvp("rotation",m.rotation),
          cereal::make_nvp("ratio",m.ratio)
          );
}
//CEREAL_REGISTER_TYPE(Transform);
#endif

#endif
