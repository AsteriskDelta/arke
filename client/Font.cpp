#include "Font.h"
#define IDT_NONE 0xFFFF
#include <sstream>
#include "Application.h"
#include "AppData.h"


using namespace ARKE;

unsigned short Font::sdfResolution = 8;

std::string Font::Kerning::toString() {
  std::stringstream ss;
  ss << "KERN " << bX << "," << bY << " +" <<oX<<","<<oY << " _" << adv;
  return ss.str();
}
Font::Font() {
  atlas = NULL;
  lineAdv = 0;
  pt = 0; sdft = 0;
  for(int i = 0; i < 128; i++) {
    idTable[i] = IDT_NONE;
    Kerning *const k = &kTable[i];
    k->adv = k->bX = k->bY = k->oX = k->oY = 0;
  }
}

Font::Font(TextureAtlas *na, std::string path, unsigned int fPt, bool sdf) { _prsguard();
  lineAdv = 0;
  for(int i = 0; i < 128; i++) {
    idTable[i] = IDT_NONE;
    Kerning *const k = &kTable[i];
    k->adv = k->bX = k->bY = k->oX = k->oY = 0;
  }
  this->setAtlas(na);
  this->load(path, fPt, sdf);
}

Font::~Font() {
  this->saveCache();
}

std::string Font::getCachePath() {
  std::stringstream ss; ss << "cache/"<<filePath<<"_p"<<pt<<"_st"<<sdft;
  return ss.str();
}

void Font::saveCache() { _prsguard();
  std::stringstream ss;
  ss << "FNTCACHE ladv=" << lineAdv << "\n";
  for(unsigned int i = 0; i < 128; i++) {
    if(idTable[i] == IDT_NONE&&i != 32) continue;
    
    ss << "CHR" << i << " #"<<idTable[i] << kTable[i].toString() <<"\n";
  }
  
  std::string str = ss.str();
  Directory dir = Application::GetLocal();
  dir.writeFile(this->getCachePath()+".txt", str.c_str(), str.size());
}
bool Font::loadCache() { _prsguard();
  std::string cachePath = this->getCachePath()+".txt";
  Directory dir = Application::GetLocal();
  if(!dir.exists(cachePath)) return false;
  
  unsigned int buffLen;
  char *buff = reinterpret_cast<char*>(dir.readFile(cachePath, buffLen, true));
  int tmpAdv;
  int headCnt = sscanf(buff, "FNTCACHE ladv=%i\n", &tmpAdv);
  if(headCnt >= 1) lineAdv = tmpAdv;
  //std::cout << "Loading font cache\n";
  unsigned int off = 0; bool nextPos = false;
  while(off < buffLen) {
    if(nextPos) {
      nextPos = false;
      int chr, idx, bx, by, ox, oy, adv;
      int cnt = sscanf(&buff[off], "CHR%i #%i KERN %i, %i +%i, %i _%i", &chr, &idx, &bx, &by, &ox, &oy, &adv);
      if(cnt > 1 && cnt < 7) {
        std::stringstream ss;
        ss << "Invalid font cache line for #" << idx << ", cnt=" << cnt << "\n";
        Console::Error(ss.str());
        continue;
      }
      if(idx < 0) {
        //std::stringstream ss; ss << "Invalid font cache ID #" << idx << "\n";
        //Console::Error(ss.str());
        idx = IDT_NONE;
        //continue;
      }
      //std::cout << "loaded " << chr << "\n";
      idTable[chr] = idx;
      Kerning *k = &kTable[chr];
      k->bX = bx; k->bY = by;
      k->oX = ox; k->oY = oy;
      k->adv = adv;
    }
    
    if(buff[off] == '\n') nextPos = true;
    off++;
  }
  
  delete[] buff;
  
  return true;
}

bool Font::load(std::string path, unsigned int fPt, bool sdf, bool force) { _prsguard();
  if(atlas == NULL) return false;
  pt = fPt; sdft = sdf? sdfResolution : 0;
  
  filePath = path;
  if(atlas->hasResource(path) && !force) {//Attempt a load from cache
    if(this->loadCache()) {
      
      return true;
    }
  }
  
  const unsigned int iSize = fPt + fPt*6/8, fOff = fPt / 8;
  ImageFont *tfont = new ImageFont(path, fPt);
  for(int c = 32; c < 127; c++) {
    ImageRGBA *gly;
    if(sdf) gly = tfont->renderGlyphQSDF(char(c),iSize, iSize, fOff, fOff, sdfResolution);
    else gly = tfont->renderGlyph(char(c),iSize, iSize, fOff, fOff);
    
    idTable[c] = atlas->emplaceMulti(path, gly);
    Kerning *const k = &kTable[c]; GlyphData *g = &(tfont->glyphs[c]);
    k->bX = g->bX; k->bY = g->bY;
    k->oX = g->oX; k->oY = g->oY;
    k->adv = g->adv;
    
    lineAdv = max(lineAdv, short(g->oY + g->h));
    delete gly;
  }
  delete tfont;
  return true;
}

void Font::setAtlas(TextureAtlas *na) {
  atlas = na;
}

static ImageAtlas::Lookup nullLK = ImageAtlas::Lookup{0,0,0,0,0,0,0,0,0,0};
ImageAtlas::Lookup& Font::chr(const unsigned short idx, Font::Kerning *const k) { _prsguard();
  if(k != NULL) *k = kTable[idx];
  auto it = atlas->find(idTable[idx]);
  if(it == nullptr) {
    if(idx != 32 && idx != 127) std::cerr << "No font char for " << idx << "\n";
    return nullLK;
  } else return *it;
}
