#ifndef LIGHT_INC
#define LIGHT_INC

#include "Transform.h"
#include "Color.h"
#include "Texture.h"

/* Functional Outline:
 * N Important lights are shadowed/cookied to screenspace intensity texture, giving easy blur on shadowing while still allowing integration of depth
 * Unimportant lights are rendered to screenspace composite a la deferred
 * 
 * C "important" lights are selected for each shaded object
 * Lighting is iterated over, essentially directional+lightA+lightB+lightC + ambient*AO + composite
 */

class Light {
public:
    enum Type {
        Ambient, Directional, Point, Spot, Tube, Area
    };
    
    Transform transform;
    
    Light(Type t = Point, ColorRGBA c = ColorRGBA(255,255,255,255), float intensity = 1.f);
    ~Light();
    
    bool operator <(const Light& o);
    
    Type type;
    ColorRGBA color;
    float intensity, radius, theta;
 
    void setCookie(Texture2D *nc, bool colored = false);
    inline Uint16 getPriority() { return priority; };
    
    void update();
    void renderShadows();
    
    //static
    
protected:
    //Cookie: grey intensity map, projection: colouring
    bool useCookie, useProjection;
    Texture2D *cookie, *projection;
    
    Uint16 priority;
};

#endif
