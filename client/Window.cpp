#include "Window.h"
#include "Application.h"
#include "Image.h"
#include "Console.h"
#include "rWindow.h"
#include "rInterface.h"

#define __FUNC__ __PRETTY_FUNCTION__

#include "Renderer.h"
#include "Input.h"

static unsigned short maskedErrorCount;
static ErrorMask* maskedErrors;

void Window::OnQuit() {
  SDL_Quit();
  
  for(unsigned short i = 0; i < maskedErrorCount; i++) {
    delete[] maskedErrors[i].data;
  }
  delete[] maskedErrors;
  maskedErrorCount = 0;
}

Window::Window(std::string newTitle, Uint16 w, Uint16 h) {
  if(newTitle == "") title = Application::GetName();
  else title = newTitle;
  if(w != 0 && h != 0) open(w, h);
}

Window::~Window() {
  close();
}

bool Window::open(Uint16 w, Uint16 h) {
  width = w; height = h;
  const int vsync = 1;
  if(maskedErrorCount == 0) SetupErrors();
  //Initialize SDL subsystems
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  //SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
  //SDL_GL_SetSwapInterval(vsync);
  
  if(SDL_Init(SDL_INIT_VIDEO) < 0) {
    printError(__FUNC__, __LINE__);
    Console::Fatal("Unable to initialize SDL!");
    return false;
  }
  
  Uint32 windowFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;// | SDL_WINDOW_BORDERLESS;
  if(width > 0 && height > 0) windowFlags |= SDL_WINDOW_RESIZABLE;
  else windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;//SDL_WINDOW_FULLSCREEN;
  /*if(Prefs.GetBool("fullscreen", false)) windowFlags |= SDL_WINDOW_FULLSCREEN;
  if(Prefs.GetBool("resizeable", true)) windowFlags  |= SDL_WINDOW_RESIZABLE;
  width  = Prefs.GetInt("windowX", 960);
  height = Prefs.GetInt("windowY", 690);*/
  pixelsPerInch = 60;//Prefs.GetInt("pixelsPerInch", 60);
  
  mainWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, windowFlags);
  //LoadIcon();
  printError(__FUNC__, __LINE__);
  if(!mainWindow) {
    Console::Fatal("Unable to open SDL/GL Window!");
  }
  
  mainContext = SDL_GL_CreateContext(mainWindow);
  printError(__FUNC__, __LINE__);
  
  //if(Prefs.GetBool("vSync", true)) SDL_GL_SetSwapInterval(1);
  if(SDL_GL_SetSwapInterval(-1) < 0) {
      SDL_GL_SetSwapInterval(1);
  }//1);//vsync);
  //SDL_SetWindowFullscreen(mainWindow, SDL_WINDOW_FULLSCREEN_DESKTOP);
  glEnable(GL_CULL_FACE);
  //glEnable(GL_TEXTURE_2D);
  //glEnable(GL_TEXTURE_2D_ARRAY);
  //glCullFace(GL_BACK);
  
  int err = glGetError();
    if(err != GL_NO_ERROR) {
      std::cout << "Window::Open OpenGL Error: 0x" << std::hex << err <<"\n";
    }
  
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable( GL_BLEND );
  glEnable(GL_MULTISAMPLE);  
  glClearDepth(1.0f);
  glLineWidth(2.2f);
  glPointSize(5.0f);
  
  glClearColor (0.0, 0.0, 0.0, 0.0 );
  //glClearColor(0.4, 0.6, 0.9, 1.0);
  glClear ( GL_COLOR_BUFFER_BIT );
  
  GLenum glew_err = glewInit();
  if (glew_err != GLEW_OK) {
    Console::Fatal("Unable to initialize GLEW!");
    Application::Quit(1);
  }
  if(!GLEW_EXT_texture_array) {
    Console::Error("No texture array support with current driver!!!");
  }
  
  {
    int majorVersion, minorVersion, maxTextures, maxTextureSize, maxUniformSize;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxTextures);
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);
    glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
    glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
    glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &maxUniformSize);
    
    std::stringstream ss;
    ss << "Renderer v" << majorVersion << "." << minorVersion << " with " << maxTextures << " texture units at a max of " << maxTextureSize << "x" << maxTextureSize << ", uniform block " << (maxUniformSize/1024)<<"kb";
    Console::Print(ss.str());
  }
  
  glHint(GL_GENERATE_MIPMAP_HINT, GL_NICEST);
  
  rContext = new Renderer(this);
  rContext->SetActive();
  rContext->targetSize = V2(width, height);
  
  for(Uint8 i = 0; i < 8; i++) {
    glEnableVertexAttribArray(i);
  }
  
  //Renderer::WindowChanged(width, height);

  SDL_GL_SwapWindow(mainWindow);
  _graphicsContextExists = true;
  
  return true;
}

void Window::close() {
  _graphicsContextExists = false;
  if(mainContext != NULL) SDL_GL_DeleteContext(mainContext);
  SDL_DestroyWindow(mainWindow);
}

void Window::setIcon(const Image<4> *const img) {
  if(img == NULL || !*img) {
    Console::Warning("Window.setIcon called with null or empty image!");
    return;
  }
  
  iconSurface = SDL_CreateRGBSurfaceFrom(img->getRawData(), img->width, img->height, img->getBPP(), img->getPitch(), RMASK, GMASK, BMASK, AMASK);
  
  SDL_SetWindowIcon(mainWindow, iconSurface);
  SDL_FreeSurface(iconSurface);
}

static SDL_Event* spoofedEvent = NULL;
void Window::processEvents() { _prsguard();
  SDL_Event event;
  //While there's an event to handle
  int checkWidth, checkHeight;
  SDL_GetWindowSize(mainWindow, &checkWidth, &checkHeight);
  
  if(width != checkWidth||height != checkHeight) {
    if(spoofedEvent == NULL) spoofedEvent = new SDL_Event();
    
    spoofedEvent->type = SDL_WINDOWEVENT_SIZE_CHANGED;
    SDL_PushEvent(spoofedEvent);
  }
  bool shouldFocus = false, wasResized = false;
  
  //Zero the input
  //Input::accelX = Input::accelY = Input::scrollX = Input::scrollY = 0;
  Input *input = NULL;
  while(SDL_PollEvent(&event)) {
    switch(event.type) {
      case SDL_KEYDOWN:
      case SDL_KEYUP:
	input = Input::GetByKeyboard(0);
	if(input != NULL) {
	  input->keyEvent(event.key.keysym.sym, event.key.keysym.scancode, event.type == SDL_KEYDOWN, event.key.keysym.mod);
	}
	//Input::KeyEvent(event.key);
	//shouldFocus = true;
	break;
      case SDL_MOUSEBUTTONDOWN:
      case SDL_MOUSEBUTTONUP:
	input = Input::GetByMouse(0);
	if(input != NULL) {
	  input->btnEvent(event.button.button - SDL_BUTTON_LEFT, event.button.state);
	}
	//Input::MouseButtonEvent(event.button);
	shouldFocus = true;
	break;
      case SDL_MOUSEWHEEL: break;//Overridden by button triggers
	input = Input::GetByMouse(0);
	if(input != NULL) {
	  if(event.wheel.x != 0) input->axisEvent(Input::Axis::ScrollX, event.wheel.x);
	  if(event.wheel.y != 0) input->axisEvent(Input::Axis::ScrollY, event.wheel.y);
	}
	//Input::MouseScrollEvent(event.wheel);
	break; 
      case SDL_MOUSEMOTION:
	//if(!focused) break;
	input = Input::GetByMouse(0);
	//std::cout << event.motion.x << ", " << event.motion.y << "\n";
	if(input != NULL) {
	  if(event.motion.x != 0) input->axisEventAbsolute(Input::Axis::MouseX, event.motion.x);
	  if(event.motion.y != 0) input->axisEventAbsolute(Input::Axis::MouseY, event.motion.y);
	}
	//Input::MouseMotionEvent(event.motion);
	break;
      //Joystick Events
      case SDL_CONTROLLERDEVICEADDED:
	//Input::AddGamepad(event.cdevice.which);
	break;
      case SDL_CONTROLLERDEVICEREMOVED:
	//Input::RemoveGamepad(event.cdevice.which);
	break;
      case SDL_CONTROLLERBUTTONDOWN:
      case SDL_CONTROLLERBUTTONUP:
	//Input::GamepadButtonEvent(event.cbutton);
	input = Input::GetByGamepad(event.cbutton.which);
	if(input != NULL) {
	  input->btnEvent(int(event.cbutton.button) - SDL_CONTROLLER_BUTTON_A + INPUT_MOUSE_BUTTONS, event.cbutton.state);//Goes after mouse buttons
	}
	break;
      case SDL_CONTROLLERAXISMOTION:
	input = Input::GetByGamepad(event.caxis.which);
	if(input != NULL) {
	  input->axisEvent(int(event.caxis.axis) - SDL_CONTROLLER_AXIS_LEFTX + Input::Axis::GPLeftX, event.caxis.value);
	}
	//Input::GamepadAxisEvent(event.caxis);
	break;
      case SDL_WINDOWEVENT_RESIZED:
      case SDL_WINDOWEVENT_SIZE_CHANGED:
	//This is VERY broken, I've had to manually emulate it for now. Dammit, SDL.
	SDL_GetWindowSize(mainWindow, &width, &height);
	if(height == 0) height = 1;
	wasResized = true;
	break;
      case SDL_QUIT:
        
  _graphicsContextExists = false;
	Application::Quit();
	break;
     }
  }
  
  if(wasResized) {
    rContext->targetSize = V2(width, height);
    rContext->resizeFrame = _globalFrame;
    rContext->resize();
  }
  
  /*if(shouldFocus && !focused) grabFocus();
  
  if(focused) {
    //Allow mouse grab
    SDL_WarpMouseInWindow(mainWindow, width / 2, height / 2);
  }*/
}

void Window::setActive() {
  SDL_GL_MakeCurrent(mainWindow, mainContext);
  renderer = this->rContext;
}

void Window::update() { _prsguard();
  if(!renderer->isActive()) {
    Console::Error("Window.update() called on inactive object!");
  }
  int err = glGetError();
    if(err != GL_NO_ERROR) {
      std::cout << "Uncaught OpenGL Error, prior to flip: 0x" << std::hex << err << std::dec <<"\n";
    }
  float targetFPS = 600.6f;
  //if(_deltaTime < ( 1000.f / targetFPS )) {
      //std::cout << "DT < " << _deltaTime << "\n";
  //    SDL_Delay( (( 1000.f / targetFPS ) - _deltaTime) );
  //  }
  { _prsguard();
  SDL_GL_SwapWindow(mainWindow);
   }
  _globalFrame++;
  
  { _prsguard();
  renderer->newFrame();
  err = glGetError();
    if(err != GL_NO_ERROR) {
      std::cout << "Uncaught OpenGL Error, after newFrame(): 0x" << std::hex << err << std::dec <<"\n";
    }
  }
  
  
  this->processEvents();
};

void Window::SetupErrors() {
  Application::OnExit(&Window::OnQuit);
  maskedErrorCount = 1;
  maskedErrors = new ErrorMask[maskedErrorCount];
  
  maskedErrors[0].text = "Unknown touch device";
  
  for(unsigned short i = 0; i < maskedErrorCount; i++) {
    maskedErrors[i].length = maskedErrors[i].text.length();
    maskedErrors[i].data = new char[maskedErrors[i].length];
    memcpy(maskedErrors[i].data, maskedErrors[i].text.c_str(), maskedErrors[i].length);
  }
}

void Window::printError(std::string func, int line) {
  const char *error = SDL_GetError();
  
  if (*error != '\0') {
    unsigned short errorLength = strlen(error);
    
    for(unsigned short i = 0; i < maskedErrorCount; i++) {
      if(errorLength == maskedErrors[i].length) {
	if(memcmp(error, maskedErrors[i].data, errorLength) == 0) return;
      }
    }
    
    std::stringstream s; 
    if(line == -1) s << "SDL Error: ";
    else s << "SDL Error in " << func << " at line " << line <<": ";
    
    s << error;
    Console::Error(s.str());
    SDL_ClearError();
  }
}

unsigned short Window::pointToPix(unsigned short pt) {
  return ceil((float)pt * (72.0f / (float)pixelsPerInch));
}

void Window::grabFocus() {
  focused = true;
  SDL_SetRelativeMouseMode((SDL_bool)true);
  //SDL_GetRelativeMouseState(&Input::mouseX, &Input::mouseY);
}

void Window::releaseFocus() {
  focused = false;
  SDL_SetRelativeMouseMode((SDL_bool)false);
  //Input::accelX = 0; Input::accelY = 0;
}
