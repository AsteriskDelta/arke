#ifndef RINT_INC
#define RINT_INC

#define riEnsureState() rIEnsureStateFunc(__FILE__, __LINE__);
bool rIEnsureStateFunc(const char* file, int line);
void rIEnsureStateError(const char *file, int line, int err);

#ifdef USEGL
  #define GL3_PROTOTYPES 1
  #ifndef GL_INCLUDED
    #include <GL/glew.h>
    #include <GL/glut.h>
    #include <GL/gl.h>
  #endif
#elif defined(USEVK)

#endif
//#include <sdl2/SDL.h>

#endif