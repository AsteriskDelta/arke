#include "Console.h"
#include "Math3D.h"
#include "Texture.h"
#include "rInterface.h"
#include "Renderer.h"

#define SHADER_CPP
#include "Shader.h"

#define MAX_SHADER_LENGTH 65536
#define RAPIDXML_NO_EXCEPTIONS
#define NDEBUG
#include <rapidxml.hpp>
#include "AppData.h"
#include <cstring>
#include "UniformBlock.h"

#define SHADER_BIND_CHECK() if(location > uniformMax) return;


using namespace ARKE;

static std::string currentPath;
static unsigned char stage;
static bool parsingFailed;

static bool checkGlShader(GlInt shader, std::string file) {
  GlInt status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if (status == GL_FALSE) {
    int logLength = 0;
    static char* logBuffer = new char[1024];
    glGetShaderInfoLog(shader, 1024, &logLength, logBuffer);
    std::stringstream ss; ss << "OpenGL Shader compilation error at stage " << stage << " in " << file << ":\n" << logBuffer << "\n";
    Console::Error(ss.str());
    return false;
  }
  
  return true;
}

static bool checkGlProgram(GLuint program, const char* file) {
  GlInt status;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  
  int logLength;
  static char* logBuffer = new char[4096];
  memset(logBuffer, 0x0, 4096);
  
  if(status == GL_FALSE) {
    glGetProgramInfoLog(program, 4096, &logLength, logBuffer);
    std::stringstream ss; ss << "OpenGL Program linking error in " << file << ":\n" << logBuffer << "\n";
    Console::Error(ss.str());
    return false;
  } else {
    glGetProgramInfoLog(program, 4096, &logLength, logBuffer);
     if(logLength > 0) {
      std::stringstream ss; ss << "OpenGL Program info message in " << file << ":\n" << logBuffer << "\n";
      Console::Warning(ss.str());
    }
    glValidateProgram(program);
    glGetProgramInfoLog(program, 4096, &logLength, logBuffer);
    if(logLength > 0) {
      std::stringstream ss; ss << "OpenGL Program validation message in " << file << ":\n" << logBuffer << "\n";
      Console::Warning(ss.str());
    }
  }
  
  return true;
}

Shader::Shader() {
  buffer = NULL;
  id = 0;
  uniforms = NULL;
  uniformCount = 0;
  compiled = false;
}
Shader::~Shader() {
  if(buffer != NULL) delete[] buffer;
}


bool Shader::define(std::string key, std::string value) {
  std::stringstream ss; ss << "#define " << key << " " << value << "\n";
  strDefine.append(ss.str());
  return true;
}

void Shader::activate() { _prsguard();
  if(!compiled) return;
  
  glUseProgram(id);
  //std::cout << "\t\tBinding renderer->matrix()\n";
  glUniformMatrix4fv(activeMatLocation, 1, GL_FALSE, &(renderer->matrix()[0][0]));
  glUniformMatrix4fv(modelViewLocation, 1, GL_FALSE, &(renderer->mvMatrix()[0][0]));
  glUniformMatrix4fv(projMatLocation, 1, GL_FALSE, &(renderer->projMatrix()[0][0]));
};
void Shader::deactivate() {
  glUseProgram(0);
}

void Shader::bindTexture(const Texture2D *const tex, const unsigned char location, const unsigned int unit) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform1i(uniforms[location], unit);
  glActiveTexture(GL_TEXTURE0 + unit);
  glBindTexture(GL_TEXTURE_2D, tex->getID());
};
void Shader::bindUnit(const unsigned char location, const unsigned int unit) {
  SHADER_BIND_CHECK();
  glUniform1i(uniforms[location], unit);
};
void Shader::bindTextureArray(const TextureArray *const tex, const unsigned char location, const unsigned int unit) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform1i(uniforms[location], unit);
  glActiveTexture(GL_TEXTURE0 + unit);
  glBindTexture(GL_TEXTURE_2D_ARRAY, tex->getID());
};
void Shader::bindMat4(const glm::mat4& mat, const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniformMatrix4fv(uniforms[location], 1, GL_FALSE, &(mat[0][0]));
};
void Shader::bindMat3(const glm::mat3& mat, const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniformMatrix3fv(uniforms[location], 1, GL_FALSE, &(mat[0][0]));
};
void Shader::bindColor(const Color4 &color   , const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform4f(uniforms[location], color.flt(0), color.flt(1), color.flt(2), color.flt(3));
};
void Shader::bindV4(const V4 &v   , const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform4f(uniforms[location], v.x, v.y, v.z, v.w);
};
void Shader::bindV3(const V3 &v   , const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform3f(uniforms[location], v.x, v.y, v.z);
};
void Shader::bindV2(const V2 &v   , const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform2f(uniforms[location], v.x, v.y);
};
void Shader::bindFloat(const float f   , const unsigned char location) { _prsguard();
  SHADER_BIND_CHECK();
  glUniform1f(uniforms[location], f);
};
void Shader::bindInt(const int v, const Uint8 location) {
  SHADER_BIND_CHECK();
  glUniform1i(uniforms[location], v);
}
void Shader::bindUint(const unsigned int v, const Uint8 location) {
  SHADER_BIND_CHECK();
  glUniform1ui(uniforms[location], v);
}

void Shader::bindBlock(const UniformBlock *const b, const Uint8 location) { _prsguard();
  SHADER_BIND_CHECK();
  
  glBindBufferBase(GL_UNIFORM_BUFFER, uniforms[location], b->getBindID());
}

inline std::string getVersionStr(rapidxml::xml_node<> *const n) {
  if(n->first_attribute("version") != NULL) {
    std::string ret = "#version ";
    ret.append(std::string(n->first_attribute("version")->value()));
    ret.append("\n");
  }
  
   return std::string("#version 450\n");
}

inline std::string getShaderSrc(std::string path) {
  Directory *dir = &AppData::base;
  unsigned int length; char* buffer;

  if (path == "" || (buffer = dir->readFileText(path, length)) == nullptr || length == 0) {
    std::stringstream ss;
    ss << "Requested shader include at path \"" << path << "\" could not be found or is empty!";
    Console::Error(ss.str());
    return "";
  }
  
  std::string ret = std::string(buffer);
  delete[] buffer;
  return ret;
}

inline bool shaderParseSegment(GlUint shaderID, GlUint segmentType, rapidxml::xml_node<> *node, std::string path) { _prsguard();
  using namespace rapidxml;
  
  switch(segmentType) {
    case GL_VERTEX_SHADER:
      stage = 'V'; break;
    case GL_TESS_CONTROL_SHADER:
      stage = 'C'; break;
    case GL_TESS_EVALUATION_SHADER:
      stage = 'E'; break;
    case GL_GEOMETRY_SHADER:
      stage = 'G'; break;
    case GL_FRAGMENT_SHADER:
      stage = 'F'; break;
    default:
      stage = '?'; break;
  }
  
  GLuint segmentID;
  std::string segmentPath = "";
  //char* segmentSrc = NULL;
  std::string segmentSrc = "";
  //unsigned int segmentLength;
  
  segmentID = glCreateShader(segmentType);
  //segmentLength = 0;
  //std::cout << "PRINTING ATTS for " << node->name() << " at " << path << "\n";
  //for (xml_attribute<> *attr = node->first_attribute(); attr; attr = attr->next_attribute()) {
  //    std::cout << "SHD attr" << attr->name() << " -> " <<attr->value() << "\n";
  //}
  //Leading version string
  segmentSrc += getVersionStr(node);
  const unsigned int versionSize = segmentSrc.size();
  for (xml_attribute<> *attr = node->first_attribute("include"); attr; attr = attr->next_attribute("include")) {
    segmentSrc += getShaderSrc(std::string(attr->value()));
      //std::cout << "SHD include " << attr->value() << "\n";
      //std::cout << getShaderSrc(std::string(attr->value())) << "\n";
  }
  
  if(node->first_attribute("path") == NULL) {
    segmentPath = path;
    if(node->value_size() == 0) segmentSrc += std::string(node->first_node()->value());
    else segmentSrc += std::string(node->value());
  } else {
    char* tmpPath = (char*)node->first_attribute("path")->value();
    int tmpLen = node->first_attribute("path")->value_size();
    
    if(tmpLen > 0 && (std::string)tmpPath != "null") {
      //segmentSrc += std::string(node->value());
      //std::cout << "SHD include " << tmpPath << "\n";
      segmentSrc += getShaderSrc(std::string(tmpPath));
      //std::cout << getShaderSrc(std::string(tmpPath)) << "\n";
      segmentPath = tmpPath;
      /*if(!Data.GetFile(segmentPath, segmentSrc, segmentLength)) {
	std::stringstream ss; ss << "Unable to include \"" << segmentPath << "\", requested in \"" << path << "\"!";
	Console::Error(ss.str());
      }*/
      //Console::Error("Shader dynamic inclusion not supported!");
    }
  }
  
  if(segmentSrc.size() > versionSize) {
    glShaderSource(segmentID, 1, (const char **)&segmentSrc, NULL);
    glCompileShader(segmentID);
    if(!checkGlShader(segmentID, segmentPath.c_str())) return false;
    glAttachShader(shaderID, segmentID);
  }
  
  //if(segmentLength != 0 && segmentSrc != NULL) delete[] segmentSrc;
  
  return true;
}

bool Shader::compile() { _prsguard();
  using namespace rapidxml;
  xml_document<> doc;
  
  //unsigned char* buffer = NULL;
  unsigned int length;
  currentPath = path; parsingFailed = false;
  Directory *dir = &AppData::base;
  
  if(path == "" || (buffer = dir->readFile(path, length, true)) == NULL || length == 0) {
    std::stringstream ss; ss << "Requested shader at path \"" << path << "\" could not be found or is empty!";
    Console::Error(ss.str());
    return false;
  }

  
  try {
    doc.parse<parse_full>((char*)buffer);
  } catch(...) {
    return false;
  }
  
  if(parsingFailed) return false;
  
  xml_node<> *shaderNode, *vertNode, *geomNode, *fragNode, *uniNode, *tcsNode, *tesNode;
  shaderNode = doc.first_node("shader");
    
  if(shaderNode == NULL) {
    std::stringstream ss; ss << "Shader at path \"" << path << "\" contains no shader node!";
    Console::Error(ss.str());
    return false;
  }
  
  if(shaderNode->first_attribute("name") != NULL) {
    name = (std::string)((char*)shaderNode->first_attribute("name")->value());
  }
  
  uniformCount = 0; uniformMax = 0;
  if(shaderNode->first_attribute("maxUniform") != NULL && shaderNode->first_attribute("maxUniform")->value_size() > 0) {
    char* rawUniform = (char*)(shaderNode->first_attribute("maxUniform")->value());
    uniformMax = (unsigned short)atoi(rawUniform);
  }
  
  uniNode = shaderNode->first_node("uniform");
  while(uniNode != NULL) {
    uniformCount++;
    char* rawUniform = (char*)(uniNode->first_attribute("id")->value());
    uniformMax = max((unsigned short)atoi(rawUniform), uniformMax);
    uniNode = uniNode->next_sibling("uniform");
  }
  uniNode = shaderNode->first_node("uniform");
  if(uniformCount == 0) uniforms = NULL;
  else uniforms = new GlInt[uniformMax + 1];
  
  vertNode = shaderNode->first_node("vertex");
  geomNode = shaderNode->first_node("geometry");
  fragNode = shaderNode->first_node("fragment");
  tcsNode  = shaderNode->first_node("tcs");
  tesNode  = shaderNode->first_node("tes");
  /*if(fragNode != nullptr) {
    std::cout << "FRAGNODE: " << fragNode->name() << " with attr:";
   for (xml_attribute<> *attr =fragNode->first_attribute(); attr; attr = attr->next_attribute()) {
   // segmentSrc += getShaderSrc(std::string(attr->value()));
      std::cout << "Frag attr " << attr->name() << " = " << attr->value() << "\n";
      //std::cout << getShaderSrc(std::string(attr->value())) << "\n";
  }
  }*/
  //if(vertNode != NULL&&vertNode->value_size() == 0) vertNode = vertNode->first_node();
  //if(geomNode != NULL&&geomNode->value_size() == 0) geomNode = geomNode->first_node();
  //if(fragNode != NULL&&fragNode->value_size() == 0) fragNode = fragNode->first_node();
  //if(tcsNode  != NULL&&tcsNode->value_size()  == 0) tcsNode  = tcsNode->first_node();
  //if(tesNode  != NULL&&tesNode->value_size()  == 0) tesNode  = tesNode->first_node();
  
  id = glCreateProgram();
  
  std::string version;
  
  if((
    !(vertNode == NULL || shaderParseSegment(id, GL_VERTEX_SHADER, vertNode, path)) ||
    !(tcsNode  == NULL || shaderParseSegment(id, GL_TESS_CONTROL_SHADER, tcsNode, path)) ||
    !(tesNode  == NULL || shaderParseSegment(id, GL_TESS_EVALUATION_SHADER, tesNode, path)) ||
    !(geomNode == NULL || shaderParseSegment(id, GL_GEOMETRY_SHADER, geomNode, path)) ||
    !(fragNode == NULL || shaderParseSegment(id, GL_FRAGMENT_SHADER, fragNode, path))   
  )) return false;
  
  doc.clear();
  glLinkProgram(id);
  compiled = checkGlProgram(id, path.c_str());
  activeMatLocation = glGetUniformLocation(id, "activeMatrix");
  modelViewLocation = glGetUniformLocation(id, "modelViewMatrix");
  projMatLocation = glGetUniformLocation(id, "projectionMatrix");
  
  /*if(activeMatLocation < 0) {
    std::stringstream ss; ss << "Unable to locate activeMatrix in shader \"" << path << "\"!";
    Console::Warning(ss.str());
  }*/
  
  for(unsigned short u = 0; u < uniformCount; u++) {
    if(uniNode == NULL) break;
    unsigned short uid = u;
    if(uniNode->first_attribute("id") != NULL && uniNode->first_attribute("id")->value_size() > 0) {
      char* rawID = (char*)uniNode->first_attribute("id")->value();
      uid = (unsigned char)atoi(rawID);
    } 
    
    if(uniNode->first_attribute("name") == NULL || uniNode->first_attribute("name")->value_size() == 0) {
      std::stringstream ss; ss << "No name attribute with content found on uniform node when parsing " << path << "!";
      Console::Warning(ss.str());
    } else {
      bool isBlock = (uniNode->first_attribute("block") != NULL);
      std::string name = (std::string)(char*)uniNode->first_attribute("name")->value();
    
      if(isBlock) {
        GlUint blockIndex = glGetUniformBlockIndex(id, name.c_str());
        glUniformBlockBinding(id, blockIndex, uniforms[uid]);
      } else uniforms[uid] = glGetUniformLocation(id, name.c_str());
      
      
      if(uniforms[uid] < 0 && false) {//Enable noisy uniform errors?
	std::stringstream ss; ss << "Uniform variable \"" << name << "\" not found when parsing " << path << "!";
	Console::Warning(ss.str());
      }
    }
    uniNode = uniNode->next_sibling("uniform");
  }
  
  if(uniNode != NULL) {
    std::stringstream ss; ss << "Shader " << path << " has more uniforms declared than specified!";
    Console::Warning(ss.str());
  }
  
  glUseProgram(0);
  
  int err = glGetError();
  if(err != GL_NO_ERROR) {
    std::stringstream ss; ss << "Error after program linking, 0x" << std::hex << err << std::dec << ".";
    Console::Error(ss.str());
  }
  return compiled;
}
/*
void rapidxml::parse_error_handler(const char *what, void *where) {
  _unused(where);
  std::stringstream ss; ss << "XML parsing error \"" << what << "\" in file \"" << currentPath << "\", caused by \"";
  for(char* p = (char*)where; *p != 0x0A && *p != 0x00; p++) {
    ss << *p;
  }
  ss << "\"";
  Console::Error(ss.str());
  parsingFailed = true;
  
  //Because rapidXML is a little b1tch, force a stack unwind to our earlier try{} block
  throw -1;
};*/

/*
static void printMat(glm::mat4  mat){
  int i,j;
  for (j=0; j<4; j++){
    for (i=0; i<4; i++){
    printf("%f ",mat[i][j]);
  }
  printf("\n");
 }
}*/

Shader* Shader::Load(std::string np, bool isPure) {
  _unused(isPure);
  Shader* ret = new Shader();
  ret->path = np;
  ret->compile();
  return ret;
}
