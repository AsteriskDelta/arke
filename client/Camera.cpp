#include "Camera.h"
#include "Pipeline.h"
#include "Renderer.h"
#include "Pipeline.h"
#include <iostream>

Camera* activeCamera = NULL;

Camera::Camera() {
  pipeline = NULL;
  targetID = 0;
  orthoScaled = false;
}

Camera::~Camera() {
  Deactivate();
}

void Camera::Activate() { _prsguard(); 
  if(activeCamera != NULL) activeCamera->Deactivate();
  
  activeCamera = this;
  buildMatrix(renderer->wasResized());
  renderer->usePipeline(pipeline);
}

void Camera::Deactivate() {
    if(activeCamera == this) activeCamera = NULL;
}

void Camera::setPipeline(RenderPipeline* const p) {
  pipeline = p;
}

void Camera::setOrtho(float w, float h, float near, float far, float offX, float offY, bool oScale) { _prsguard();
  type = CameraType::Orthographic;
  fov = 0.f;
  orthoWidth = w; orthoHeight = h;
  nearClip = near; farClip = far;
  orthoOffX = offX; orthoOffY = offY;
  orthoScaled = oScale;
  buildMatrix(true);
}

void Camera::setPerspective(float f, float near, float far) { _prsguard();
  type = CameraType::Perspective;
  fov = f;
  orthoWidth = 0.f; orthoHeight = 0.f;
  nearClip = near; farClip = far;
  buildMatrix(true);
}

float Camera::getAspect() {
  return (renderer->getAspect());
}

float Camera::getWidth() {
  return renderer->targetSize.x;
}

float Camera::getHeight() {
  return renderer->targetSize.y;
}

void Camera::buildMatrix(bool reproject) { _prsguard();
  if(reproject) {
    if(type == CameraType::Perspective) {
      //std::stringstream ss; ss << "mkCam: " << fov <<"fov, at " << getAspect() << " from " << nearClip << " to " << farClip << ": ";
      //Console::Print(ss.str());
      projectionMatrix = glm::perspective(fov * DEG_TO_RAD, getAspect(), nearClip, farClip);
      //projectionMatrix[2][2] = (nearClip) / (nearClip - farClip);
      //projectionMatrix[3][2] = (farClip*nearClip)/(nearClip - farClip);
    } else if(type == CameraType::Orthographic) {
        float useWidth = orthoWidth, useHeight = orthoHeight;
        
         if(orthoScaled) {
            useWidth *= renderer->targetSize.x; useHeight *= renderer->targetSize.y;
            //std::cout << "scaled " << glm::to_string(renderer->targetSize) << " to " << useWidth <<", " << useHeight << "\n";
        }
        
        if(orthoWidth == 0 && orthoHeight != 0) useWidth = useHeight * (renderer->getAspect());
        else if(orthoHeight == 0 && orthoWidth != 0) useHeight = useWidth * (1.f/renderer->getAspect());
        
        //std::cout << "scale? " << orthoScaled << " : "  << glm::to_string(renderer->targetSize) << " to " << useWidth <<", " << useHeight << ", aspect= " << renderer->getAspect() << "\n";
       
        
        projectionMatrix = glm::ortho((orthoOffX-1.f) * useWidth, orthoOffX*useWidth, (orthoOffY - 1.f)*useHeight, orthoOffY*useHeight, nearClip, farClip);
    }
  }
  
  viewMatrix = glm::lookAt(transform.position, transform.position + transform.forward(), V3_UP );
  //std::cout << " at " << glm::to_string(transform.position) << ", looking at " << glm::to_string(transform.position + transform.forward()) << ", with up " << glm::to_string(transform.up()) << "\n";
  effectiveMatrix = projectionMatrix * viewMatrix;
}