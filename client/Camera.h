#ifndef CAMERA_INC
#define CAMERA_INC

#include "client.h"
#include "Transform.h"
#include "Matrix.h"
#include "V4.h"

enum class CameraType {
  Perspective,
  Orthographic
};
class RenderPipeline;

class Camera {
public:
  Camera();
  ~Camera();
  
  void Activate();
  void Deactivate();
  inline bool isActive();
  
  void setPipeline(RenderPipeline *const p);
  
  void setOrtho(float w, float h, float near, float far, float offX = 0.5f, float offY = 0.5f, bool isScaled = false);
  void setPerspective(float f, float near, float far);
  
  inline float getAspect();
  inline const Mat4& getMatrix() const { return effectiveMatrix; };
  inline const Mat4& getViewMatrix() const { return viewMatrix; };
  
  inline const Mat4& projMatrix() const { return projectionMatrix; };
  
  inline V3 getClipInfo();
  inline V4 getProjInfo();
  
  float getWidth();
  float getHeight();
  
  Transform transform;
  CameraType type;
  
  inline V3 forward() const { return transform.forward(); };
  
  inline V3 project(const V3& pt) const {
      return V3(effectiveMatrix * V4(pt, 1.f));
  }
  
  float nearClip, farClip;
  //@if(type == CameraType::Perspective)
  float fov;//@R(0 < x < 180)
  //@if(type == CameraType::Orthographic)
  float orthoWidth, orthoHeight;
  float orthoOffX, orthoOffY;
  bool orthoScaled;
protected:
    RenderPipeline *pipeline;
    GlUint targetID;
   
  Mat4 projectionMatrix, viewMatrix;
  Mat4 effectiveMatrix;
  
  void buildMatrix(bool reproject = false);
};

extern Camera* activeCamera;

inline bool Camera::isActive() {
     return activeCamera == this;
}

inline V3 Camera::getClipInfo() {
    return V3(nearClip * farClip, nearClip - farClip, farClip);
}

inline V4 Camera::getProjInfo() {
    return V4(-2.0f / (this->getWidth()  * projectionMatrix[0][0]), 
              -2.0f / (this->getHeight() * projectionMatrix[1][1]),
              ( 1.0f - projectionMatrix[0][2]) / projectionMatrix[0][0], 
              ( 1.0f + projectionMatrix[1][2]) / projectionMatrix[1][1]
             );
}

#endif
