
//Various inline
inline bool InputKey::tellHooks() {
  bool ret = false;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(hooks[i] != NULL) {
      ret = true;
      if( hooks[i](this) ) return true;
    }
  }
  return ret;
}

inline void InputButton::tellHooks() {
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(hooks[i] != NULL) {
      if( hooks[i](this) ) return;
    }
  }
}

inline Uint8 InputCombo::getState(const Input *const p) const {
  if(pressed(p)) return p->getKeyConst(primaryKey)->state;
  else return 0x00;
}
inline double InputCombo::getPressTime(const Input *const p) const {
  if(pressed(p)) return p->getKeyConst(primaryKey)->pressTime;
  else return 0;
}
inline Uint32 InputCombo::getPressFrame(const Input *const p) const {
  if(pressed(p)) return p->getKeyConst(primaryKey)->pressFrame;
  else return 0;
}

inline void InputButton::update(Input *const ip) {
  state = 0x00;
  for(Uint8 i = 0; i < INPUT_MAX_COMBOS; i++) {
    const InputCombo *const combo = &combos[i];
    state |= combo->getState(ip);
    pressFrame = std::max(pressFrame, combo->getPressFrame(ip));
    pressTime = std::max(pressTime, combo->getPressTime(ip));
  }
}

//KeyCombo inline
inline bool InputCombo::pressed(const Input *const p) const {
  bool ret = p->getKeyPress(primaryKey);
  ret &= keyMod1 == 0 || p->getKeyDown(keyMod1);
  ret &= keyMod2 == 0 || p->getKeyDown(keyMod2);
  return ret;
}

inline bool InputCombo::down(const Input *const p) const {
  bool ret = p->getKeyDown(primaryKey);
  ret &= keyMod1 == 0 || p->getKeyDown(keyMod1);
  ret &= keyMod2 == 0 || p->getKeyDown(keyMod2);
  return ret;
}

//InputAxis inline
inline void InputAxis::set(Int16 newValue) {
  const Int16 newDelta = std::max(std::min(newValue - value, MAX_Int16), MIN_Int16);
  deriv = std::max(std::min(newDelta - delta, MAX_Int16), MIN_Int16);
  delta = newDelta;
  value = newValue;
  /*if(abs(value) < deadZone) value = 0;
  else {
    value = (int)sgn(value) * (int)round(pow((double)std::abs( ((double)value / (double)MAX_Int16)), power) * (double)MAX_Int16) * (int)sensitivity / 1000;
  }*/
}

inline void InputAxis::setDelta(Int16 newDelta) {
  deriv = std::max(std::min(newDelta - delta, MAX_Int16), MIN_Int16);
  delta = newDelta;
  if(abs(delta) < deadZone) delta = 0;
  else {
    delta = (int)ARKE::sgn(delta) * (int)round(pow((double)std::abs( ((double)delta / (double)MAX_Int16)), power) * (double)MAX_Int16) * (int)sensitivity / 1000;
  }
  
  value = std::max(std::min(value + delta, MAX_Int16), MIN_Int16);
}

inline void InputAxis::tellHooks() {
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(hooks[i] != NULL) {
      if( hooks[i](this) ) return;
    }
  }
}

inline void InputAxis::update(Input *const ip, int ms) {
  Int16 tmpDelta = 0;
  if(axisPlus  != INPUT_BTN_NULL) tmpDelta += Int16(ip->getButtonDown(axisPlus ));
  if(axisMinus != INPUT_BTN_NULL) tmpDelta -= Int16(ip->getButtonDown(axisMinus));
  if(tmpDelta == 0) return;
  
  this->setDelta(tmpDelta * ms);
}

inline InputButton* Input::getButton(Uint16 buttonID) {
  return &buttons[std::min(buttonID, (Uint16)INPUT_MAX_BUTTONS)];
}
inline bool Input::getButtonDown(Uint16 buttonID) const {
  return buttons[std::min(buttonID, (Uint16)INPUT_MAX_BUTTONS)].down();
}
inline bool Input::getButtonPress(Uint16 buttonID) const {
  return buttons[std::min(buttonID, (Uint16)INPUT_MAX_BUTTONS)].pressed();
}

inline InputKey* Input::getKey(Uint16 keyID) {
  return &keys[std::min(keyID, (Uint16)INPUT_MAX_KEYS)];
}
inline const InputKey* Input::getKeyConst(Uint16 keyID) const {
  return &keys[std::min(keyID, (Uint16)INPUT_MAX_KEYS)];
}
inline bool Input::getKeyDown(Uint16 keyID) const {
  return keys[std::min(keyID, (Uint16)INPUT_MAX_KEYS)].down();
}
inline bool Input::getKeyPress(Uint16 keyID) const {
  return keys[std::min(keyID, (Uint16)INPUT_MAX_KEYS)].pressed();
}

inline InputAxis* Input::getAxis(Uint8 axisID) { 
  return &axes[std::min(axisID, (Uint8)INPUT_MAX_AXES)];
};
inline Int16 Input::getAxisValue(Uint8 axisID) const { 
  return axes[std::min(axisID, (Uint8)INPUT_MAX_AXES)].value;
};
inline Int16 Input::getAxisDelta(Uint8 axisID) const { 
  return axes[std::min(axisID, (Uint8)INPUT_MAX_AXES)].delta;
};
