/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TextureAtlas.h
 * Author: Galen Swain
 *
 * Created on April 27, 2016, 6:00 AM
 */

#ifndef TEXTUREATLAS_H
#define TEXTUREATLAS_H
#include "client.h"
#include "ImageAtlas.h"
#include "UniformBlock.h"
#include "Texture.h"

class Shader;
class TextureAtlas : public ImageAtlas, public Serializable {
public:
    TextureAtlas();
    TextureAtlas(unsigned int w, unsigned int h, unsigned int l = 0);
    virtual ~TextureAtlas();
    virtual void setDimensions(unsigned int w, unsigned int h, unsigned int l = 0);
    
    void setMipmap(bool state);
    
    void debugWrite(std::string path);
    void upload();
    void uploadUniforms();
    void uploadLayers();
    
    void bind(Shader *const shader, unsigned short texID, unsigned short uniID);
    
    UniformBlock *ub;
    Texture<true> *tex;
private:
    bool doMipmap;
};

#endif /* TEXTUREATLAS_H */

