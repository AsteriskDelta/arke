#ifndef SHADER_INC
#define SHADER_INC
#include "client.h"
#include "Color.h"
#include "Matrix.h"
#include "V3.h"
#include "V2.h"
#include "V4.h"

#ifndef SHADER_CPP
template<bool Arr> class Texture;
typedef Texture<false> Texture2D;
typedef Texture<true> TextureArray;
#endif

class UniformBlock;

class Shader {
public:
  Shader();
  ~Shader();
  
  bool define(std::string key, std::string value);
  
  bool compile();
  
  void activate();
  void deactivate();
  
  void bindUnit(const Uint8 location, const unsigned int unit);
  
  void bindTexture(const Texture2D *const tex, const Uint8 location, const unsigned int unit);
  void bindTextureArray(const TextureArray *const tex, const Uint8 location, const unsigned int unit);
  
  void bindMat4(const Mat4& mat, const Uint8 location);
  void bindMat3(const Mat3& mat, const Uint8 location);
  void bindColor(const Color4 &color   , const Uint8 location);
  void bindV4(const V4 &v   , const Uint8 location);
  void bindV3(const V3 &v   , const Uint8 location);
  void bindV2(const V2 &v   , const Uint8 location);
  void bindFloat(const float f   , const Uint8 location);
  void bindInt(const int v, const Uint8 location);
  void bindUint(const unsigned int v, const Uint8 location);
  
  void bindBlock(const UniformBlock *const b, const Uint8 location);
  
#ifdef CONSOLE_INC
  inline GlUint uni(Uint8 uid) {
    if(compiled&&uid < uniformCount) {
      return uniforms[uid];
    } else {
      Console::Warning("Uniform requested out of range or has invalid host shader!");
      return 0;
    }
  };
#endif
  
  //Static methods of allocation and loading
  static Shader* Load(std::string np, bool isPure = false);
private:
  GlUint id;
  GlInt activeMatLocation, modelViewLocation, projMatLocation;
  bool compiled;
  bool hasVertex, hasGeometry, hasFragment, hasTCS, hasTES;
  
  std::string name, path;
  
  GlInt* uniforms;
  Uint16 uniformCount, uniformMax;
  std::string strDefine;
  unsigned char* buffer;
};

#endif
