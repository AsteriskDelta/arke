#ifndef WINDOW_INC
#define WINDOW_INC
#include "client.h"
#include "Image.h"

struct ErrorMask {
  unsigned short length;
  std::string text;
  char* data;
};

class Renderer;
class SDL_Window;
typedef void* SDL_GLContext;
struct SDL_Surface;

class Window {
friend class Renderer;
public:
  static void OnQuit();
  
  Window(std::string newTitle = "",Uint16 w = 0, Uint16 h = 0);
  ~Window();
  
  bool open(Uint16 width, Uint16 height);
  void close();
  void processEvents();
  
  void grabFocus();
  void releaseFocus();
  
  void setIcon(const Image<4> *const img);
  
  void setActive();
  void update();
  
  unsigned short pointToPix(unsigned short pt);
  
  int framesPerSecond;
  int msPerFrame;
  
  int width, height;
  int pixelsPerInch;
  bool focused;
  
  Renderer *rContext;
private:
  std::string title;
  SDL_Window* mainWindow;
  SDL_GLContext mainContext;
  
  static void SetupErrors();
  void printError(std::string func = "", int line = -1);
  
  uint8_t bpp;
  
  SDL_Surface* iconSurface;
  Image<4> *icon;
};

#endif
