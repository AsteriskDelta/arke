#include "Input.h"
#include <sstream>
#include <Application.h>
#include "InputKeys.h"

/* InputCombo Functions
 * 
 */
void InputCombo::addModifier(Uint16 mk) {
  if(keyMod1 == 0) keyMod1 = mk;
  else keyMod2 = mk;
}

void InputCombo::clear() {
  keyMod1 = keyMod2 = primaryKey = 0;
}

void InputCombo::clearModifiers() {
  keyMod1 = keyMod2 = 0;
}

std::string InputCombo::toString() {
  if(primaryKey == 0) return "[Unbound]";
  
  std::stringstream ss;
  if(keyMod1 != 0) ss << Input::getKeyName(keyMod1) << " ";
  if(keyMod2 != 0) ss << Input::getKeyName(keyMod2) << " ";
  ss << Input::getKeyName(primaryKey);
  return ss.str();
}

Uint32 InputCombo::serialLength() {
  return sizeof(InputCombo);
}

void InputCombo::serialize(char *const buff, int length) {
  _unused(buff); _unused(length);
}

/* Input class functions
 * 
 */
SizedList<Input*, INPUT_MAX_INSTANCES> Input::inputs;

void Input::Initialize() {
  LoadKeyNames();
}
void Input::Register(Input *const src) {
  if(inputs.count() == 0) {
    src->keyboardID = src->mouseID = 0;
  }
  inputs.add(src);
}
void Input::Unregister(Input *const src) {
  inputs.remove(src);
}

Input* Input::GetByMouse(Uint32 mID) {
  for(Uint8 i = 0; i < inputs.count(); i++) {
    if(inputs[i]->mouseID == mID) return inputs[i];
  }
  return NULL;
}
Input* Input::GetByKeyboard(Uint32 kID) {
  for(Uint8 i = 0; i < inputs.count(); i++) {
    if(inputs[i]->keyboardID == kID) return inputs[i];
  }
  return NULL;
}
Input* Input::GetByGamepad(Uint32 gID) {
  for(Uint8 i = 0; i < inputs.count(); i++) {
    if(inputs[i]->gamepadID == gID) return inputs[i];
  }
  return NULL;
}

//Instance
Input::Input() {
  keyboardID = mouseID = gamepadID = 0;
  axisCount = btnCount = 0;
  Register(this);
  
  InputAxis *axis = getAxis(ScrollX);
  axis->sensitivity = 32000;
  axis = getAxis(ScrollY);
  axis->sensitivity = 32000;
  axis = getAxis(MouseX);
  axis->sensitivity = 32000;
  axis = getAxis(MouseY);
  axis->sensitivity = 32000;
}

Input::~Input() {
  
  Unregister(this);
}

void Input::zero() {
  for(Uint8 a = 0; a < INPUT_MAX_AXES; a++) {
    InputAxis *const axis = &axes[a];
    axis->delta = 0;
  }
}

void Input::update(int ms) { _prsguard();
  _unused(ms);
  for(Uint8 b = 0; b < INPUT_MAX_BUTTONS; b++) {
    InputButton *const button = &buttons[b];
    button->update(this);
  }
  
  for(Uint8 a = 0; a < INPUT_MAX_AXES; a++) {
    InputAxis *const axis = &axes[a];
    axis->update(this, ms);
  }
}

/*void Input::clearHeld() {
  
}*/

bool Input::hookButton(Uint16 btnID, INPUT_BTNHOOK_R (*function)INPUT_BTNHOOK_D) {
  InputButton *const btn = this->getButton(btnID);
  if(btn == NULL) return false;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(btn->hooks[i] == NULL) {
      btn->hooks[i] = function;
      return true;
    }
  }
  return false;
}

bool Input::hookKey(Uint16 keyID, INPUT_KEYHOOK_R (*function)INPUT_KEYHOOK_D) {
  InputKey *const key = this->getKey(keyID);
  if(key == NULL) return false;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(key->hooks[i] == NULL) {
      key->hooks[i] = function;
      return true;
    }
  }
  return false;
}
bool Input::hookAxis(Uint8 axisID, INPUT_AXISHOOK_R (*function)INPUT_AXISHOOK_D) {
  InputAxis *const axis = this->getAxis(axisID);
  if(axis == NULL) return false;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(axis->hooks[i] == NULL) {
      axis->hooks[i] = function;
      return true;
    }
  }
  return false;
}

void Input::unhookButton(Uint16 btnID, INPUT_BTNHOOK_R (*function)INPUT_BTNHOOK_D) {
  InputButton *const btn = this->getButton(btnID);
  if(btn == NULL) return;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(btn->hooks[i] == function) {
      btn->hooks[i] = NULL; return;
    }
  }
}
void Input::unhookKey(Uint16 keyID, INPUT_KEYHOOK_R (*function)INPUT_KEYHOOK_D) {
  InputKey *const key = this->getKey(keyID);
  if(key == NULL) return;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(key->hooks[i] == function) {
      key->hooks[i] = NULL; return;
    }
  }
}
void Input::unhookAxis(Uint8 axisID, INPUT_AXISHOOK_R (*function)INPUT_AXISHOOK_D) {
  InputAxis *const axis = this->getAxis(axisID);
  if(axis == NULL) return;
  for(Uint8 i = 0; i < INPUT_MAX_HOOKS; i++) {
    if(axis->hooks[i] == function) {
      axis->hooks[i] = NULL; return;
    }
  }
}

//Text mapping stuff:
inline bool IsReadable(unsigned short key) {
  return (key >= 32&&key <= 126);
}

//Text mapping stuff:
inline bool IsKeyInput(unsigned short key) {
  return (key >= 32&&key <= 126) && !(key >= 65 && key <= 90);
}

inline unsigned short ToUpper(unsigned short key) { _prsguard();
  if(key >= 97&&key <= 122) return key - 32;
  
  switch(KTOC(key)) {
    case '0': return CTOK(')'); break;
    case '1': return CTOK('!'); break;
    case '2': return CTOK('@'); break;
    case '3': return CTOK('#'); break;
    case '4': return CTOK('$'); break;
    case '5': return CTOK('%'); break;
    case '6': return CTOK('^'); break;
    case '7': return CTOK('&'); break;
    case '8': return CTOK('*'); break;
    case '9': return CTOK('('); break;
    
    case '`': return CTOK('~'); break;
    case '-': return CTOK('_'); break;
    case '=': return CTOK('+'); break;
    case '\\': return CTOK('|'); break;
    
    case '[': return CTOK('{'); break;
    case ']': return CTOK('}'); break;
    case ';': return CTOK(':'); break;
    case '\'': return CTOK('"'); break;
    case ',': return CTOK('<'); break;
    case '.': return CTOK('>'); break;
    case '/': return CTOK('?'); break;
  }
  
  return key;
}

#define US(s) ((unsigned short) s)
inline unsigned short CheckScancode(unsigned short key, unsigned short scanCode) { _prsguard();
  switch(scanCode) {
    case SDL_SCANCODE_GRAVE: 		key = CTOK('`');	break;
    case SDL_SCANCODE_BACKSPACE:	key = SDLK_BACKSPACE; 	break;
    case SDL_SCANCODE_RETURN: 		key = SDLK_RETURN; 	break;
    case SDL_SCANCODE_TAB: 		key = SDLK_TAB; 	break;
    case SDL_SCANCODE_ESCAPE:		key = SDLK_ESCAPE;	break;
    case SDL_SCANCODE_LEFTBRACKET: 	key = SDLK_LEFTBRACKET;	break;
    case SDL_SCANCODE_RIGHTBRACKET: 	key = SDLK_RIGHTBRACKET;break;
    case SDL_SCANCODE_BACKSLASH:	key = SDLK_BACKSLASH;	break;
    case SDL_SCANCODE_NONUSHASH:	key = CTOK('#');	break;
    case SDL_SCANCODE_SEMICOLON: 	key = SDLK_SEMICOLON;	break;
    case SDL_SCANCODE_APOSTROPHE:	key = CTOK('\'');	break;
    case SDL_SCANCODE_DELETE: 		key = SDLK_DELETE;	break;
    
    case SDL_SCANCODE_UP:		key = STK(SDLK_UP);	break;
    case SDL_SCANCODE_DOWN:	 	key = STK(SDLK_DOWN);	break;
    case SDL_SCANCODE_LEFT:		key = STK(SDLK_LEFT);	break;
    case SDL_SCANCODE_RIGHT: 		key = STK(SDLK_RIGHT);	break;
    //...B, A, Start? No? Oh... okay
  };
  
  bool specMap = false;
  
  //if(keysGrabbed) {
    //std::cout << "Got grabbed scancode, mapping '" << key << "' to '";
    switch(scanCode) {
      case SDL_SCANCODE_KP_DIVIDE:	key = CTOK('/');	break;
      case SDL_SCANCODE_KP_MULTIPLY:	key = CTOK('*');	break;
      case SDL_SCANCODE_KP_MINUS:	key = CTOK('-');	break;
      case SDL_SCANCODE_KP_PLUS:	key = CTOK('+');	break;
      case SDL_SCANCODE_KP_ENTER:	key = SDLK_RETURN;	break;
      case SDL_SCANCODE_KP_1:		key = CTOK('1');	break;
      case SDL_SCANCODE_KP_2:		key = CTOK('2');	break;
      case SDL_SCANCODE_KP_3:		key = CTOK('3');	break;
      case SDL_SCANCODE_KP_4:		key = CTOK('4');	break;
      case SDL_SCANCODE_KP_5:		key = CTOK('5');	break;
      case SDL_SCANCODE_KP_6:		key = CTOK('6');	break;
      case SDL_SCANCODE_KP_7:		key = CTOK('7');	break;
      case SDL_SCANCODE_KP_8:		key = CTOK('8');	break;
      case SDL_SCANCODE_KP_9:		key = CTOK('9');	break;
      case SDL_SCANCODE_KP_0:		key = CTOK('0');	break;
      case SDL_SCANCODE_KP_PERIOD:	key = CTOK('.');	break;
      default:				specMap = true;		break;
    }
    //std::cout << key << "',";
  /*} else specMap = true;
  
  if(specMap&&scanCode >= 57 && scanCode <= 99) {
    key = SPK(scanCode);
  }*/
  
  //std::cout << " after alphak '" << key << "'\n";
  
  return key;
}

void Input::axisEvent(Int8 id, Int32 delta) { _prsguard();
  InputAxis *const axis = this->getAxis(id);
  if(axis == NULL) return;
  axis->setDelta(delta);
  //std::cout << int(id) << " = " << axis->value << "\n";
  axis->tellHooks();
}
void Input::axisEventAbsolute(Int8 id, Int32 value) { _prsguard();
  InputAxis *const axis = this->getAxis(id);
  if(axis == NULL) return;
  axis->set(value);
  //std::cout << int(id) << "("<<axis<<")  = " << axis->delta << "\n";
  axis->tellHooks();
}
void Input::keyEvent(Uint16 code, Uint16 scan, Uint8 state, Uint16 mod) { _prsguard();
  Uint16 keyCode = CheckScancode(code, scan);
  //uint8_t state = (uint8_t)(key.type == SDL_KEYDOWN);
  //std::cout << "Got event with " << keyCode << ":" << scan << "\n";
  //if(IsReadable(keyCode) && IsKeyInput(keyCode)) std::cout << char(keyCode) << "\n";
  //else if(keyNames[keyCode].length() > 0) std::cout << keyNames[keyCode] << "\n";
  
  InputKey *const key = this->getKey(keyCode);
  if(key == NULL) {
    std::stringstream s; s << "Out of range keysym value (" << keyCode << ") reported!";
    Console::Warning(s.str());
    return;
  } else if(scan == SDL_SCANCODE_CAPSLOCK) {
    return;
  }
  
  
  /*if(keysGrabbed) {
    //Check if the key should be capitalied
    if(grabbedCaps && (modifier & (KMOD_CAPS | KMOD_SHIFT)) != 0) {
      heldKey = keyCode;
      keyCode = ToUpper(keyCode);
    } else {
      heldKey = keyCode;
    }
    heldScan = key.keysym.scancode;
    
    if(state == 1) {
      repeatTime = repeatDelay;
    } else {
      heldKey = 0;
      repeatTime = 0;
    }
    
    handled = (kGrabFunction)(keyCode, buttons[keyCode].state, state, modifier);
    buttons[keyCode].previousState = buttons[keyCode].state;
    buttons[keyCode].state = state;
    buttons[keyCode].pressTime = rawTicks;
  } else */{
    key->previousState = buttons[keyCode].state;
    key->state = state;
    key->pressFrame = _globalFrame;
    key->pressTime = _globalTime;
    //std::cout << "Got event with " << keyCode << ":" << key.keysym.scancode << " state:" << ((int)state) << " pState:" << ((int)buttons[keyCode].previousState) <<"\n";
  }
  
  //Emergency escape for unhandled cases
  if(!key->tellHooks() && keyCode == SDLK_ESCAPE) {
    Console::Warning("Unhandled escape executed!");
    Application::Quit(0);
    //Window::ReleaseFocus();
  }
  
  key->tellHooks();
}
void Input::btnEvent(Uint8 id, Uint8 state) { _prsguard();
  Uint16 actID = BPK(id);
  InputKey *const key = this->getKey(actID);
  if(key == NULL) return;
  if(keyNames[actID].length() > 0) std::cout << keyNames[actID] << "\n";
  key->previousState = key->state;
  key->state = state;
  if(state) {
    key->pressFrame = _globalFrame;
    key->pressTime = _globalTime;
  }
  key->tellHooks();
}
void Input::deviceEvent() {
  Console::Warning("Input::deviceEvent passed without implementation!");
}

void Input::setGamepad(Uint32 gID) {
  gamepadID = gID;
}
