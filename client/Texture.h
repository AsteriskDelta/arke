#ifndef TEXTURE_INC
#define TEXTURE_INC
#include "client.h"

#ifndef IMAGE_INC
class ImageRGBA;
#endif

enum class TextureFormat : unsigned char {
  SignedNormal,
  Unsigned,
  Signed,
  Float,
  Depth,
  DepthStencil
};
enum class TextureWrap : unsigned char {
  Repeat,
  RepeatMirrored,
  Clamp,
  Border
};
enum class TextureFilter : unsigned char {
  Nearest,
  Linear,
  Ansiotropic
};

template<bool Arr = false>
class Texture {
public:
  Texture();
  Texture(TextureFormat f, Uint8 bits, Uint8 cCount = 4, Uint8 ls = 1);
  Texture(const ImageRGBA *const img);
  ~Texture();
  
  inline operator GlUint() const { return id; };
  inline GlUint getID() const { return id; };
  
  GlUint getBindTarget();
  
  inline void setMipmap(bool s) { setDirty(s != doMipmap); doMipmap = s; };
  inline void setMipmapCount(unsigned char c) { setDirty(c != mipmaps); mipmaps = c; };
  inline void setWrap(TextureWrap w) { setDirty(w != wrap); wrap = w; };
  inline void setFilter(TextureFilter f) { setDirty(f != filter); filter = f; };
  inline void setSamples(Uint8 s) { setDirty(s != samples); samples = s; };
  void setSize(Uint16 x, Uint16 y);
  void setSize(Uint16 x, Uint16 y, Uint16 z);
  
  bool allocate();
  void deallocate();
  void commit();
  void mipmap();
  
  //void upload(const char *const data, unsigned short width, unsigned short height, unsigned short layer, unsigned short xOff = 0, unsigned short yOff = 0);
  void upload(const ImageRGBA *const img, unsigned short xOff = 0, unsigned short yOff = 0, unsigned short layer = 0);
  
  Uint16 width, height, layers;
  
  static TextureFormat GetFormat(const std::string& s);
  static Uint8 GetChannels(const std::string& s);
  static TextureFilter GetFilter(const std::string& s);
protected:
  GlUint id;
  
  Uint8 channels, bpp, samples, mipmaps;
  TextureFormat format;
  GlUint compChannels, compFormat, compType;
  TextureWrap wrap;
  TextureFilter filter;
  bool doMipmap;
  bool allocated, committed;
  
  inline void setDirty(bool s) { if(s) committed = false; };
  void compileFormatData();
};

typedef Texture<false> Texture2D;
typedef Texture<true> TextureArray;

#endif