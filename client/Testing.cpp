#include "Testing.h"
#include "Mesh.h"


template class Mesh<TestVert>;
template class Mesh<TestVertSimple>;

Testing::Testing() {
  sphere = NULL;
  plane = NULL;
  triangle = NULL;
  buildSphere();
  buildTriangle();
  buildPlane();
}

Testing::~Testing() {
  if(sphere != NULL) delete sphere;
  if(plane != NULL) delete plane;
  if(triangle != NULL) delete triangle;
}

void Testing::DrawSphere() {
  if(sphere == NULL) {
    Console::Error("Testing sphere NULL!");
    return;
  }
  sphere->draw();
}

void Testing::DrawTriangle() {
  if(triangle == NULL) {
    Console::Error("Testing triangle NULL!");
    return;
  }
  triangle->draw();
}

void Testing::DrawPlane() {
  if(plane == NULL) {
    Console::Error("Testing plane NULL!");
    return;
  }
  plane->draw();
}

void Testing::buildSphere() {
  if(sphere != NULL) delete sphere;
  const float lats = 48, longs = 48;
  unsigned int sphereSize = 4 * (lats+1) * (longs+1);
  sphere = new Mesh<TestVert>(MeshType::Quads, sphereSize);
  
  int vertIndex = 0;
  for(int i = 1; i <= lats; i++) {
    float lat0 = M_PI * (-0.5f + (float) (i - 1) / lats);
    float z0  = sin(lat0);
    float zr0 =  cos(lat0);
    
    float lat1 = M_PI * (-0.5f + (float) i / lats);
    float z1 = sin(lat1);
    float zr1 = cos(lat1);
    TestVert prevA, prevB; bool begun = false;
    for(int j = 0; j <= longs; j++) {
      float lng = 2.0f * M_PI * (float) (j - 1) / longs;
      float x = cos(lng);
      float y = sin(lng);
      
      if(!begun) {
	prevA.pos = V3{x * zr0, y * zr0, z0}; prevA.normal = glm::normalize(V3{x * zr0, y * zr0, z0}); 
	prevB.pos = V3{x * zr1, y * zr1, z1}; prevB.normal = glm::normalize(V3{x * zr1, y * zr1, z1}); 
	begun = true;
      } else {
	sphere->setVert(vertIndex++, prevB);
	sphere->setVert(vertIndex++, prevA);
	
	prevA.pos = sphere->vert(vertIndex).pos = V3{x * zr0, y * zr0, z0}*V3(1.f, 1.f, (z0 > 0.f)? 2.f : 1.f); 
	prevA.normal = sphere->vert(vertIndex++).normal = glm::normalize(V3{x * zr0, y * zr0, z0}); 
	prevB.pos = sphere->vert(vertIndex).pos = V3{x * zr1, y * zr1, z1}*V3(1.f, 1.f, (z1 > 0.f)? 2.f : 1.f);
	prevB.normal = sphere->vert(vertIndex++).normal = glm::normalize(V3{x * zr1, y * zr1, z1}); 
      }
    }
  }
  sphere->upload();
  /*
  glGenBuffers(1, &sphereVbo);
  glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(TestVert) * sphereSize, sphereVerts, GL_STATIC_DRAW);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  
  glGenVertexArrays(1, &(sphereVao));
  glBindVertexArray(sphereVao);
  glBindBuffer(GL_ARRAY_BUFFER, sphereVbo);
  
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(TestVert), 0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(TestVert), (void*)(sizeof(V3)));
  glBindVertexArray(0);
  spherePrepared = true;*/
}

void Testing::buildTriangle() {
  if(triangle != NULL) delete triangle;
  triangle = new Mesh<TestVertSimple>(MeshType::Tris, 3);
  triangle->vert(0).pos = V3(-1.0f, -1.0f, 0.0f);
  triangle->vert(1).pos = V3(1.0f, -1.0f, 0.0f);
  triangle->vert(2).pos = V3(0.0f,  1.0f, 0.0f);
  
  triangle->upload();
}

void Testing::buildPlane() {
  if(plane != NULL) delete plane;
  plane = new Mesh<TestVertSimple>(MeshType::Quads, 4);
  plane->vert(0).pos = V3(-1.0f, 0.f, -1.0f);
  plane->vert(1).pos = V3( 1.0f, 0.f, -1.0f);
  plane->vert(2).pos = V3( 1.0f, 0.f,  1.0f);
  plane->vert(3).pos = V3(-1.0f, 0.f,  1.0f);
  plane->upload();
}
