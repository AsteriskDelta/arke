#include "client.h"
#include "Pipeline.h"
#include "Renderer.h"
#include "rInterface.h"
#include "XML.h"
#include <iostream>
#include "Shader.h"
#include "Mesh.h"


template class Mesh<PTileVert>;

thread_local RenderPipeline* activePipe;

void RenderPipeline::initVars() {
  name = id = "uninit";
  globalPipelines.add(this);
  lightTiles = NULL;
}

RenderPipeline::RenderPipeline() {
  initVars();
}

RenderPipeline::RenderPipeline(const std::string file, bool isRelPath) {
  initVars();
  this->load(file, isRelPath);
}

RenderPipeline::~RenderPipeline() {
  Deactivate();
  Deallocate();
  globalPipelines.remove(this);
}

bool RenderPipeline::load(const std::string file, bool isRelPath) { _prsguard();
  XMLDoc doc;
  if(!doc.load(file, isRelPath)) {
    std::stringstream ss;
    ss << "RenderPipelined at \"" << file << "\" failed to load due to XML parsing error!";
    Console::Error(ss.str());
    return false;
  }
  
  XMLNode rp = doc.child("renderPipeline");
  id = rp.get<std::string>("id");
  name = rp.get<std::string>("ui.name");
  //std::cout << "PL: id: " << id << " name: " << name << "\n";
  
  for(XMLNode prefNode = rp.child("ui.pref"); prefNode; ++prefNode) {
    //std::cout << prefNode.get<std::string>("id") << " : " << prefNode.get<std::string>() << "\n";
  }
  
  for(XMLNode presetNode = rp.child("preset"); presetNode; ++presetNode) {
    //std::cout << presetNode.get<std::string>("id") << " : " << presetNode.get<std::string>() << "\n";
    for(XMLNode vNode = presetNode.child(); vNode; vNode = vNode.sibling()) {
      //std::cout << "\t"<<vNode.name() << " : " << vNode.get<std::string>("value") << "\n";
    }
  }
  
  for(XMLNode buffNode = rp.child("buffers").child(); buffNode; buffNode = buffNode.sibling()) {
    //std::cout << buffNode.get<std::string>("id") << " : " << buffNode.get<std::string>("type") << "\n";
    RenderBuffer buff;
    buff.id = buffNode.get<std::string>("id");
    buff.typeID = buffNode.get<std::string>("type");
    buff.bpp = buffNode.get<Uint8>("bpp", 8);
    buff.sampleCount = buffNode.get<Uint8>("samples", 1);
    buff.condition = buffNode.get<std::string>("if", "");
    buff.sizeMod = buffNode.get<float>("sizeMod", 1.f);
    buff.layers = buffNode.get<Uint8>("layers", 1);
    buff.mipmaps = buffNode.get<Uint8>("mipmaps", 0);
    buff.filterRaw = buffNode.get<std::string>("filter", "nearest");
    buffers.add(buff);
  }
  
  XMLNode lightNode = rp.child("lighting");
  tileSize = lightNode.get<Uint16>("tileSize", 32);
  
  for(XMLNode stgNode = rp.child("render").child(); stgNode; stgNode = stgNode.sibling()) {
    //std::cout << "Stage " << stgNode.get<std::string>("id") << ": \n";
    RenderStage obj;
    obj.id = stgNode.get<std::string>("id");
    obj.idxMax = 0;
    obj.gpuID = 0;
    
    for(XMLNode targNode = stgNode.child("target"); targNode; ++targNode) {
      Uint8 idx; std::string rawIdx = targNode.get<std::string>("idx");
      if(rawIdx == "_depth") idx = PIPELINE_DEPTH;
      else if(rawIdx == "_stencil") idx = PIPELINE_STENCIL;
      else idx = targNode.get<Uint8>("idx");
      
      if(idx > PIPELINE_OBJ_MAX_T) {
        std::stringstream ss; ss << "Stage node targets \"" << rawIdx << "\", which cannot be mapped!";
        Console::Error(ss.str());
      }
      obj.idxMax = std::max(idx, obj.idxMax);
      
      int buffID = buffers.findID(targNode.get<std::string>("id", ""));
      if(buffID < 0) {
        std::stringstream ss; ss << "Stage node wants to write \"" << targNode.get<std::string>("id", "") << "\", which is not listed as a buffer!";
        Console::Error(ss.str());
      }
      obj.buffers[idx] = (Uint8)buffID;
      
      //std::cout << "\t" << int(idx) << " as " << targNode.get<std::string>("id", "") <<":" << buffID <<"\n";
    }
    
    for(XMLNode srcNode = stgNode.child("source"); srcNode; ++srcNode) {
      Uint8 idx = std::min(srcNode.get<Uint8>("idx"), Uint8(PIPELINE_OBJ_MAX_T-1));
      std::string srcID = srcNode.get<std::string>("id");
      obj.sourcesRaw[idx] = srcID;
      int buffID = buffers.findID(srcID);
      if(buffID < 0) {
        std::stringstream ss; ss << "Stage node wants to read \"" << srcID << "\", which is not listed as a buffer!";
        Console::Error(ss.str());
        continue;
      }
      
      obj.sources[idx] = &(buffers[buffID].texture);
    }
    
    for(XMLNode argNode = stgNode.child("arg"); argNode; ++argNode) {
      std::string nId = argNode.get<std::string>("id");
      std::string nValue = argNode.get<std::string>("value");
      
      if(nValue == "_clipInfo") obj.setFlag(RenderStage::BindClipInfo);
      else if(nValue == "_projInfo") obj.setFlag(RenderStage::BindProjInfo);
      else if(nValue == "_fbInfo") obj.setFlag(RenderStage::BindFBInfo);
      
      //std::cout << "PVAL: \"" << nValue << "\"\n";
    }
    
    obj.setFlag(RenderStage::ClearDepth, (bool)stgNode.child("clearDepth"));
    obj.setFlag(RenderStage::ClearColor, (bool)stgNode.child("clearColor"));
    obj.setFlag(RenderStage::DrawOpaque, (bool)stgNode.child("drawOpaque"));
    obj.setFlag(RenderStage::DepthOnly,  (bool)stgNode.child("depthOnly"));
    obj.setFlag(RenderStage::DrawTransparent, (bool)stgNode.child("drawTransparent"));
    obj.setFlag(RenderStage::Barrier, (bool)stgNode.child("barrier"));
    obj.setFlag(RenderStage::DrawUI, (bool)stgNode.child("drawUI"));
    
    XMLNode dtNode = stgNode.child("depthTest");
    if((bool)dtNode) {
      obj.setFlag(RenderStage::DepthTest, true);
      obj.setDepthTest(dtNode.get<std::string>("func", "less or equal"));
    }
    
    XMLNode blendNode = stgNode.child("blend");
    if((bool)blendNode) {
      obj.setFlag(RenderStage::Blend, true);
      obj.setBlending(dtNode.get<std::string>("src", ""), dtNode.get<std::string>("dest", ""));
    }
    
    XMLNode resNode = stgNode.child("resolve");
    if((bool)resNode) {
      obj.setFlag(RenderStage::Resolve, true);
      obj.chainRaw = resNode.get<std::string>("target", "");
    }
    
    XMLNode postNode = stgNode.child("post");
    if((bool)postNode) {
      obj.setFlag(RenderStage::PostProc, true);
      std::string shaderPath = postNode.get<std::string>("shader");
      obj.postShader = Shader::Load(shaderPath);
    }
    XMLNode tilepostNode = stgNode.child("postTiled");
    if((bool)tilepostNode) {
      obj.setFlag(RenderStage::PostTiled, true);
      std::string shaderPath = tilepostNode.get<std::string>("shader");
      obj.postShader = Shader::Load(shaderPath);
    }
    
    XMLNode mipNode = stgNode.child("mipmap");
    if((bool)mipNode) {
      obj.setFlag(RenderStage::MipMap, true);
      obj.pTargetRaw = mipNode.get<std::string>("target");
    }
    
    stages.add(obj);
    //std::cout << "\tDraws opaque: " << (bool)stgNode.child("drawOpaque") << "\n";
  }
  
  return true;
}

void RenderPipeline::Allocate() { _prsguard();
  riEnsureState();
  this->Activate();
  for(Uint8 i = 0; i < buffers.count(); i++) {
    RenderBuffer *const buff = &(buffers[i]);
    //std::cout << buff << "\n";
    buff->texture = Texture2D(Texture<false>::GetFormat(buff->typeID), buff->bpp, Texture<false>::GetChannels(buff->typeID), 1);
    buff->texture.setSize(round(renderer->targetSize.x * buff->sizeMod), round(renderer->targetSize.y * buff->sizeMod));
    buff->texture.setFilter(Texture<>::GetFilter(buff->filterRaw));
    buff->texture.setSamples(buff->sampleCount);
    buff->texture.setWrap(TextureWrap::Clamp);
    buff->texture.setMipmapCount(buff->mipmaps);
    
    buff->texture.allocate();
    buff->texture.commit();
  }
  
  for(Uint8 i = 0; i < stages.count(); i++) {
    RenderStage *const stage = &(stages[i]);
    //std::cout << "Building stage" << int(i) << "\n";
    stage->build();
  }
  for(Uint8 i = 0; i < stages.count(); i++) {
    RenderStage *const stage = &(stages[i]);
    if(stage->chainRaw != "") {
      int stageID = stages.findID(stage->chainRaw);
      if(stageID >= 0) stage->chainID = stages[stageID].gpuID;
    }
    
    if(stage->pTargetRaw != "") {
      int ptID = buffers.findID(stage->pTargetRaw);
      if(ptID >= 0) stage->postTarget = &(buffers[ptID].texture);
    }
  }
  
  /*tileSize = (Uint16)round(max(float(tileSize), max(renderer->targetSize.x, renderer->targetSize.y) / 256.f ));
  if(lightTiles != NULL) delete lightTiles;
  xTiles = ceil(renderer->targetSize.x / float(tileSize));
  yTiles = ceil(renderer->targetSize.y / float(tileSize));
  Uint16 tileCount = (xTiles * yTiles), tilePoints = (xTiles + 1) * (yTiles + 1);
  lightTiles = new Mesh<PTileVert>(MeshType::Quads, tilePoints, tileCount*4);
  std::cout << "ts: "<<tileSize << "xt:" << xTiles << ", yTiles: "<<yTiles << " total " << tileCount << " and p=" << tilePoints << "\n";
  for(Uint16 x = 0; x <= xTiles; x++) {
    for(Uint16 y = 0; y <= yTiles; y++) {
      PTileVert v;
      v.pos = V2(float(x)/float(xTiles)*2.f - 1.f, float(y)/float(yTiles) * 2.f - 1.f);
      v.tileID = (x*xTiles) + y;
      std::cout << v.tileID << "\n";
      lightTiles->setData(x*xTiles + y, v);
    }
  }
  for(Uint16 x = 1; x <= xTiles; x++) {
    for(Uint16 y = 1; y <= yTiles; y++) {
      lightTiles->addInd((x  )*xTiles + (y-1));
      lightTiles->addInd((x-1)*xTiles + (y-1));
      lightTiles->addInd((x-1)*xTiles + (y  ));
      lightTiles->addInd(x*xTiles + y);
    }
  }
  lightTiles->upload();*/
  
  this->Deactivate();
  riEnsureState();
}

void RenderPipeline::Deallocate() {
  for(Uint8 i = 0; i < buffers.count(); i++) {
    RenderBuffer *const buff = &(buffers[i]);
    buff->texture.deallocate();
  }
}

void RenderPipeline::ReallocateWSZ() {
  Deallocate();
  Allocate();
}

void RenderPipeline::Activate() {
  if(activePipe != NULL) activePipe->Deactivate();
  activePipe = this;
}

void RenderPipeline::Execute(REND_DRAW_R (*renderCall)REND_DRAW_D) { _prsguard();
  riEnsureState();
  for(Uint8 i = 0; i < stages.count(); i++) {
    RenderStage *const stage = &stages[i];
    _prsguardt(stage->id.c_str());
    stage->bind();
    riEnsureState();
    renderCall(stage);
    riEnsureState();
    stage->unbind();
    riEnsureState();
  }
  
  //this->blit();
}

void RenderPipeline::Blit() {
  riEnsureState();
  riEnsureState();
}

void RenderPipeline::Deactivate() {
  if(activePipe != this) return;
  activePipe = NULL;
}
