/* 
 * File:   UniformBlock.cpp
 * Author: Galen Swain
 * 
 * Created on June 19, 2015, 9:16 AM
 */

#include "UniformBlock.h"
#include "rInterface.h"
#include "Application.h"

UniformBlock::UniformBlock() {
  bufferID = 0;
}

UniformBlock::~UniformBlock() {
  deallocate();
}

bool UniformBlock::allocate() { _prsguard();
  glGenBuffers(1, &bufferID);
  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
  return bufferID != 0;
}

void UniformBlock::deallocate() {
  if(bufferID == 0) return;
  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
  if(_graphicsContextExists) glDeleteBuffers(1, &bufferID);
}

bool UniformBlock::upload(const char* data, unsigned int length, bool dynamic) { _prsguard();
  if(data == NULL || length == 0 || bufferID == 0) return false;
  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
  glBufferData(GL_UNIFORM_BUFFER, length, data, dynamic? GL_DYNAMIC_DRAW : GL_STATIC_DRAW);
  
  return true;
}

bool UniformBlock::bindTo(GlUint bindPoint) const { _prsguard();
  if(bufferID == 0) return false;
  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
  glBindBufferBase(GL_UNIFORM_BUFFER, bindPoint, this->getBindID());
  return true;
}