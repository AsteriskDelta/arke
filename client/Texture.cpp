#include "Image.h"
#include "Console.h"
#include "rInterface.h"

#include "Texture.h"

template<bool Arr>
Texture<Arr>::Texture() {
  width = height = 0; layers = 0;
  id = 0;
  bpp = 0; samples = 1;
  allocated = committed = false;
  doMipmap = false;
  wrap = TextureWrap::Clamp;
  filter = TextureFilter::Nearest;
  mipmaps = 0;
}

template<bool Arr>
Texture<Arr>::Texture(TextureFormat f, Uint8 bits, Uint8 cCount, Uint8 ls) {
  id = 0;
  layers = ls;
  bpp = bits;
  format = f;
  channels = cCount;
  samples = 1;
  doMipmap = false;
  wrap = TextureWrap::Repeat;
  filter = TextureFilter::Nearest;
  compileFormatData();
  allocated = committed = false;
  mipmaps = 0;
}
template<bool Arr>
Texture<Arr>::Texture(const ImageRGBA *const img) {
  id = 0;
  layers = 1;
  bpp = 8;
  format = TextureFormat::Unsigned;
  doMipmap = true;
  allocated = committed = false;
  samples = 1;
  filter = TextureFilter::Ansiotropic;
  wrap = TextureWrap::Repeat;
  mipmaps = 0;
  if(img == NULL || !*img) {
    channels = 4;
    width = height = 0;
    compileFormatData();
  } else {
    channels = img->getChannels();
    compileFormatData();
    width = img->width;
    height = img->height;
    this->allocate(); 
    this->commit();
    this->upload(img);
  }
}
template<bool Arr>
Texture<Arr>::~Texture() {
  if(id != 0) deallocate();
}

template<bool Arr>
void Texture<Arr>::setSize(Uint16 x, Uint16 y) {
  //if(allocated) deallocate();
  width = x;
  height = y;
}

template<bool Arr>
void Texture<Arr>::setSize(Uint16 x, Uint16 y, Uint16 z) {
  //if(allocated) deallocate();
  width = x;
  height = y;
  if(Arr) layers = z;
}

#ifdef USEGL
template<bool Arr>
bool Texture<Arr>::allocate() { _prsguard();
  riEnsureState();
  if(width == 0 || height == 0 || layers == 0) return false;
  if(allocated) deallocate();
  
  if(mipmaps == 0) mipmaps = doMipmap ? log2(std::max(width, height))-1 : 1;
  
  if(id == 0) {
    glGenTextures(1, &id);
  }
  
  if(Arr) {//Array/3D
    glBindTexture(GL_TEXTURE_2D_ARRAY, id);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, mipmaps, compFormat, width, height, layers);
    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
  } else if(samples > 1) {
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, id);
    glTexStorage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, compFormat, width, height, GL_TRUE);
    glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
  } else {//2D
    glBindTexture(GL_TEXTURE_2D, id);
    glTexStorage2D(GL_TEXTURE_2D, mipmaps, compFormat, width, height);
    glBindTexture(GL_TEXTURE_2D, 0);
  }
  
  if(compChannels == GL_DEPTH_COMPONENT) glDepthRangedNV(-1.0, 1.0);
  
  allocated = true;
  return riEnsureState();
}

template<bool Arr>
void Texture<Arr>::deallocate() {
  if(!allocated) return;
  
  if(Arr) {
    glDeleteTextures(1, &id);
  } else {
    glDeleteTextures(1, &id);
  }
  
  id = 0;
  allocated = committed = false;
}

template<bool Arr>
GlUint Texture<Arr>::getBindTarget() {
  if(Arr) {//Array/3D
    return GL_TEXTURE_2D_ARRAY;
  } else if(samples > 1) {
    return GL_TEXTURE_2D_MULTISAMPLE;
  } else {//2D
    return GL_TEXTURE_2D;
  }
}

template<bool Arr>
void Texture<Arr>::commit() { _prsguard();
  riEnsureState();
  GlUint tEnum = this->getBindTarget();
  glBindTexture(tEnum, id);
  
  glTexParameteri(tEnum, GL_TEXTURE_BASE_LEVEL, 0);
  
  //std::cout << "texturecommit\n";
  if(samples <= 1) {
    if(filter == TextureFilter::Nearest||true) {
        //std::cout << "nearest!!!\n";
      if(doMipmap) {
        glTexParameteri(tEnum,GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
        glTexParameteri(tEnum,GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(tEnum, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
      } else {
        glTexParameteri(tEnum,GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(tEnum,GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      }
    } else {
        //std::cout << "linear!!!\n";
      if(doMipmap) {
        glTexParameteri(tEnum,GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(tEnum,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        
        glTexParameteri(tEnum, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
      } else {
        glTexParameteri(tEnum,GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(tEnum,GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      }
    }
  }riEnsureState();
  
  if(filter == TextureFilter::Ansiotropic) {
    float aniso = 0.0f;
    glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
    glTexParameterf(tEnum, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4); 
  } riEnsureState();
  
  GlUint wrapCGL;
  switch(wrap) {
    default: case TextureWrap::Repeat: wrapCGL = GL_REPEAT; break;
    case TextureWrap::RepeatMirrored: wrapCGL = GL_MIRRORED_REPEAT; break;
    case TextureWrap::Border:  wrapCGL = GL_CLAMP_TO_BORDER; break;
    case TextureWrap::Clamp: wrapCGL = GL_CLAMP_TO_EDGE; break;
  } riEnsureState();
  
  if(samples <= 1) {
    glTexParameteri(tEnum, GL_TEXTURE_WRAP_S, wrapCGL);
    glTexParameteri(tEnum, GL_TEXTURE_WRAP_T, wrapCGL);
  }
  committed = true;
  glBindTexture(tEnum, 0);
  riEnsureState();
}

template<bool Arr>
void Texture<Arr>::mipmap() { _prsguard();
  GlUint tEnum = this->getBindTarget();
  glBindTexture(tEnum, id);
  glGenerateMipmap(tEnum);
  glBindTexture(tEnum, 0);
  riEnsureState();
}

template<bool Arr>
void Texture<Arr>::upload(const ImageRGBA *const img, unsigned short xOff, unsigned short yOff, unsigned short layer) { _prsguard();
  if(img == NULL || !*img) return;
  riEnsureState();
  
  const int effWidth  = std::min(int(img->width ), width  - xOff);
  const int effHeight = std::min(int(img->height), height - yOff); 
  const int mipmapLevel = 0;
  compileFormatData();
  glBindTexture(this->getBindTarget() ,id);
  if(Arr) {
    const int layersToUpdate = 1;
    glTexSubImage3D(GL_TEXTURE_2D_ARRAY, mipmapLevel, xOff, yOff, layer, effWidth, effHeight, layersToUpdate, compChannels, compType, img->getRawData());
  } else {
    glTexSubImage2D(this->getBindTarget(), mipmapLevel, xOff, yOff, effWidth, effHeight, compChannels, compType, img->getRawData());
  }
  if(doMipmap) { this->mipmap(); };
  
  glBindTexture(this->getBindTarget(), 0);
  riEnsureState();
}

template<bool Arr>
void Texture<Arr>::compileFormatData() {
  compFormat = compChannels = 0;
  if(format == TextureFormat::Depth) {
    switch(bpp) {
      case 16: compFormat = GL_DEPTH_COMPONENT16;  compChannels = GL_DEPTH_COMPONENT;
      default:
      case 24: compFormat = GL_DEPTH_COMPONENT24;  compChannels = GL_DEPTH_COMPONENT;
      case 32: compFormat = GL_DEPTH_COMPONENT32F; compChannels = GL_DEPTH_COMPONENT;
    }
  } else if(format == TextureFormat::DepthStencil) {
    switch(bpp) {
      default:
      case 24: compFormat = GL_DEPTH24_STENCIL8;  compChannels = GL_DEPTH_STENCIL;
      case 32: compFormat = GL_DEPTH32F_STENCIL8; compChannels = GL_DEPTH_STENCIL;
    }
  } else if(format == TextureFormat::Float) {
    if(bpp == 32 && channels == 1) {
      compFormat = GL_R32F;
      compChannels = GL_INTENSITY;
    }
    compType = GL_FLOAT;
  } else if(format == TextureFormat::SignedNormal && bpp == 8) {
    compFormat = GL_RGBA8_SNORM;
    compChannels = GL_RGBA;
    compType = GL_BYTE;
  } else {
    switch(channels) {
      case 1:
	compChannels = GL_LUMINANCE;
	break;
      /*case 2:
	
	break;*/
      case 3:
	compChannels = GL_RGB;
	break;
      case 4:
	compChannels = GL_RGBA;
	if(bpp == 8 && format == TextureFormat::Unsigned) { compFormat = GL_RGBA8; compType = GL_UNSIGNED_BYTE; }
	else if(bpp == 8  && format == TextureFormat::Signed) { compFormat = GL_RGBA8I; compType = GL_BYTE; }
	else if(bpp == 16 && format == TextureFormat::Unsigned) { compFormat = GL_RGBA16; compType = GL_UNSIGNED_SHORT; }
	else if(bpp == 16 && format == TextureFormat::Signed) { compFormat = GL_RGBA16I; compType = GL_SHORT; }
	break;
      default:
	Console::Warning("Unknown channel count requested for texture!");
	break;
    };
  }
  
  if(compFormat == 0 || compChannels == 0) {
    Console::Error("Format and channel data result in unspecified behavior!");
  }
}

#elif defined(USEVK)

#endif

#include <algorithm>

inline static bool cmpstrtx(const std::string& str, const std::string& sub) {
  auto it = std::search(
    str.begin(), str.end(),
    sub.begin(),   sub.end(),
    [](char ch1, char ch2) { return std::toupper(ch1) == std::toupper(ch2); }
  );
  return (it != str.end() );
}

template<bool Arr>
TextureFormat Texture<Arr>::GetFormat(const std::string& s) {
  if(cmpstrtx(s, "depthstencil")) return TextureFormat::DepthStencil;
  else if(cmpstrtx(s, "depth")) return TextureFormat::Depth;
  else if(cmpstrtx(s, "fr") || cmpstrtx(s, "fluma")) return TextureFormat::Float;
  else if(cmpstrtx(s, "sgnorm")) return TextureFormat::SignedNormal;
  else if(cmpstrtx(s, "sgn")) return TextureFormat::Signed;
  else return TextureFormat::Unsigned;
}

template<bool Arr>
Uint8 Texture<Arr>::GetChannels(const std::string& s) {
  if(cmpstrtx(s, "rgba")) return 4;
  else if(cmpstrtx(s, "rgb")) return 3;
  else if(cmpstrtx(s, "r")) return 1;
  else if(cmpstrtx(s, "luma")) return 1;
  else return 4;
}

template<bool Arr>
TextureFilter Texture<Arr>::GetFilter(const std::string& s) {
  if(cmpstrtx(s, "linear")) {
    return TextureFilter::Linear;
  } else if(cmpstrtx(s, "ansiotropic")) {
    return TextureFilter::Ansiotropic;
  } else {
    return TextureFilter::Nearest;
  }
}

template class Texture<false>;
template class Texture<true>;
