#ifndef PIPESTAGE_INC
#define PIPESTAGE_INC
#include "client.h"
#include "Texture.h"

#define RBUFF_NULL 0xFF
#define PIPELINE_OBJ_MAX_T (8+3)
#define PIPELINE_DEPTH (PIPELINE_OBJ_MAX_T-2)
#define PIPELINE_STENCIL (PIPELINE_OBJ_MAX_T-1)
#define PIPELINE_DEPTHSTENCIL (PIPELINE_OBJ_MAX_T-3)

class Shader;

class RenderStage {
public:
    enum {
      DrawOpaque        = BIT00,
      DrawTransparent   = BIT01,
      DrawMod           = BIT02,
      Resolve           = BIT03,
      ClearDepth        = BIT04,
      ClearColor        = BIT05,
      PostProc          = BIT06,
      Barrier           = BIT07,
      MipMap            = BIT08,
      BindClipInfo      = BIT09,
      BindProjInfo      = BIT10,
      Blend             = BIT11,
      DepthTest         = BIT12,
      PostTiled         = BIT13,
      DepthOnly         = BIT14,
      BindFBInfo        = BIT15,
      DrawUI            = BIT16
    };
    
    RenderStage();
    ~RenderStage();
    
    void build();
    void destroy();
    
    bool bind();
    bool unbind();
    
    inline bool hasFlag(unsigned int f) { return (flags & f) != 0; };
    inline void setFlag(unsigned int f) { flags |= f; };
    inline void setFlag(unsigned int f, bool s) { flags |= f & (s? ~0 : 0); };
    inline void clearFlag(unsigned int f) { flags &= ~f; };
    
    void setBlending(std::string from, std::string to);
    void setDepthTest(std::string test);
    
    void processBinds(Shader *const shader);
    
  unsigned char buffers[PIPELINE_OBJ_MAX_T];
  Texture2D* sources[PIPELINE_OBJ_MAX_T];
  unsigned char idxMax;
  GlUint gpuID, chainID;
  
  Shader *postShader;
  Texture2D *postTarget;
  
  std::string id;
  unsigned int flags;
  
  GlUint blendFrom, blendTo, depthFunc;
  
  std::string sourcesRaw[PIPELINE_OBJ_MAX_T];
  std::string shaderRaw, chainRaw, pTargetRaw;
protected:
    double avgTime, curTime;
};

#endif
