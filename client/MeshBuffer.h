/* 
 * File:   MeshBuffer.h
 * Author: Galen Swain
 *
 * Created on May 23, 2015, 3:49 PM
 */

#ifndef MESHBUFFER_H
#define	MESHBUFFER_H
#include "Shared.h"
#include <cstring>

#define MESHBUFFER_MSZ (512*1024)

template <typename T, typename IT = unsigned int>
class MeshBuffer {
public:
    T bufferVert[MESHBUFFER_MSZ];
    IT bufferInd[MESHBUFFER_MSZ];
    
    MeshBuffer() {
        vertCount = indCount = 0;
        //verts = NULL;
        //inds = NULL;
    }
    virtual ~MeshBuffer() {
        //clear();
    }
    
    inline bool isBaked() { return true; };
    inline T& operator[](unsigned int i) { return bufferVert[i]; };
  
  inline T& vert(unsigned int i) { return bufferVert[i]; };
  
  inline void bake() {
      if(isBaked()) clear();
      //verts = new T[vertCount];
      //inds = new unsigned int[indCount];
      //memcpy(verts, bufferVert, vertCount*sizeof(T));
      //memcpy(inds, bufferInd, indCount*sizeof(unsigned int));
  }
  
  inline Uint32 getLeadingID() { return vertCount; };
  
  inline void setData(Uint32 idx, const T& a) { bufferVert[idx] = a; };
  inline void setData(Uint32 idx, const T& a, const T& b) { bufferVert[idx++] = a; bufferVert[idx++] = b;};
  inline void setData(Uint32 idx, const T& a, const T& b, const T& c) { bufferVert[idx++] = a; bufferVert[idx++] = b; bufferVert[idx++] = c;};
  inline void setData(Uint32 idx, const T& a, const T& b, const T& c, const T& d) { bufferVert[idx++] = a; bufferVert[idx++] = b; bufferVert[idx++] = c; bufferVert[idx++] = d;};
  void setData(Uint32 idx, Uint32 dCount, const T *const &d);
  
  inline Uint32 addVert(const T& a) { setVert(vertCount, a); return postIncrement(vertCount, Uint32(1)); };
  inline Uint32 addLine(const T& a, const T& b) { setLine(vertCount, a, b); return postIncrement(vertCount,  Uint32(2)); };
  inline Uint32 addTri(const T& a, const T& b, const T& c) { setTri(vertCount, a, b, c); return postIncrement(vertCount,  Uint32(3)); };
  inline Uint32 addQuad(const T& a, const T& b, const T& c, const T& d) { setQuad(vertCount, a, b, c, d); return postIncrement(vertCount,  Uint32(4)); };
  
  inline void setVert(Uint32 idx, const T& a) { setData(idx, a); };
  inline void setLine(Uint32 idx, const T& a, const T& b) { setData(idx, a, b); };
  inline void setTri(Uint32 idx, const T& a, const T& b, const T& c) { setData(idx, a, b, c); };
  inline void setQuad(Uint32 idx, const T& a, const T& b, const T& c, const T& d) { setData(idx, a, b, c, d); };
  
  inline void addInd(unsigned int ind) { bufferInd[indCount++] = ind; };
  inline void addLineInd(unsigned int a, unsigned int b) { addInd(a); addInd(b); };
  inline void addTriInd(unsigned int a, unsigned int b, unsigned int c) { addInd(a); addInd(b); addInd(c); };
  inline void addQuadInd(unsigned int a, unsigned int b, unsigned int c, unsigned int d) { addInd(a); addInd(b); addInd(c); addInd(d);};
  
  inline Uint32 getVertCount() { return vertCount; };
  inline Uint32 getIndCount() { return indCount; };
  
  void setBound(bool s);
  
  unsigned int vertCount, indCount;
    //T* verts;
    //unsigned int *inds;
    
    inline char* getDataPtr() { return (char*) bufferVert; };
  inline char* getIndPtr() { return (char*) bufferInd; };
  
   inline const char* getDataPtrConst() const { return (char*) bufferVert; };
  inline const char* getIndPtrConst() const { return (char*) bufferInd; };

  inline void clear() {
      vertCount = indCount = 0;
    }
    
};
//template<typename T, typename IT> T MeshBuffer<T, IT>::bufferVert[MESHBUFFER_MSZ];
//template<typename T, typename IT> IT MeshBuffer<T, IT>::bufferInd[MESHBUFFER_MSZ];
#endif	/* MESHBUFFER_H */

