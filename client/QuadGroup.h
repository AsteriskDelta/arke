#ifndef QUADGROUP_H
#define QUADGROUP_H
#include "Mesh.h"
#include "TextureAtlas.h"
#include "Shader.h"
#include <vector>

template<typename V, int C = 4>
class FaceGroup {
public:
    typedef V Vert;
    struct Face {
        Vert verts[C];
    };//  __attribute__ ((aligned(C*sizeof(Vert))));
    typedef unsigned short FaceID;
    
    inline FaceGroup(unsigned int startSize, bool canUpsize = true, bool dyn = false) : mesh(nullptr), count(0), edgeCount(0), maxCount(startSize), allowUpsize(canUpsize), shaderBound(false),
            meshDirty(true), atlas(nullptr), shader(nullptr), freeFaceIDs() {
        MeshType mType;
        switch(C) {
            case 1: mType = MeshType::Points; break;
            case 2: mType = MeshType::Lines; break;
            case 3: mType = MeshType::Tris; break;
            default: case 4: mType = MeshType::Quads; break;
        }
        mesh = new Mesh<V>(mType,vertsPerFace()*startSize, indsPerFace()*startSize, dyn);
        mesh->setType(mType);
    };
    inline virtual ~FaceGroup() {
        if(mesh != nullptr) delete mesh;
    }
    
    Mesh<V> *mesh;
    int count, edgeCount;
    int maxCount;
    bool allowUpsize, shaderBound, meshDirty;
    inline unsigned int size() const { return count; };
    
    TextureAtlas *atlas;
    Shader *shader;
    
    std::vector<FaceID> freeFaceIDs;
    inline FaceID getNewID() {
        FaceID ret;
        if(freeFaceIDs.size() > 0) {
            ret = freeFaceIDs.back();
            freeFaceIDs.pop_back();
        } else if(edgeCount < maxCount) {
            ret = edgeCount;
            edgeCount++;
        } else {
            this->resize(std::max(10, maxCount)*2);
            return this->getNewID();
        }
        count++;
        return ret;
    }
    
    inline void mkFaceIndices(FaceID fid) {
        unsigned int indOff = ((unsigned int)fid) * indsPerFace();
        unsigned int vertOff = ((unsigned int)fid) * vertsPerFace();
        for(unsigned int off = 0; off < indsPerFace(); off++) {
            mesh->indices[indOff+off] = vertOff+off;
        }
        meshDirty = true;
    }
    inline void nullFaceIndices(FaceID fid) {
        unsigned int indOff = ((unsigned int)fid) * indsPerFace();
        for(unsigned int off = 0; off < indsPerFace(); off++) {
            mesh->indices[indOff+off] = 0;
        }
        meshDirty = true;
    }
    
    inline Face *add() {
        FaceID newID = this->getNewID();
        mkFaceIndices(newID);
        return this->get(newID);
    }
    inline FaceID addID() {
        FaceID newID = this->getNewID();
        mkFaceIndices(newID);
        return newID;
    }
    inline void clear() {
        count = edgeCount = 0;
        meshDirty = true;
        freeFaceIDs.clear();
        mesh->clear();
    }
    inline void remove(FaceID id) {
        if(id >= maxCount) return;
        count--;
        nullFaceIndices(id);
        freeFaceIDs.push_back(id);
    }
    inline void remove(Face *v) {
        FaceID id = this->getID(v);
        if(id >= maxCount) return;
        count--;
        nullFaceIndices(id);
        freeFaceIDs.push_back(id);
    }
    
    inline void setDirty() {
        meshDirty = true;
    }
    inline bool dirty() const {
        return meshDirty;
    }
    
    inline void upload(bool force = false) {
        if(meshDirty || force) {
       // std::cout << "Uploading FG\n";
        //for(int i = 0; i < count; i++) {
        //  std::cout <<"\t"<<glm::to_string(mesh->data[i].pos) << "\n";
        //}
        
        mesh->setVertCount(edgeCount*vertsPerFace());
        mesh->setIndCount(edgeCount*indsPerFace());
        
        mesh->upload();
        riEnsureState();
        meshDirty = false;
      }
    }
    inline void draw() {
        riEnsureState();
        if(meshDirty) this->upload(true);
        riEnsureState();
        if(shader != nullptr) {
          riEnsureState();
          this->bindShader();
        }
          riEnsureState();
          //shader->bindV3(activeCamera->forward(), 27);
          if(atlas != nullptr) {
            atlas->bind(shader, 3, 13);
          }

          //std::cout << "drawing particlesystem with " << mesh->getGPUVertCount() << " pts\n";
          mesh->draw();
          riEnsureState();
          if(shader != nullptr) this->unbindShader();
    }
    
    inline void setShader(Shader *newShader) {
        shader = newShader;
    }
    
    inline void setAtlas(TextureAtlas *atl) {
        atlas = atl;
    }
    
    inline FaceID getID(Face *f) {
        return reinterpret_cast<size_t>(f - this->get(0))/sizeof(Face);
    }
    
    inline unsigned int vertsPerFace() const { return C; };
    inline unsigned int indsPerFace() const { return C; };
    inline void resize(unsigned int newCnt) {
        mesh->resize(vertsPerFace()*newCnt, indsPerFace()*newCnt);
        maxCount = newCnt;
    }
    
    inline Face *get(unsigned int idx) {
        //return &(reinterpret_cast<Face*>(mesh->getDataPtr())[idx]);
        return reinterpret_cast<Face*>(&(mesh->getDataPtr()[idx*sizeof(Face)]));
    }

    inline void bindShader() {
        if(shaderBound || shader == nullptr) return;
        shader->activate();
        shaderBound = true;
    }
    inline void unbindShader() {
        if(!shaderBound || shader == nullptr) return;
        shader->deactivate();
        shaderBound = false;
    }
private:

};

#endif /* QUADGROUP_H */

