#ifndef LINEMESH_INC
#define LINEMESH_INC
#include "Mesh.h"
#include "V3.h"
#include "V2.h"
#include "Color.h"
#include "Texture.h"

class Camera;
class Shader;

struct LineVert {
    V3 pos;
    V2 texCoord;
    ColorRGBA tint;
    float str;
  
  void mapAttributes(MeshContainer *const mesh) {
    Uint8 offset = 0;
    mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 2, MeshAttribute::Float, sizeof(float));
    mesh->addAttribute(offset, 3, MeshAttribute::Unsigned, sizeof(Uint8));
    mesh->addAttribute(offset, 1, MeshAttribute::Float, sizeof(float));
  }
};

struct LinePoint {
    V3 pos;
    ColorRGBA tint; 
    float width; 
};

class LineMesh {
public:
    LineMesh(const Uint32 maxLines = 512);
    ~LineMesh();
    
    void addLine(const V3& s, const V3& e, const float sWidth = 1.f, const float eWidth = 1.f);
    void addLine(const V3& s, const ColorRGBA& sc, const V3& e, const ColorRGBA& ec, const float sWidth = 1.f, const float eWidth = 1.f);
    
    void addPoint(const V3& p, const float width = 1.f);
    void addPoint(const V3& p, const ColorRGBA& sc, const float width = 1.f);
    
    void update();
    void render();
    
    void clear();
    
protected:
    Uint32 lineCount, lineMax;
    LinePoint *points;
    Mesh<LineVert> mesh;
    
};

#endif
