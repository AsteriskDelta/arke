#define RENDERER_CPP
#include "Math3D.h"
#include "rInterface.h"

#include "Renderer.h"
#include "Mesh.h"
#include "Pipeline.h"

SizedList<RenderPipeline*, PIPELINE_CNT_MAX> RenderPipeline::globalPipelines;
thread_local Renderer* renderer = NULL;

Renderer::Renderer(Window *const w) : modelMatrixStack() {
  window = w;
  boundMesh = NULL; activePipeline = NULL;
  modelMatrix = Mat4();
  targetSize = V2(0,0);
  resizeFrame = _globalFrame;
  modelMatrixStack.reserve(32);
}

Renderer::~Renderer() {
  
}

void Renderer::usePipeline(RenderPipeline* const p) {
    if(activePipeline != NULL) activePipeline->Deactivate();
    activePipeline = p;
    if(activePipeline != NULL) activePipeline->Activate();
}

void Renderer::setBound(MeshContainer *const mesh) {
  if(boundMesh != NULL) boundMesh->setBound(false);
  boundMesh = mesh;
  mesh->setBound(true);
}

void Renderer::newFrame() {
  drawcalls = 0;
}

void Renderer::resize() { _prsguard();
  if(!active) this->SetActive();
  glViewport(0, 0, targetSize.x, targetSize.y);
  for(Uint8 i = 0; i < RenderPipeline::globalPipelines.count(); i++) {
    RenderPipeline::globalPipelines[i]->ReallocateWSZ();
  }
}

void Renderer::SetActive() {
  if(renderer != NULL) renderer->active = false;
  renderer = this;
  active = true;
}

bool rIEnsureStateFunc(const char* file, int line) {
  int err = glGetError();
  if (err != GL_NO_ERROR) {
    rIEnsureStateError(file, line, err);
    return false;
  }
  return true;
}

void rIEnsureStateError(const char* file, int line, int err)  {
  std::stringstream ss; ss << "Uncaught OpenGL Error "<< std::hex << err << std::dec <<", detected in " << file << " at line " << line;
  Console::Error(ss.str());
}

void Renderer::PushTransform(const Transform& trans) {
    PushMatrix(trans.toMatrix());
}
void Renderer::PopTransform() {
    PopMatrix();
}

void Renderer::PushMatrix(const Mat4& mat) {
    if(renderer == nullptr) {
        Console::Err("Render::PushMatrix called with null renderer object (not active right now?)");
        return;
    }
    renderer->modelMatrixStack.push_back(renderer->modelMatrix * mat);
    renderer->modelMatrix = renderer->modelMatrixStack.back();
    //std::cout << "pushing transform " << glm::to_string(trans.position) << " for modelmatrix\n";
}
void Renderer::PopMatrix() {
    if(renderer == nullptr) {
        Console::Err("Render::PushMatrix called with null renderer object (not active right now?)");
        return;
    }
    if(renderer->modelMatrixStack.size() == 0) {
        Console::Warn("Renderer::PopMatrix stack mismatch");
    }
    renderer->modelMatrixStack.pop_back();
    if(renderer->modelMatrixStack.size() == 0) renderer->modelMatrix = Mat4(1.0);
    else renderer->modelMatrix = renderer->modelMatrixStack.back();
}