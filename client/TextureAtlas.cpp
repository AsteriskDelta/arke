/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   TextureAtlas.cpp
 * Author: Galen Swain
 * 
 * Created on April 27, 2016, 6:00 AM
 */

#include "TextureAtlas.h"
#include "Shader.h"
#include "rInterface.h"

TextureAtlas::TextureAtlas() : ImageAtlas() {
  ub = NULL;
  tex = NULL;
  doMipmap = false;
}

TextureAtlas::TextureAtlas(unsigned int w, unsigned int h, unsigned int l) : ImageAtlas(w,h,l) {
  ub = NULL; tex = NULL;
  doMipmap = false;
  //this->setDimensions(w,h,l);
}

TextureAtlas::~TextureAtlas() {
  delete ub;
  delete tex;
}

void TextureAtlas::setDimensions(unsigned int w, unsigned int h, unsigned int l) {
  ((ImageAtlas*)this)->setDimensions(w,h,l);
  
}

void TextureAtlas::debugWrite(std::string path) {
  for(unsigned int i = 0; i < layers.size(); i++) {
    if(layers[i].img->width <= 0 || layers[i].img->height <= 0) continue;
    std::stringstream ss; ss << path<<"_l"<<i<<".png";
    //std::cout << "writing " << ss.str() << "\n";
    layers[i].img->Save(ss.str());
  }
}

void TextureAtlas::bind(Shader *const shader, unsigned short texID, unsigned short uniID) { _prsguard();
  if(ub == NULL || tex == NULL) {
    std::stringstream ss; ss << "TextureAtlas::bind() called prior to upload()!";
    Console::Error(ss.str());
    return;
  }
  riEnsureState();
  ub->bindTo(uniID);
  riEnsureState();
  shader->bindTextureArray(tex, texID, texID);
  riEnsureState();
}

void TextureAtlas::upload() { _prsguard();
  riEnsureState();
  this->uploadLayers();
  this->uploadUniforms();
  riEnsureState();
}


void TextureAtlas::uploadLayers() { _prsguard();
  if(tex != NULL && tex->layers < layers.size()) {
    delete tex;
    tex = NULL;
  }
  if(tex == NULL) {
      //std::cout << "texatlas uploadLayers() allocate\n";
    tex = new Texture<true>(TextureFormat::Unsigned, 8, 4, layers.size());
    tex->setMipmap(false);
    tex->setMipmapCount(0);
    tex->setSamples(0);
    tex->setWrap(TextureWrap::Clamp);
    tex->setFilter(TextureFilter::Nearest);
    tex->setSize(width,height);
    tex->commit();
    tex->allocate();
    //std::cout << "tex allocated\n";
  }
  unsigned int layerID = 0;
  for(auto it = layers.begin(); it != layers.end(); it++) {
    tex->upload(it->img, 0, 0, layerID);
    layerID++;
  }
  //std::cout << "TextureAtlas uploaded " << layers.size() << " layers\n";
  tex->mipmap();
  riEnsureState();
}

void TextureAtlas::uploadUniforms() { _prsguard();
  if(ub == NULL) {
    ub = new UniformBlock();
    ub->allocate();
  }
  unsigned int buffSize = sizeof(ImageAtlas::LookupFP) * 256;
  char *buff = new char[buffSize]; ImageAtlas::LookupFP tmp;
  for(auto it = lookupMap.begin(); it != lookupMap.end(); ++it) {
    tmp = it->second.asFP(width, height);
    tmp.x += 2048.f * it->second.layer;
    //tmp.x = tmp.y = it->first;
    //std::cout << "writing offset " << it->first << " as starting from " << tmp.x << ","<<tmp.y<<" to " << (tmp.x+tmp.w) << ","<<(tmp.y+tmp.h) << "\n";
    memcpy(&buff[it->first*sizeof(ImageAtlas::LookupFP)], &tmp, sizeof(ImageAtlas::LookupFP));
  }
  ub->upload(buff, buffSize);
  delete[] buff;
  riEnsureState();
}

void TextureAtlas::setMipmap(bool state) {
  doMipmap = state;
}