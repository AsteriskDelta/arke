/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Font.h
 * Author: Galen Swain
 *
 * Created on April 27, 2016, 6:01 AM
 */

#ifndef FONT_H
#define FONT_H
#include "client.h"
#include "TextureAtlas.h"
#include "ImageFont.h"
#include "Image.h"

class Font {
public:
    struct Kerning {
        short bX, bY;//Bearing, raw offset from origin
        short oX, oY;//True offsets from origin
        short adv;//X distance to advance (pixels)
        
        std::string toString();
    };
    Font();
    Font(TextureAtlas *na, std::string path, unsigned int fPt = 96, bool sdf = false) ;
    virtual ~Font();
    
    ImageAtlas::Lookup& chr(const unsigned short idx, Font::Kerning *const k = NULL);
    
    void saveCache();
    bool loadCache();
    
    bool load(std::string path, unsigned int fPt = 96, bool sdf = false, bool force = false);
    
    void setAtlas(TextureAtlas *newAtlas);
    
    TextureAtlas *atlas;
    ImageAtlas::IDT idTable[128];
    Kerning kTable[128];
    
    short lineAdv;
    
    static unsigned short sdfResolution;
private:
    unsigned int pt, sdft;
    std::string getCachePath();
    std::string filePath;
};

#endif /* FONT_H */

