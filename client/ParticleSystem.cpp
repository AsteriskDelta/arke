#include "ParticleSystem.h"
#include "Console.h"
#include "Mesh.cpp"
#include <algorithm>


using namespace ARKE;

static thread_local V3 sortingRootPt = V3(0.f);
static thread_local V3 sortingRootVec = V3_FORWARD;
bool ParticleVert::operator<(const ParticleVert& o) const {//Sort as descending
  V3 del = glm::normalize(o.pos - pos);
 // V3 cam = glm::normalize((o.pos + pos)/2.f - sortingRootPt);
  return glm::dot(del,glm::normalize(normal+o.normal)) <= 0.f;
  //return glm::distance2(pos, sortingRootPt) > glm::distance2(o.pos, sortingRootPt);
  //const float projT = glm::dot(sortingRootVec, pos - sortingRootPt);
  //const float projO = glm::dot(sortingRootVec, o.pos - sortingRootPt);
  //return projT > projO;
}

ParticleSystem::ParticleSystem(unsigned int vcnt, bool up)  :  simulationEnabled(false), sortOnRender(false), shader(NULL), meshDirty(true), allowUpsizing(up), shaderBound(false) {
  //std::cout << "begin ps cons\n";
  atlas = NULL;
  maxParticles = vcnt; activeParticles = 0;
  firstFree = 0;
  //std::cout << "as lines\n";
  mesh = new Mesh<ParticleVert>(MeshType::Points, maxParticles, 0, true);//Dynamic mesh without indicies
  //std::cout << "correcting mesh type\n";
  mesh->setType(MeshType::Points);
  meta.resize(maxParticles);
  //std::cout << "end ps cons\n";
  for(unsigned short i = 0; i < PARTICLESYSTEM_MAX_GROUPS; i++) groupTable[i] = 0;
}

ParticleSystem::~ParticleSystem() {
  if(mesh != NULL) delete mesh;
}

void ParticleSystem::resize(unsigned int newCnt) {
  maxParticles = newCnt;
  meta.resize(maxParticles);
  mesh->resize(maxParticles, 0, true);
  meshDirty = true;
}

ParticleVert *ParticleSystem::addVert() { _prsguard();
  if(activeParticles == maxParticles || firstFree >= maxParticles) {
    std::cerr << "OUT OF UNALLOC pARTiCLES\n";
    if(!allowUpsizing) return nullptr;
    
    //const unsigned int oldMax = maxParticles;
    maxParticles = std::max(maxParticles*2, firstFree+1);
    this->resize(maxParticles);
  }
  meshDirty = true; activeParticles++;
  ParticleVert *ret = &mesh->data[firstFree];
  
  //Find the next free vert
  for(unsigned int i = firstFree+1; i < maxParticles; i++) {
    const bool vertValid = mesh->data[i].isValid();
    //std::cout << "vv("<<i<<") = " << vertValid << "\n";
    if(!( vertValid )) {
      firstFree = i;
      ret = &mesh->data[firstFree];
      break;
    }
  }
  //std::cout << "set nextFree to " << firstFree << "\n";
  return ret;
}
unsigned int ParticleSystem::addVertID() { 
  return getID(this->addVert());
}

void ParticleSystem::clear() {
  meshDirty = true;
  activeParticles = 0;
  mesh->clear();
}
  
void ParticleSystem::removeVert(unsigned int id) {
  if(id > maxParticles) return;
  mesh->data[id].makeInvalid();
  if(id < firstFree) firstFree = id;
  meshDirty = true;
}
void ParticleSystem::removeVert(ParticleVert *v) {
  this->removeVert((unsigned int)(v - mesh->data));
}

void ParticleSystem::minimize() {
  if(firstFree < maxParticles) this->resize(firstFree);
}

void ParticleSystem::update(float dT) {
  if(simulationEnabled) {
    for(unsigned int i = 0; i < maxParticles; i++) {
      const bool vertValid = mesh->data[i].isValid();
      if(!vertValid) continue;
      
      ParticleVert *const v = this->getVert(i);
      ParticleMeta *const m = this->getMeta(i);
      
      v->pos += m->velocity * dT;
      m->velocity += m->accel * dT;
      m->lifetime -= dT;
      //v->detA = (v->detA & BIT07) | ((~BIT07) & int(round((m->lifetime/m->lifetimeMax)*127.f)));
      v->color.v[3] = (min(255, int(round((m->lifetime/m->lifetimeMax)*255.f))));
      if(m->lifetime <= 0.f) {
        v->makeInvalid();
        const Uint16 g = m->group;
        if(rebirth && rebirthFuncs.size() > g && rebirthFuncs[g]) rebirthFuncs[g](this);
      }
    }
    meshDirty = true;
  }
}
void ParticleSystem::upload(bool force) { _prsguard();
  if(meshDirty || force) {
    //std::cout << "Uploading PS\n";
    //for(int i = 0; i < maxParticles; i++) {
    //  std::cout <<"\t"<<glm::to_string(mesh->data[i].pos) << "\n";
    //}
    if(sortOnRender) {
      sortingRootPt = activeCamera->transform.position;
      sortingRootVec = activeCamera->transform.forward();
      std::sort(mesh->data, mesh->data + mesh->getVertCountMax());
    }
    
    mesh->upload();
    riEnsureState();
    meshDirty = false;
  }
}
void ParticleSystem::bindShader() {
  if(shaderBound || shader == nullptr) return;
  shader->activate();
  shaderBound = true;
}

void ParticleSystem::unbindShader() {
  if(!shaderBound || shader == nullptr) return;
  shader->deactivate();
  shaderBound = false;
}

void ParticleSystem::draw() { _prsguard();
  riEnsureState();
  if(meshDirty || sortOnRender) this->upload(true);
  riEnsureState();
  if(shader != NULL) {
    riEnsureState();
    this->bindShader();
    riEnsureState();
    shader->bindV3(activeCamera->forward(), 27);
    if(atlas != NULL) {
      atlas->bind(shader, 3, 13);
    }
    
    //std::cout << "drawing particlesystem with " << mesh->getGPUVertCount() << " pts\n";
    mesh->draw();
    riEnsureState();
    this->unbindShader();
  } else {
    Console::Error("ParticleSystem::Draw() called with NULL shader");
  }
}

ParticleVert* ParticleSystem::getVert(unsigned int idx) {
  return  &mesh->data[idx];
}
ParticleMeta* ParticleSystem::getMeta(unsigned int idx) {
  return &meta[idx];
}

void ParticleSystem::setShader(Shader *newShader) {
  if(shader == newShader) return;
  shader = newShader;
}

void ParticleSystem::setSimulation(bool state) {
  simulationEnabled = state;
}

void ParticleSystem::setSorted(bool state) {
  sortOnRender = state;
}

void ParticleSystem::setAtlas(TextureAtlas *atl) {
  if(atlas == atl) return;
  atlas = atl;
}

unsigned int ParticleSystem::getID(ParticleVert *v) {
  return (unsigned int)(v - mesh->data);
}

void ParticleSystem::setRebirth(std::function<void(ParticleSystem*)> func, Uint16 gid) {
  if(gid >= rebirthFuncs.size()) rebirthFuncs.resize(gid+1);
  rebirthFuncs[gid] = func;
  rebirth = true;
}
void ParticleSystem::disableRebirth() {
  rebirth = false;
}

Uint16 ParticleSystem::addGroup() {
  for(unsigned short i = 1; i < PARTICLESYSTEM_MAX_GROUPS; i++) {
    if(!groupTable[i]) {
      groupTable[i] = 1;
      return i;
    }
  }
  return 0;
}

void ParticleSystem::removeGroup(Uint16 group) {
  bool wasChanged = false;
  for(unsigned int i = 0; i < maxParticles; i++) {
    const bool vertValid = mesh->data[i].isValid();
    if(!vertValid) continue;
    
    ParticleVert *const v = this->getVert(i);
    ParticleMeta *const m = this->getMeta(i);
    
    if(m->group == group) {
      v->makeInvalid();
      wasChanged = true;
      activeParticles--;
      if(i < firstFree) firstFree = i;
    }
  }
  
  groupTable[group] = 0;
  //std::cout << "removed group " << group << "\n";
  if(wasChanged) meshDirty = true;
}
