/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Skydome.h
 * Author: Galen Swain
 *
 * Created on April 19, 2016, 6:17 PM
 */

#ifndef SKYDOME_H
#define SKYDOME_H

#include "client.h"
#include "rInterface.h"
#include "Mesh.h"
#include "Shader.h"
#include "V3.h"
#include "V2.h"
#include "Renderer.h"
#include "Texture.h"

class Skydome {
public:
    struct cVert {
      V3 pos; 
      V3 normal;
      V2 polar;
      
      void mapAttributes(MeshContainer *const mesh) {
        Uint8 offset = 0;
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
        mesh->addAttribute(offset, 3, MeshAttribute::Float, sizeof(V3)/3);
    }
    
    };

    Skydome(float newRadius = 1000.f);
    Skydome(const Skydome& orig);
    virtual ~Skydome();
    
    void setTime(float newTime);
    
    void render(V3 center);
    
    
    static Texture2D *noise, *gradient;
private:
    static void LoadResources();
    static void UnloadResources();
    static Mesh<cVert> *mesh;  
    static Shader *shader;
    float sTime, radius;
    
};

#endif /* SKYDOME_H */

