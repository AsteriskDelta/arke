/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIText.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 9:34 AM
 */

#ifndef UITEXT_H
#define UITEXT_H
#include "Engine.h"
#include "UIObject.h"

class FontIndex;

class UIText : public UIObject {
public:
    enum Align {
        Left, Middle, Right, Top, Center, Bottom
    };
    UIText();
    virtual ~UIText();
    
    FontIndex *font;
    std::string text;
    bool isBlock, allowEdit;
    float textSize;
    Align hAlign, vAlign;
    
    void setFont(FontIndex *f);
    void setTextSize(float sz);
    void setText(const std::string& str);
    void setAlign(Align hori, Align vert);
    virtual bool draw();
private:
    
};

#endif /* UITEXT_H */

