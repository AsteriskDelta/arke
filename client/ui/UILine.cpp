/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UILine.cpp
 * Author: Galen Swain
 * 
 * Created on June 2, 2016, 2:15 PM
 */

#include "UILine.h"
#include "UIResources.h"

UILine::UILine() {
  
}
UILine::UILine(const std::string& rn) {
  this->setResource(rn);
}

UILine::~UILine() {
}

bool UILine::setResource(const std::string& resourceName) {
  resource = UIResources::getLineResources(resourceName);
  return resource != nullptr;
}

void UILine::setPosition(V3 s, V3 e) {
  UIObject::setPosition((s+e)/2.f);
  this->setSize(V2(glm::distance(e, s), 0.1f));
  this->setNormal(this->normal());
}

bool UILine::draw() {
  if(UIObject::draw()) return false;
  if(resource == nullptr) return true;
  
  V3 wpCenter = workplane.getOffsetCenter();
  V2 cornerSize = V2(this->getPixelRatio()*resource->figSize);
  V3 cornerMid = V3(0.f);
  
  this->addPart(wpCenter, workplane.size - 2.f*cornerSize*V2(1.f, 0.f), 1);
  
  cornerSize *= 2.f;
  this->addPart(cornerMid + wpCenter*V3_UP, cornerSize, 0);
  this->addPart(V3(wpCenter.x*2.f, 0.f, 0.f) + wpCenter*V3_UP, cornerSize, 2);
  
  return true;
}