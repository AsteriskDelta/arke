#ifndef UIRESOURCES_H
#define UIRESOURCES_H
#include "Engine.h"
#include "V2.h"

class TextureAtlas;
class FontIndex;

namespace UIResources {
    struct UIR {
        Uint16 figIDs[9];
        std::string path;
        V2 size, figSize;
        UIR();
    };
    
    void Initialize(TextureAtlas *const atl);
    
    bool hasResource(const std::string& path);
    UIR* getRectResources(const std::string& path);
    UIR* getLineResources(const std::string& path);
    UIR* getIconResources(const std::string& path, bool sdf = true);
    
    FontIndex* getDefaultFont();
    void setDefaultFont(FontIndex *f);
};

#endif /* UIRESOURCES_H */

