#include "UIResources.h"
#include <unordered_map>
#include "Image.h"
#include "ImageAtlas.h"
#include "TextureAtlas.h"

using namespace ARKE;

namespace UIResources {
  std::unordered_map<std::string, UIR> resourceList;
  TextureAtlas *atlas = nullptr;
  
  UIR::UIR()  {
            for(Uint8 i = 0; i < 9; i++) figIDs[i] = 0;
        }
  
  void Initialize(TextureAtlas *const atl) {
    atlas = atl;
  }
  
  FontIndex *defaultFont = nullptr;
  FontIndex* getDefaultFont() {
    return defaultFont;
  }
  void setDefaultFont(FontIndex *f) {
    defaultFont = f;
  }
  
  bool hasResource(const std::string& path) {
    auto it = resourceList.find(path);
    if(it == resourceList.end()) return false;
    else return true;
  }
  UIR* getResources(const std::string &path) {
    if(hasResource(path)) return &resourceList[path];
    else return nullptr;
  }
  
  void procImg(ImageRGBA *&img) {
    _unused(img);
    //img->generateSDF(8);
    //std::cout << "toemplace " << img->width << "x" << img->height << "\n";
  }
  
  UIR* getRectResources(const std::string& path) { _prsguard();
    UIR* ret = getResources(path);
    if(ret != nullptr) return ret;
    
    Uint16 w = 0,h = 0,midw = 0,midh = 0;
    if(!atlas->hasResource(path+"0")) {
      ImageRGBA *img = new ImageRGBA(path);
      if(img == nullptr) {
        std::stringstream ss; ss << "Error loading UI resource at " << path;
        Console::Error(ss.str());
        return nullptr;
      }
      
      w = img->width;h = img->height; midw = img->width/2;midh = img->height/2;
      _unused(w); _unused(h);
      ImageRGBA *slice;
      slice = img->getSlice(0      , 0      , midw  , midh  ); procImg(slice); atlas->emplace(path+"0", slice); delete slice;//UL
      slice = img->getSlice(midw-1 , 0      , 2     , midh  ); procImg(slice); atlas->emplace(path+"1", slice); delete slice;//UM
      slice = img->getSlice(midw   , 0      , midw  , midh  ); procImg(slice); atlas->emplace(path+"2", slice); delete slice;//UR
      
      slice = img->getSlice(0      , midh-1 , midw  , 2     ); procImg(slice); atlas->emplace(path+"3", slice); delete slice;//ML
      slice = img->getSlice(midw-1 , midh-1 , 2     , 2     ); procImg(slice); atlas->emplace(path+"4", slice); delete slice;//MM
      slice = img->getSlice(midw   , midh-1 , midw  , 2     ); procImg(slice); atlas->emplace(path+"5", slice); delete slice;//MR
      
      slice = img->getSlice(0      , midh   , midw  , midh  ); procImg(slice); atlas->emplace(path+"6", slice); delete slice;//LL
      slice = img->getSlice(midw-1 , midh   , 2     , midh  ); procImg(slice); atlas->emplace(path+"7", slice); delete slice;//LM
      slice = img->getSlice(midw   , midh   , midw  , midh  ); procImg(slice); atlas->emplace(path+"8", slice); delete slice;//LR
            
      delete img;
    }
    
    resourceList[path] = UIR();
    ret = getResources(path);
    ret->path = path;
    ret->size = V2(w,h);
    ret->figSize = V2(midw,midh);
    //std::cout << "loading rect with figsize " << midw << ","<<midh <<"\n";
    for(int i = 0; i < 9; i++) {
      std::stringstream ss; ss << path << i;
      ImageAtlas::Lookup *lk = atlas->find(ss.str());
      //std::cout << "UIRES loc " << ss.str() << "\n";
      if(lk == nullptr) {
        std::stringstream es; es << "UI Rectangle resource with id \"" << ss.str() << "\" couldn't be found";
        Console::Error(es.str());
        ret->figIDs[i] = 0;
      } else {
        ret->figIDs[i] = lk->selfID;
        ret->figSize.x = max(ret->figSize.x, float(lk->ow));
        ret->figSize.y = max(ret->figSize.y, float(lk->oh));
        //std::cout << "reg lookup of " << lk->w << "x" << lk->h << " with o=" << lk->ow <<"x"<<lk->oh<<"\n";
      }
    }
    if(w == 0 || h == 0) {
      midw = round(ret->figSize.x);
      midh = round(ret->figSize.y);
      w = 2*midw;
      h = 2*midh;
    }
    
    return ret;
  }
  UIR* getLineResources(const std::string& path) { _prsguard();
    UIR* ret = getResources(path);
    if(ret != nullptr) return ret;
    
    Uint16 w,h,midw,midh;
    if(!atlas->hasResource(path+"0")) {
      ImageRGBA *img = new ImageRGBA(path);
      if(img == nullptr) {
        std::stringstream ss; ss << "Error loading UI resource (line) at " << path;
        Console::Error(ss.str());
        return nullptr;
      }
      
      
      w = img->width;h = img->height; midw = img->width/2;midh = img->height/2;
      _unused(w); _unused(h);
      ImageRGBA *slice;
      slice = img->getSlice(0            , 0      , midw/2  , midh  ); procImg(slice); atlas->emplace(path+"0", slice); delete slice;//UL
      slice = img->getSlice(midw/2       , 0      , midw    , midh  ); procImg(slice); atlas->emplace(path+"1", slice); delete slice;//UM
      slice = img->getSlice(midw + midw/2, 0      , midw/2  , midh  ); procImg(slice); atlas->emplace(path+"2", slice); delete slice;//UR
            
      delete img;
    }
    
    resourceList[path] = UIR();
    ret = getResources(path);
    ret->path = path;
    ret->size = V2(w,h);
    ret->figSize = V2(midw,midh);
    for(int i = 0; i < 3; i++) {
      std::stringstream ss; ss << path << i;
      ImageAtlas::Lookup *lk = atlas->find(ss.str());
      if(lk == nullptr) {
        std::stringstream es; es << "UI Line resource with id \"" << ss.str() << "\" couldn't be found";
        Console::Error(es.str());
        ret->figIDs[i] = 0;
      } else {
        ret->figIDs[i] = lk->selfID;
      }
    }
    return ret;
  }
  UIR* getIconResources(const std::string& path, bool sdf) { _prsguard();
    UIR* ret = getResources(path);
    if(ret != nullptr) return ret;
    
    ImageAtlas::Lookup* lk = atlas->find(path);
    Uint16 w, h;
    if(lk == nullptr) {
      ImageRGBA *img = new ImageRGBA(path);
      w = img->width;
      h = img->height;
      if(sdf) procImg(img);
      lk = atlas->find(atlas->emplace(path, img));
      delete img;
    }
    if(lk == nullptr) {
      std::stringstream ss; ss << "Error loading UI resource at " << path;
      Console::Error(ss.str());
      return nullptr;
    }
    
    resourceList[path] = UIR();
    ret = getResources(path);
    ret->path = path;
    ret->figIDs[0] = lk->selfID;
    ret->size = V2(w,h);
    ret->figSize = V2(w, h);
    return ret;
  }
};
