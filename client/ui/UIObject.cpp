/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIObject.cpp
 * Author: Galen Swain
 * 
 * Created on June 2, 2016, 2:21 PM
 */

#include "UIObject.h"
#include "client/ParticleSystem.h"
#include <algorithm>
#include "UIResources.h"

UIObject::UIObject() : parent(nullptr), children() , tint(255,255,255,255), system(nullptr), systemGroup(0), uioDirty(true), childDirty(false), passthrough(false) {
}

UIObject::~UIObject() {
  this->free();
}

ParticleSystem* UIObject::sys() const {
  return system;
}
void UIObject::setSystem(ParticleSystem *s) {
  if(s == system) return;
  
  ParticleSystem* oldSystem = system;
  if(system != nullptr && systemGroup != 0) system->removeGroup(systemGroup);
  system = s;
  if(s != nullptr) systemGroup = system->addGroup();
  uioDirty = true;
  
  for(UIObject*& c : children) {
    if(c->system == nullptr || c->system == oldSystem) c->setSystem(s);
  }
}

void UIObject::addChild(UIObject* c) {
  auto it = std::find(children.begin(), children.end(), c);
  if(it == children.end()) {
    children.push_back(c);
    c->setParent(this);
  }
  childDirty = true;
}
void UIObject::removeChild(UIObject *c) {
  auto it = std::find(children.begin(), children.end(), c);
  if(it != children.end()) {
    (*it)->setParent(nullptr);
    children.erase(it);
  }
}
void UIObject::setParent(UIObject *p) {
  UIObject *const oldParent = parent;
  parent = nullptr;//Prevent recursive loop
  
  if(oldParent != nullptr) {
    if(oldParent->sys() == system) this->setSystem(nullptr);
    oldParent->removeChild(this);
  }
  parent = p;
  
  if(system == nullptr && p != nullptr) this->setSystem(p->sys());
}

float UIObject::getChildZ() const {
  return 0.01f;
}
float UIObject::getLocalZ() const {
  return workplane.center.z;
}

void UIObject::setTint(ColorRGBA t) {
  tint = t;
  this->setDirty();
}
void UIObject::setPosition(const V3& p) {
  workplane.center = p;
  this->setDirty();
}
void UIObject::setPosition(const V2& p, short layerOffset) {
  workplane.center = V3(p, (parent == nullptr)? 0.f : layerOffset*parent->getChildZ());
  this->setDirty();
}
void UIObject::setSize(const V2& s) {
  if(s == workplane.size) return;
  workplane.size = s;
  this->setDirty();
}
void UIObject::setNormal(const V3& n) {
  workplane.normal = n;
  this->setDirty();
}

float UIObject::getPixelRatio() const {
  return 1.f/64.f;
}

V3 UIObject::normal(const V3 &n) {
  if(parent == nullptr) return workplane.mkNormal(n);
  return parent->normal(workplane.mkNormal(n));
}
V3 UIObject::resolve(V3 in) {
  V3 ret = workplane.llResolve(in);
  
  if(parent == nullptr) return ret;
  else return parent->resolve(ret);
}

void UIObject::init() {
  
}
void UIObject::free(bool del) {
  if(del) {
    for(UIObject*& c : children) {
      c->free(true);
      delete c;
    }
  } else {
    for(UIObject*& c : children) c->free();
  }
  children.clear();
  if(system != nullptr && systemGroup != 0) system->removeGroup(systemGroup);
  if(parent != nullptr) parent->removeChild(this);
}

void UIObject::deleteChildren() {
  this->free(true);
}

void UIObject::setDirty() {
  uioDirty = true;
}
void UIObject::setClean() {
  uioDirty = false;
}

bool UIObject::needsRedraw() {
  return childDirty || uioDirty;
}

bool UIObject::draw() {
  if(uioDirty || childDirty) {
    //Pseudo-depth sorting
    children.sort([](const UIObject*const & a, const UIObject*const &b) { return a->getLocalZ() < b->getLocalZ(); });
    for(UIObject* &c : children) c->draw();
    childDirty = false;
  }
  if(!uioDirty) return false;
  
  if(system != nullptr && systemGroup != 0) system->removeGroup(systemGroup);
  this->setClean();
  //std::cout << "redrawing ui object \n";
  return true;
}

ParticleVert* UIObject::addVert() {
  if(system == nullptr) return nullptr;
  
  ParticleVert *ret = system->addVert();
  ret->color = tint;
  ParticleMeta *meta = system->getMeta(ret);
  meta->group = systemGroup;
  return ret;
}

bool UIObject::setResource(const std::string& rName) {
  std::stringstream ss;
  ss << "setResource("<<rName<<") requested of generic UIObject!";
  Console::Error(ss.str());
  return false;
}

ParticleVert* UIObject::addPart(V3 pos, V2 size, Uint16 figID, bool relFig) {
  const unsigned short pType = relFig? resource->figIDs[figID] : figID;
  //V3 r = workplane.right(), u = workplane.up(), f = workplane.forward();
  V3 r = V3_RIGHT,//this->resolve(V3_RIGHT),
     u = V3_UP, //this->resolve(V3_UP),
     f = V3_FORWARD;//this->resolve(V3_FORWARD);
  //V3 r = V3(0.f, 0.f, 1.f), u = V3(0.f, 1.f, 0.f);
  //std::cout << "WP: r=" << glm::to_string(r) << ", u=" << glm::to_string(u) << ", f=" << glm::to_string(f) << "\n";
  
  //float rot = (-glm::dot(f, V3_FORWARD)) * fmod(1.f - glm::dot(r, V3_RIGHT) + rotAdd, 2.f) * RAD_TO_DEG / 180.f;
  //unsigned char detA = clamp(int(round(rot * 128.f + 128.f)), 1, 255);
  
  ParticleVert *vert = this->addVert();
  if (vert != NULL) {
    vert->size = size;
    vert->pos = this->resolve(pos);
    //std::cout << "resolving " << glm::to_string(pos) << " to " << glm::to_string(vert->pos) << "\n";
    vert->normal =  this->normal(f);//glm::normalize(glm::cross(r, u));//workplane.forward();//this->normal(); //glm::normalize(glm::cross(right, up));
    vert->figureID = pType;
    vert->detA = 128;//detA;
    vert->detB = 0x0;
    vert->color = tint;
    vert->right = this->normal(r);
    //std::cout << "set rect tint to " << tint.toString() << "\n";
    //std::cout << "drawing rect with f#" << pType << " at " << glm::to_string(vert->pos) << " with size=" << glm::to_string(vert->size) << " and norm=" << glm::to_string(vert->normal) << "\n";
  }
  
  return vert;
}

void UIObject::setPassthrough(bool s) {
  passthrough = s;
}