/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIImage.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 9:34 AM
 */

#ifndef UIIMAGE_H
#define UIIMAGE_H
#include "Image.h"

#ifndef TEXTURE_INC
class Texture2D;
#endif

class UIImage {
public:
    UIImage();
    UIImage(const UIImage& orig);
    virtual ~UIImage();
    
    ImageRGBA *image;
    Texture2D *texture;
private:

};

#endif /* UIIMAGE_H */

