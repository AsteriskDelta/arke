/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIRect.cpp
 * Author: Galen Swain
 * 
 * Created on June 2, 2016, 2:14 PM
 */

#include "UIRect.h"
#include "ParticleSystem.h"
#include "TextureAtlas.h"
#include "UIResources.h"

UIRect::UIRect() {
}

UIRect::UIRect(const std::string& rname) {
  this->setResource(rname);
}

UIRect::~UIRect() {
}

bool UIRect::setResource(const std::string& rname) {
  resource = UIResources::getRectResources(rname);
  return resource != nullptr;
}

bool UIRect::draw() {
  if(!UIObject::draw()) return false;
  if(resource == nullptr) return true;
  
  //std::cout << "raw info: " << glm::to_string(workplane.center) << " with size="<<glm::to_string(workplane.size) <<"\n";
  V3 wpCenter = workplane.getOffsetCenter();
  V2 cornerSize = V2(this->getPixelRatio()*resource->figSize);
  //std::cout << "FGS: "<< glm::to_string(resource->figSize) << "\n";
  V3 cornerMid = V3(0.f);//V3(cornerSize*0.5f, 0.f);
  //this->addPart(wpCenter, workplane.size, 4);
  
  V3 maxCorner = V3(workplane.size, 0.f);
  V3 xCorner = V3(0.f, maxCorner.y, 0.f);
  V3 yCorner = V3(maxCorner.x, 0.f, 0.f);
  
  
  this->addPart(wpCenter, workplane.size - 2.f*cornerSize, 4);
  
  V3 broadSize = V3(workplane.size - cornerSize*2.f, 0.f);
  this->addPart(cornerMid + wpCenter*V3_UP, V2(cornerSize.x*2.f, broadSize.y), 3);
  this->addPart(maxCorner - wpCenter*V3_UP, V2(cornerSize.x*2.f, broadSize.y), 5);
  
  this->addPart(cornerMid + wpCenter*V3_RIGHT, V2(broadSize.x, cornerSize.t*2.f), 1);
  this->addPart(maxCorner - wpCenter*V3_RIGHT, V2(broadSize.x, cornerSize.t*2.f), 7);
  
  cornerSize *= 2.f;
  this->addPart(cornerMid, cornerSize, 0);
  this->addPart(maxCorner - cornerMid, cornerSize, 8);
  this->addPart(xCorner - cornerMid*V3_UP + cornerMid*V3_RIGHT, cornerSize, 6);
  this->addPart(yCorner + cornerMid*V3_UP - cornerMid*V3_RIGHT, cornerSize, 2);
  
  return true;
}
