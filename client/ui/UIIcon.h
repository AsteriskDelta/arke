/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIIcon.h
 * Author: Galen Swain
 *
 * Created on June 3, 2016, 12:59 PM
 */

#ifndef UIICON_H
#define UIICON_H
#include "Engine.h"
#include "UIObject.h"
#include "UIResources.h"

class UIIcon : public UIObject{
public:
    UIIcon();
    UIIcon(const std::string& resourceName);
    virtual ~UIIcon();

    bool setResource(const std::string& resourceName);
    
    virtual bool draw();
private:

};

#endif /* UIICON_H */

