/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIWindow.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 9:33 AM
 */

#ifndef UIWINDOW_H
#define UIWINDOW_H
#include "client.h"
#include "UIObject.h"
#include <vector>
class ParticleSystem;
class TextureAtlas;
class UIRect;
class Shader;

class UIWindow : public UIObject {
public:
    enum Layer {
        Default = 0,
        
        Manual = 0xFFFE,
        Any = 0xFFFF,
        None = 0xFFFF
    };
    
    UIWindow();
    virtual ~UIWindow();
    
    ParticleSystem *sys;
    TextureAtlas *atlas;
    Shader *shader;
    
    UIRect *background;
    Uint16 layer;
    
    void setBackground(std::string rname);
    void clearBackground();
    
    void setAtlas(TextureAtlas *atl = nullptr);
    void setShader(Shader *shd = nullptr);
    void createSystem();
    void destroySystem();
    
    void activate(Uint16 l = 0xFFFF);
    void deactivate();
    bool draw();
    
    static TextureAtlas *defaultAtlas;
    static Shader *defaultShader;
    static std::vector<UIWindow*> activeWindows;
    static bool DrawAll(Uint16 layerID = 0xFFFF);
    static void SetDefaults(TextureAtlas *atl, Shader *shd);
private:

};

#endif /* UIWINDOW_H */

