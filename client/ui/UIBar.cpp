#include "UIBar.h"

using namespace ARKE;

UIBar::UIBar() : background(), label(), maximum(0.f), decimals(1.f), components(), labelMode(LabelMode::None) {
}

UIBar::UIBar(const std::string& bk, const std::string& bt) : background(), label(), maximum(0.f), decimals(1.f), components(), labelMode(LabelMode::None) {
  this->setResource(bk, bt);
  this->addChild(&background);
  this->addChild(&label);
}

UIBar::~UIBar() {
  for(Component& c : components) {
    delete c.bar;
  }
}

UIBar::Component::Component() : value(0.f), color(255,255,255,255), bar(nullptr) {
}

void UIBar::setResource(const std::string& backt, const std::string& bart) {
  backType = backt; barType = bart;
  background.setResource(backType);
  for(Component& c : components) c.bar->setResource(barType);
}

void UIBar::setLabelMode(Uint8 m) {
  labelMode = m;
  this->setDirty();
}
void UIBar::setValue(float v, Uint8 component) {
  if(component >= components.size()) components.resize(component+1);
  components[component].value = v;
  this->setDirty();
}
void UIBar::setColor(ColorRGBA color, Uint8 component) {
  if(component >= components.size()) components.resize(component+1);
  components[component].color = color;
  this->setDirty();
}
void UIBar::setMaximum(float max) {
  maximum = max;
  this->setDirty();
}

bool UIBar::draw() {
  if(!UIObject::needsRedraw()) return false;
  
  background.setPosition(V2(0.f), 0);
  background.setSize(workplane.size);
  background.setTint(tint);
  
  const V2 padding = V2(min(workplane.size.x, workplane.size.y)) * 0.1f;
  const V2 drawBounds = workplane.size - 2.f*padding;
  float current = 0.f;
  for(Component& c : components) {
    if(c.bar == nullptr) {
      c.bar = new UIRect(barType);
      this->addChild(c.bar);
    }
    
    c.bar->setPosition(padding + V2(1.f, 0.f)*drawBounds*(current/maximum), 1);
    c.bar->setTint(c.color);
    c.bar->setSize(drawBounds * V2(c.value/maximum, 1.f) * V2(1.f, 0.f) + V2(0.f, drawBounds.y));
    
    current += c.value;
  }
  current = clamp(current, 0.f, maximum);
  
  std::stringstream labelText;
  switch(labelMode) {
    case LabelMode::None:
      break;
    case LabelMode::Current:
      labelText << (round(current * decimals)/decimals);
      break;
    case LabelMode::Full:
      labelText << (round(current * decimals)/decimals) << "/" << (round(maximum * decimals)/decimals);
      break;
    default:
    case LabelMode::Percent:
      labelText << (round((current/maximum*100.f) * decimals)/decimals);
      break;
  }
  
  label.setTextSize(0.04f);
  label.setText(labelText.str());
  label.setPosition(V2(0.f), 2);
  label.setAlign(UIText::Align::Right, UIText::Align::Center);
  label.setSize(workplane.size);
  label.setTint(tint);
  
  
  
  UIObject::draw();//We may have added new children, re-draw
  
  
  return true;
}
