/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UILine.h
 * Author: Galen Swain
 *
 * Created on June 2, 2016, 2:15 PM
 */

#ifndef UILINE_H
#define UILINE_H
#include "client.h"
#include "UIObject.h"

class UILine : public UIObject {
public:
    UILine();
    UILine(const std::string& resourceName);
    virtual ~UILine();

    bool setResource(const std::string& resourceName);
    virtual void setPosition(V3 s, V3 e);
    
    virtual bool draw();
protected:
    

};

#endif /* UILINE_H */

