#include "UIText.h"

#include "Font.h"
#include "FontIndex.h"
#include "ParticleSystem.h"
#include "TextureAtlas.h"
#include "UIResources.h"

using namespace ARKE;

UIText::UIText() : font(nullptr), text(""), isBlock(false), allowEdit(false), textSize(1.f) {
}

UIText::~UIText() {
}

void UIText::setFont(FontIndex *f) {
  font = f;
  this->setDirty();
}

void UIText::setTextSize(float s) {
  textSize = s;
  this->setDirty();
}

void UIText::setText(const std::string& str) {
  text = str;
  this->setDirty();
}
void UIText::setAlign(UIText::Align hori, UIText::Align vert) {
  hAlign = hori; vAlign = vert;
  this->setDirty();
}

bool UIText::draw() {
  if(!UIObject::draw()) return false;
  if(font == nullptr) {
    this->setFont(UIResources::getDefaultFont());
    if(font == nullptr) return true;
  }
  
  unsigned short cx = 0, cy = 0;
  float sx = textSize, sy = textSize;
  Font::Kerning ker;

  V3 pos = V3(0.f);//workplane.center;
  V3 lineSize = V3(sx, sy*font->lineAdv(), 0.f);
  V3 right = glm::normalize(workplane.right());
  V3 up = glm::normalize(workplane.up());

  V3 norm = glm::normalize(glm::cross(right, up));
  const V2 bounds = workplane.size;
  
  if(!isBlock) {
    const float lineShift = lineSize.y/5.f;
    switch(vAlign) {
      case Align::Top: 
        pos.y += workplane.size.y - lineSize.y/2.f - lineShift;
        break;
      default:
      case Align::Center:
        pos.y += workplane.size.y/2.f - lineShift/2.f;// - lineSize.y/20.f;
        break;
      case Align::Bottom:
        pos.y += lineSize.y/2.f;
        break;
    }
  }
  
  up = glm::normalize(glm::cross(right, norm));
  float rot = (1.f - glm::dot(right, V3(1.f, 0.f, 0.f))) * RAD_TO_DEG / 180.f;
  unsigned char detA = clamp(int(round(rot * 128.f + 128.f)), 1, 255);
  int pLine = 0, lineAdv = 0;
  struct LineMeta {
    unsigned int id;
    V2 base;
  };
  std::list<LineMeta> lineIDs = std::list<LineMeta>();
  for (int i = 0; i < int(text.size()); i++) {
    ImageAtlas::Lookup lk = font->chr(text[i], &ker);

    if (lk.w > 0 && lk.h > 0) {
      unsigned short pType = lk.selfID;
      /*
      ParticleVert *vert = this->addVert();
      if (vert == NULL) continue;
      vert->size = V2(sx * float(lk.ow), sy * float(lk.oh));
      vert->pos = this->resolve(pos + float(cx + vert->size.x / 2.f) * sx * right + float(cy + vert->size.y / 2.f) * sy*up);
      vert->normal = norm; //glm::normalize(glm::cross(right, up));
      vert->figureID = pType;
      vert->detA = detA;
      vert->detB = 0x0;
      //std::cout << "Writing " << text[i] << " at " << cx << ", " << cy <<":" << ker.oX << ", " << ker.oY << " figID = " << vert->figureID << "\n";
      //if(g->img != NULL) img->overlay(cx+g->oX,cy+g->oY, g->img);
       * */
      V2 vSize = V2(sx * float(lk.ow), sy * float(lk.oh));
      V3 vPos = V3(float(cx + vSize.x / 2.f) * sx,  float(cy + vSize.y / 2.f) * sy, 0.f);
      ParticleVert *vert = this->addPart(vPos, vSize, pType, false);
      lineIDs.push_back(LineMeta{sys()->getID(vert), V2(cx, cy)});
      lineAdv = max(lineAdv, int(font->lineAdv(text[i])));
    }

    cx += ker.adv;
    const bool wrapLine = cx * sx > bounds.x;
    const bool endLine = i == (int(text.size())-1);
    if (wrapLine || endLine) {
      int pOff = i+1;
      
      if(wrapLine) {
        cy += lineAdv;
        if (cy * sy > bounds.y || !isBlock) return true;
        cx = 0;
        lineAdv = 0;
        //backtrack to last space
        for (int x = pOff; x > pLine; x--) {
          if (std::isspace(text[x]) || std::ispunct(text[x])) {
            pOff = x;
            break;
          }
        }
        pLine = pOff;
      }

      const float lineMax = cx - ker.adv;
      const float remainingX = (bounds.x - lineMax*sx);
      V3 ptOffset;
      switch(hAlign) {
        default:
        case Left:
          ptOffset = V3(0.f);
          break;
        case Middle:
          ptOffset = remainingX* right / 2.f;
          break;
        case Right:
          ptOffset = remainingX * right;
          break;
      }
      //if(lineIDs.begin() != lineIDs.end()) lineMin = sys()->getVert(lineIDs.begin()->id);
      
      //if(lineIDs.begin() != lineIDs.end()) lineMin = sys()->getVert(lineIDs.begin()->id);
      
      for (auto it = lineIDs.rbegin(); it != lineIDs.rend(); ++it) {
        if (i <= pOff - 1) {//Shift according to alignment
          ParticleVert *v = sys()->getVert(it->id);
          v->pos += ptOffset;
        } else {
          sys()->removeVert(it->id);
          i--;
        }
      }
      lineIDs.clear();
      //i = pOff-1;
      continue;
    }
    
  }
  
  return true;
}
