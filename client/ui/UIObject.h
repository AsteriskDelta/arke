/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIObject.h
 * Author: Galen Swain
 *
 * Created on June 2, 2016, 2:21 PM
 */

#ifndef UIOBJECT_H
#define UIOBJECT_H
#include "client.h"
#include "Workplane.h"
#include "IntersectionHull.h"
#include "Color.h"

class ParticleSystem;
class TexureAtlas;
class ParticleVert;
namespace UIResources {
    class UIR;
};

class UIObject {
public:
    UIObject();
    virtual ~UIObject();
    
    Workplane workplane;
    UIObject *parent;
    std::list<UIObject*> children;
    ColorRGBA tint;
    IntersectionHull localHull;
    
    bool useMouse, useKeyboard;
    
    ParticleSystem* sys() const;
    void setSystem(ParticleSystem *s);
    
    void addChild(UIObject* c);
    void removeChild(UIObject *c);
    void setParent(UIObject *p);
    float getChildZ() const;
    
    virtual bool setResource(const std::string& rName);
    
    void setTint(ColorRGBA t);
    void setPosition(const V3& p);
    void setPosition(const V2& p, short layerOffset = 1);
    void setSize(const V2& s);
    void setNormal(const V3& n);
    
    float getPixelRatio() const;
    float getLocalZ() const;
    
    V3 normal(const V3& nRel = V3_FORWARD);
    V3 resolve(V3 in);
    
    void init();
    void free(bool del = false);
    void deleteChildren();
    
    void setDirty();
    void setClean();
    
    virtual bool draw();
    bool needsRedraw();
    
    //Input functions
    void setPassthrough(bool s);
protected:
    ParticleSystem *system;
    Uint16 systemGroup;
    bool uioDirty, childDirty;
    UIResources::UIR* resource;
    bool passthrough;
    
    ParticleVert* addVert();
    ParticleVert*  addPart(V3 pos, V2 size, Uint16 figID, bool relFigID = true);
};

#endif /* UIOBJECT_H */

