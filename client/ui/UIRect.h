/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIRect.h
 * Author: Galen Swain
 *
 * Created on June 2, 2016, 2:14 PM
 */

#ifndef UIRECT_H
#define UIRECT_H
#include "Engine.h"
#include "UIObject.h"
#include "UIResources.h"

class UIRect : public UIObject {
public:
    UIRect();
    UIRect(const std::string& resourceName);
    virtual ~UIRect();
    
    bool setResource(const std::string& resourceName);
    
    virtual bool draw();
protected:
    
};

#endif /* UIRECT_H */

