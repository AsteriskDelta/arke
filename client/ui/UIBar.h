/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIBar.h
 * Author: Galen Swain
 *
 * Created on May 25, 2016, 9:35 AM
 */

#ifndef UIBAR_H
#define UIBAR_H
#include "Engine.h"
#include "Color.h"
#include "UIObject.h"
#include "UIRect.h"
#include "UIText.h"
#include <vector>

class UIBar : public UIObject {
public:
    enum LabelMode {
        None = 0,
        Current = 1,
        Full = 2,
        Percent = 3
    };
    
    UIBar();
    UIBar(const std::string& bk, const std::string& bt);
    virtual ~UIBar();
    
    UIRect background;
    UIText label;
    
    float maximum, decimals;
    struct Component {
        float value;
        ColorRGBA color;
        UIRect *bar;
        Component();
    };
    std::vector<Component> components;
    Uint8 labelMode;
    std::string backType, barType;
    
    void setResource(const std::string& backType, const std::string& barType);
    
    void setLabelMode(Uint8 m);
    void setValue(float v, Uint8 component = 0);
    void setColor(ColorRGBA color, Uint8 component = 0);
    void setMaximum(float max);
    
    virtual bool draw();
private:

};

#endif /* UIBAR_H */

