/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIWindow.cpp
 * Author: Galen Swain
 * 
 * Created on May 25, 2016, 9:33 AM
 */

#include "UIWindow.h"
#include "ParticleSystem.h"
#include "TextureAtlas.h"
#include "Shader.h"
#include <algorithm>
#include "UIRect.h"

TextureAtlas *UIWindow::defaultAtlas = nullptr;
Shader *UIWindow::defaultShader = nullptr;
std::vector<UIWindow*> UIWindow::activeWindows = std::vector<UIWindow*>();

UIWindow::UIWindow() : sys(nullptr), atlas(defaultAtlas), shader(defaultShader), background(nullptr), layer(0) {
  this->createSystem();
}

UIWindow::~UIWindow() {
  this->deactivate();
  this->clearBackground();
  this->destroySystem();
}

void UIWindow::setBackground(std::string rname) {
  if(background != nullptr) this->clearBackground();
  background = new UIRect(rname);
  this->addChild(background);
  background->setSize(this->workplane.size);
}
void UIWindow::clearBackground() {
  if(background != nullptr) {
    delete background;
    background = nullptr;
  }
}

void UIWindow::setAtlas(TextureAtlas *atl) {
  if(atl == nullptr) atlas = defaultAtlas;
  else atlas = atl;
}
void UIWindow::setShader(Shader *shd) {
  if(shd == nullptr) shader = defaultShader;
  else shader = shd;
}

void UIWindow::createSystem() {
  sys = new ParticleSystem(1024, true);
}
void UIWindow::destroySystem() {
  if(sys != nullptr) delete sys;
}

void UIWindow::activate(Uint16 l) {
  if(l != 0xFFFF) layer = l;
  
  auto it = std::find(activeWindows.begin(), activeWindows.end(), this);
  if(it == activeWindows.end()) activeWindows.push_back(this);
}
void UIWindow::deactivate() {
  auto it = std::find(activeWindows.begin(), activeWindows.end(), this);
  if(it != activeWindows.end()) activeWindows.erase(it);
}

bool UIWindow::draw() {
  sys->setAtlas(atlas);
  sys->setShader(shader);
  if(background != nullptr) background->setSize(this->workplane.size);
  bool ret = UIObject::draw();
  
  sys->draw();
  return ret;
}

bool UIWindow::DrawAll(Uint16 layerID) {
  for(UIWindow *const & window : activeWindows) {
    if(window->layer == Layer::Manual) continue;
    if(window->layer == layerID || layerID == Layer::Any) window->draw();
  }
  return true;
}

void UIWindow::SetDefaults(TextureAtlas *atl, Shader *shd) {
  defaultAtlas = atl;
  defaultShader = shd;
}