/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   UIIcon.cpp
 * Author: Galen Swain
 * 
 * Created on June 3, 2016, 12:59 PM
 */

#include "UIIcon.h"


UIIcon::UIIcon() {
}

UIIcon::UIIcon(const std::string& resourceName) {
  this->setResource(resourceName);
}

UIIcon::~UIIcon() {
}

bool UIIcon::setResource(const std::string& rname) {
  resource = UIResources::getIconResources(rname);
  return resource != nullptr;
}

bool UIIcon::draw() {
  if(!UIObject::draw()) return false;
  if(resource == nullptr) return true;
  
  V3 wpCenter = workplane.getOffsetCenter();
  this->addPart(wpCenter, workplane.size, 0);
  
  return true;
}
