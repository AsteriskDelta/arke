/* 
 * File:   UniformBlock.h
 * Author: Galen Swain
 *
 * Created on June 19, 2015, 9:16 AM
 */

#ifndef UNIFORMBLOCK_H
#define	UNIFORMBLOCK_H
#include "client.h"

class UniformBlock {
public:
    UniformBlock();
    virtual ~UniformBlock();
    
    bool allocate();
    void deallocate();
    
    bool upload(const char* data, unsigned int length, bool dynamic = false);
    inline GlUint getBindID() const { return bufferID; };
    bool bindTo(GlUint bindPoint) const;
private:
    GlUint bufferID;
};

#endif	/* UNIFORMBLOCK_H */

