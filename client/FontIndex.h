/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FontIndex.h
 * Author: Galen Swain
 *
 * Created on April 27, 2016, 6:29 AM
 */

#ifndef FONTINDEX_H
#define FONTINDEX_H
#include "Font.h"
#include "TextureAtlas.h"
#include <list>

class FontIndex {
public:
    struct FontRange {
        Font *font;
        unsigned short start, end;
        short offset;
        bool active;
        
        inline bool contains(unsigned short idx) const {
            return idx >= start && idx < end;
        }
    };
    
    FontIndex();
    FontIndex(TextureAtlas *at, unsigned short pt);
    virtual ~FontIndex();
    
    void setSize(unsigned short pt);
    void setAtlas(TextureAtlas *newAtlas);
    
    bool loadFont(std::string path, unsigned short rangeOffset = 0, unsigned short firstChar = 32, unsigned short lastChar = 126);
    void unload();
    
    //ImageAtlas::Lookup& operator[](const unsigned short idx);
    ImageAtlas::Lookup& chr(const unsigned short idx, Font::Kerning *const k = NULL);
    unsigned short lineAdv(const unsigned short idx = 0xFFFF);//Defaults to max of all active fonts
    
    bool useSDF;
    inline void setSDF(bool state) {
        useSDF = state;
    }
private:
    void initVar();
    
    TextureAtlas *atlas;
    unsigned short ftSize;
    
    std::list<FontRange> fonts;
    ImageAtlas::Lookup nullLookup;
    unsigned short charMin, charMax, charCount;
};

#endif /* FONTINDEX_H */

