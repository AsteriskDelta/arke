#define RENDERER_CPP
#include <cassert>
#include <vulkan/vulkan.h>
#include <vulkan/vk_platform.h>
#include "Application.h"
#include "Console.h"
#define USE_SWAPCHAIN_EXTENSIONS
#include "Renderer.h"
#include "Window.h"
#include "GLDevice.h"
#include <SDL.h>
#include <SDL_syswm.h>
#include <SDL_vulkan.h>

#define ArraySize(a) (sizeof(a)/sizeof(*a))

#if defined(VK_USE_PLATFORM_XLIB_KHR) || defined(VK_USE_PLATFORM_XCB_KHR)
#include <X11/Xutil.h>
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
#include <linux/input.h>
#endif

namespace arke {

    //SizedList<RenderPipeline*, PIPELINE_CNT_MAX> RenderPipeline::GlobalPipelines;
    thread_local Renderer* ActiveRenderer = nullptr;
    thread_local VkInstance_T* Renderer::Instance = nullptr;

    Renderer::Renderer(Window *const w) : active(false), window(w), activePipeline(nullptr), instance(nullptr)/* : modelMatrixStack()*/ {
        //window = w;
        //modelMatrix = Mat4();
        targetSize = window->size;
        resizeFrame = _globalFrame;
        //modelMatrixStack.reserve(32);
        
        this->createInstance();
        this->enumerateDevices();
    }

    Renderer::~Renderer() {
        this->destroyInstance();
    }

    void Renderer::usePipeline(RenderPipeline* const p) {
        //if(this->activePipeline != nullptr) this->activePipeline->deactivate();
        //this->activePipeline = p;
        //if(this->activePipeline != nullptr) this->activePipeline->activate();
    }

    void Renderer::newFrame() {
        drawcalls = 0;
    }

    void Renderer::resize(Vec2 sz) { _prsguard();
        if(!active) this->setActive();
        _unused(sz);
        //for(Uint8 i = 0; i < RenderPipeline::GlobalPipelines.count(); i++) {
            //RenderPipeline::globalPipelines[i]->reallocateWSZ();
        //}
    }

    void Renderer::setActive() {
        if(ActiveRenderer != nullptr) ActiveRenderer->active = false;
        
        ActiveRenderer = this;
        Instance = this->instance;
        active = true;
    }
    
    bool Renderer::createInstance() {
        const char* instanceExtensionNames[] = {
            VK_KHR_SURFACE_EXTENSION_NAME
        };
        
        VkApplicationInfo ai = {};
        ai.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        ai.pNext = nullptr;
        ai.pApplicationName = Application::GetName().c_str();
        ai.applicationVersion = 1;
        ai.pEngineName = "ARKE";
        ai.engineVersion = 1;
        ai.apiVersion = VK_API_VERSION_1_0;
        
        const char **names;
        unsigned int count = 0;
        
        {
            if(!SDL_Vulkan_GetInstanceExtensions(window->mainWindow, &count, NULL)) {
                Console::Err("Query of Vulkan instance extension count failed...\n");
                return false;
            }
            
            static const char *const additionalExtensions[] ={
                VK_EXT_DEBUG_REPORT_EXTENSION_NAME, // example additional extension
            };
            size_t additionalExtensionsCount = sizeof(additionalExtensions) / sizeof(additionalExtensions[0]);
            size_t extensionCount = count + additionalExtensionsCount;
            names = reinterpret_cast<const char**>(malloc(sizeof(const char *) * extensionCount));
            
            // get names of required extensions
            if(!SDL_Vulkan_GetInstanceExtensions(window->mainWindow, &count, names)) {
                Console::Err("Query of Vulkan instance extensions failed...\n");
                return false;
            }
            
            // copy additional extensions after required extensions
            for(size_t i = 0; i < additionalExtensionsCount; i++) names[i + count] = additionalExtensions[i];
        }
        
        std::string dumpStr = "VK_LAYER_LUNARG_api_dump";
        const char* dumpPtr = dumpStr.c_str();
        
        VkInstanceCreateInfo ici = {};
        ici.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        ici.pNext = nullptr;
        ici.flags = 0;
        ici.pApplicationInfo = &ai;
        ici.enabledLayerCount = 0;//1;
        ici.ppEnabledLayerNames = nullptr;//&dumpPtr;
        ici.enabledExtensionCount = count;
        ici.ppEnabledExtensionNames = names;
        
        Console::Out("Creating Vulkan Instance...\n");
        assert(vkCreateInstance(&ici, nullptr, &instance) == VK_SUCCESS);
        
        free(names);
        return true;
    }
    bool Renderer::destroyInstance() {
        if(instance != nullptr) vkDestroyInstance(instance, nullptr);
    }
    
    void Renderer::enumerateDevices() {
        if(!this->active) this->setActive();
        /*Console::Out("Enumerating physical devices... ");
        VkPhysicalDevice physicalDevices[8];
        uint32_t physicalDeviceCount = ArraySize(physicalDevices);
        assert( vkEnumeratePhysicalDevices(
                instance,
                &physicalDeviceCount,
                physicalDevices)
            == VK_SUCCESS);
        
        Console::Out(physicalDeviceCount," physical device(s)\n");
        
        assert(physicalDeviceCount > 0);*/
        Device::EnumerateAll();
    }
    
/*
    void Renderer::PushTransform(const Transform& trans) {
        PushMatrix(trans.toMatrix());
    }
    void Renderer::PopTransform() {
        PopMatrix();
    }

    void Renderer::PushMatrix(const Mat4& mat) {
        if(renderer == nullptr) {
            Console::Err("Render::PushMatrix called with null renderer object (not active right now?)");
            return;
        }
        renderer->modelMatrixStack.push_back(renderer->modelMatrix * mat);
        renderer->modelMatrix = renderer->modelMatrixStack.back();
        //std::cout << "pushing transform " << glm::to_string(trans.position) << " for modelmatrix\n";
    }
    void Renderer::PopMatrix() {
        if(renderer == nullptr) {
            Console::Err("Render::PushMatrix called with null renderer object (not active right now?)");
            return;
        }
        if(renderer->modelMatrixStack.size() == 0) {
            Console::Warn("Renderer::PopMatrix stack mismatch");
        }
        renderer->modelMatrixStack.pop_back();
        if(renderer->modelMatrixStack.size() == 0) renderer->modelMatrix = Mat4(1.0);
        else renderer->modelMatrix = renderer->modelMatrixStack.back();
    }
*/
};
