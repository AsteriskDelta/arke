#include "Window.h"
#include "Application.h"
#include "Image.h"
#include "Console.h"

#include "Renderer.h"
#define USE_SWAPCHAIN_EXTENSIONS
#include <SDL.h>
#include <SDL_syswm.h>
#include <SDL_vulkan.h>
#include <vulkan/vulkan.h>
#include <vulkan/vk_platform.h>

namespace arke {
    Window* Window::Active = nullptr;

    void Window::OnQuit() {
        SDL_Quit();
    }

    Window::Window(std::string newTitle, Vec2 sz) {
        static bool OnQuitHooked = false;
        if(!OnQuitHooked) {
            Application::OnExit(&Window::OnQuit);
            OnQuitHooked = true;
        }

        if(newTitle == "") title = Application::GetName();
        else title = newTitle;

        Active = this;
        this->open(sz);
    }

    Window::~Window() {
        close();
    }

    bool Window::open(Vec2 sz) {
        size = sz;
        const int vsync = 1;

        SDL_SetHint(SDL_HINT_FRAMEBUFFER_ACCELERATION, "1");
        if(SDL_Init(SDL_INIT_EVERYTHING) < 0) {
            Console::Fatal("Unable to initialize SDL!");
            return false;
        }

        Uint32 windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_VULKAN ;// | SDL_WINDOW_BORDERLESS;
        if(size.x > 0 && size.y > 0) windowFlags |= SDL_WINDOW_RESIZABLE;
        else if(size.x < 0 && size.y < 0) windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;//SDL_WINDOW_FULLSCREEN;
        else windowFlags |= SDL_WINDOW_MAXIMIZED;

        /*if(Prefs.GetBool("fullscreen", false)) windowFlags |= SDL_WINDOW_FULLSCREEN;
        if(Prefs.GetBool("resizeable", true)) windowFlags  |= SDL_WINDOW_RESIZABLE;
        width  = Prefs.GetInt("windowX", 960);
        height = Prefs.GetInt("windowY", 690);*/
        pixelsPerInch = 60;//Prefs.GetInt("pixelsPerInch", 60);

        mainWindow = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, int(size.x), int(size.y), windowFlags);
        //LoadIcon();

        if(!mainWindow) {
            Console::Fatal("Unable to open SDL Window!");
            return false;
        }

        Console::Out("Window creating, initializing creating renderer object");
        this->renderer = new Renderer(this);

        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        assert(SDL_GetWindowWMInfo(mainWindow, &info) == SDL_TRUE);

        auto result = SDL_Vulkan_CreateSurface(mainWindow, Renderer::Instance, &this->surface);

        if (result != SDL_TRUE) {
            fprintf(stderr, "Failed to create Vulkan surface: %d\n", result);
            abort();
        }

        return true;
    }

    void Window::close() {
        SDL_DestroyWindow(mainWindow);
    }

    void Window::setIcon(const ImageRGBA *const img) {
        if(img == NULL || !*img) {
            Console::Warning("Window.setIcon called with null or empty image!");
            return;
        }

        iconSurface = SDL_CreateRGBSurfaceFrom(img->getRawData(), img->width, img->height, img->getBPP(), img->getPitch(), RMASK, GMASK, BMASK, AMASK);

        SDL_SetWindowIcon(mainWindow, iconSurface);
        SDL_FreeSurface(iconSurface);
    }

    static SDL_Event* spoofedEvent = NULL;
    void Window::processEvents() { _prsguard();
        SDL_Event event;
        //While there's an event to handle
        int checkWidth, checkHeight;
        SDL_GetWindowSize(mainWindow, &checkWidth, &checkHeight);

        //Fake a resize event if SDL *forgets* to send us a window resize notification
        if(size.x != checkWidth || size.y != checkHeight) {
            if(spoofedEvent == NULL) spoofedEvent = new SDL_Event();

            spoofedEvent->type = SDL_WINDOWEVENT_SIZE_CHANGED;
            SDL_PushEvent(spoofedEvent);
        }

        bool shouldFocus = false, wasResized = false;

        //Zero the input
        //Input::accelX = Input::accelY = Input::scrollX = Input::scrollY = 0;
        //Input *input = NULL;
        while(SDL_PollEvent(&event)) {
            switch(event.type) {
            /*case SDL_KEYDOWN:
            case SDL_KEYUP:
            input = Input::GetByKeyboard(0);
            if(input != NULL) {
            input->keyEvent(event.key.keysym.sym, event.key.keysym.scancode, event.type == SDL_KEYDOWN, event.key.keysym.mod);
            }
            //Input::KeyEvent(event.key);
            //shouldFocus = true;
            break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            input = Input::GetByMouse(0);
            if(input != NULL) {
            input->btnEvent(event.button.button - SDL_BUTTON_LEFT, event.button.state);
            }
            //Input::MouseButtonEvent(event.button);
            shouldFocus = true;
            break;
            case SDL_MOUSEWHEEL: break;//Overridden by button triggers
            input = Input::GetByMouse(0);
            if(input != NULL) {
            if(event.wheel.x != 0) input->axisEvent(Input::Axis::ScrollX, event.wheel.x);
            if(event.wheel.y != 0) input->axisEvent(Input::Axis::ScrollY, event.wheel.y);
            }
            //Input::MouseScrollEvent(event.wheel);
            break;
            case SDL_MOUSEMOTION:
            //if(!focused) break;
            input = Input::GetByMouse(0);
            //std::cout << event.motion.x << ", " << event.motion.y << "\n";
            if(input != NULL) {
            if(event.motion.x != 0) input->axisEventAbsolute(Input::Axis::MouseX, event.motion.x);
            if(event.motion.y != 0) input->axisEventAbsolute(Input::Axis::MouseY, event.motion.y);
            }
            //Input::MouseMotionEvent(event.motion);
            break;
            //Joystick Events
            case SDL_CONTROLLERDEVICEADDED:
            //Input::AddGamepad(event.cdevice.which);
            break;
            case SDL_CONTROLLERDEVICEREMOVED:
            //Input::RemoveGamepad(event.cdevice.which);
            break;
            case SDL_CONTROLLERBUTTONDOWN:
            case SDL_CONTROLLERBUTTONUP:
            //Input::GamepadButtonEvent(event.cbutton);
            input = Input::GetByGamepad(event.cbutton.which);
            if(input != NULL) {
            input->btnEvent(int(event.cbutton.button) - SDL_CONTROLLER_BUTTON_A + INPUT_MOUSE_BUTTONS, event.cbutton.state);//Goes after mouse buttons
            }
            break;
            case SDL_CONTROLLERAXISMOTION:
            input = Input::GetByGamepad(event.caxis.which);
            if(input != NULL) {
            input->axisEvent(int(event.caxis.axis) - SDL_CONTROLLER_AXIS_LEFTX + Input::Axis::GPLeftX, event.caxis.value);
            }
            //Input::GamepadAxisEvent(event.caxis);
            break;*/
            case SDL_WINDOWEVENT_RESIZED:
            case SDL_WINDOWEVENT_SIZE_CHANGED:
                //This is VERY broken, I've had to manually emulate it for now. Dammit, SDL.
                int szX,szY;
                SDL_GetWindowSize(mainWindow, &szX, &szY);
                size.x = szX;
                size.y = szY;
                if(size.y == 0) size.y = 1;
                wasResized = true;
                break;
            case SDL_QUIT:
                Application::Quit();
            break;
            }
        }

        if(wasResized) {
            renderer->resize(this->size);
        }

        /*if(shouldFocus && !focused) grabFocus();

        if(focused) {
            //Allow mouse grab
            SDL_WarpMouseInWindow(mainWindow, width / 2, height / 2);
        }*/
    }

    void Window::setActive() {
        ActiveRenderer = this->renderer;
    }

    void Window::update() { _prsguard();
        if(!renderer->isActive()) {
            Console::Error("Window.update() called on inactive object!");
        }
        /*int err = glGetError();
            if(err != GL_NO_ERROR) {
            std::cout << "Uncaught OpenGL Error, prior to flip: 0x" << std::hex << err << std::dec <<"\n";
            }*/
        float targetFPS = 60.0f;
        //if(_deltaTime < ( 1000.f / targetFPS )) {
            //std::cout << "DT < " << _deltaTime << "\n";
        //    SDL_Delay( (( 1000.f / targetFPS ) - _deltaTime) );
        //  }
        /*{ _prsguard();
        SDL_GL_SwapWindow(mainWindow);
        }*/
        _globalFrame++;

        { _prsguard();
        renderer->newFrame();
        /*err = glGetError();
            if(err != GL_NO_ERROR) {
            std::cout << "Uncaught OpenGL Error, after newFrame(): 0x" << std::hex << err << std::dec <<"\n";
            }*/
        }


        this->processEvents();
    };
/*
    void Window::SetupErrors() {
        Application::OnExit(&Window::OnQuit);
        maskedErrorCount = 1;
        maskedErrors = new ErrorMask[maskedErrorCount];

        maskedErrors[0].text = "Unknown touch device";

        for(unsigned short i = 0; i < maskedErrorCount; i++) {
            maskedErrors[i].length = maskedErrors[i].text.length();
            maskedErrors[i].data = new char[maskedErrors[i].length];
            memcpy(maskedErrors[i].data, maskedErrors[i].text.c_str(), maskedErrors[i].length);
        }
        }

        void Window::printError(std::string func, int line) {
        const char *error = SDL_GetError();

        if (*error != '\0') {
            unsigned short errorLength = strlen(error);

            for(unsigned short i = 0; i < maskedErrorCount; i++) {
            if(errorLength == maskedErrors[i].length) {
            if(memcmp(error, maskedErrors[i].data, errorLength) == 0) return;
            }
            }

            std::stringstream s;
            if(line == -1) s << "SDL Error: ";
            else s << "SDL Error in " << func << " at line " << line <<": ";

            s << error;
            Console::Error(s.str());
            SDL_ClearError();
        }
    }
*/
    unsigned short Window::pointToPix(unsigned short pt) {
        return ceil((float)pt * (72.0f / (float)pixelsPerInch));
    }

    void Window::grabFocus() {
        focused = true;
        SDL_SetRelativeMouseMode((SDL_bool)true);
        //SDL_GetRelativeMouseState(&Input::mouseX, &Input::mouseY);
    }

    void Window::releaseFocus() {
        focused = false;
        SDL_SetRelativeMouseMode((SDL_bool)false);
        //Input::accelX = 0; Input::accelY = 0;
    }

};
