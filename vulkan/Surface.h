#ifndef ARKE_SURFACE_INC
#define ARKE_SURFACE_INC
#include "ARKEBase.h"

struct VkInstance_T;
struct VkSurfaceKHR_T;

namespace arke {
    class Renderer;

    class Surface {
    public:
        Surface();
        virtual ~Surface();

        inline VkSurfaceKHR_T* handle() const {
            return surface;
        }

        Renderer *renderer;
        Device *device;
    protected:
        VkSurfaceKHR_T *surface;
    };
};

#endif
