#ifndef ARKE_WINDOW_INC
#define ARKE_WINDOW_INC
#include "ARKEBase.h"
#include "Image.h"
#include "Surface.h"

class SDL_Window;
struct SDL_Surface;

namespace arke {

    class Renderer;

    class Window : public Surface {
    friend class Renderer;
    public:
        static void OnQuit();

        Window(std::string newTitle = "", Vec2 sz = Vec2());
        ~Window();

        bool open(Vec2 sz);
        void close();
        void processEvents();

        void grabFocus();
        void releaseFocus();

        void setIcon(const ImageRGBA *const img);

        void setActive();
        void update();

        unsigned short pointToPix(unsigned short pt);

        int framesPerSecond;
        int msPerFrame;

        Vec2 size;
        int pixelsPerInch;
        bool focused;

        //Renderer *renderer;
        static Window *Active;
    protected:
        std::string title;
        SDL_Window* mainWindow;

        uint8_t bpp;

        SDL_Surface* iconSurface;
        ImageRGBA *icon;
    };

};

#endif
