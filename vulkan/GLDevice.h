#ifndef ARKE_GLDEVICE_INC
#define ARKE_GLDEVICE_INC
#include "ARKEBase.h"
#include <vector>
#include <unordered_map>

struct VkInstance_T;
struct VkPhysicalDeviceProperties;
struct VkPhysicalDeviceFeatures;
struct VkPhysicalDevice_T;
struct VkDevice_T;

namespace arke {
    class Device {
    public:
        typedef unsigned short Idt_t;
        enum Type {
            Invalid, Physical, Logical
        };
        enum Features : Uint64 {
            Tessellation         = _bit(0),
            Geometry            = _bit(1),
            MultiDraw           = _bit(2),
            DepthClamp          = _bit(3),
            DepthBias           = _bit(4),
            DepthBounds         = _bit(5),
            Anisotropic         = _bit(6),
            OcclusionQuery      = _bit(7),
            PipelineStats       = _bit(8),
            VariableSampleRate  = _bit(9),
            InheritedQueries    = _bit(10),
            SparseBind          = _bit(11),
            SparseBuffer        = _bit(12),
            SparseImage2D       = _bit(13),
            SparseImage3D       = _bit(14),
            SparseAlias         = _bit(15),
            SparseSample2       = _bit(16),
            SparseSample4       = _bit(17),
            SparseSample8       = _bit(18),
            SparseSample16      = _bit(19),
            ClipDistance        = _bit(20),
            CullDistance        = _bit(21),
            ExtendedImageFormat = _bit(22),
            MultiViewport       = _bit(23),
            AlphaToOne          = _bit(24),
            TextureBC           = _bit(25),
            DualSourceBlend     = _bit(26),
            CubemapArray        = _bit(27),
            IndependentBlend    = _bit(28),
            TextureASTC         = _bit(29),
            TextureETC2         = _bit(30),
            ShaderHighp         = _bit(31),
            ShaderI64           = _bit(32),
            RobustBuffers       = _bit(33),
            End = _bit(63),
            None = 0x0
        };

        struct Properties {
            struct {
                Uint32 api, driver;
            } version;

            Uint32 vendor, id;
            std::string name;

            Uint64 cacheUUID;
        };

        struct Layer {
            std::string name, description;
            struct {
                Uint32 spec, impl;
            } version;
        };

        struct Extension {
            std::string name;
            Uint32 version;
        };

        Device();
        Device(VkPhysicalDevice_T *phy);
        ~Device();

        Type type;
        Features features;
        Properties properties;

        std::vector<Layer*> layers;
        std::vector<Extension*> extensions;

        //Pre-init functions
        void require(const Features ft);
        void request(const Features ft);//optional
        bool has(const Features ft) const;

        bool initialize(VkPhysicalDevice_T *host);

        std::string summary() const;
        std::string summarizeFeatures() const;
        std::string summarizeLayers() const;
        std::string summarizeExtensions() const;
        std::string str() const;

        static std::vector<Device*> Devices;
        static int EnumerateAll();

        static std::string WriteFeatures(const Features features);
        static Features ReadFeatures(const std::string& str);
        static void WriteFeatures(VkPhysicalDeviceFeatures *const features, Features dat);
        static Features ReadFeatures(VkPhysicalDeviceFeatures *const features);

        static void Create(const std::vector<Idt_t> memberIDs);
    //protected:
        struct {
            bool initialized = false;
        } raw;

        //union {
        VkPhysicalDevice_T *phyHandle;
        VkDevice_T *device;
        //};
        uint32_t graphicsQueueFamily;
        uint32_t presentQueueFamily;

        inline auto dev() const {
            return device;
        }
        inline VkPhysicalDevice_T *handle() const {
            return phyHandle;
        }

        static std::vector<std::string> FeatureNames;
        static std::unordered_map<std::string, Layer*> Layers;
        static std::unordered_map<std::string, Extension*> Extensions;
        static Uint32 NextLogicalID;
    };
};

#endif
