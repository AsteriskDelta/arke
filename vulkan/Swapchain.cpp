#include "Swapchain.h"
#include "Renderer.h"
#include <vulkan/vulkan.h>
#include <vulkan/vk_platform.h>
#include "Application.h"
#include "Console.h"
#include "GLDevice.h"
#define USE_SWAPCHAIN_EXTENSIONS

namespace arke  {
    VkSurfaceFormatKHR chooseSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
		// We can either choose any format
		if (availableFormats.size() == 1 && availableFormats[0].format == VK_FORMAT_UNDEFINED) {
			return { VK_FORMAT_R8G8B8A8_UNORM, VK_COLORSPACE_SRGB_NONLINEAR_KHR };
		}

		// Or go with the standard format - if available
		for (const auto& availableSurfaceFormat : availableFormats) {
			if (availableSurfaceFormat.format == VK_FORMAT_R8G8B8A8_UNORM) {
				return availableSurfaceFormat;
			}
		}

		// Or fall back to the first available one
		return availableFormats[0];
	}

	VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& surfaceCapabilities, iVec2 sz) {
		//if (surfaceCapabilities.currentExtent.width == -1) {
			VkExtent2D swapChainExtent = {};

			swapChainExtent.width = (int)min(max(sz.x, surfaceCapabilities.minImageExtent.width), surfaceCapabilities.maxImageExtent.width);
			swapChainExtent.height = (int)min(max(sz.y, surfaceCapabilities.minImageExtent.height), surfaceCapabilities.maxImageExtent.height);

			return swapChainExtent;
		//} else {
		//	return surfaceCapabilities.currentExtent;
		//}
	}

	VkPresentModeKHR choosePresentMode(const std::vector<VkPresentModeKHR> presentModes) {
		for (const auto& presentMode : presentModes) {
			if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
				return presentMode;
			}
		}

		// If mailbox is unavailable, fall back to FIFO (guaranteed to be available)
		return VK_PRESENT_MODE_FIFO_KHR;
	}

    Swapchain::Swapchain(Surface *newSurface) : size(0,0), rawFlags(0x0), length(0), layers(1), surface(newSurface), rawHandle(nullptr) {

    }
    Swapchain::~Swapchain() {
        this->free();
    }

    bool Swapchain::allocate() {
        const auto physicalDevice = surface->device->handle();
        const auto device = surface->device;//The logical device aliases the physical one automatically, so they may both refer to the same variable
        const auto windowSurface = surface->handle();
        // Find surface capabilities
		VkSurfaceCapabilitiesKHR surfaceCapabilities;
		if (vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, windowSurface, &surfaceCapabilities) != VK_SUCCESS) {
			std::cerr << "failed to acquire presentation surface capabilities" << std::endl;
			exit(1);
		}

		// Find supported surface formats
		uint32_t formatCount = 0;
		if (vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, windowSurface, &formatCount, nullptr) != VK_SUCCESS || formatCount == 0) {
			std::cerr << "failed to get number of supported surface formats" << std::endl;
			exit(1);
		}

		std::vector<VkSurfaceFormatKHR> surfaceFormats(formatCount);
		if (vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, windowSurface, &formatCount, surfaceFormats.data()) != VK_SUCCESS) {
			std::cerr << "failed to get supported surface formats" << std::endl;
			exit(1);
		}

		// Find supported present modes
		uint32_t presentModeCount = 0;
		if (vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, windowSurface, &presentModeCount, nullptr) != VK_SUCCESS || presentModeCount == 0) {
			std::cerr << "failed to get number of supported presentation modes" << std::endl;
			exit(1);
		}

		std::vector<VkPresentModeKHR> presentModes(presentModeCount);
		if (vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, windowSurface, &presentModeCount, presentModes.data()) != VK_SUCCESS) {
			std::cerr << "failed to get supported presentation modes" << std::endl;
			exit(1);
		}

		// Determine number of images for swap chain
		uint32_t imageCount = surfaceCapabilities.minImageCount + 1;
		if (surfaceCapabilities.maxImageCount != 0 && imageCount > surfaceCapabilities.maxImageCount) {
			imageCount = surfaceCapabilities.maxImageCount;
		}

		std::cout << "using " << imageCount << " images for swap chain" << std::endl;
        this->length = imageCount;

		// Select a surface format
		VkSurfaceFormatKHR surfaceFormat = chooseSurfaceFormat(surfaceFormats);

		// Select swap chain size
		VkExtent2D swapChainExtent = chooseSwapExtent(surfaceCapabilities, size);

		// Check if swap chain supports being the destination of an image transfer
		// Note: AMD driver bug, though it would be nice to implement a workaround that doesn't use transfering
		if (!(surfaceCapabilities.supportedUsageFlags & VK_IMAGE_USAGE_TRANSFER_DST_BIT)) {
			std::cerr << "swap chain image does not support VK_IMAGE_TRANSFER_DST usage" << std::endl;
			//exit(1);
		}

		// Determine transformation to use (preferring no transform)
		VkSurfaceTransformFlagBitsKHR surfaceTransform;
		if (surfaceCapabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
			surfaceTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
		} else {
			surfaceTransform = surfaceCapabilities.currentTransform;
		}

		// Choose presentation mode (preferring MAILBOX ~= triple buffering)
		VkPresentModeKHR presentMode = choosePresentMode(presentModes);

		// Finally, create the swap chain
		VkSwapchainCreateInfoKHR createInfo;
		createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
		createInfo.surface = windowSurface;
		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = swapChainExtent;
		createInfo.imageArrayLayers = layers;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT;
		createInfo.imageSharingMode = flags.concurrent? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
		createInfo.preTransform = surfaceTransform;
		createInfo.compositeAlpha = flags.preserveAlpha? VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR: VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;
		createInfo.oldSwapchain = this->handle();

		if (vkCreateSwapchainKHR(device->dev(), &createInfo, nullptr, &rawHandle) != VK_SUCCESS) {
			std::cerr << "failed to create swap chain" << std::endl;
			exit(1);
		} else {
			std::cout << "created swap chain" << std::endl;
		}

		// Store the images used by the swap chain
		// Note: these are the images that swap chain image indices refer to
		// Note: actual number of images may differ from requested number, since it's a lower bound
		uint32_t actualImageCount = 0;
		if (vkGetSwapchainImagesKHR(device->dev(), this->handle(), &actualImageCount, nullptr) != VK_SUCCESS || actualImageCount == 0) {
			std::cerr << "failed to acquire number of swap chain images" << std::endl;
			exit(1);
		}

		std::vector<VkImage_T*> swapChainImages(actualImageCount);
        buffers.resize(actualImageCount);
        swapChainImages.resize(actualImageCount);

		if (vkGetSwapchainImagesKHR(device->dev(), this->handle(), &actualImageCount, swapChainImages.data()) != VK_SUCCESS) {
			std::cerr << "failed to acquire swap chain images" << std::endl;
			exit(1);

        }

        VkResult err;

        VkFormat actualFormat(VK_FORMAT_R8G8B8A8_UNORM);
        for (uint32_t i = 0; i < actualImageCount; i++) {
            buffers[i].image = swapChainImages[i];

            VkImageViewCreateInfo cav;
            cav.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            cav.format = actualFormat;
            cav.components = {
                    .r = VK_COMPONENT_SWIZZLE_R,
                    .g = VK_COMPONENT_SWIZZLE_G,
                    .b = VK_COMPONENT_SWIZZLE_B,
                    .a = VK_COMPONENT_SWIZZLE_A,
                };
            cav.subresourceRange = {
                    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                    .baseMipLevel = 0,
                    .levelCount = 1,
                    .baseArrayLayer = 0,
                    .layerCount = 1
                };
            cav.viewType = VK_IMAGE_VIEW_TYPE_2D;
            cav.image = buffers[i].image;

            err = vkCreateImageView(device->dev(), &cav, NULL, &buffers[i].view);
            assert(!err);
        }

        //Framebuffer creation
        VkAttachmentDescription view_attachments[1];
        view_attachments[0].format = actualFormat;
        view_attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
        view_attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
        view_attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        view_attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        view_attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        view_attachments[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        view_attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkAttachmentReference color_reference;
        color_reference.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        VkSubpassDescription subpass;
        subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &color_reference;

        VkRenderPassCreateInfo rp_info;
        rp_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        rp_info.attachmentCount = 1;
        rp_info.pAttachments = view_attachments;
        rp_info.subpassCount = 1;
        rp_info.pSubpasses = &subpass;

        VkRenderPass render_pass;
        err = vkCreateRenderPass(device->dev(), &rp_info, NULL, &render_pass);
        assert(!err);

        for (uint32_t i = 0; i < buffers.size(); i++) {
            VkImageView attachments[1] = {
                [0] = buffers[i].view,
            };

            VkFramebufferCreateInfo fb_info;
            fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
            fb_info.renderPass = render_pass;
            fb_info.attachmentCount = 1;
            fb_info.pAttachments = attachments;
            fb_info.width = swapChainExtent.width;
            fb_info.height = swapChainExtent.height;
            fb_info.layers = 1;

            err = vkCreateFramebuffer(device->dev(), &fb_info, NULL, &buffers[i].fb);
            assert(!err);
        }

		//std::cout << "acquired swap chain images" << std::endl;
        return true;
    }
    bool Swapchain::free() {
        if(this->handle() != nullptr) vkDestroySwapchainKHR(this->surface->device->dev(), this->handle(), nullptr);
        return true;
    }

    bool Swapchain::resize(iVec2 sz) {
        size = sz;
        return this->allocate();
    }
};
