#ifndef ARKE_SWAPCHAIN_INC
#define ARKE_SWAPCHAIN_INC
#include "ARKEBase.h"
#include "RenderQueue.h"

struct VkSwapchainKHR_T;
struct VkImage_T;
struct VkImageView_T;
struct VkFramebuffer_T;

namespace arke {
    class Swapchain {
    public:
        Swapchain(Surface *newSurface);
        ~Swapchain();

        bool allocate();
        bool free();

        iVec2 size;

        bool resize(iVec2 newSize);

        enum Rotation {
            Rot0 = 0,
            Rot90 = 1,
            Rot180 = 2,
            Rot270 = 3,
            Rot360 = 0
        };

        union {
            struct {
                bool concurrent : 1;
                bool flipped : 1;
                bool preserveAlpha : 1;
                bool immediate : 1;
                bool clipping : 1;
                unsigned char rotation : 2;
            } flags;
            Uint8 rawFlags;
        };
        Uint8 length;
        Uint16 layers;

        std::vector<RenderQueue*> queues;
        std::vector<Texture*> textures;

        inline auto handle() const {
            return rawHandle;
        }
    protected:
        Surface *surface;
        VkSwapchainKHR_T *rawHandle;

        struct Buffer {
                VkImage_T *image;
                VkImageView_T *view;
                VkFramebuffer_T *fb;
        };
        std::vector<Buffer> buffers;
    };
};

#endif
