#include "GLDevice.h"
#include <vulkan/vulkan.h>
#include <sstream>
#include <algorithm>
#include "Renderer.h"
#include "Console.h"
#include "Window.h"

namespace arke {
    std::vector<std::string> Device::FeatureNames = {
        "Tessellation",
        "Geometry",
        "MultiDraw",
        "DepthClamp",
        "DepthBias",
        "DepthBounds",
        "Anisotropic",
        "OcclusionQuery",
        "PipelineStats",
        "VariableSampleRate",
        "InheritedQueries",
        "SparseBind",
        "SparseBuffer",
        "SparseImage2D",
        "SparseImage3D",
        "SparseAlias",
        "SparseSample2",
        "SparseSample4",
        "SparseSample8",
        "SparseSample16",
        "ClipDistance",
        "CullDistance",
        "ExtendedImageFormat",
        "MultiViewport",
        "AlphaToOne",
        "TextureBC",
        "DualSourceBlend",
        "CubemapArray",
        "IndependentBlend",
        "TextureASTC",
        "TextureETC2",
        "ShaderHighp",
        "ShaderI64",
        "RobustBuffers"
    };

    std::vector<Device*> Device::Devices;
    std::unordered_map<std::string, Device::Layer*> Device::Layers;
    std::unordered_map<std::string, Device::Extension*> Device::Extensions;
    Uint32 Device::NextLogicalID = 0;

    Device::Device() : type(Type::Invalid), features(Features::None), properties(), raw(), phyHandle(nullptr), device(nullptr) {

    }

    Device::Device(VkPhysicalDevice_T *phy) : type(Type::Physical), features(Features::None), properties(), raw(), phyHandle(phy), device(nullptr) {
        VkPhysicalDeviceProperties props;
        vkGetPhysicalDeviceProperties(phyHandle, &props);

        properties.vendor = props.vendorID;
        properties.id = props.deviceID;
        properties.name = std::string(props.deviceName);
        properties.version = {props.apiVersion, props.driverVersion};
        properties.cacheUUID = *reinterpret_cast<Uint64*>(props.pipelineCacheUUID);

        VkPhysicalDeviceFeatures vkFeatures;
        vkGetPhysicalDeviceFeatures(phyHandle, &vkFeatures);
        this->features = ReadFeatures(&vkFeatures);
    }

    Device::~Device() {
        if(this->type == Type::Logical) {
            vkDeviceWaitIdle(this->device);
            vkDestroyDevice(this->device, nullptr);
        }
    }

    void Device::require(Features ft) {
        features = Features(features | ft);
    }
    bool Device::has(const Features ft) const {
        return (ft & this->features) == ft;
    }

    bool Device::initialize(VkPhysicalDevice_T *host) {
        phyHandle = host;
        if(this->type != Type::Invalid) {
            Console::Err("Device::initialize() called on object that already was initialized!\n");
            return false;
        }

        uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(host, &queueFamilyCount, nullptr);

		if (queueFamilyCount == 0) {
			std::cout << "physical device has no queue families!" << std::endl;
			exit(1);
		}

		// Find queue family with graphics support
		// Note: is a transfer queue necessary to copy vertices to the gpu or can a graphics queue handle that?
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(host, &queueFamilyCount, queueFamilies.data());

		std::cout << "physical device has " << queueFamilyCount << " queue families" << std::endl;

		bool foundGraphicsQueueFamily = false;
		bool foundPresentQueueFamily = false;

		for (uint32_t i = 0; i < queueFamilyCount; i++) {
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(host, i, Window::Active->handle(), &presentSupport);

			if (queueFamilies[i].queueCount > 0 && queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				graphicsQueueFamily = i;
				foundGraphicsQueueFamily = true;

				if (presentSupport) {
					presentQueueFamily = i;
					foundPresentQueueFamily = true;
					break;
				}
			}

			if (!foundPresentQueueFamily && presentSupport) {
				presentQueueFamily = i;
				foundPresentQueueFamily = true;
			}
		}

		if (foundGraphicsQueueFamily) {
			std::cout << "queue family #" << graphicsQueueFamily << " supports graphics" << std::endl;

			if (foundPresentQueueFamily) {
				std::cout << "queue family #" << presentQueueFamily << " supports presentation" << std::endl;
			} else {
				std::cerr << "could not find a valid queue family with present support" << std::endl;
				exit(1);
			}
		} else {
			std::cerr << "could not find a valid queue family with graphics support" << std::endl;
			exit(1);
		}

        const VkDeviceQueueCreateInfo qInfo = {
            VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            nullptr,//pNext
            0,//flags
            0,//qFamiltIndex
            1,//qCount
            nullptr//priorities
        };

        VkPhysicalDeviceFeatures phyFeatures;
        WriteFeatures(&phyFeatures, this->features);

        std::vector<const char*> layerIdens, extIdens;
        for(Layer *layer : layers) layerIdens.push_back(layer->name.c_str());
        for(Extension *ext : extensions) extIdens.push_back(ext->name.c_str());

        const char** layerIdensPtr = layerIdens.empty()? nullptr : &layerIdens[0];
        const char** extIdensPtr  = extIdens.empty()? nullptr : &extIdens[0];

        const VkDeviceCreateInfo devInfo = {
            VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            nullptr,//pNext
            0,//flags
            1,///qCreateInfoCount
            &qInfo,
            layerIdens.size(), layerIdensPtr,
            extIdens.size(), extIdensPtr,
            &phyFeatures
        };

        Console::Out("Creating vk Logical device...\n");
        auto result = vkCreateDevice(host, &devInfo, nullptr, &(this->device));

        if(result == VK_SUCCESS) {
            Devices.push_back(this);

            this->type = Type::Logical;
            this->raw.initialized = true;
            return true;
        } else {
            Console::Err("Failed to create vkLogicalDevice, err#", result,"\n");
            if(result == -7) {
                Console::Err("\tRequested extensions were", this->summarizeExtensions(),"\n");

            };
            return false;
        }
    }

    std::string Device::summary() const {
        std::stringstream ss;
        ss << "Device(" << properties.name << " " << properties.vendor << ":" << properties.id << " version Vk" << properties.version.api << " Drv" << properties.version.driver << ")";
        return ss.str();
    }
    std::string Device::str() const {
        std::stringstream ss;
        ss << "Device(" << properties.name << " " << properties.vendor << ":" << properties.id << ")";
        return ss.str();
    }

    std::string Device::summarizeFeatures() const {
        std::stringstream ss;
        ss << "Features[" << WriteFeatures(this->features) << "]";
        return ss.str();
    }
    std::string Device::summarizeLayers() const {
        std::stringstream ss;
        ss << "Layers[";
        for(Layer *layer : layers) ss << layer->name << " ";
        ss << "]";
        return ss.str();
    }
    std::string Device::summarizeExtensions() const {
        std::stringstream ss;
        ss << "Extensions[";
        for(Extension *ext : extensions) ss << ext->name << " ";
        ss << "]";
        return ss.str();
    }

    int Device::EnumerateAll() {
        Console::Out("Enumerating physical devices... ");
        std::vector<VkPhysicalDevice_T*> physicalDevices;
        unsigned int devCount;

        vkEnumeratePhysicalDevices(Renderer::Instance, &devCount, nullptr);

        physicalDevices.resize(devCount);

        vkEnumeratePhysicalDevices(Renderer::Instance, &devCount, &physicalDevices[0]);

        Console::Out(devCount," physical device(s)\n");

        Devices.resize(devCount);
        for(unsigned int i = 0; i < devCount; i++) {
            Device *dev = Devices[i] = new Device(physicalDevices[i]);

            //Query layers
            std::vector<VkLayerProperties> qLayers;
            Uint32 qLayerCount;
            vkEnumerateInstanceLayerProperties(&qLayerCount, nullptr);
            qLayers.resize(qLayerCount);
            vkEnumerateInstanceLayerProperties(&qLayerCount, &qLayers[0]);

            for(const VkLayerProperties lay : qLayers) {
                auto it = Layers.find(std::string(lay.layerName));
                if(it == Layers.end()) {
                    Layer *ld = new Layer();
                    Layers[std::string(lay.layerName)] = ld;

                    ld->name = std::string(lay.layerName);
                    ld->description = std::string(lay.description);
                    ld->version.spec = lay.specVersion;
                    ld->version.impl = lay.implementationVersion;

                    it = Layers.find(std::string(lay.layerName));
                }

                dev->layers.push_back(it->second);
            }

            //Query extensions
            std::vector<VkExtensionProperties> qExtensions;
            Uint32 qExtensionCount;
            vkEnumerateInstanceExtensionProperties(nullptr, &qExtensionCount, nullptr);
            qExtensions.resize(qExtensionCount);
            vkEnumerateInstanceExtensionProperties(nullptr, &qExtensionCount, &qExtensions[0]);

            for(const VkExtensionProperties ext : qExtensions) {
                auto it = Extensions.find(std::string(ext.extensionName));
                if(it == Extensions.end()) {
                    Extension *ld = new Extension();
                    Extensions[std::string(ext.extensionName)] = ld;

                    ld->name = std::string(ext.extensionName);
                    ld->version = ext.specVersion;

                    it = Extensions.find(ld->name);
                }

                dev->extensions.push_back(it->second);
            }

            Console::Out(dev->summary(), "\n\t", dev->summarizeFeatures(), "\n\t", dev->summarizeLayers(), "\n\t", dev->summarizeExtensions(),"\n");
        }

        return devCount;
    }

    std::string Device::WriteFeatures(const Features features) {
        std::stringstream ss;
        bool first = true;

        for(Uint64 i = 1; i < FeatureNames.size(); i++) {
            if((features & _bit(i)) != 0x0) {
                if(!first) ss << ", ";
                ss << FeatureNames[i];

                if(first) first = false;
            }
        }

        return ss.str();
    }

    Device::Features Device::ReadFeatures(const std::string& str) {
        Uint64 ret;
        std::istringstream iss(str);

        std::string feat;
        const std::string *fbegin = &FeatureNames[0], *fend = &FeatureNames[FeatureNames.size()-1];
        while (std::getline(iss, feat, ',')) {
            trim(feat);
            const std::string *found = std::find(fbegin, fend, feat);

            if(found == fend) continue;

            ret |= _bit(Features(found - fbegin));
        }

        return Features(ret);
    }

    void Device::WriteFeatures(VkPhysicalDeviceFeatures *const features, Features dat) {
        features->robustBufferAccess = bool(dat & Features::RobustBuffers);
        features->imageCubeArray = bool(dat & Features::CubemapArray);
        features->tessellationShader = bool(dat & Features::Tessellation);
        features->geometryShader = bool(dat & Features::Geometry);
        features->depthClamp = bool(dat & Features::DepthClamp);
        features->depthBiasClamp = bool(dat & Features::DepthBias);
        features->depthBounds = bool(dat & Features::DepthBounds);
        features->multiDrawIndirect = bool(dat & Features::MultiDraw);
        features->samplerAnisotropy = bool(dat & Features::Anisotropic);
        features->textureCompressionETC2 = bool(dat & Features::TextureETC2);
        features->textureCompressionBC = bool(dat & Features::TextureBC);
        features->textureCompressionASTC_LDR = bool(dat & Features::TextureASTC);
        features->shaderClipDistance = bool(dat & Features::ClipDistance);
        features->shaderCullDistance = bool(dat & Features::CullDistance);
        features->pipelineStatisticsQuery = bool(dat & Features::PipelineStats);
        features->shaderStorageImageExtendedFormats = bool(dat & Features::ExtendedImageFormat);
        features->multiViewport = bool(dat & Features::MultiViewport);
        features->alphaToOne = bool(dat & Features::AlphaToOne);
        features->dualSrcBlend = bool(dat & Features::DualSourceBlend);
        features->independentBlend = bool(dat & Features::IndependentBlend);
        features->shaderFloat64 = bool(dat & Features::ShaderHighp);
        features->shaderInt64 = bool(dat & Features::ShaderI64);
        features->sparseBinding = bool(dat & Features::SparseBind);
        features->sparseResidencyImage2D = bool(dat & Features::SparseImage2D);
        features->sparseResidencyImage3D = bool(dat & Features::SparseImage3D);
        features->sparseResidencyBuffer = bool(dat & Features::SparseBuffer);
        features->occlusionQueryPrecise = bool(dat & Features::OcclusionQuery);
        features->variableMultisampleRate = bool(dat & Features::VariableSampleRate);
        features->inheritedQueries = bool(dat & Features::InheritedQueries);
        features->sparseResidencyAliased = bool(dat & Features::SparseAlias);
    }

    Device::Features Device::ReadFeatures(VkPhysicalDeviceFeatures *const features) {
        Uint64 ret = 0x0;

        ret |= features->robustBufferAccess? Features::RobustBuffers : Features::None;
        ret |= features->imageCubeArray? Features::CubemapArray : Features::None;
        ret |= features->tessellationShader? Features::Tessellation : Features::None;
        ret |= features->geometryShader? Features::Geometry : Features::None;
        ret |= features->depthClamp ? Features::DepthClamp : Features::None;
        ret |= features->depthBiasClamp? Features::DepthBias : Features::None;
        ret |= features->depthBounds? Features::DepthBounds : Features::None;
        ret |= features->multiDrawIndirect? Features::MultiDraw : Features::None;
        ret |= features->samplerAnisotropy? Features::Anisotropic : Features::None;
        ret |= features->textureCompressionETC2? Features::TextureETC2 : Features::None;
        ret |= features->textureCompressionASTC_LDR? Features::TextureASTC : Features::None;
        ret |= features->textureCompressionBC? Features::TextureBC : Features::None;
        ret |= features->shaderClipDistance? Features::ClipDistance : Features::None;
        ret |= features->shaderCullDistance? Features::CullDistance : Features::None;
        ret |= features->pipelineStatisticsQuery? Features::PipelineStats : Features::None;
        ret |= features->shaderStorageImageExtendedFormats? Features::ExtendedImageFormat : Features::None;
        ret |= features->multiViewport? Features::MultiViewport : Features::None;
        ret |= features->alphaToOne? Features::AlphaToOne : Features::None;
        ret |= features->textureCompressionBC? Features::TextureBC : Features::None;
        ret |= features->textureCompressionETC2? Features::TextureETC2 : Features::None;
        ret |= features->textureCompressionASTC_LDR? Features::TextureASTC : Features::None;
        ret |= features->dualSrcBlend? Features::DualSourceBlend : Features::None;
        ret |= features->independentBlend? Features::IndependentBlend : Features::None;
        ret |= features->shaderFloat64? Features::ShaderHighp : Features::None;
        ret |= features->shaderInt64? Features::ShaderI64 : Features::None;
        ret |= features->sparseBinding? Features::SparseBind : Features::None;
        ret |= features->sparseResidencyImage2D? Features::SparseImage2D : Features::None;
        ret |= features->sparseResidencyImage3D? Features::SparseImage3D : Features::None;
        ret |= features->sparseResidencyBuffer? Features::SparseBuffer : Features::None;
        ret |= features->occlusionQueryPrecise? Features::OcclusionQuery : Features::None;
        ret |= features->variableMultisampleRate? Features::VariableSampleRate : Features::None;
        ret |= features->inheritedQueries? Features::InheritedQueries : Features::None;
        ret |= features->sparseResidencyAliased? Features::SparseAlias : Features::None;

        return Features(ret);
    }

    void Device::Create(const std::vector<Idt_t> memberIDs) {

    }
};
