#ifndef ARKE_RENDERER_INC
#define ARKE_RENDERER_INC
#include "ARKEBase.h"
#include "Surface.h"
#define _drawcall() ::arke::ActiveRenderer->drawcalls++;

struct VkInstance_T;
struct VkSurfaceKHR_T;

namespace arke {

    class Window;
    class RenderPipeline;

    class Renderer {
        friend class Window;
    public:
        Renderer(Window *const w);
        ~Renderer();

        inline bool isActive() { return active; };

        inline RenderPipeline* pipeline() { return activePipeline; };

        void usePipeline(RenderPipeline *const p);

        inline auto getAspect() {
            return (targetSize.x / targetSize.y);
        };
        bool wasResized() {
            return (resizeFrame == GlobalFrameID);
        };
        void newFrame();

        unsigned int drawcalls;
        Vec2 targetSize;

        void resize(Vec2 sz);

        void setActive();
        Uint32 resizeFrame;

        static thread_local VkInstance_T *Instance;
    protected:
        bool active;
        Window* window;
        RenderPipeline *activePipeline;
        VkInstance_T *instance;
        //VkSurfaceKHR_T *surface;

        bool createInstance();
        bool destroyInstance();

        void enumerateDevices();

        //Mat4 modelMatrix, effectiveMatrix, modelViewMatrix;
        //std::vector<Mat4> modelMatrixStack;
    };


    extern thread_local Renderer* ActiveRenderer;
};

#endif
