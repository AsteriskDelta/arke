#include "ARKEBase.h"
#include "vulkan/Window.h"
#include "vulkan/Renderer.h"
#include "vulkan/GLDevice.h"
#include "Application.h"
#include <vulkan/vulkan.h>
#include "vulkan/Swapchain.h"

using namespace arke;

namespace arke {
    Uint64 GlobalFrameID = 0;
};

int main(int argc, char** argv) {
    _unused(argc, argv);

    Application::BeginInitialization();
    Application::SetName("Vulkan Test");
    Application::SetCodename("com.d3.arke.vkTest");
    Application::SetCompanyName("Delta III Technologies");
    Application::SetVersion("0.0r1-X");
    Application::SetDataDir("../../data");
    Application::EndInitialization();

    const unsigned short lclPort = 2939;
    Spin::MemberInfo myInfo;
    myInfo.type = Application::GetCodename();
    Spin::MemberID myID(5, lclPort);

    Spin::Initialize(myID, myInfo);

    Window *window = new Window("", Vec2(600,600));
    //window->open();
    window->setActive();

    Device *logical = new Device();
    logical->require(Device::Features(Device::Features::Geometry | Device::Features::MultiDraw | Device::Features::TextureBC | Device::Features::PipelineStats));
    logical->layers.push_back(Device::Layers["VK_LAYER_LUNARG_core_validation"]);
    Device::Extensions[VK_KHR_SWAPCHAIN_EXTENSION_NAME] = new Device::Extension{VK_KHR_SWAPCHAIN_EXTENSION_NAME};
    logical->extensions.push_back(Device::Extensions[VK_KHR_SWAPCHAIN_EXTENSION_NAME]);

    logical->initialize(Device::Devices[0]->handle());
    window->device = logical;

    Swapchain *swapchain = new Swapchain(window);
    swapchain->size = iVec2(512,512);
    swapchain->allocate();

    while(true) {
        window->processEvents();

        window->update();
        usleep(10000);
        GlobalFrameID++;
    }

    window->close();

    return 0;
}
