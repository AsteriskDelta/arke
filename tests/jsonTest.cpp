#include "ARKE.h"
#include "lib/io/JSON.h"


using namespace arke;
int main(int argc, char** argv) {
    JSON *json = new JSON();
    json->blank();

    json->set<std::string>("test", "index_of_test");
    json->set<double>(52.8, "a_number");

    std::vector<double> arr;
    arr.push_back(6.2);
    arr.push_back(3.9);
    arr.push_back(7.3);
    arr.push_back(1.5);

    json->array<double>(arr, "arr");

    std::vector<std::string> strs;
    strs.push_back("first");
    strs.push_back("second");
    strs.push_back("third");

    json->array<std::string>(strs, "strs");

    json->set<double>(3.6, "testing.a.deep.struct.value");
    
    json->save("test.json");
    delete json;

    return 0;
}
