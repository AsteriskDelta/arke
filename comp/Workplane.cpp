#include "Workplane.h"
#include <iostream>
#include "V4.h"
#include "Quat.h"

Workplane::Workplane() : center(0.f), normal(0.f), normalRotation(0.f), size(1.f) {
  
}
Workplane::Workplane(const V3& c, const V3& n, const float nr, const V2& s) : center(c), normal(n), normalRotation(nr), size(s) {
  
}

Workplane::Workplane(const Workplane& orig) : center(orig.center), normal(orig.normal), normalRotation(orig.normalRotation), size(orig.size) {
  
}

Workplane::~Workplane() {
}

V3 Workplane::resolve(V3 p) const {
  //std::cout << "resolve() c=" << glm::to_string(center) << ", n=" << glm::to_string(normal) << " of " << glm::to_string(p) << " = " << glm::to_string(V3(this->toMatrix() * V4(p, 1.f))) << "\n";
  return V3(this->toMatrix() * V4(p, 1.f));
}

V3 Workplane::llResolve(V3 p, float w) const {
  return w*center + right()*p.x + up()*p.y + forward()*p.z;
}

V3 Workplane::project(V3 p) const {
  return V3(this->fromMatrix() * V4(p, 1.f));
}

Mat4 Workplane::toMatrix() const { _prsguard();
  Mat4 trans =  glm::translate(center);//Mat4();
  //if(normal != V3(0.f,1.f,0.f) && normal != V3(0.f,-1.f,0.f)) trans *= glm::lookAt(normal, V3(0.f), V3(0.f, 1.f, 0.f));
  //if(normal != V3_FORWARD && normal != -V3_FORWARD) trans *= glm::lookAt(normal, V3(0.f), V3_FORWARD);
  trans *= glm::mat4_cast(this->localQuat());
  //trans *= glm::translate(center);
  
  return trans;
}

Mat4 Workplane::fromMatrix() const { _prsguard();
  Mat4 ret = this->toMatrix();
  ret = glm::inverse(ret);
  return ret;
}

V3 Workplane::up() const {
  return V3_UP * this->localQuat();// * scale.y;
}
V3 Workplane::right() const {
  return V3_RIGHT * this->localQuat();// * scale.x;
}
V3 Workplane::forward() const {
  return V3_FORWARD * this->localQuat();
}
V3 Workplane::mkNormal(const V3& v) const {
  return v * this->localQuat();
}

V3 Workplane::intersectLine(V3 s, V3 e, float *interTime) {
  V3 lineDelta = e - s;
  float linePlaneDot = glm::dot(normal, lineDelta);
  if(linePlaneDot == 0.f) {
    if(glm::dot(normal, s - center) != 0.f) return V3_NULL;
    
    if(interTime != nullptr) *interTime = 0.f;
    return s;
  }
  
  const float t = glm::dot(normal, center - s) / glm::dot(normal, lineDelta);
  if(t < 0.f || t > 1.f) return V3_NULL;
  
  if(interTime != nullptr) *interTime = t;
  return s + lineDelta*t;
}

V3 Workplane::getOffsetCenter() const {
  return V3(this->size/2.f, 0.f);
}

Quat Workplane::localQuat() const {
  return quatFromToRotation(V3_FORWARD, normal) * glm::angleAxis(normalRotation, normal);
}