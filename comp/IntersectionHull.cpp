#include "IntersectionHull.h"
#include <sstream>

IntersectionHull::IntersectionHull() : dim(true),dirty(true),valid(false),type(Type::Flat),root(V3()),del(V3()){
  
}

IntersectionHull::IntersectionHull(const IntersectionHull& orig) {
  root = orig.root;
  del = orig.del;
  type = orig.type;
  dim = orig.dim;
  dirty = orig.dirty;
  valid = orig.valid;
  cachedVolume = orig.cachedVolume;
}

IntersectionHull::~IntersectionHull() {

}

void IntersectionHull::setType3D(const Type newType) {
  IntersectionHull temp = *this;
  type = newType;
  this->root = this->del = V3(0.f);
  this->encompass3D(temp);
}
void IntersectionHull::setType2D(const Type newType) {
  IntersectionHull temp = *this;
  type = newType;
  this->root = this->del = V3(0.f);
  this->encompass2D(temp);
}

bool IntersectionHull::contains(const IntersectionHull& o) const { _prsguard();
  auto testPts = o.getExtremes3D();
  for(V3 pt : testPts) {
    if(!contains(pt)) return false;
  }
  return true;
}
bool IntersectionHull::contains(V3 pt) const { _prsguard();
  float d;
  switch(type) {
    case Type::Curved:
      d = glm::distance(root, pt);
      return bool(d < del.x); break;
    case Type::Elip:
      return ((pt - root)/del).length() <= 1; break;
    default:
    case Type::Flat:
      pt -= root;
      return (0 <= pt.x && pt.x <= del.x) &&
             (0 <= pt.y && pt.y <= del.y) &&
             (0 <= pt.z && pt.z <= del.z); break;
  }
}
bool IntersectionHull::contains(V2 pt) const { _prsguard();
  float d;
  switch(type) {
    case Type::Curved:
      d = glm::distance(V2(root), pt);
      return bool(d < del.x); break;
    case Type::Elip:
      pt -= V2(root);
      return V2(pt.x, pt.y*(del.x/del.y)).length() <= del.x; break;
    default:
    case Type::Flat:
      pt -= V2(root);
      return (0 <= pt.x && pt.x <= del.x) &&
             (0 <= pt.y && pt.y <= del.y); break;
  }
}

bool IntersectionHull::intersect3D(const IntersectionHull& o) const {
  auto testPts = o.getExtremes3D();
  bool f = false, p = false;
  for(V3 pt : testPts) {
    if(contains(pt)) p = true;
    else f = true;
  }
  return f && p;
}
bool IntersectionHull::intersect2D(const IntersectionHull& o) const {
  auto testPts = o.getExtremes2D();
  bool f = false, p = false;
  for(V2 pt : testPts) {
    if(contains(pt)) p = true;
    else f = true;
  }
  return f && p;
}

bool IntersectionHull::containsExtreme(const IntersectionHull& o) const {
  auto testPts = o.getExtremes3D();
  for(V3 pt : testPts) {
    if(contains(pt)) return true;
  }
  return false;
}

bool IntersectionHull::intersectsOrContains3D(const IntersectionHull& o) const {
  return this->containsExtreme(o) || o.containsExtreme(*this);
}

void IntersectionHull::encompass3D(const IntersectionHull& o, Mat4* mat) { _prsguard();
  auto testPts = o.getExtremes3D(mat);
  for(V3 pt : testPts) {
    //std::cout << "ENCOMPASS3D: " << glm::to_string(pt) << "\n";
    this->encompass(pt);
  }
}
void IntersectionHull::encompass2D(const IntersectionHull& o) { _prsguard();
  auto testPts = o.getExtremes2D();
  for(V2 pt : testPts) this->encompass(pt);
}

void IntersectionHull::encompass(V3 pt) { _prsguard();
if(!valid) {
    valid = true;
    root = V3(pt);
    del = V3(0.f);
  } else {
    V3 oldRoot;
    switch(type) {
      case Type::Curved:
        if(!contains(pt)) del.x = glm::distance(pt, root);
        break;
      case Type::Elip:
        if(contains(pt)) return;
        pt -= root;
        break;
      default:
      case Type::Flat:
        if(pt.x < root.x || pt.y < root.y || pt.z < root.z) {//Below
          oldRoot = root;
          root = V3(std::min(pt.x, root.x), std::min(pt.y, root.y),std::min(pt.z, root.z));
          del += (oldRoot - root);

          //std::cout << "offsetting del by " << glm::to_string(oldRoot-root) << "\n";
        }
        {//Always evaluate this as a prospective second case!!!
          pt -= root;
          if(pt.x > del.x || pt.y > del.y || pt.z > del.z) {//Above
            del = V3(std::max(pt.x, del.x), std::max(pt.y, del.y), std::max(pt.z, del.z));
          }
        }
        break;
    }
  }
  dirty = true;
}
void IntersectionHull::encompass(V2 pt) { _prsguard();
  if(!valid) {
    valid = true;
    root = V3(pt, 0.f);
    del = V3(0.f);
  } else {
    V3 oldRoot;
    switch(type) {
      case Type::Curved:
        if(!contains(pt)) del.x = glm::distance(pt, V2(root));
        break;
      case Type::Elip:

        break;
      default:
      case Type::Flat:
        if(pt.x < root.x || pt.y < root.y) {//Below
          oldRoot = root;
          root = V3(std::min(pt.x, root.x), std::min(pt.y, root.y),root.z);
          del += (oldRoot - root);
          //std::cout << "offsetting del by " << glm::to_string(oldRoot-root) << "\n";
        }
        {//Always evaluate this as a prospective second case!!!
          pt -= V2(root);
          if(pt.x > del.x || pt.y > del.y) {//Above
            del = V3(std::max(pt.x, del.x), std::max(pt.y, del.y), del.z);
          }
        }
        break;
    }
  }
  dirty = true;
}

void IntersectionHull::encompassWorkplane(const IntersectionHull& o, const V3& center, const V3& dir) {
  this->encompassWorkplane(o, Workplane(center, dir));
}

void IntersectionHull::encompassWorkplane(const IntersectionHull& o, const Workplane& wp) { _prsguard();
  Mat4 trans = wp.toMatrix(); Mat4 basis =  glm::mat4_cast(quatFromToRotation(V3_FORWARD, wp.normal));
  auto pts = o.getExtremes3D(&basis);
  //std::cout << "translating to " << glm::to_string(wp.center) << " along " << glm::to_string(wp.normal) << "\n";
  for(V3 pt : pts) {
    //std::cout << "encompass " << glm::to_string(V3(pt.x, pt.y, pt.z)) << " -> ";
    V4 proc = trans * V4(pt.x, pt.y, pt.z, 1.f);
    //std::cout << glm::to_string(V3(proc)) << "\n";
    this->encompass(V3(proc.x, proc.y, proc.z));
  }
}

float IntersectionHull::volume() {
  if(!dirty && dim) return cachedVolume;
  float ret;
  
  switch(type) {
    case Type::Curved:
      ret = (4.f/3.f)*M_PI*pow(del.x, 3.f); break;
    case Type::Elip:
      ret = (4.f/3.f)*M_PI * del.x*del.y*del.z; break;
    default:
    case Type::Flat:
      ret = del.x*del.y*del.z; break;
  }
  cachedVolume = ret; dirty = false; dim = true;
  
  return ret;
}

float IntersectionHull::area()  {
  if(!dirty && !dim) return cachedVolume;
  float ret;
  
  switch(type) {
    case Type::Curved:
      ret = M_PI*pow(del.x, 2.f); break;
    case Type::Elip:
      ret = M_PI*del.x*del.y; break;
    default:
    case Type::Flat:
      ret = del.x*del.y; break;
  }
  cachedVolume = ret; dirty = false; dim = false;
  
  return ret;
}

float IntersectionHull::margin() const {
  return glm::dot(del, V3(1.f));
}

IntersectionHull IntersectionHull::transform(const Mat4& mat) const {
  IntersectionHull ret = *this;
  ret.root = V3(mat * V4(root, 1.f));
  ret.del = Mat3(mat) * del;
  return ret;
}

std::vector<V3> IntersectionHull::getExtremes3D(Mat4* axes) const { _prsguard();
  std::vector<V3> ret; ret.reserve(8);
  Mat3 smat = (axes == NULL)?Mat3(1.f) : Mat3(*axes);
  switch(type) {
    case Type::Curved:
      /*ret.push_back(V3(root.x-del.x, root.y      , root.z      ));
      ret.push_back(V3(root.x+del.x, root.y      , root.z      ));
      
      ret.push_back(V3(root.x      , root.y-del.x, root.z      ));
      ret.push_back(V3(root.x      , root.y+del.x, root.z      ));
      
      ret.push_back(V3(root.x      , root.y      , root.z-del.x));
      ret.push_back(V3(root.x      , root.y      , root.z+del.x));*/
      ret.push_back(root + smat*V3(-del.x, 0.f, 0.f));
      ret.push_back(root + smat*V3( del.x, 0.f, 0.f));
      
      ret.push_back(root + smat*V3(0.f,  del.y, 0.f));
      ret.push_back(root + smat*V3(0.f, -del.y, 0.f));
      
      ret.push_back(root + smat*V3(0.f,  0.f,  del.z));
      ret.push_back(root + smat*V3(0.f,  0.f, -del.z));
      break;
    case Type::Elip:
      ret.push_back(V3(root.x-del.x, root.y      , root.z      ));
      ret.push_back(V3(root.x+del.x, root.y      , root.z      ));
      
      ret.push_back(V3(root.x      , root.y-del.y, root.z      ));
      ret.push_back(V3(root.x      , root.y+del.y, root.z      ));
      
      ret.push_back(V3(root.x      , root.y      , root.z-del.z));
      ret.push_back(V3(root.x      , root.y      , root.z+del.z));
      break;
    default:
    case Type::Flat:
      ret.push_back(V3(root.x       , root.y        , root.z        ));
      
      ret.push_back(V3(root.x+del.x , root.y        , root.z        ));
      ret.push_back(V3(root.x       , root.y+del.y  , root.z        ));
      ret.push_back(V3(root.x       , root.y        , root.z+del.z  ));
      
      ret.push_back(V3(root.x       , root.y+del.y  , root.z+del.z  ));
      ret.push_back(V3(root.x+del.x , root.y        , root.z+del.z  ));
      ret.push_back(V3(root.x+del.x , root.y+del.y  , root.z        ));
      
      ret.push_back(V3(root.x+del.x , root.y+del.y  , root.z+del.z  ));
      break;
  }
  return ret;
}
std::vector<V2> IntersectionHull::getExtremes2D() const { _prsguard();
  std::vector<V2> ret; ret.reserve(4);
  switch(type) {
    case Type::Curved:
      ret.push_back(V2(root.x-del.x, root.y      ));
      ret.push_back(V2(root.x+del.x, root.y      ));
      
      ret.push_back(V2(root.x      , root.y-del.x));
      ret.push_back(V2(root.x      , root.y+del.x));
      break;
    case Type::Elip:
      ret.push_back(V2(root.x-del.x, root.y      ));
      ret.push_back(V2(root.x+del.x, root.y      ));
      
      ret.push_back(V2(root.x      , root.y-del.y));
      ret.push_back(V2(root.x      , root.y+del.y));
      break;
    default:
    case Type::Flat:
      ret.push_back(V2(root.x       , root.y        ));
      
      ret.push_back(V2(root.x+del.x , root.y        ));
      ret.push_back(V2(root.x       , root.y+del.y  ));
      
      ret.push_back(V2(root.x+del.x , root.y+del.y  ));
  }
  return ret;
}
