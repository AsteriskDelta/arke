#ifndef INTERSECTIONHULL_H
#define INTERSECTIONHULL_H
#include "ARKEBase.h"
#include <vector>
#include "Workplane.h"
#include <sstream>

class IntersectionHull {
public:
    enum Type : Uint8 {
        Optimal, Flat, Curved, Elip, Poly, All, None
    };
    
    union {
    struct {
        bool dim    : 1;
        bool dirty  : 1;
        bool valid  : 1;
        Uint8 type  : 5;
    };
    Uint8 flags;
    };
    V3 root;
    V3 del;
    
    inline std::string toStr() const {
        std::string ret;
        std::stringstream ss;
        ss << "IntersectionHull(from:"<<glm::to_string(root) << ", " << glm::to_string(root+del) << ")";
        ret = ss.str();
        return ret;
    }
    
    IntersectionHull();
    IntersectionHull(const IntersectionHull& orig);
    virtual ~IntersectionHull();
    
    inline operator bool() {
        return del != V3();
    }
    
    inline void clear() {
        root = del = V3(0.f);
        valid = false;
    }
    
    void setType3D(const Type newType);
    void setType2D(const Type newType);
    
    bool contains(const IntersectionHull& o) const;
    bool contains(V3 pt) const;
    bool contains(V2 pt) const;
    
    bool intersect3D(const IntersectionHull& o) const;
    bool intersect2D(const IntersectionHull& o) const;
    
    bool containsExtreme(const IntersectionHull& o) const;
    bool intersectsOrContains3D(const IntersectionHull& o) const;
    
    void encompass3D(const IntersectionHull& o, Mat4* mat = NULL);
    void encompass2D(const IntersectionHull& o);
    void encompass(V3 pt);
    void encompass(V2 pt);
    
    //o is assumed to be 2D!!!
    void encompassWorkplane(const IntersectionHull& o, const V3& center, const V3& dir);
    void encompassWorkplane(const IntersectionHull& o, const Workplane& wp);
    
    float volume();
    float area();
    float margin() const;
    
    IntersectionHull transform(const Mat4& mat) const;
    
    std::vector<V3> getExtremes3D(Mat4* mat = NULL) const;
    std::vector<V2> getExtremes2D() const;
    
    template<typename TPMeshType, typename TPObjectVert>
    void constructMesh(float epsilon, void* mesh);
    
#ifdef CEREAL_INCLUDED
    template<class Archive>
    void serialize(Archive &ar) { 
      ar(CEREAL_NVP(flags),
         CEREAL_NVP(root),
         CEREAL_NVP(del)
      );
    }
#endif
private:
    float cachedVolume;
};

typedef IntersectionHull IHull;


template<typename TPMeshType, typename TPObjectVert>
inline void IntersectionHull::constructMesh(float epsilon, void* m) { _prsguard();
  TPMeshType *const mesh = reinterpret_cast<TPMeshType*>(m);
  std::vector<V3> extremes = this->getExtremes3D();
  TPObjectVert addVert;
  std::vector<unsigned int> vertIDs;
  //std::cout << "constructing bound hull of type: " << int(type) << "\n";
  //std::cout << "constructing mesh IntersectionHull(from:"<<glm::to_string(root) << ", " << glm::to_string(root+del) << ")\n";
  switch(type) {
    default:
    case Type::Flat:
      //std::cout << "started with " << mesh->vertCount << " verts\n";
      for(auto it = extremes.begin(); it != extremes.end(); ++it) {
        addVert.pos = *it;
        vertIDs.push_back(mesh->addVert(addVert));
      }
      mesh->addLineInd(vertIDs[0], vertIDs[1]);
      mesh->addLineInd(vertIDs[0], vertIDs[2]);//Vertical
      mesh->addLineInd(vertIDs[0], vertIDs[3]);
      
      mesh->addLineInd(vertIDs[2], vertIDs[4]);
      mesh->addLineInd(vertIDs[2], vertIDs[6]);
      
      mesh->addLineInd(vertIDs[3], vertIDs[4]);
      mesh->addLineInd(vertIDs[1], vertIDs[6]);
      
      mesh->addLineInd(vertIDs[5], vertIDs[1]);
      mesh->addLineInd(vertIDs[5], vertIDs[3]);
      
      mesh->addLineInd(vertIDs[7], vertIDs[4]);
      mesh->addLineInd(vertIDs[7], vertIDs[5]);//Vertical
      mesh->addLineInd(vertIDs[7], vertIDs[6]);
      
      //std::cout << "ended with " << mesh->vertCount << " verts\n";
      break;
  }
}

#endif /* INTERSECTIONHULL_H */

