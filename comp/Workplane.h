#ifndef WORKPLANE_H
#define WORKPLANE_H
#include "ARKEBase.h"

class Workplane {
public:
    Workplane();
    Workplane(const V3& c, const V3& n, const float rn = 0.f, const V2& s = V2(1.f));
    Workplane(const Workplane& orig);
    virtual ~Workplane();
    
    V3 center;
    V3 normal;
    float normalRotation;
    V2 size;
    
    V3 resolve(V3 p) const;//wp->world
    V3 llResolve(V3 p, float w = 1.f) const;//wp->world
    V3 project(V3 p) const;//Reverses resolve: world->wp
    
    V3 intersectLine(V3 s, V3 e, float *interTime = nullptr);
    
    V3 getOffsetCenter() const;
    
    V3 up() const;
    V3 right() const;
    V3 forward() const;
    V3 mkNormal(const V3& v) const;
    
    //matrix from plane->relative space, to plane<-relative
    Mat4 fromMatrix() const;
    Mat4 toMatrix() const;
    Quat localQuat() const;
private:

};

#endif /* WORKPLANE_H */

