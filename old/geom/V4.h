#ifndef V4_INC
#define V4_INC

//#ifdef OPT_INLINE
//#include <glm/vec4.inl>
//#else
#include <glm/vec4.hpp>
//#endif
#ifdef VEC_DOUBLE
typedef glm::dvec4 vec4;
typedef glm::dvec4 V4;
#else 
typedef glm::vec4 vec4;
typedef glm::vec4 V4;
#endif 

template <typename T>
using V4T = glm::tvec4<T>;

#ifdef CEREAL_INCLUDED
namespace glm {
template<class Archive>
void serialize(Archive& archive, V4 & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) , CEREAL_NVP(m.w));
}
};
//CEREAL_REGISTER_TYPE(V4);

/*template<class Archive>
void serialize(Archive& archive, glm::tvec4<float, (glm::precision)0u> & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) , CEREAL_NVP(m.w) );
}*/
#endif

#endif