#include "Rect.h"
#include <sstream>

template <typename T>
std::string Rect<T>::toString() {
  std::stringstream ss;
  ss << "Rect(" << s.x << ", " << s.y << " -> " << e.x << ", " << e.y << " : w" << getWidth() << " h" << getHeight() << ")";
  return ss.str();
}