#ifndef V3_INC
#define V3_INC

//#ifdef OPT_INLINE
//#include <glm/vec3.inl>
//#else
#include <glm/vec3.hpp>
//#endif

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>

#ifdef VEC_DOUBLE
typedef glm::dvec3 vec3;
typedef glm::dvec3 V3;
static_assert(sizeof(V3) == sizeof(double) * 3, "Platform doesn't support double vec3's natively.");
#else
typedef glm::vec3 vec3;
typedef glm::vec3 V3;
//static_assert(sizeof(V3) == sizeof(float) * 3, "Platform doesn't support vec3's natively.");
#endif
template <typename T> using V3T = glm::tvec3<T>;

#define V3_FORWARD V3(0.f, 0.f, 1.f)
#define V3_UP V3(0.f, 1.f, 0.f)
#define V3_RIGHT V3(1.f, 0.f, 0.f)

#define V3NULL (V3_NULL)
#define V3_NULL V3(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity())

#ifdef CEREAL_INCLUDED
namespace glm {
template<class Archive>
void serialize(Archive& archive, V3 & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) );
}
};
/*template<class Archive>
void serialize(Archive& archive, glm::tvec3<float, (glm::precision)0u> & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) );
}*/
//CEREAL_REGISTER_TYPE(V3);
#endif
/*
typedef V3T<float> V3;

template<typename T> class V3T {
public:
  T x, y, z;
protected:
  
}*/

#endif