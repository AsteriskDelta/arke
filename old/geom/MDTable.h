/* 
 * File:   MDTable.h
 * Author: Galen Swain
 *
 * Created on June 26, 2015, 1:40 PM
 */

#ifndef MDTABLE_H
#define	MDTABLE_H
#include <list>
#include <cfloat>
#include "shared/Shared.h"

template<int D, typename K, typename V>
class MDTable {
    struct Store {
        K key; V value;
    };
public:
    inline MDTable();
    inline ~MDTable();
    
    inline void add(const K& pt, const V& value);
    
    inline void remove(const V& item);
    inline void remove(V* ptr);
    
    inline V* getNearest(const K& pt);
    inline std::list<V*> getNearest(const K& pt, int c);
private:
    std::list<Store> points;
};

template<int D, typename K, typename V>
inline MDTable<D, K, V>::MDTable() {
    
}
template<int D, typename K, typename V>
inline MDTable<D, K, V>::~MDTable() {
    
}

template<int D, typename K, typename V>
inline void MDTable<D, K, V>::add(const K& pt, const V& item) {
    points.push_back(Store{pt, item});
}

template<int D, typename K, typename V>
inline void MDTable<D, K, V>::remove(const V& item) {
    for(auto it = points.begin(); it != points.end(); ++it) {
        if(it->value == item) {
            points.erase(it);
            break;
        }
    }
    return;
}

template<int D, typename K, typename V>
inline void MDTable<D, K, V>::remove(V* ptr) {
    for(auto it = points.begin(); it != points.end(); ++it) {
        if(&(it->value) == ptr) {
            points.erase(it);
            break;
        }
    }
    return;
}

template<int D, typename K, typename V>
inline V* MDTable<D, K, V>::getNearest(const K& pt) {
    auto best = points.end(); float bestDiff = FLT_MAX;
    for(auto it = points.begin(); it != points.end(); ++it) {
        float diff = 0.f;
        for(Uint8 di = 0; di < D; di++) {
            diff += pow(pt.D(di) - it->key.D(di), 2);
        }
        diff = pow(diff, 1.f/float(D));
        
        if(diff < bestDiff) {
            best = it;
            bestDiff = diff;
        }
    }
    
    if(best == points.end()) return NULL;
    return &(best->value);
}

template<int D, typename K, typename V>
inline std::list<V*> MDTable<D, K, V>::getNearest(const K& pt, int c) {
    
}

#endif	/* MDTABLE_H */

