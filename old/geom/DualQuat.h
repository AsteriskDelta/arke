#ifndef DUALQUAT_H
#define DUALQUAT_H
#include "V3.h"
#ifdef OPT_INLINE
#include <glm/gtx/dual_quaternion.hpp>
#else
#include <glm/gtx/dual_quaternion.hpp>
#endif

#ifdef VEC_DOUBLE
typedef glm::tdualquat<double, glm::highp> dquat;
typedef glm::tdualquat<double, glm::highp> DualQuat;
#else
typedef glm::tdualquat<float, glm::highp> dquat;
typedef glm::tdualquat<float, glm::highp> DualQuat;
#endif
template <typename T>
using TDualQuat = glm::tdualquat<T, glm::highp>;

#endif /* DUALQUAT_H */

