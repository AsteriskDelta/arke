#ifndef MATRIX_INC
#define MATRIX_INC

#define GLM_FORCE_RADIANS
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef VEC_DOUBLE
typedef glm::tmat3x3<double> mat3;
typedef glm::tmat4x4<double> mat4;
typedef glm::tmat3x3<double> Mat3;
typedef glm::tmat4x4<double> Mat4;
typedef glm::tmat3x3<double> M3;
typedef glm::tmat4x4<double> M4;
#else
typedef glm::tmat3x3<float> mat3;
typedef glm::tmat4x4<float> mat4;
typedef glm::tmat3x3<float> Mat3;
typedef glm::tmat4x4<float> Mat4;
typedef glm::tmat3x3<float> M3;
typedef glm::tmat4x4<float> M4;
#endif

template <typename T>
using M3T = glm::tmat3x3<T>;

template <typename T>
using M4T = glm::tmat4x4<T>;

#endif