#ifndef QUAT_INC
#define QUAT_INC

#include "V3.h"
#ifdef OPT_INLINE
#include <glm/gtc/quaternion.hpp>
#else
#include <glm/gtc/quaternion.hpp>
#endif

#ifdef VEC_DOUBLE
typedef glm::tquat<double, glm::highp> quat;
typedef glm::tquat<double, glm::highp> Quat;
#else
typedef glm::tquat<float, glm::highp> quat;
typedef glm::tquat<float, glm::highp> Quat;
#endif
template <typename T>
using TQuat = glm::tquat<T, glm::highp>;
/*
inline glm::quat quatFromToRotation(glm::vec3 start, glm::vec3 dest){
  start = glm::normalize(start);
  dest = glm::normalize(dest);
  
  float cosTheta = glm::dot(start, dest);
  glm::vec3 rotationAxis;
  
  if ((cosTheta) <= -1.f + 0.001f){
    // special case when vectors in opposite directions:
    // there is no "ideal" rotation axis
    // So guess one; any will do as long as it's perpendicular to start
    rotationAxis = glm::cross(glm::vec3(0.0f, 0.0f, 1.0f), start);
    if (glm::length2(rotationAxis) < 0.01 ) // bad luck, they were parallel, try again!
      rotationAxis = glm::cross(glm::vec3(1.0f, 0.0f, 0.0f), start);
    
    rotationAxis = glm::normalize(rotationAxis);
    return glm::angleAxis(180.0f, rotationAxis);
  } else if(cosTheta >= 1.f - 0.001f) {
      return glm::quat();
  }
  
  rotationAxis = glm::cross(start, dest);
  
  float s = sqrt( (1.f+cosTheta)*2.f );
  float invs = 1.f / s;
  
  return glm::quat(
    s * 0.5f, 
    rotationAxis.x * invs,
    rotationAxis.y * invs,
    rotationAxis.z * invs
  );
  
}*/

inline glm::quat quatFromToRotation(glm::vec3 U, glm::vec3 V) {
    glm::vec3 w = glm::cross(U,V);
    glm::quat q = glm::quat(glm::dot(U,V), w.x, w.y, w.z);
    q.w += sqrt(q.x*q.x + q.w*q.w + q.y*q.y + q.z*q.z);
    return glm::normalize(q);
}

#ifdef CEREAL_INCLUDED
namespace glm {
template<class Archive>
void serialize(Archive& archive, Quat& m) {
  archive(CEREAL_NVP(m.w),  CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) );
}
};
//CEREAL_REGISTER_TYPE(Quat);
/*
template<class Archive>
void serialize(Archive& archive, glm::tquat<float, (glm::precision)0u>& m) {
  archive(CEREAL_NVP(m.w),  CEREAL_NVP(m.x), CEREAL_NVP(m.y), CEREAL_NVP(m.z) );
}*/
#endif

#endif