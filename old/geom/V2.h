#ifndef V2_INC
#define V2_INC
#include <limits>

//#ifdef OPT_INLINE
//#include <glm/vec2.inl>
//#else
#include <glm/vec2.hpp>
#include <glm/gtx/vector_angle.hpp>
//#endif

#ifdef VEC_DOUBLE
typedef glm::dvec2 vec2;
typedef glm::dvec2 V2;
#else 
typedef glm::vec2 vec2;
typedef glm::vec2 V2;
#endif

template <typename T>
using V2T = glm::tvec2<T>;

#define V2_NULL (V2NULL)
#define V2NULL V2(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity())

#define V2_UP (V2(0.f, 1.f))
#define V2_RIGHT (V2(1.f, 0.f))
#define V2_DOWN (-V2_UP)
#define V2_LEFT (-V2_RIGHT)

inline char raw_get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, 
    float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y) {
    float s1_x, s1_y, s2_x, s2_y;
    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

    float s, t;
    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
        // Collision detected
        if (i_x != NULL)
            *i_x = p0_x + (t * s1_x);
        if (i_y != NULL)
            *i_y = p0_y + (t * s1_y);
        return 1;
    }

    return 0; // No collision
}

inline V2 V2IntersectLines(const V2& a, const V2 &b, const V2 &c, const V2& d, float *time = nullptr) {
    V2 sol = V2NULL;
    char state = raw_get_line_intersection(a.x, a.y, b.x, b.y, c.x, c.y, d.x, d.y, &sol.x, &sol.y);
    
    if(state) {
        
        if(time != nullptr) *time = glm::distance(a, sol) / glm::distance(a, b);
    } else if(time != nullptr) *time = -1.f;
    
    return sol;
}

namespace glm {
    inline float cross(const vec2& a, const vec2& b) {
        return a.x * b.y - a.y * b.x;
    }
    inline vec2 cross(const vec2& a, const float& s) {
        return vec2( s * a.y, -s * a.x );
    }
    inline vec2 cross(const float& s, const vec2& a) {
        return vec2( -s * a.y, s * a.x );
    }
    inline vec2 perp (const vec2& v) {
        return vec2(-v.y, v.x);
    }
}

#ifdef CEREAL_INCLUDED
namespace glm {
template<class Archive>
void serialize(Archive& archive, V2 & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y) );
}
};
/*template<class Archive>
void serialize(Archive& archive, glm::tvec2<float, (glm::precision)0u> & m) {
  archive( CEREAL_NVP(m.x), CEREAL_NVP(m.y));
}*/
//CEREAL_REGISTER_TYPE(V2);
#endif

#endif