#ifndef RECT_INC
#define RECT_INC
#include "V2.h"
#include <string>

#define RECT_NEGTERM 
//#define RECT_NEGTERM || (s.x > e.x && (p.x < s.x && p.x > e.x && p.y < s.y && p.y > e.y))

template <typename T> class Rect {
public:
  V2T<T> s, e;
  
  inline Rect() : s(V2T<T>(0,0)), e(V2T<T>(0,0)) {};
  inline Rect(V2T<T>newS, V2T<T>newE) : s(newS), e(newE) {};
  inline Rect(V2T<T>newS, T width, T height) : s(newS), e(V2T<T>(newS.x + width,newS.y + height)) {};
  inline Rect(T width, T height) : s(V2T<T>(0,0)), e(V2T<T>(width,height)) {};
  
  template <typename K = T> inline K getWidth()  const  {return (K)e.x - (K)s.x;}
  template <typename K = T> inline K getHeight() const {return (K)e.y - (K)s.y;}
  inline void setWidth(T newWidth);
  inline void setHeight(T newHeight);
  
  inline bool intersects(const Rect<T> &o) const;
  inline bool contains(const V2T<T> p) const;
  
  inline V2T<T> getCenter() const;
  inline void setCenter(const V2T<T> &center);
  
  std::string toString();
  
  inline bool operator==(const Rect<T> &o) const {
    return s == o.s && e == o.e;
  }
  inline bool operator!=(const Rect<T> &o) const {
    return !(s == o.s && e == o.e);
  }
  
  //inline T getArea();
  template <typename K = T> inline K getArea() const {
    return this->getWidth<K>() * this->getHeight<K>();
  }
protected:
  
};

template <typename T>
inline void Rect<T>::setWidth(T newWidth) {
  e.x = s.x + newWidth;
}

template <typename T>
inline void Rect<T>::setHeight(T newHeight) {
  e.y = s.y + newHeight;
}

template <typename T>
inline bool Rect<T>::intersects(const Rect<T> &o) const {
  return (s.x < o.e.x && e.x > o.s.x && s.y < o.e.y && e.y > o.s.y);
}

template <typename T>
inline bool Rect<T>::contains(const V2T<T> p) const {
  return (p.x > s.x && p.x < e.x && p.y > s.y && p.y < e.y) RECT_NEGTERM;
}

template <typename T>
inline V2T<T> Rect<T>::getCenter() const {
  return V2T<T>(s.x + getWidth()/(T)2, s.y + getHeight/(T)2);
}

template <typename T>
inline void Rect<T>::setCenter(const V2T<T> &center) {
  const V2T<T> deltaCenter = center - this->getCenter();
  s.x += deltaCenter.x; e.x += deltaCenter.x;
  s.y += deltaCenter.y; e.y += deltaCenter.y;
}

#endif