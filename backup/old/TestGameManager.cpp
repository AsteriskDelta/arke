#include <iostream>
#include "Color.h"
#include "Image.h"
#include "AppData.h"
#include "Console.h"
#include "Application.h"
#include <unistd.h>
#include <V3.h>

#include "Window.h"
#include "Input.h"
#include "Testing.h"
#include "Camera.h"
#include "Shader.h"
#include "Pipeline.h"
#include "rInterface.h"
#include "Model.h"
#include "PipelineStage.h"
#include "GameManager.h"
#include "rWindow.h"

#include <NVX/NVX.h>
#include <NVX/NVGeom.h>
using namespace nvx;
#include <NVX/NPolygon.impl.h>

#include "BasicMain.impl.h"

namespace nvx {
    typedef ::nvx::NPolygon<::nvx::N2, ::nvx::NPolygonPt<::nvx::N2,  ::nvx::NPolygonPtBase<::nvx::N2, false>>> N2Poly;
    typedef ::nvx::NPolygon<::nvx::N2Meta, ::nvx::NPolygonPt<::nvx::N2Meta,  ::nvx::NPolygonPtBase<::nvx::N2Meta, false>>> N2MetaPoly;
};

namespace GameManager {
  
  Window *window; Camera *camera;
  RenderPipeline *pipeline;
  Input *input;
  Model *model;
  Shader *opaque;
  Testing *testing;
  
  //Universal
  void Init() {//Called immediatly on execution
    isServer = isClient = true;
    Application::BeginInitialization();
    Application::SetDataDir("data/");
    Application::SetName("ARKE Test");
    Application::SetCodename("arkecn");
    Application::SetCompanyName("Delta III Tech");
    Application::SetVersion("0.0r1X");
    Application::SetDebug(true);
    Application::EndInitialization();
    Input::Initialize();
  }
  void PrimaryLoad() {//Load required assets (for low-level, ie pre loading screen)
    window = new Window(Application::GetName(), 500, 500);
    input = new Input();
    testing = new Testing();

    opaque = Shader::Load("opaqueTest.prgm");
    camera = new Camera();
    camera->transform.position = V3(0, 0.1, -2);
    camera->setPerspective(90.f, 1.f, 3000.f);
    camera->Activate();
    
    ImageRGBA bug("icon.png");
    if(bug) {
    std::cout << "Bug Loaded: " << bug.width << "x" << bug.height << "\n";
    //bug.drawLine(0,10, 63, 63, Color4(0,0,0,255));
    //bug.drawLine(0,20, 63, 40, Color4(0,0,0,255));
    //bug.Save("outBug.png");
    window->setIcon(&bug);
    } else std::cout << "Couldn't load bug...\n";
  }
  void SecondaryLoad() {//Loads assets(run aschronously with loading screen)
    pipeline = new RenderPipeline();
    pipeline->load("pipeline/MainRP.xml");
    pipeline->Allocate();
  
    model = new Model("sponza.obj");
  }
  /*typedef NPolygon<N2, 
        NPolygonPt<N2, 
        NPolygonPtBase<N2, false>
>> NPoly;*/
  //typedef N2_TestPoly NPoly;
  /*
  typedef N2_TestPoly UPoly;
  typedef N2 UPt;
    namespace CLP = CLIPPER_T(N2, N2_TestPoly);
*/
  typedef N2MetaPoly UPoly;
  typedef N2Meta UPt;
  //  namespace CLP = CLIPPER_T(N2Meta, N2Meta);

  inline void addPoint(long long x, long long y, UPoly *path) {
      UPt ip;
    ip.X = x;
    ip.Y = y;
    ip.Z() = ip.X + ip.Y;
    std::cout << "set metaID to: " << ip.Z() << " for " << ip << "\n";
    path->push_back(ip);
  }
  
/* template<typename T>
typename std::enable_if<true, std::ostream&>::type 
operator<<(std::ostream& out, const T:56& g) {
    return out << T(g);//"NI(" << ((signed long long)g) << ")";;
}*/
  /*inline operator<<(std::ostream& out, const signed long long:56 &g) {
      return out << ((signed long long)g);
  }*/
#include <typeinfo>
  //Low-level client
  void ClientSetup() {//Called after SecondaryLoad is completed
      //typedef NVX_IStruct<signed long long, 1000> NI;
      N2 nvec, nvec2(13.9,6.003), cvec2(5.4, 6.821), nvec3(18, 21), nvec4(-11.5, 13.666);
      N2 norg1(1, 0), norg2(0, 1), norgm(1,1);
      N3 n3v(2.f, 0.f, 2.f);
      
      std::cout << "NI div:\n";
      std::cout << "1/3 = " << NI(1.0) / 3 << "\n";
      std::cout << "1 / highp(3) = "<< (NI(1) / highp(NI(3))) << "\n";
      std::cout << "highp(1) / 3 = "<< (highp(NI(1)) / NI(3)) << "\n";
      std::cout << "highp(1) / highp(3) = "<< (highp(NI(1)) / highp(NI(3))) << "\n";
      std::cout << "3/1 = " << NI(3)/NI(1) << "\n";
      //exit(0);
      
      std::cout << "sizeof(N2) = " << sizeof(N2) << ", sizeof(N2T<unsigned short, 1>) = "<<sizeof(N2T<unsigned short, 1>)  << "\n";
      std::cout << "vecs: " << nvec << " : " << nvec2 << " : " << cvec2 << " and v3="<<n3v << " ;; n2sz=" << sizeof(nvec)<<":"<<sizeof(N2::Dat) << ", n3sz=" << sizeof(n3v) << ":"<<sizeof(N3::Dat) << " \n";
      std::cout << "mul " << (nvec2 * cvec2) << "\n";
      std::cout << "div " << NI(1.0)/NI(2.0) << ", " << NI(6.f)/NI(2.2f) << "\n";
      std::cout << "add " << (nvec3 + nvec2) << " and scaler " << (nvec3 + 15.6) <<"\n";
      std::cout << "sub " << (nvec3 - nvec2) << " and scaler " << (nvec3 - 15.6) <<"\n";
      std::cout << "neg " << -nvec2 << "\n";
      std::cout << "mod " << (nvec3 % cvec2) << "\n";
      std::cout << "abs " << abs(nvec4) << ", nvxi: " << abs(NI(-33)) << " and " << abs(NI(42)) << "\n";
      std::cout << "floor " << nvec2.x << " = " << nvx::floor(nvec2.x) << ", ceil " << cvec2.x << " = " << nvx::ceil(cvec2.x) << "\n";
      
      N2 testA2;
      std::cout << "\nassign " << -nvec4 << " = " << (testA2 = -nvec4) << "\n"; 
      std::cout << "frac " << nvec4 << " = " << nvx::frac(nvec4) << ", fmult = " << decltype(nvx::frac(nvec4))::Multiplier << "\n";
      std::cout << "intp " << nvec4 << " = " << nvx::intp(nvec4) << ", imult = " << decltype(nvx::intp(nvec4))::Multiplier << "\n";
      std::cout << "intp add intp("<<nvec2<<") + " << cvec2 << " = " << (nvx::intp(nvec2) + cvec2) << ", mult= " << decltype(nvx::intp(nvec2) + cvec2)::Multiplier << "\n";
      
      std::cout << "round " << nvec4.y << " = " << nvx::round(nvec4.y) << ", "<<cvec2.x << " = " << nvx::round(cvec2.x) << "\n";
      std::cout << "length " << cvec2 << " = " << nvx::length(cvec2) << "\n";
      std::cout << "normalize " << norgm << " = " << nvx::normalize(norgm) << "\n";
      std::cout << "angle " <<"U,V=" << nvx::anglen(norg1, norg2) << ", angle " << norg1 << ", " << norgm << " = " << nvx::angle(norg1, norgm)<<"\n";
      //std::cout << "rotate: " << norg1 << " ?= " << nvx::rotate(nvx::normalize(norgm), nvx::angle(norg1, norgm)) << ", " << norg1 << " rot -135 = " << nvx::rotate(norg1, -135) << "\n";
      
      N2 ntest = cvec2;
      std::cout << ntest << " x2 = " << (ntest*2) << " neg=" << -ntest << "\n";
      
      
      NI test1(56.05), test2(22.97), test3(200.766);
      std::cout << test1 << " + " << test2 << " = " <<
              (test1+test2) << ", "<<test3<<" / " << test2 << " = " 
              << (test3/test2) << "\n";
      
      nvec = cvec2;
      
      NI testEq = 22;
      std::cout << "Bad:  " << (testEq == test1) << ", " << (testEq == 200) << "\n";
      std::cout << "Good: " << (testEq == test2) << ", " << (testEq == 22)<< "\n";
      
      std::cout << "components of " << nvec << ": " << nvec.x << ", " << nvec.y  << "\n";
      std::cout << nvec << " to neg " << -nvec.rxx() << ", " << -nvec.ryy() << " eq " << N2(nvec.x,nvec.y) << "\n";
      
      std::cout << "funcs:\n";
      std::cout << "\tdot " << nvx::dot(cvec2, nvec2) << "\n";
      
      V3 ftest3(56.f, -5.f, 1.f); V2 ftest2 = V2(36.f, 12.f);
      N3 ntest3 = ftest3;
      N2 ntest2 = ftest2;
      
      std::cout << "converted glm -> nvx on " << ntest3 << " and " << ntest2 << "\n";
      
      N2 ntestOp = ftest2 * cvec2 + V2(1.f, 1.f) - nvec2;
      std::cout << "testOps=" << ntestOp << ", added=" << (n3v + V3(1.f)) << "\n";
      
      V3 from3 = ntest3 * V3(1.f); V2 from2 = nvec2;
      std::cout << "converted nvx -> glm " << glm::to_string(from3) << " and " << glm::to_string(from2) << ", cons via " << glm::to_string(V3(ntest3)) << "\n";
      
      std::cout << "composition: n3 " << N3(N2(5.1), -0.433) << ", n4 " << N4(1.11f, V3(34.f)) << ", n4_2 = " << N4(N2(1.1), N2(2.2)) << "\n";
      
      std::cout << "\ninvariant conversion:\n";
      std::cout << "\t" << N2(5.13, 6.66) << " -> " << N2Raw(N2(5.13, 6.66)) << " -> " << N2(N2Raw(N2(5.13, 6.66))) << "\n";
      std::cout << "\t" << N2Raw(540, 666) << " -> " << N2(N2Raw(540, 6)) << " -> " << N2Raw(N2(N2Raw(540, 6))) << "\n";
      
      std::cout << "\nmetadata preservation: earlier= " << test1 << "\n";
      N2Meta testMeta(23.2, 18.13);
      testMeta.Z() = 412;
      std::cout << testMeta << " -> " << N2MetaRaw(testMeta) << " -> " << N2Meta(N2MetaRaw(testMeta)) << "\n";
      
      /*std::cout << "\nop assignments:\n";
      nhighp fopTest(2.2), fopTest2(200.766);
      //opTest.from(6.453);
      std::cout << "\tbefore sequence point: " << fopTest << ", " << (fopTest + NI(0.5)) <<"\n";
      std::cout << "\n\n\n\n\n";
      auto nt = fopTest + 0.34;
      std::cout << "\n\n\n\n\n";
      auto nt2 = 0.34 + fopTest;
      std::cout << "\n\n\n\n\n";
      auto nt3 = 2 + fopTest;
      std::cout << "\n\n\n\n\n";
      auto nt4 = fopTest + 1;*/
      
      //std::cout << "\tmult= " << NI::Multiplier << " raw=" << fopTest.raw() << ", " << fopTest << " + " << 1.4 << " = " << (fopTest+1.4) << " ==!== " << (fopTest+=1) << "\n";
      //std::cout << "\tfop2= " << fopTest2 <<"\n";
      
      Line<N2> lineA(N2(0), N2(10.0)), lineB(N2(0, 10), N2(9.4, -2)); Ray<N2> ray1(N2(1), N2(1, -1)); TrueLine<N2> ray2(N2(6.3, 0), N2(0, 1));
      std::cout << "lineA = " << lineA << ", lineB = " << lineB << "\n";
      std::cout << "lineA length = " << lineA.length() << ", lineB length = " << lineB.length() << "\n";
      std::cout << "lineA parametric: "; for(NHP t = 0; t <= 1; t += 0.1) std::cout << t << "->" << lineA.evaluate(t) << ", "; std::cout << "\n";
      std::cout << "lineB parametric: "; for(NHP t = 0; t <= 1; t += 0.1) std::cout << t << "->" << lineB.evaluate(t) << ", "; std::cout << "\n";
      std::cout << "ray1 parametric : "; for(NHP t = 0; t <= 8; t += 0.8) std::cout << t << "->" << ray1.evaluate(t) << ", "; std::cout << "\n";
      std::cout << "lineA intersects: " << N2(1) << "? " << lineA.intersect(N2(1)) <<", " << N2(4, 3.5) << "? " << lineA.intersect(N2(4, 3.5)) << "\n";
      std::cout << "ray1 intersects: " << N2(2, 0) << "? " << ray1.intersect(N2(2,0)) << ", " << N2(4,7.6) << "? " << ray1.intersect(N2(4,7.6) ) << "\n";

      std::cout << "lineA intersect lineB? " << lineA.intersect(lineB) << ", lineB|lineA = " << lineB.intersect(lineA) << "\n";
      std::cout << "lineA intersect ray1?" << lineA.intersect(ray1) << ", ray2 intersect lineA = " << ray2.intersect(lineA) << "\n";
      std::cout << "lineB and ray1? " << lineB.intersect(ray1) << "\n";
      std::cout << "ray1 and ray2 (not parr.) " << ray1.intersect(ray2) << "\n";
      std::cout << "\n";
      
      typedef typename UPoly::List NPolyList;
      NPolyList subj, clip, solution;
      UPoly p;
        addPoint(100,100, &p);
        addPoint(200,100, &p);
        addPoint(200,200, &p);
        addPoint(100,200, &p);
        subj.push_back(p);

        UPoly p2;
        addPoint(150,50, &p2);
        addPoint(175,50, &p2);
        addPoint(175,250, &p2);
        addPoint(150,250, &p2);
        clip.push_back(p2);
        
        p.repair();
        p2.repair();
        std::cout << "p1 has area=" << p.area() << " ?= 10000, accCenter=" << p.accumulatedCentroid() << ", center=" << p.centroid() << "\n";
        
        std::cout << "fail contains: 199.300, 199.300: " << p.contains(N2(199.300, 199.300)) << "\n";;
        //exit(0);
        
        std::cout << "Testing contains:\n";
        std::cout << "\tcontains? " << p.contains(N2(150, 150)) << "\n";
        std::cout << "\tcontains? " << p.contains(N2(50, 150)) << "\n";
        std::cout << "\tcontains? " << p.contains(N2(300)) << "\n";
        std::cout << "\tnedge contains? " << p.contains(N2(150, 150), false) << "\n";
        std::cout << "\tnedge contains? " << p.contains(N2(199, 199), false) << "\n";
        std::cout << "\tnedge contains? " << p.contains(N2(150, 200), false) << "\n";
        std::cout << "\tnedge contains? " << p.contains(N2(200, 200), false) << "\n";
        
        std::cout << "\tpathologic contains? " << p.contains(N2(100, 149)) << "\n";
        //exit(0);
        /*std::cout << "\nbeginning contain fault test: \n";
#pragma omp parallel for
        for(int ox = 0; ox <= 100*100; ox++) {
            for(int oy = 0; oy <= 100*100; oy++) {
                N2 pt = N2(100) + N2(ox,oy) / 100;
                if(!p.contains(pt)) {
                    std::cout << "\tDISCONT AT " << pt << "\n";
                }
            }
        }
        std::cout << "end test\n\n";
        */
        /*std::cout << "beginning intersect fault test:\n";
#pragma omp parallel for
        for(int ox = -10000; ox <= 10000; ox++) {
            for(int oy = -10000; oy <= 10000; oy++) {
                if(ox == 0 && oy == 0) continue;
                auto normal = normalize(N2(ox,oy));// N2(100) + N2(ox,oy);
                auto inter = p.intersect(TrueLine<N2::HighpVec>(N2(151.222, 147.333), normal), true);
                const bool hasEdge = p.contains(inter.start(), false) || p.contains(inter.end(), false);
                const bool contained = p.contains(inter.start()) || p.contains(inter.end());
                if(!inter || !contained || hasEdge) {
                    std::cout << "\tDISCONT AT " << normal << " inter=" << inter << "\n";
                }
            }
        }
        std::cout << "end test\n";
        exit(0);*/
        
        std::cout << "pathogenic intersect:\n";
        N2 ist = N2(11), est = N2(2, 9.129);
        
        exit(0);
        
        std::cout << "isolated should be: " << p.intersect(TrueLine<N2>(N2(151.222, 147.333), N2::HighpVec(0.721387, 0.692532)), true) << "\n";
        std::cout << "isolated intersect fault: " << p.intersect(TrueLine<N2::HighpVec>(N2(151.222, 147.333), N2::HighpVec(0.721387, 0.692532)), true) << "\n";
        
        std::cout << "fault mult:\n\n";
        std::cout << NI(100.000)<<" * "<<NI::HighpRow(0.69250) << " = " << NI(100.000) * NI::HighpRow(0.69250) << "\n";
        std::cout << NI(100.000)<<" + "<<NI::HighpRow(0.69250) << " = " << (NI(100.000) + NI::HighpRow(0.69250)) << "\n";
        //exit(0);
        
        std::cout << "Testing intersection:\n";
        std::cout << "\tunit diagonal? " << p.intersect(Ray<N2>(N2(0), N2(1))) << "\n";
        std::cout << "\tunit internal? " << p.intersect(Ray<N2>(N2(150), N2(1))) << "\n";
        std::cout << "\tunit miss? " << p.intersect(Ray<N2>(N2(205), N2(1))) << "\n";
        std::cout << "\thorizontal edge hit? " << p.intersect(Ray<N2>(N2(200), N2(1,0))) << "\n";
        std::cout << "\thorizontal miss? " << p.intersect(Ray<N2>(N2(250), N2(1,0))) << "\n";
        std::cout << "\nInternal intersections:\n";
        
        std::cout << "\tlc diagonal? " << p.intersect(TrueLine<N2>(N2(125, 150), N2(1)), true) << "\n";
        std::cout << "\trc diagonal? " << p.intersect(TrueLine<N2>(N2(175, 150), N2(1)), true) << "\n";
        std::cout << "\tcu diagonal? " << p.intersect(TrueLine<N2>(N2(150, 175), N2(1)), true) << "\n";
        std::cout << "\tcd diagonal? " << p.intersect(TrueLine<N2>(N2(150, 125), N2(1)), true) << "\n";
        
        //p.csgIntersect(&p2);
     /*CLP::Clipper c;
    
    c.AddPaths(subj, CLP::ptSubject, true);
    c.AddPaths(clip, CLP::ptClip, true);
    c.Execute(CLP::ctIntersection, solution, CLP::pftNonZero, CLP::pftNonZero);
    
    printf("solution size = %d\n",(int)solution.size());
    for (unsigned i=0; i<solution.size(); i++)
    {
        UPoly& p3 = solution[i];
        std::cout << "\tgot poly sz=" << p3.size() << "\n";
        for(const UPoly::Point& pt : p3) std::cout << "\t\t" << pt.position() << "\n";
        
    }*/
      
  }
  void WindowChanged() {//Called at window change
  
  }
  
  //Client-side stuff, called in order listed
  void Update(float newDeltaTime) {
    _unused(newDeltaTime);
    Application::UpdateGlobals();
    input->zero();
    window->update();
    Application::UpdateDelta();

    input->update(1);
    
    float moveSpeed = 140.f;
    float rotateSpeed = 4.f;

    V3 deltaPos = V3(0.0f), deltaPosPr = V3(0.f);
    if(input->getKeyDown('w'))	deltaPos.z += moveSpeed * _deltaTime;
    if(input->getKeyDown('s'))	deltaPos.z -= moveSpeed * _deltaTime;
    if(input->getKeyDown('d'))	deltaPos.x -= moveSpeed * _deltaTime;
    if(input->getKeyDown('a'))	deltaPos.x += moveSpeed * _deltaTime;
    if(input->getKeyDown('r'))	deltaPosPr.y += moveSpeed * _deltaTime;
    if(input->getKeyDown('f'))	deltaPosPr.y -= moveSpeed * _deltaTime;
    camera->transform.translate(deltaPos * camera->transform.rotation + deltaPosPr);
    
    V3 forward = V3_FORWARD;//camera->transform.forward();
    forward = glm::rotate(forward, input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed * _deltaTime, V3_UP);
    forward = glm::rotate(forward, input->getAxisDelta(Input::Axis::MouseY) * -rotateSpeed * _deltaTime, V3_RIGHT);
    
    
    Quat cRot = quatFromToRotation(V3_FORWARD, forward);
    camera->transform.rotate(cRot);
    //camera->transform.rotate(Quat(V3(input->getAxisDelta(Input::Axis::MouseY) * -rotateSpeed * _deltaTime, input->getAxisDelta(Input::Axis::MouseX) * rotateSpeed * _deltaTime, 0.f)));
    //std::cout << glm::to_string(forward) << "\n";
    
  }
  
  REND_DRAW_R renderScene REND_DRAW_D {
    if(stage->hasFlag(RenderStage::DrawOpaque)) {
      OnOpaque3D(camera, stage);
    }
    return 1;
  }
  
  void OnOpaque3D(Camera *const camera, RenderStage *const stage) {
    _unused(camera);
    model->draw(stage);
      //opaque->activate();
      //testing->DrawSphere();
      //opaque->deactivate();
  }
  void OnTransparent3D(Camera *const camera, RenderStage *const stage) {
    _unused(camera);
  }
  void OnOrtho() {
    
  }
  
  void Render() {
     pipeline->Activate();
    //camera->transform.position.z = -0.1f;
    //camera->transform.lookAt(V3(0,0,0));
    camera->Activate();
    opaque->activate();
    
    //testing->DrawPlane();
    //testing->DrawSphere();
    //model->draw();
    pipeline->Execute(renderScene);
    //testing->DrawTriangle();
    opaque->deactivate();
    pipeline->Blit();
    pipeline->Deactivate();
    
    
    //window->update();
    
    camera->Deactivate();
  }
  
  unsigned short GetDistortionCount() {
    return 0;
  }
  void OnDistortion(unsigned short id) {
    _unused(id);
  }
  
  //Server-side stuff
  void OnTick() {
    
  }
  
  bool isServer, isClient;
};
