#ifndef NPOLYGON_CLIP_MMPL_H
#define NPOLYGON_CLIP_MMPL_H

#define CLIP_NM_PRE template<typename IntPoint, typename Path, typename Paths>
#define CLIP_NM_T ClipperLib<IntPoint, Path, Paths>


//------------------------------------------------------------------------------
// PolyTree methods ...
//------------------------------------------------------------------------------

CLIP_NM_PRE
void CLIP_NM_T::PolyTree::Clear() {
    for (typename CLIP_NM_T::PolyNodes::size_type i = 0; i < AllNodes.size(); ++i)
        delete AllNodes[i];
    AllNodes.resize(0);
    Childs.resize(0);
}
//------------------------------------------------------------------------------
CLIP_NM_PRE
CLIP_NM_T::PolyNode* CLIP_NM_T::PolyTree::GetFirst() const {
    if (!Childs.empty())
        return Childs[0];
    else
        return 0;
}
//------------------------------------------------------------------------------
CLIP_NM_PRE
int CLIP_NM_T::PolyTree::Total() const {
    int result = (int) AllNodes.size();
    //with negative offsets, ignore the hidden outer polygon ...
    if (result > 0 && Childs[0] != AllNodes[0]) result--;
    return result;
}

//------------------------------------------------------------------------------
    // CLIP_NM_T::PolyNode methods ...
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::PolyNode::CLIP_NM_T::PolyNode() : Parent(0), Index(0), m_IsOpen(false) {
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    int CLIP_NM_T::PolyNode::ChildCount() const {
        return (int) Childs.size();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::PolyNode::AddChild(CLIP_NM_T::PolyNode& child) {
        unsigned cnt = (unsigned) Childs.size();
        Childs.push_back(&child);
        child.Parent = this;
        child.Index = cnt;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::PolyNode* CLIP_NM_T::PolyNode::GetNext() const {
        if (!Childs.empty())
            return Childs[0];
        else
            return GetNextSiblingUp();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::PolyNode* CLIP_NM_T::PolyNode::GetNextSiblingUp() const {
        if (!Parent) //protects against PolyTree.GetNextSiblingUp()
            return 0;
        else if (Index == Parent->Childs.size() - 1)
            return Parent->GetNextSiblingUp();
        else
            return Parent->Childs[Index + 1];
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::PolyNode::IsHole() const {
        bool result = true;
        CLIP_NM_T::PolyNode* node = Parent;
        while (node) {
            result = !result;
            node = node->Parent;
        }
        return result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::PolyNode::IsOpen() const {
        return m_IsOpen;
    }

    //------------------------------------------------------------------------------
    // CLIP_NM_T::ClipperBase class methods ...
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::ClipperBase::CLIP_NM_T::ClipperBase() //constructor
    {
        m_CurrentLM = m_MinimaList.begin(); //begin() == end() here
        m_UseFullRange = false;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::ClipperBase::~CLIP_NM_T::ClipperBase() //destructor
    {
        Clear();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    static void RangeTest(const IntPoint& Pt, bool& useFullRange) {
        if (useFullRange) {
            if (Pt.X > hiRange || Pt.Y > hiRange || -Pt.X > hiRange || -Pt.Y > hiRange)
                throw clipperException("Coordinate outside allowed range");
        } else if (Pt.X > loRange || Pt.Y > loRange || -Pt.X > loRange || -Pt.Y > loRange) {
            useFullRange = true;
            RangeTest(Pt, useFullRange);
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    static TEdge* FindNextLocMin(TEdge* E) {
        for (;;) {
            while (E->Bot != E->Prev->Bot || E->Curr == E->Top) E = E->Next;
            if (!IsHorizontal(*E) && !IsHorizontal(*E->Prev)) break;
            while (IsHorizontal(*E->Prev)) E = E->Prev;
            TEdge* E2 = E;
            while (IsHorizontal(*E)) E = E->Next;
            if (E->Top.Y == E->Prev->Bot.Y) continue; //ie just an intermediate horz.
            if (E2->Prev->Bot.X < E->Bot.X) E = E2;
            break;
        }
        return E;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    static TEdge* CLIP_NM_T::ClipperBase::ProcessBound(TEdge* E, bool NextIsForward) {
        TEdge *Result = E;
        TEdge *Horz = 0;

        if (E->OutIdx == Skip) {
            //if edges still remain in the current bound beyond the skip edge then
            //create another LocMin and call ProcessBound once more
            if (NextIsForward) {
                while (E->Top.Y == E->Next->Bot.Y) E = E->Next;
                //don't include top horizontals when parsing a bound a second time,
                //they will be contained in the opposite bound ...
                while (E != Result && IsHorizontal(*E)) E = E->Prev;
            } else {
                while (E->Top.Y == E->Prev->Bot.Y) E = E->Prev;
                while (E != Result && IsHorizontal(*E)) E = E->Next;
            }

            if (E == Result) {
                if (NextIsForward) Result = E->Next;
                else Result = E->Prev;
            } else {
                //there are more edges in the bound beyond result starting with E
                if (NextIsForward)
                    E = Result->Next;
                else
                    E = Result->Prev;
                MinimaList::value_type locMin;
                locMin.Y = E->Bot.Y;
                locMin.LeftBound = 0;
                locMin.RightBound = E;
                E->WindDelta = 0;
                Result = ProcessBound(E, NextIsForward);
                m_MinimaList.push_back(locMin);
            }
            return Result;
        }

        TEdge *EStart;

        if (IsHorizontal(*E)) {
            //We need to be careful with open paths because this may not be a
            //true local minima (ie E may be following a skip edge).
            //Also, consecutive horz. edges may start heading left before going right.
            if (NextIsForward)
                EStart = E->Prev;
            else
                EStart = E->Next;
            if (IsHorizontal(*EStart)) //ie an adjoining horizontal skip edge
            {
                if (EStart->Bot.X != E->Bot.X && EStart->Top.X != E->Bot.X)
                    ReverseHorizontal(*E);
            } else if (EStart->Bot.X != E->Bot.X)
                ReverseHorizontal(*E);
        }

        EStart = E;
        if (NextIsForward) {
            while (Result->Top.Y == Result->Next->Bot.Y && Result->Next->OutIdx != Skip)
                Result = Result->Next;
            if (IsHorizontal(*Result) && Result->Next->OutIdx != Skip) {
                //nb: at the top of a bound, horizontals are added to the bound
                //only when the preceding edge attaches to the horizontal's left vertex
                //unless a Skip edge is encountered when that becomes the top divide
                Horz = Result;
                while (IsHorizontal(*Horz->Prev)) Horz = Horz->Prev;
                if (Horz->Prev->Top.X > Result->Next->Top.X) Result = Horz->Prev;
            }
            while (E != Result) {
                E->NextInLML = E->Next;
                if (IsHorizontal(*E) && E != EStart &&
                        E->Bot.X != E->Prev->Top.X) ReverseHorizontal(*E);
                E = E->Next;
            }
            if (IsHorizontal(*E) && E != EStart && E->Bot.X != E->Prev->Top.X)
                ReverseHorizontal(*E);
            Result = Result->Next; //move to the edge just beyond current bound
        } else {
            while (Result->Top.Y == Result->Prev->Bot.Y && Result->Prev->OutIdx != Skip)
                Result = Result->Prev;
            if (IsHorizontal(*Result) && Result->Prev->OutIdx != Skip) {
                Horz = Result;
                while (IsHorizontal(*Horz->Next)) Horz = Horz->Next;
                if (Horz->Next->Top.X == Result->Prev->Top.X ||
                        Horz->Next->Top.X > Result->Prev->Top.X) Result = Horz->Next;
            }

            while (E != Result) {
                E->NextInLML = E->Prev;
                if (IsHorizontal(*E) && E != EStart && E->Bot.X != E->Next->Top.X)
                    ReverseHorizontal(*E);
                E = E->Prev;
            }
            if (IsHorizontal(*E) && E != EStart && E->Bot.X != E->Next->Top.X)
                ReverseHorizontal(*E);
            Result = Result->Prev; //move to the edge just beyond current bound
        }

        return Result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::ClipperBase::AddPath(const Path &pg, PolyType PolyTyp, bool Closed) {
#ifdef use_lines
        if (!Closed && PolyTyp == ptClip)
            throw clipperException("AddPath: Open paths must be subject.");
#else
        if (!Closed)
            throw clipperException("AddPath: Open paths have been disabled.");
#endif

        int highI = (int) pg.size() - 1;
        if (Closed) while (highI > 0 && (pg[highI] == pg[0])) --highI;
        while (highI > 0 && (pg[highI] == pg[highI - 1])) --highI;
        if ((Closed && highI < 2) || (!Closed && highI < 1)) return false;

        //create a new edge array ...
        TEdge *edges = new TEdge [highI + 1];

        bool IsFlat = true;
        //1. Basic (first) edge initialization ...
        try {
            edges[1].Curr = pg[1];
            RangeTest(pg[0], m_UseFullRange);
            RangeTest(pg[highI], m_UseFullRange);
            InitEdge(&edges[0], &edges[1], &edges[highI], pg[0]);
            InitEdge(&edges[highI], &edges[0], &edges[highI - 1], pg[highI]);
            for (int i = highI - 1; i >= 1; --i) {
                RangeTest(pg[i], m_UseFullRange);
                InitEdge(&edges[i], &edges[i + 1], &edges[i - 1], pg[i]);
            }
        }        catch (...) {
            delete [] edges;
            throw; //range test fails
        }
        TEdge *eStart = &edges[0];

        //2. Remove duplicate vertices, and (when closed) collinear edges ...
        TEdge *E = eStart, *eLoopStop = eStart;
        for (;;) {
            //nb: allows matching start and end points when not Closed ...
            if (E->Curr == E->Next->Curr && (Closed || E->Next != eStart)) {
                if (E == E->Next) break;
                if (E == eStart) eStart = E->Next;
                E = RemoveEdge(E);
                eLoopStop = E;
                continue;
            }
            if (E->Prev == E->Next)
                break; //only two vertices
            else if (Closed &&
                    SlopesEqual(E->Prev->Curr, E->Curr, E->Next->Curr, m_UseFullRange) &&
                    (!m_PreserveCollinear ||
                    !Pt2IsBetweenPt1AndPt3(E->Prev->Curr, E->Curr, E->Next->Curr))) {
                //Collinear edges are allowed for open paths but in closed paths
                //the default is to merge adjacent collinear edges into a single edge.
                //However, if the PreserveCollinear property is enabled, only overlapping
                //collinear edges (ie spikes) will be removed from closed paths.
                if (E == eStart) eStart = E->Next;
                E = RemoveEdge(E);
                E = E->Prev;
                eLoopStop = E;
                continue;
            }
            E = E->Next;
            if ((E == eLoopStop) || (!Closed && E->Next == eStart)) break;
        }

        if ((!Closed && (E == E->Next)) || (Closed && (E->Prev == E->Next))) {
            delete [] edges;
            return false;
        }

        if (!Closed) {
            m_HasOpenPaths = true;
            eStart->Prev->OutIdx = Skip;
        }

        //3. Do second stage of edge initialization ...
        E = eStart;
        do {
            InitEdge2(*E, PolyTyp);
            E = E->Next;
            if (IsFlat && E->Curr.Y != eStart->Curr.Y) IsFlat = false;
        }        while (E != eStart);

        //4. Finally, add edge bounds to LocalMinima list ...

        //Totally flat paths must be handled differently when adding them
        //to LocalMinima list to avoid endless loops etc ...
        if (IsFlat) {
            if (Closed) {
                delete [] edges;
                return false;
            }
            E->Prev->OutIdx = Skip;
            MinimaList::value_type locMin;
            locMin.Y = E->Bot.Y;
            locMin.LeftBound = 0;
            locMin.RightBound = E;
            locMin.RightBound->Side = esRight;
            locMin.RightBound->WindDelta = 0;
            for (;;) {
                if (E->Bot.X != E->Prev->Top.X) ReverseHorizontal(*E);
                if (E->Next->OutIdx == Skip) break;
                E->NextInLML = E->Next;
                E = E->Next;
            }
            m_MinimaList.push_back(locMin);
            m_edges.push_back(edges);
            return true;
        }

        m_edges.push_back(edges);
        bool leftBoundIsForward;
        TEdge* EMin = 0;

        //workaround to avoid an endless loop in the while loop below when
        //open paths have matching start and end points ...
        if (E->Prev->Bot == E->Prev->Top) E = E->Next;

        for (;;) {
            E = FindNextLocMin(E);
            if (E == EMin) break;
            else if (!EMin) EMin = E;

            //E and E.Prev now share a local minima (left aligned if horizontal).
            //Compare their slopes to find which starts which bound ...
            MinimaList::value_type locMin;
            locMin.Y = E->Bot.Y;
            if (E->Dx < E->Prev->Dx) {
                locMin.LeftBound = E->Prev;
                locMin.RightBound = E;
                leftBoundIsForward = false; //Q.nextInLML = Q.prev
            } else {
                locMin.LeftBound = E;
                locMin.RightBound = E->Prev;
                leftBoundIsForward = true; //Q.nextInLML = Q.next
            }

            if (!Closed) locMin.LeftBound->WindDelta = 0;
            else if (locMin.LeftBound->Next == locMin.RightBound)
                locMin.LeftBound->WindDelta = -1;
            else locMin.LeftBound->WindDelta = 1;
            locMin.RightBound->WindDelta = -locMin.LeftBound->WindDelta;

            E = ProcessBound(locMin.LeftBound, leftBoundIsForward);
            if (E->OutIdx == Skip) E = ProcessBound(E, leftBoundIsForward);

            TEdge* E2 = ProcessBound(locMin.RightBound, !leftBoundIsForward);
            if (E2->OutIdx == Skip) E2 = ProcessBound(E2, !leftBoundIsForward);

            if (locMin.LeftBound->OutIdx == Skip)
                locMin.LeftBound = 0;
            else if (locMin.RightBound->OutIdx == Skip)
                locMin.RightBound = 0;
            m_MinimaList.push_back(locMin);
            if (!leftBoundIsForward) E = E2;
        }
        return true;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::ClipperBase::AddPaths(const Paths &ppg, PolyType PolyTyp, bool Closed) {
        bool result = false;
        for (typename Paths::size_type i = 0; i < ppg.size(); ++i)
            if (AddPath(ppg[i], PolyTyp, Closed)) result = true;
        return result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::Clear() {
        DisposeLocalMinimaList();
        for (EdgeList::size_type i = 0; i < m_edges.size(); ++i) {
            TEdge* edges = m_edges[i];
            delete [] edges;
        }
        m_edges.clear();
        m_UseFullRange = false;
        m_HasOpenPaths = false;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::Reset() {
        m_CurrentLM = m_MinimaList.begin();
        if (m_CurrentLM == m_MinimaList.end()) return; //ie nothing to process
        std::sort(m_MinimaList.begin(), m_MinimaList.end(), LocMinSorter());

        m_Scanbeam = ScanbeamList(); //clears/resets priority_queue
        //reset all edges ...
        for (MinimaList::iterator lm = m_MinimaList.begin(); lm != m_MinimaList.end(); ++lm) {
            InsertScanbeam(lm->Y);
            TEdge* e = lm->LeftBound;
            if (e) {
                e->Curr = e->Bot;
                e->Side = esLeft;
                e->OutIdx = Unassigned;
            }

            e = lm->RightBound;
            if (e) {
                e->Curr = e->Bot;
                e->Side = esRight;
                e->OutIdx = Unassigned;
            }
        }
        m_ActiveEdges = 0;
        m_CurrentLM = m_MinimaList.begin();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::DisposeLocalMinimaList() {
        m_MinimaList.clear();
        m_CurrentLM = m_MinimaList.begin();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::ClipperBase::PopLocalMinima(cInt Y, const LocalMinimum *&locMin) {
        if (m_CurrentLM == m_MinimaList.end() || (*m_CurrentLM).Y != Y) return false;
        locMin = &(*m_CurrentLM);
        ++m_CurrentLM;
        return true;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    IntRect CLIP_NM_T::ClipperBase::GetBounds() {
        IntRect result;
        MinimaList::iterator lm = m_MinimaList.begin();
        if (lm == m_MinimaList.end()) {
            result.left = result.top = result.right = result.bottom = 0;
            return result;
        }
        result.left = lm->LeftBound->Bot.X;
        result.top = lm->LeftBound->Bot.Y;
        result.right = lm->LeftBound->Bot.X;
        result.bottom = lm->LeftBound->Bot.Y;
        while (lm != m_MinimaList.end()) {
            //todo - needs fixing for open paths
            result.bottom = std::max(result.bottom, lm->LeftBound->Bot.Y);
            TEdge* e = lm->LeftBound;
            for (;;) {
                TEdge* bottomE = e;
                while (e->NextInLML) {
                    if (e->Bot.X < result.left) result.left = e->Bot.X;
                    if (e->Bot.X > result.right) result.right = e->Bot.X;
                    e = e->NextInLML;
                }
                result.left = std::min(result.left, e->Bot.X);
                result.right = std::max(result.right, e->Bot.X);
                result.left = std::min(result.left, e->Top.X);
                result.right = std::max(result.right, e->Top.X);
                result.top = std::min(result.top, e->Top.Y);
                if (bottomE == lm->LeftBound) e = lm->RightBound;
                else break;
            }
            ++lm;
        }
        return result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::InsertScanbeam(const cInt Y) {
        m_Scanbeam.push(Y);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::ClipperBase::PopScanbeam(cInt &Y) {
        if (m_Scanbeam.empty()) return false;
        Y = m_Scanbeam.top();
        m_Scanbeam.pop();
        while (!m_Scanbeam.empty() && Y == m_Scanbeam.top()) {
            m_Scanbeam.pop();
        } // Pop duplicates.
        return true;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::DisposeAllOutRecs() {
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i)
            DisposeOutRec(i);
        m_PolyOuts.clear();
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::DisposeOutRec(PolyOutList::size_type index) {
        OutRec *outRec = m_PolyOuts[index];
        if (outRec->Pts) DisposeOutPts(outRec->Pts);
        delete outRec;
        m_PolyOuts[index] = 0;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::DeleteFromAEL(TEdge *e) {
        TEdge* AelPrev = e->PrevInAEL;
        TEdge* AelNext = e->NextInAEL;
        if (!AelPrev && !AelNext && (e != m_ActiveEdges)) return; //already deleted
        if (AelPrev) AelPrev->NextInAEL = AelNext;
        else m_ActiveEdges = AelNext;
        if (AelNext) AelNext->PrevInAEL = AelPrev;
        e->NextInAEL = 0;
        e->PrevInAEL = 0;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutRec* CLIP_NM_T::ClipperBase::CreateOutRec() {
        OutRec* result = new OutRec;
        result->IsHole = false;
        result->IsOpen = false;
        result->FirstLeft = 0;
        result->Pts = 0;
        result->BottomPt = 0;
        result->PolyNd = 0;
        m_PolyOuts.push_back(result);
        result->Idx = (int) m_PolyOuts.size() - 1;
        return result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::SwapPositionsInAEL(TEdge *Edge1, TEdge *Edge2) {
        //check that one or other edge hasn't already been removed from AEL ...
        if (Edge1->NextInAEL == Edge1->PrevInAEL ||
                Edge2->NextInAEL == Edge2->PrevInAEL) return;

        if (Edge1->NextInAEL == Edge2) {
            TEdge* Next = Edge2->NextInAEL;
            if (Next) Next->PrevInAEL = Edge1;
            TEdge* Prev = Edge1->PrevInAEL;
            if (Prev) Prev->NextInAEL = Edge2;
            Edge2->PrevInAEL = Prev;
            Edge2->NextInAEL = Edge1;
            Edge1->PrevInAEL = Edge2;
            Edge1->NextInAEL = Next;
        } else if (Edge2->NextInAEL == Edge1) {
            TEdge* Next = Edge1->NextInAEL;
            if (Next) Next->PrevInAEL = Edge2;
            TEdge* Prev = Edge2->PrevInAEL;
            if (Prev) Prev->NextInAEL = Edge1;
            Edge1->PrevInAEL = Prev;
            Edge1->NextInAEL = Edge2;
            Edge2->PrevInAEL = Edge1;
            Edge2->NextInAEL = Next;
        } else {
            TEdge* Next = Edge1->NextInAEL;
            TEdge* Prev = Edge1->PrevInAEL;
            Edge1->NextInAEL = Edge2->NextInAEL;
            if (Edge1->NextInAEL) Edge1->NextInAEL->PrevInAEL = Edge1;
            Edge1->PrevInAEL = Edge2->PrevInAEL;
            if (Edge1->PrevInAEL) Edge1->PrevInAEL->NextInAEL = Edge1;
            Edge2->NextInAEL = Next;
            if (Edge2->NextInAEL) Edge2->NextInAEL->PrevInAEL = Edge2;
            Edge2->PrevInAEL = Prev;
            if (Edge2->PrevInAEL) Edge2->PrevInAEL->NextInAEL = Edge2;
        }

        if (!Edge1->PrevInAEL) m_ActiveEdges = Edge1;
        else if (!Edge2->PrevInAEL) m_ActiveEdges = Edge2;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::ClipperBase::UpdateEdgeIntoAEL(TEdge *&e) {
        if (!e->NextInLML)
            throw clipperException("UpdateEdgeIntoAEL: invalid call");

        e->NextInLML->OutIdx = e->OutIdx;
        TEdge* AelPrev = e->PrevInAEL;
        TEdge* AelNext = e->NextInAEL;
        if (AelPrev) AelPrev->NextInAEL = e->NextInLML;
        else m_ActiveEdges = e->NextInLML;
        if (AelNext) AelNext->PrevInAEL = e->NextInLML;
        e->NextInLML->Side = e->Side;
        e->NextInLML->WindDelta = e->WindDelta;
        e->NextInLML->WindCnt = e->WindCnt;
        e->NextInLML->WindCnt2 = e->WindCnt2;
        e = e->NextInLML;
        e->Curr = e->Bot;
        e->PrevInAEL = AelPrev;
        e->NextInAEL = AelNext;
        if (!IsHorizontal(*e)) InsertScanbeam(e->Top.Y);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::ClipperBase::LocalMinimaPending() {
        return (m_CurrentLM != m_MinimaList.end());
    }

    //------------------------------------------------------------------------------
    // TClipper methods ...
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    CLIP_NM_T::Clipper::Clipper(int initOptions) : CLIP_NM_T::ClipperBase() //constructor
    {
        m_ExecuteLocked = false;
        m_UseFullRange = false;
        m_ReverseOutput = ((initOptions & ioReverseSolution) != 0);
        m_StrictSimple = ((initOptions & ioStrictlySimple) != 0);
        m_PreserveCollinear = ((initOptions & ioPreserveCollinear) != 0);
        m_HasOpenPaths = false;
#ifdef use_xyz  
        m_ZFill = 0;
#endif
    }
    //------------------------------------------------------------------------------

#ifdef use_xyz  
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::ZFillFunction(ZFillCallback zFillFunc) {
        m_ZFill = zFillFunc;
    }
#endif
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::Execute(ClipType clipType, Paths &solution, PolyFillType fillType) {
        return Execute(clipType, solution, fillType, fillType);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::Execute(ClipType clipType, PolyTree &polytree, PolyFillType fillType) {
        return Execute(clipType, polytree, fillType, fillType);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::Execute(ClipType clipType, Paths &solution,
            PolyFillType subjFillType, PolyFillType clipFillType) {
        if (m_ExecuteLocked) return false;
        if (m_HasOpenPaths)
            throw clipperException("Error: PolyTree struct is needed for open path clipping.");
        m_ExecuteLocked = true;
        solution.resize(0);
        m_SubjFillType = subjFillType;
        m_ClipFillType = clipFillType;
        m_ClipType = clipType;
        m_UsingPolyTree = false;
        bool succeeded = ExecuteInternal();
        if (succeeded) BuildResult(solution);
        DisposeAllOutRecs();
        m_ExecuteLocked = false;
        return succeeded;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::Execute(ClipType clipType, PolyTree& polytree,
            PolyFillType subjFillType, PolyFillType clipFillType) {
        if (m_ExecuteLocked) return false;
        m_ExecuteLocked = true;
        m_SubjFillType = subjFillType;
        m_ClipFillType = clipFillType;
        m_ClipType = clipType;
        m_UsingPolyTree = true;
        bool succeeded = ExecuteInternal();
        if (succeeded) BuildResult2(polytree);
        DisposeAllOutRecs();
        m_ExecuteLocked = false;
        return succeeded;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::FixHoleLinkage(OutRec &outrec) {
        //skip OutRecs that (a) contain outermost polygons or
        //(b) already have the correct owner/child linkage ...
        if (!outrec.FirstLeft ||
                (outrec.IsHole != outrec.FirstLeft->IsHole &&
                outrec.FirstLeft->Pts)) return;

        OutRec* orfl = outrec.FirstLeft;
        while (orfl && ((orfl->IsHole == outrec.IsHole) || !orfl->Pts))
            orfl = orfl->FirstLeft;
        outrec.FirstLeft = orfl;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::ExecuteInternal() {
        bool succeeded = true;
        try {
            Reset();
            m_Maxima = MaximaList();
            m_SortedEdges = 0;

            succeeded = true;
            cInt botY, topY;
            if (!PopScanbeam(botY)) return false;
            InsertLocalMinimaIntoAEL(botY);
            while (PopScanbeam(topY) || LocalMinimaPending()) {
                ProcessHorizontals();
                ClearGhostJoins();
                if (!ProcessIntersections(topY)) {
                    succeeded = false;
                    break;
                }
                ProcessEdgesAtTopOfScanbeam(topY);
                botY = topY;
                InsertLocalMinimaIntoAEL(botY);
            }
        }        catch (...) {
            succeeded = false;
        }

        if (succeeded) {
            //fix orientations ...
            for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
                OutRec *outRec = m_PolyOuts[i];
                if (!outRec->Pts || outRec->IsOpen) continue;
                if ((outRec->IsHole ^ m_ReverseOutput) == (Area(*outRec) > 0))
                    ReversePolyPtLinks(outRec->Pts);
            }

            if (!m_Joins.empty()) JoinCommonEdges();

            //unfortunately FixupOutPolygon() must be done after JoinCommonEdges()
            for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
                OutRec *outRec = m_PolyOuts[i];
                if (!outRec->Pts) continue;
                if (outRec->IsOpen)
                    FixupOutPolyline(*outRec);
                else
                    FixupOutPolygon(*outRec);
            }

            if (m_StrictSimple) DoSimplePolygons();
        }

        ClearJoins();
        ClearGhostJoins();
        return succeeded;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::SetWindingCount(TEdge &edge) {
        TEdge *e = edge.PrevInAEL;
        //find the edge of the same polytype that immediately preceeds 'edge' in AEL
        while (e && ((e->PolyTyp != edge.PolyTyp) || (e->WindDelta == 0))) e = e->PrevInAEL;
        if (!e) {
            if (edge.WindDelta == 0) {
                PolyFillType pft = (edge.PolyTyp == ptSubject ? m_SubjFillType : m_ClipFillType);
                edge.WindCnt = (pft == pftNegative ? -1 : 1);
            } else
                edge.WindCnt = edge.WindDelta;
            edge.WindCnt2 = 0;
            e = m_ActiveEdges; //ie get ready to calc WindCnt2
        }
        else if (edge.WindDelta == 0 && m_ClipType != ctUnion) {
            edge.WindCnt = 1;
            edge.WindCnt2 = e->WindCnt2;
            e = e->NextInAEL; //ie get ready to calc WindCnt2
        } else if (IsEvenOddFillType(edge)) {
            //EvenOdd filling ...
            if (edge.WindDelta == 0) {
                //are we inside a subj polygon ...
                bool Inside = true;
                TEdge *e2 = e->PrevInAEL;
                while (e2) {
                    if (e2->PolyTyp == e->PolyTyp && e2->WindDelta != 0)
                        Inside = !Inside;
                    e2 = e2->PrevInAEL;
                }
                edge.WindCnt = (Inside ? 0 : 1);
            } else {
                edge.WindCnt = edge.WindDelta;
            }
            edge.WindCnt2 = e->WindCnt2;
            e = e->NextInAEL; //ie get ready to calc WindCnt2
        }
        else {
            //nonZero, Positive or Negative filling ...
            if (e->WindCnt * e->WindDelta < 0) {
                //prev edge is 'decreasing' WindCount (WC) toward zero
                //so we're outside the previous polygon ...
                if (Abs(e->WindCnt) > 1) {
                    //outside prev poly but still inside another.
                    //when reversing direction of prev poly use the same WC 
                    if (e->WindDelta * edge.WindDelta < 0) edge.WindCnt = e->WindCnt;
                        //otherwise continue to 'decrease' WC ...
                    else edge.WindCnt = e->WindCnt + edge.WindDelta;
                }
                else
                    //now outside all polys of same polytype so set own WC ...
                    edge.WindCnt = (edge.WindDelta == 0 ? 1 : edge.WindDelta);
            } else {
                //prev edge is 'increasing' WindCount (WC) away from zero
                //so we're inside the previous polygon ...
                if (edge.WindDelta == 0)
                    edge.WindCnt = (e->WindCnt < 0 ? e->WindCnt - 1 : e->WindCnt + 1);
                    //if wind direction is reversing prev then use same WC
                else if (e->WindDelta * edge.WindDelta < 0) edge.WindCnt = e->WindCnt;
                    //otherwise add to WC ...
                else edge.WindCnt = e->WindCnt + edge.WindDelta;
            }
            edge.WindCnt2 = e->WindCnt2;
            e = e->NextInAEL; //ie get ready to calc WindCnt2
        }

        //update WindCnt2 ...
        if (IsEvenOddAltFillType(edge)) {
            //EvenOdd filling ...
            while (e != &edge) {
                if (e->WindDelta != 0)
                    edge.WindCnt2 = (edge.WindCnt2 == 0 ? 1 : 0);
                e = e->NextInAEL;
            }
        } else {
            //nonZero, Positive or Negative filling ...
            while (e != &edge) {
                edge.WindCnt2 += e->WindDelta;
                e = e->NextInAEL;
            }
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::IsEvenOddFillType(const TEdge& edge) const {
        if (edge.PolyTyp == ptSubject)
            return m_SubjFillType == pftEvenOdd;
        else
            return m_ClipFillType == pftEvenOdd;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::IsEvenOddAltFillType(const TEdge& edge) const {
        if (edge.PolyTyp == ptSubject)
            return m_ClipFillType == pftEvenOdd;
        else
            return m_SubjFillType == pftEvenOdd;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::IsContributing(const TEdge& edge) const {
        PolyFillType pft, pft2;
        if (edge.PolyTyp == ptSubject) {
            pft = m_SubjFillType;
            pft2 = m_ClipFillType;
        } else {
            pft = m_ClipFillType;
            pft2 = m_SubjFillType;
        }

        switch (pft) {
            case pftEvenOdd:
                //return false if a subj line has been flagged as inside a subj polygon
                if (edge.WindDelta == 0 && edge.WindCnt != 1) return false;
                break;
            case pftNonZero:
                if (Abs(edge.WindCnt) != 1) return false;
                break;
            case pftPositive:
                if (edge.WindCnt != 1) return false;
                break;
            default: //pftNegative
                if (edge.WindCnt != -1) return false;
        }

        switch (m_ClipType) {
            case ctIntersection:
                switch (pft2) {
                    case pftEvenOdd:
                    case pftNonZero:
                        return (edge.WindCnt2 != 0);
                    case pftPositive:
                        return (edge.WindCnt2 > 0);
                    default:
                        return (edge.WindCnt2 < 0);
                }
                break;
            case ctUnion:
                switch (pft2) {
                    case pftEvenOdd:
                    case pftNonZero:
                        return (edge.WindCnt2 == 0);
                    case pftPositive:
                        return (edge.WindCnt2 <= 0);
                    default:
                        return (edge.WindCnt2 >= 0);
                }
                break;
            case ctDifference:
                if (edge.PolyTyp == ptSubject)
                    switch (pft2) {
                        case pftEvenOdd:
                        case pftNonZero:
                            return (edge.WindCnt2 == 0);
                        case pftPositive:
                            return (edge.WindCnt2 <= 0);
                        default:
                            return (edge.WindCnt2 >= 0);
                    } else
                    switch (pft2) {
                        case pftEvenOdd:
                        case pftNonZero:
                            return (edge.WindCnt2 != 0);
                        case pftPositive:
                            return (edge.WindCnt2 > 0);
                        default:
                            return (edge.WindCnt2 < 0);
                    }
                break;
            case ctXor:
                if (edge.WindDelta == 0) //XOr always contributing unless open
                    switch (pft2) {
                        case pftEvenOdd:
                        case pftNonZero:
                            return (edge.WindCnt2 == 0);
                        case pftPositive:
                            return (edge.WindCnt2 <= 0);
                        default:
                            return (edge.WindCnt2 >= 0);
                    } else
                    return true;
                break;
            default:
                return true;
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutPt* CLIP_NM_T::Clipper::AddLocalMinPoly(TEdge *e1, TEdge *e2, const IntPoint &Pt) {
        OutPt* result;
        TEdge *e, *prevE;
        if (IsHorizontal(*e2) || (e1->Dx > e2->Dx)) {
            result = AddOutPt(e1, Pt);
            e2->OutIdx = e1->OutIdx;
            e1->Side = esLeft;
            e2->Side = esRight;
            e = e1;
            if (e->PrevInAEL == e2)
                prevE = e2->PrevInAEL;
            else
                prevE = e->PrevInAEL;
        } else {
            result = AddOutPt(e2, Pt);
            e1->OutIdx = e2->OutIdx;
            e1->Side = esRight;
            e2->Side = esLeft;
            e = e2;
            if (e->PrevInAEL == e1)
                prevE = e1->PrevInAEL;
            else
                prevE = e->PrevInAEL;
        }

        if (prevE && prevE->OutIdx >= 0 && prevE->Top.Y < Pt.Y && e->Top.Y < Pt.Y) {
            cInt xPrev = TopX(*prevE, Pt.Y);
            cInt xE = TopX(*e, Pt.Y);
            if (xPrev == xE && (e->WindDelta != 0) && (prevE->WindDelta != 0) &&
                    SlopesEqual(IntPoint(xPrev, Pt.Y), prevE->Top, IntPoint(xE, Pt.Y), e->Top, m_UseFullRange)) {
                OutPt* outPt = AddOutPt(prevE, Pt);
                AddJoin(result, outPt, e->Top);
            }
        }
        return result;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::AddLocalMaxPoly(TEdge *e1, TEdge *e2, const IntPoint &Pt) {
        AddOutPt(e1, Pt);
        if (e2->WindDelta == 0) AddOutPt(e2, Pt);
        if (e1->OutIdx == e2->OutIdx) {
            e1->OutIdx = Unassigned;
            e2->OutIdx = Unassigned;
        } else if (e1->OutIdx < e2->OutIdx)
            AppendPolygon(e1, e2);
        else
            AppendPolygon(e2, e1);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::AddEdgeToSEL(TEdge *edge) {
        //SEL pointers in PEdge are reused to build a list of horizontal edges.
        //However, we don't need to worry about order with horizontal edge processing.
        if (!m_SortedEdges) {
            m_SortedEdges = edge;
            edge->PrevInSEL = 0;
            edge->NextInSEL = 0;
        } else {
            edge->NextInSEL = m_SortedEdges;
            edge->PrevInSEL = 0;
            m_SortedEdges->PrevInSEL = edge;
            m_SortedEdges = edge;
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool CLIP_NM_T::Clipper::PopEdgeFromSEL(TEdge *&edge) {
        if (!m_SortedEdges) return false;
        edge = m_SortedEdges;
        DeleteFromSEL(m_SortedEdges);
        return true;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::CopyAELToSEL() {
        TEdge* e = m_ActiveEdges;
        m_SortedEdges = e;
        while (e) {
            e->PrevInSEL = e->PrevInAEL;
            e->NextInSEL = e->NextInAEL;
            e = e->NextInAEL;
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::AddJoin(OutPt *op1, OutPt *op2, const IntPoint OffPt) {
        Join* j = new Join;
        j->OutPt1 = op1;
        j->OutPt2 = op2;
        j->OffPt = OffPt;
        m_Joins.push_back(j);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::ClearJoins() {
        for (JoinList::size_type i = 0; i < m_Joins.size(); i++)
            delete m_Joins[i];
        m_Joins.resize(0);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::ClearGhostJoins() {
        for (JoinList::size_type i = 0; i < m_GhostJoins.size(); i++)
            delete m_GhostJoins[i];
        m_GhostJoins.resize(0);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::AddGhostJoin(OutPt *op, const IntPoint OffPt) {
        Join* j = new Join;
        j->OutPt1 = op;
        j->OutPt2 = 0;
        j->OffPt = OffPt;
        m_GhostJoins.push_back(j);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::InsertLocalMinimaIntoAEL(const cInt botY) {
        const LocalMinimum *lm;
        while (PopLocalMinima(botY, lm)) {
            TEdge* lb = lm->LeftBound;
            TEdge* rb = lm->RightBound;

            OutPt *Op1 = 0;
            if (!lb) {
                //nb: don't insert LB into either AEL or SEL
                InsertEdgeIntoAEL(rb, 0);
                SetWindingCount(*rb);
                if (IsContributing(*rb))
                    Op1 = AddOutPt(rb, rb->Bot);
            }
            else if (!rb) {
                InsertEdgeIntoAEL(lb, 0);
                SetWindingCount(*lb);
                if (IsContributing(*lb))
                    Op1 = AddOutPt(lb, lb->Bot);
                InsertScanbeam(lb->Top.Y);
            } else {
                InsertEdgeIntoAEL(lb, 0);
                InsertEdgeIntoAEL(rb, lb);
                SetWindingCount(*lb);
                rb->WindCnt = lb->WindCnt;
                rb->WindCnt2 = lb->WindCnt2;
                if (IsContributing(*lb))
                    Op1 = AddLocalMinPoly(lb, rb, lb->Bot);
                InsertScanbeam(lb->Top.Y);
            }

            if (rb) {
                if (IsHorizontal(*rb)) {
                    AddEdgeToSEL(rb);
                    if (rb->NextInLML)
                        InsertScanbeam(rb->NextInLML->Top.Y);
                } else InsertScanbeam(rb->Top.Y);
            }

            if (!lb || !rb) continue;

            //if any output polygons share an edge, they'll need joining later ...
            if (Op1 && IsHorizontal(*rb) &&
                    m_GhostJoins.size() > 0 && (rb->WindDelta != 0)) {
                for (JoinList::size_type i = 0; i < m_GhostJoins.size(); ++i) {
                    Join* jr = m_GhostJoins[i];
                    //if the horizontal Rb and a 'ghost' horizontal overlap, then convert
                    //the 'ghost' join to a real join ready for later ...
                    if (HorzSegmentsOverlap(jr->OutPt1->Pt.X, jr->OffPt.X, rb->Bot.X, rb->Top.X))
                        AddJoin(jr->OutPt1, Op1, jr->OffPt);
                }
            }

            if (lb->OutIdx >= 0 && lb->PrevInAEL &&
                    lb->PrevInAEL->Curr.X == lb->Bot.X &&
                    lb->PrevInAEL->OutIdx >= 0 &&
                    SlopesEqual(lb->PrevInAEL->Bot, lb->PrevInAEL->Top, lb->Curr, lb->Top, m_UseFullRange) &&
                    (lb->WindDelta != 0) && (lb->PrevInAEL->WindDelta != 0)) {
                OutPt *Op2 = AddOutPt(lb->PrevInAEL, lb->Bot);
                AddJoin(Op1, Op2, lb->Top);
            }

            if (lb->NextInAEL != rb) {

                if (rb->OutIdx >= 0 && rb->PrevInAEL->OutIdx >= 0 &&
                        SlopesEqual(rb->PrevInAEL->Curr, rb->PrevInAEL->Top, rb->Curr, rb->Top, m_UseFullRange) &&
                        (rb->WindDelta != 0) && (rb->PrevInAEL->WindDelta != 0)) {
                    OutPt *Op2 = AddOutPt(rb->PrevInAEL, rb->Bot);
                    AddJoin(Op1, Op2, rb->Top);
                }

                TEdge* e = lb->NextInAEL;
                if (e) {
                    while (e != rb) {
                        //nb: For calculating winding counts etc, IntersectEdges() assumes
                        //that param1 will be to the Right of param2 ABOVE the intersection ...
                        IntersectEdges(rb, e, lb->Curr); //order important here
                        e = e->NextInAEL;
                    }
                }
            }

        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::DeleteFromSEL(TEdge *e) {
        TEdge* SelPrev = e->PrevInSEL;
        TEdge* SelNext = e->NextInSEL;
        if (!SelPrev && !SelNext && (e != m_SortedEdges)) return; //already deleted
        if (SelPrev) SelPrev->NextInSEL = SelNext;
        else m_SortedEdges = SelNext;
        if (SelNext) SelNext->PrevInSEL = SelPrev;
        e->NextInSEL = 0;
        e->PrevInSEL = 0;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
#ifdef use_xyz

    void CLIP_NM_T::Clipper::SetZ(IntPoint& pt, TEdge& e1, TEdge& e2) {
        if (pt.Z != 0 || !m_ZFill) return;
        else if (pt == e1.Bot) pt.Z = e1.Bot.Z;
        else if (pt == e1.Top) pt.Z = e1.Top.Z;
        else if (pt == e2.Bot) pt.Z = e2.Bot.Z;
        else if (pt == e2.Top) pt.Z = e2.Top.Z;
        else (*m_ZFill)(e1.Bot, e1.Top, e2.Bot, e2.Top, pt);
    }
    //------------------------------------------------------------------------------
#endif

CLIP_NM_PRE
    void CLIP_NM_T::Clipper::IntersectEdges(TEdge *e1, TEdge *e2, IntPoint &Pt) {
        bool e1Contributing = (e1->OutIdx >= 0);
        bool e2Contributing = (e2->OutIdx >= 0);

#ifdef use_xyz
        SetZ(Pt, *e1, *e2);
#endif

#ifdef use_lines
        //if either edge is on an OPEN path ...
        if (e1->WindDelta == 0 || e2->WindDelta == 0) {
            //ignore subject-subject open path intersections UNLESS they
            //are both open paths, AND they are both 'contributing maximas' ...
            if (e1->WindDelta == 0 && e2->WindDelta == 0) return;

                //if intersecting a subj line with a subj poly ...
            else if (e1->PolyTyp == e2->PolyTyp &&
                    e1->WindDelta != e2->WindDelta && m_ClipType == ctUnion) {
                if (e1->WindDelta == 0) {
                    if (e2Contributing) {
                        AddOutPt(e1, Pt);
                        if (e1Contributing) e1->OutIdx = Unassigned;
                    }
                } else {
                    if (e1Contributing) {
                        AddOutPt(e2, Pt);
                        if (e2Contributing) e2->OutIdx = Unassigned;
                    }
                }
            } else if (e1->PolyTyp != e2->PolyTyp) {
                //toggle subj open path OutIdx on/off when Abs(clip.WndCnt) == 1 ...
                if ((e1->WindDelta == 0) && abs(e2->WindCnt) == 1 &&
                        (m_ClipType != ctUnion || e2->WindCnt2 == 0)) {
                    AddOutPt(e1, Pt);
                    if (e1Contributing) e1->OutIdx = Unassigned;
                } else if ((e2->WindDelta == 0) && (abs(e1->WindCnt) == 1) &&
                        (m_ClipType != ctUnion || e1->WindCnt2 == 0)) {
                    AddOutPt(e2, Pt);
                    if (e2Contributing) e2->OutIdx = Unassigned;
                }
            }
            return;
        }
#endif

        //update winding counts...
        //assumes that e1 will be to the Right of e2 ABOVE the intersection
        if (e1->PolyTyp == e2->PolyTyp) {
            if (IsEvenOddFillType(*e1)) {
                int oldE1WindCnt = e1->WindCnt;
                e1->WindCnt = e2->WindCnt;
                e2->WindCnt = oldE1WindCnt;
            } else {
                if (e1->WindCnt + e2->WindDelta == 0) e1->WindCnt = -e1->WindCnt;
                else e1->WindCnt += e2->WindDelta;
                if (e2->WindCnt - e1->WindDelta == 0) e2->WindCnt = -e2->WindCnt;
                else e2->WindCnt -= e1->WindDelta;
            }
        } else {
            if (!IsEvenOddFillType(*e2)) e1->WindCnt2 += e2->WindDelta;
            else e1->WindCnt2 = (e1->WindCnt2 == 0) ? 1 : 0;
            if (!IsEvenOddFillType(*e1)) e2->WindCnt2 -= e1->WindDelta;
            else e2->WindCnt2 = (e2->WindCnt2 == 0) ? 1 : 0;
        }

        PolyFillType e1FillType, e2FillType, e1FillType2, e2FillType2;
        if (e1->PolyTyp == ptSubject) {
            e1FillType = m_SubjFillType;
            e1FillType2 = m_ClipFillType;
        } else {
            e1FillType = m_ClipFillType;
            e1FillType2 = m_SubjFillType;
        }
        if (e2->PolyTyp == ptSubject) {
            e2FillType = m_SubjFillType;
            e2FillType2 = m_ClipFillType;
        } else {
            e2FillType = m_ClipFillType;
            e2FillType2 = m_SubjFillType;
        }

        cInt e1Wc, e2Wc;
        switch (e1FillType) {
            case pftPositive: e1Wc = e1->WindCnt;
                break;
            case pftNegative: e1Wc = -e1->WindCnt;
                break;
            default: e1Wc = Abs(e1->WindCnt);
        }
        switch (e2FillType) {
            case pftPositive: e2Wc = e2->WindCnt;
                break;
            case pftNegative: e2Wc = -e2->WindCnt;
                break;
            default: e2Wc = Abs(e2->WindCnt);
        }

        if (e1Contributing && e2Contributing) {
            if ((e1Wc != 0 && e1Wc != 1) || (e2Wc != 0 && e2Wc != 1) ||
                    (e1->PolyTyp != e2->PolyTyp && m_ClipType != ctXor)) {
                AddLocalMaxPoly(e1, e2, Pt);
            } else {
                AddOutPt(e1, Pt);
                AddOutPt(e2, Pt);
                SwapSides(*e1, *e2);
                SwapPolyIndexes(*e1, *e2);
            }
        } else if (e1Contributing) {
            if (e2Wc == 0 || e2Wc == 1) {
                AddOutPt(e1, Pt);
                SwapSides(*e1, *e2);
                SwapPolyIndexes(*e1, *e2);
            }
        } else if (e2Contributing) {
            if (e1Wc == 0 || e1Wc == 1) {
                AddOutPt(e2, Pt);
                SwapSides(*e1, *e2);
                SwapPolyIndexes(*e1, *e2);
            }
        }
        else if ((e1Wc == 0 || e1Wc == 1) && (e2Wc == 0 || e2Wc == 1)) {
            //neither edge is currently contributing ...

            cInt e1Wc2, e2Wc2;
            switch (e1FillType2) {
                case pftPositive: e1Wc2 = e1->WindCnt2;
                    break;
                case pftNegative: e1Wc2 = -e1->WindCnt2;
                    break;
                default: e1Wc2 = Abs(e1->WindCnt2);
            }
            switch (e2FillType2) {
                case pftPositive: e2Wc2 = e2->WindCnt2;
                    break;
                case pftNegative: e2Wc2 = -e2->WindCnt2;
                    break;
                default: e2Wc2 = Abs(e2->WindCnt2);
            }

            if (e1->PolyTyp != e2->PolyTyp) {
                AddLocalMinPoly(e1, e2, Pt);
            } else if (e1Wc == 1 && e2Wc == 1)
                switch (m_ClipType) {
                    case ctIntersection:
                        if (e1Wc2 > 0 && e2Wc2 > 0)
                            AddLocalMinPoly(e1, e2, Pt);
                        break;
                    case ctUnion:
                        if (e1Wc2 <= 0 && e2Wc2 <= 0)
                            AddLocalMinPoly(e1, e2, Pt);
                        break;
                    case ctDifference:
                        if (((e1->PolyTyp == ptClip) && (e1Wc2 > 0) && (e2Wc2 > 0)) ||
                                ((e1->PolyTyp == ptSubject) && (e1Wc2 <= 0) && (e2Wc2 <= 0)))
                            AddLocalMinPoly(e1, e2, Pt);
                        break;
                    case ctXor:
                        AddLocalMinPoly(e1, e2, Pt);
                } else
                SwapSides(*e1, *e2);
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::SetHoleState(TEdge *e, OutRec *outrec) {
        TEdge *e2 = e->PrevInAEL;
        TEdge *eTmp = 0;
        while (e2) {
            if (e2->OutIdx >= 0 && e2->WindDelta != 0) {
                if (!eTmp) eTmp = e2;
                else if (eTmp->OutIdx == e2->OutIdx) eTmp = 0;
            }
            e2 = e2->PrevInAEL;
        }
        if (!eTmp) {
            outrec->FirstLeft = 0;
            outrec->IsHole = false;
        } else {
            outrec->FirstLeft = m_PolyOuts[eTmp->OutIdx];
            outrec->IsHole = !outrec->FirstLeft->IsHole;
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutRec* GetLowermostRec(OutRec *outRec1, OutRec *outRec2) {
        //work out which polygon fragment has the correct hole state ...
        if (!outRec1->BottomPt)
            outRec1->BottomPt = GetBottomPt(outRec1->Pts);
        if (!outRec2->BottomPt)
            outRec2->BottomPt = GetBottomPt(outRec2->Pts);
        OutPt *OutPt1 = outRec1->BottomPt;
        OutPt *OutPt2 = outRec2->BottomPt;
        if (OutPt1->Pt.Y > OutPt2->Pt.Y) return outRec1;
        else if (OutPt1->Pt.Y < OutPt2->Pt.Y) return outRec2;
        else if (OutPt1->Pt.X < OutPt2->Pt.X) return outRec1;
        else if (OutPt1->Pt.X > OutPt2->Pt.X) return outRec2;
        else if (OutPt1->Next == OutPt1) return outRec2;
        else if (OutPt2->Next == OutPt2) return outRec1;
        else if (FirstIsBottomPt(OutPt1, OutPt2)) return outRec1;
        else return outRec2;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    bool OutRec1RightOfOutRec2(OutRec* outRec1, OutRec* outRec2) {
        do {
            outRec1 = outRec1->FirstLeft;
            if (outRec1 == outRec2) return true;
        } while (outRec1);
        return false;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutRec* CLIP_NM_T::Clipper::GetOutRec(int Idx) {
        OutRec* outrec = m_PolyOuts[Idx];
        while (outrec != m_PolyOuts[outrec->Idx])
            outrec = m_PolyOuts[outrec->Idx];
        return outrec;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::AppendPolygon(TEdge *e1, TEdge *e2) {
        //get the start and ends of both output polygons ...
        OutRec *outRec1 = m_PolyOuts[e1->OutIdx];
        OutRec *outRec2 = m_PolyOuts[e2->OutIdx];

        OutRec *holeStateRec;
        if (OutRec1RightOfOutRec2(outRec1, outRec2))
            holeStateRec = outRec2;
        else if (OutRec1RightOfOutRec2(outRec2, outRec1))
            holeStateRec = outRec1;
        else
            holeStateRec = GetLowermostRec(outRec1, outRec2);

        //get the start and ends of both output polygons and
        //join e2 poly onto e1 poly and delete pointers to e2 ...

        OutPt* p1_lft = outRec1->Pts;
        OutPt* p1_rt = p1_lft->Prev;
        OutPt* p2_lft = outRec2->Pts;
        OutPt* p2_rt = p2_lft->Prev;

        //join e2 poly onto e1 poly and delete pointers to e2 ...
        if (e1->Side == esLeft) {
            if (e2->Side == esLeft) {
                //z y x a b c
                ReversePolyPtLinks(p2_lft);
                p2_lft->Next = p1_lft;
                p1_lft->Prev = p2_lft;
                p1_rt->Next = p2_rt;
                p2_rt->Prev = p1_rt;
                outRec1->Pts = p2_rt;
            } else {
                //x y z a b c
                p2_rt->Next = p1_lft;
                p1_lft->Prev = p2_rt;
                p2_lft->Prev = p1_rt;
                p1_rt->Next = p2_lft;
                outRec1->Pts = p2_lft;
            }
        } else {
            if (e2->Side == esRight) {
                //a b c z y x
                ReversePolyPtLinks(p2_lft);
                p1_rt->Next = p2_rt;
                p2_rt->Prev = p1_rt;
                p2_lft->Next = p1_lft;
                p1_lft->Prev = p2_lft;
            } else {
                //a b c x y z
                p1_rt->Next = p2_lft;
                p2_lft->Prev = p1_rt;
                p1_lft->Prev = p2_rt;
                p2_rt->Next = p1_lft;
            }
        }

        outRec1->BottomPt = 0;
        if (holeStateRec == outRec2) {
            if (outRec2->FirstLeft != outRec1)
                outRec1->FirstLeft = outRec2->FirstLeft;
            outRec1->IsHole = outRec2->IsHole;
        }
        outRec2->Pts = 0;
        outRec2->BottomPt = 0;
        outRec2->FirstLeft = outRec1;

        int OKIdx = e1->OutIdx;
        int ObsoleteIdx = e2->OutIdx;

        e1->OutIdx = Unassigned; //nb: safe because we only get here via AddLocalMaxPoly
        e2->OutIdx = Unassigned;

        TEdge* e = m_ActiveEdges;
        while (e) {
            if (e->OutIdx == ObsoleteIdx) {
                e->OutIdx = OKIdx;
                e->Side = e1->Side;
                break;
            }
            e = e->NextInAEL;
        }

        outRec2->Idx = outRec1->Idx;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutPt* CLIP_NM_T::Clipper::AddOutPt(TEdge *e, const IntPoint &pt) {
        if (e->OutIdx < 0) {
            OutRec *outRec = CreateOutRec();
            outRec->IsOpen = (e->WindDelta == 0);
            OutPt* newOp = new OutPt;
            outRec->Pts = newOp;
            newOp->Idx = outRec->Idx;
            newOp->Pt = pt;
            newOp->Next = newOp;
            newOp->Prev = newOp;
            if (!outRec->IsOpen)
                SetHoleState(e, outRec);
            e->OutIdx = outRec->Idx;
            return newOp;
        } else {
            OutRec *outRec = m_PolyOuts[e->OutIdx];
            //OutRec.Pts is the 'Left-most' point & OutRec.Pts.Prev is the 'Right-most'
            OutPt* op = outRec->Pts;

            bool ToFront = (e->Side == esLeft);
            if (ToFront && (pt == op->Pt)) return op;
            else if (!ToFront && (pt == op->Prev->Pt)) return op->Prev;

            OutPt* newOp = new OutPt;
            newOp->Idx = outRec->Idx;
            newOp->Pt = pt;
            newOp->Next = op;
            newOp->Prev = op->Prev;
            newOp->Prev->Next = newOp;
            op->Prev = newOp;
            if (ToFront) outRec->Pts = newOp;
            return newOp;
        }
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    OutPt* CLIP_NM_T::Clipper::GetLastOutPt(TEdge *e) {
        OutRec *outRec = m_PolyOuts[e->OutIdx];
        if (e->Side == esLeft)
            return outRec->Pts;
        else
            return outRec->Pts->Prev;
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
    void CLIP_NM_T::Clipper::ProcessHorizontals() {
        TEdge* horzEdge;
        while (PopEdgeFromSEL(horzEdge))
            ProcessHorizontal(horzEdge);
    }
    //------------------------------------------------------------------------------
CLIP_NM_PRE
#endif /* NPOLYGON_CLIP_MMPL_H */
