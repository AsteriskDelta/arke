//#ifndef NPOLYGON_CLIP_IMPL_H
//#define NPOLYGON_CLIP_IMPL_H
/*
#include <cmath>
#include <vector>
#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <cstdlib>
#include <ostream>
#include <functional>

namespace ClipperLib {
*/
    static double constexpr pi = 3.141592653589793238;
    static double constexpr two_pi = pi * 2;
    static double constexpr def_arc_tolerance = 0.25;

    enum Direction {
        dRightToLeft, dLeftToRight
    };

    static int constexpr Unassigned = -1; //edge not currently 'owning' a solution
    static int constexpr Skip = -2; //edge that would otherwise close a path

#define HORIZONTAL (-1.0E+40)
#define TOLERANCE (1.0e-20)
#define NEAR_ZERO(val) (((val) > -TOLERANCE) && ((val) < TOLERANCE))

    struct TEdge {
        IntPoint Bot;
        IntPoint Curr; //current (updated for every new scanbeam)
        IntPoint Top;
        double Dx;
        PolyType PolyTyp;
        EdgeSide Side; //side only refers to current side of solution poly
        int WindDelta; //1 or -1 depending on winding direction
        int WindCnt;
        int WindCnt2; //winding count of the opposite polytype
        int OutIdx;
        TEdge *Next;
        TEdge *Prev;
        TEdge *NextInLML;
        TEdge *NextInAEL;
        TEdge *PrevInAEL;
        TEdge *NextInSEL;
        TEdge *PrevInSEL;
    };

    struct IntersectNode {
        TEdge *Edge1;
        TEdge *Edge2;
        IntPoint Pt;
    };

    struct LocalMinimum {
        cInt Y;
        TEdge *LeftBound;
        TEdge *RightBound;
    };

    struct OutPt;

    //OutRec: contains a path in the clipping solution. Edges in the AEL will
    //carry a pointer to an OutRec when they are part of the clipping solution.

    struct OutRec {
        int Idx;
        bool IsHole;
        bool IsOpen;
        OutRec *FirstLeft; //see comments in clipper.pas
        PolyNode *PolyNd;
        OutPt *Pts;
        OutPt *BottomPt;
    };

    struct OutPt {
        int Idx;
        IntPoint Pt;
        OutPt *Next;
        OutPt *Prev;
    };

    struct Join {
        OutPt *OutPt1;
        OutPt *OutPt2;
        IntPoint OffPt;
    };

    struct LocMinSorter {

        inline bool operator()(const LocalMinimum& locMin1, const LocalMinimum& locMin2) {
            return locMin2.Y < locMin1.Y;
        }
    };

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------

    inline static cInt Round(double val) {
        if ((val < 0)) return static_cast<cInt> (val - 0.5);
        else return static_cast<cInt> (val + 0.5);
    }
    //------------------------------------------------------------------------------

    inline static cInt Abs(cInt val) {
        return val < 0 ? -val : val;
    }


    
#ifndef use_int32

    //------------------------------------------------------------------------------
    // Int128 class (enables safe math on signed 64bit integers)
    // eg Int128 val1((long64)9223372036854775807); //ie 2^63 -1
    //    Int128 val2((long64)9223372036854775807);
    //    Int128 val3 = val1 * val2;
    //    val3.AsString => "85070591730234615847396907784232501249" (8.5e+37)
    //------------------------------------------------------------------------------

    class Int128 {
    public:
        ulong64 lo;
        long64 hi;

        Int128(long64 _lo = 0) {
            lo = (ulong64) _lo;
            if (_lo < 0) hi = -1;
            else hi = 0;
        }

        Int128(const Int128 &val) : lo(val.lo), hi(val.hi) {
        }

        Int128(const long64& _hi, const ulong64& _lo) : lo(_lo), hi(_hi) {
        }

        Int128& operator=(const long64 &val) {
            lo = (ulong64) val;
            if (val < 0) hi = -1;
            else hi = 0;
            return *this;
        }

        bool operator==(const Int128 &val) const {
            return (hi == val.hi && lo == val.lo);
        }

        bool operator!=(const Int128 &val) const {
            return !(*this == val);
        }

        bool operator>(const Int128 &val) const {
            if (hi != val.hi)
                return hi > val.hi;
            else
                return lo > val.lo;
        }

        bool operator<(const Int128 &val) const {
            if (hi != val.hi)
                return hi < val.hi;
            else
                return lo < val.lo;
        }

        bool operator>=(const Int128 &val) const {
            return !(*this < val);
        }

        bool operator<=(const Int128 &val) const {
            return !(*this > val);
        }

        Int128& operator+=(const Int128 &rhs) {
            hi += rhs.hi;
            lo += rhs.lo;
            if (lo < rhs.lo) hi++;
            return *this;
        }

        Int128 operator+(const Int128 &rhs) const {
            Int128 result(*this);
            result += rhs;
            return result;
        }

        Int128& operator-=(const Int128 &rhs) {
            *this += -rhs;
            return *this;
        }

        Int128 operator-(const Int128 &rhs) const {
            Int128 result(*this);
            result -= rhs;
            return result;
        }

        Int128 operator-() const //unary negation
        {
            if (lo == 0)
                return Int128(-hi, 0);
            else
                return Int128(~hi, ~lo + 1);
        }

        operator double() const {
            const double shift64 = 18446744073709551616.0; //2^64
            if (hi < 0) {
                if (lo == 0) return (double) hi * shift64;
                else return -(double) (~lo + ~hi * shift64);
            } else
                return (double) (lo + hi * shift64);
        }

    };
    //------------------------------------------------------------------------------

    static Int128 Int128Mul(long64 lhs, long64 rhs) {
        bool negate = (lhs < 0) != (rhs < 0);

        if (lhs < 0) lhs = -lhs;
        ulong64 int1Hi = ulong64(lhs) >> 32;
        ulong64 int1Lo = ulong64(lhs & 0xFFFFFFFF);

        if (rhs < 0) rhs = -rhs;
        ulong64 int2Hi = ulong64(rhs) >> 32;
        ulong64 int2Lo = ulong64(rhs & 0xFFFFFFFF);

        //nb: see comments in clipper.pas
        ulong64 a = int1Hi * int2Hi;
        ulong64 b = int1Lo * int2Lo;
        ulong64 c = int1Hi * int2Lo + int1Lo * int2Hi;

        Int128 tmp;
        tmp.hi = long64(a + (c >> 32));
        tmp.lo = long64(c << 32);
        tmp.lo += long64(b);
        if (tmp.lo < b) tmp.hi++;
        if (negate) tmp = -tmp;
        return tmp;
    };
#endif

    //------------------------------------------------------------------------------
    // Miscellaneous global functions
    //------------------------------------------------------------------------------

    static bool Orientation(const Path &poly) {
        return Area(poly) >= 0;
    }
    //------------------------------------------------------------------------------

    static double Area(const Path &poly) {
        int size = (int) poly.size();
        if (size < 3) return 0;

        double a = 0;
        for (int i = 0, j = size - 1; i < size; ++i) {
            a += ((double) poly[j].X + poly[i].X) * ((double) poly[j].Y - poly[i].Y);
            j = i;
        }
        return -a * 0.5;
    }
    //------------------------------------------------------------------------------

    static double Area(const OutPt *op) {
        const OutPt *startOp = op;
        if (!op) return 0;
        double a = 0;
        do {
            a += (double) (op->Prev->Pt.X + op->Pt.X) * (double) (op->Prev->Pt.Y - op->Pt.Y);
            op = op->Next;
        } while (op != startOp);
        return a * 0.5;
    }
    //------------------------------------------------------------------------------

    static double Area(const OutRec &outRec) {
        return Area(outRec.Pts);
    }
    //------------------------------------------------------------------------------

    static bool PointIsVertex(const IntPoint &Pt, OutPt *pp) {
        OutPt *pp2 = pp;
        do {
            if (pp2->Pt == Pt) return true;
            pp2 = pp2->Next;
        }        while (pp2 != pp);
        return false;
    }
    //------------------------------------------------------------------------------

    //See "The Point in Polygon Problem for Arbitrary Polygons" by Hormann & Agathos
    //http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.88.5498&rep=rep1&type=pdf

    static int PointInPolygon(const IntPoint &pt, const Path &path) {
        //returns 0 if false, +1 if true, -1 if pt ON polygon boundary
        int result = 0;
        size_t cnt = path.size();
        if (cnt < 3) return 0;
        IntPoint ip = path[0];
        for (size_t i = 1; i <= cnt; ++i) {
            IntPoint ipNext = (i == cnt ? path[0] : path[i]);
            if (ipNext.Y == pt.Y) {
                if ((ipNext.X == pt.X) || (ip.Y == pt.Y &&
                        ((ipNext.X > pt.X) == (ip.X < pt.X)))) return -1;
            }
            if ((ip.Y < pt.Y) != (ipNext.Y < pt.Y)) {
                if (ip.X >= pt.X) {
                    if (ipNext.X > pt.X) result = 1 - result;
                    else {
                        double d = (double) (ip.X - pt.X) * (ipNext.Y - pt.Y) -
                                (double) (ipNext.X - pt.X) * (ip.Y - pt.Y);
                        if (!d) return -1;
                        if ((d > 0) == (ipNext.Y > ip.Y)) result = 1 - result;
                    }
                } else {
                    if (ipNext.X > pt.X) {
                        double d = (double) (ip.X - pt.X) * (ipNext.Y - pt.Y) -
                                (double) (ipNext.X - pt.X) * (ip.Y - pt.Y);
                        if (!d) return -1;
                        if ((d > 0) == (ipNext.Y > ip.Y)) result = 1 - result;
                    }
                }
            }
            ip = ipNext;
        }
        return result;
    }
    //------------------------------------------------------------------------------

    static int PointInPolygon(const IntPoint &pt, OutPt *op) {
        //returns 0 if false, +1 if true, -1 if pt ON polygon boundary
        int result = 0;
        OutPt* startOp = op;
        for (;;) {
            if (op->Next->Pt.Y == pt.Y) {
                if ((op->Next->Pt.X == pt.X) || (op->Pt.Y == pt.Y &&
                        ((op->Next->Pt.X > pt.X) == (op->Pt.X < pt.X)))) return -1;
            }
            if ((op->Pt.Y < pt.Y) != (op->Next->Pt.Y < pt.Y)) {
                if (op->Pt.X >= pt.X) {
                    if (op->Next->Pt.X > pt.X) result = 1 - result;
                    else {
                        double d = (double) (op->Pt.X - pt.X) * (op->Next->Pt.Y - pt.Y) -
                                (double) (op->Next->Pt.X - pt.X) * (op->Pt.Y - pt.Y);
                        if (!d) return -1;
                        if ((d > 0) == (op->Next->Pt.Y > op->Pt.Y)) result = 1 - result;
                    }
                } else {
                    if (op->Next->Pt.X > pt.X) {
                        double d = (double) (op->Pt.X - pt.X) * (op->Next->Pt.Y - pt.Y) -
                                (double) (op->Next->Pt.X - pt.X) * (op->Pt.Y - pt.Y);
                        if (!d) return -1;
                        if ((d > 0) == (op->Next->Pt.Y > op->Pt.Y)) result = 1 - result;
                    }
                }
            }
            op = op->Next;
            if (startOp == op) break;
        }
        return result;
    }
    //------------------------------------------------------------------------------

    static bool Poly2ContainsPoly1(OutPt *OutPt1, OutPt *OutPt2) {
        OutPt* op = OutPt1;
        do {
            //nb: PointInPolygon returns 0 if false, +1 if true, -1 if pt on polygon
            int res = PointInPolygon(op->Pt, OutPt2);
            if (res >= 0) return res > 0;
            op = op->Next;
        }        while (op != OutPt1);
        return true;
    }
    //----------------------------------------------------------------------

    static bool SlopesEqual(const TEdge &e1, const TEdge &e2, bool UseFullInt64Range) {
#ifndef use_int32
        if (UseFullInt64Range)
            return Int128Mul(e1.Top.Y - e1.Bot.Y, e2.Top.X - e2.Bot.X) ==
            Int128Mul(e1.Top.X - e1.Bot.X, e2.Top.Y - e2.Bot.Y);
        else
#endif
            return (e1.Top.Y - e1.Bot.Y) * (e2.Top.X - e2.Bot.X) ==
            (e1.Top.X - e1.Bot.X) * (e2.Top.Y - e2.Bot.Y);
    }
    //------------------------------------------------------------------------------

    static bool SlopesEqual(const IntPoint pt1, const IntPoint pt2,
            const IntPoint pt3, bool UseFullInt64Range) {
#ifndef use_int32
        if (UseFullInt64Range)
            return Int128Mul(pt1.Y - pt2.Y, pt2.X - pt3.X) == Int128Mul(pt1.X - pt2.X, pt2.Y - pt3.Y);
        else
#endif
            return (pt1.Y - pt2.Y)*(pt2.X - pt3.X) == (pt1.X - pt2.X)*(pt2.Y - pt3.Y);
    }
    //------------------------------------------------------------------------------

    static bool SlopesEqual(const IntPoint pt1, const IntPoint pt2,
            const IntPoint pt3, const IntPoint pt4, bool UseFullInt64Range) {
#ifndef use_int32
        if (UseFullInt64Range)
            return Int128Mul(pt1.Y - pt2.Y, pt3.X - pt4.X) == Int128Mul(pt1.X - pt2.X, pt3.Y - pt4.Y);
        else
#endif
            return (pt1.Y - pt2.Y)*(pt3.X - pt4.X) == (pt1.X - pt2.X)*(pt3.Y - pt4.Y);
    }
    //------------------------------------------------------------------------------

    inline static bool IsHorizontal(TEdge &e) {
        return e.Dx == HORIZONTAL;
    }
    //------------------------------------------------------------------------------

    inline static double GetDx(const IntPoint pt1, const IntPoint pt2) {
        return (pt1.Y == pt2.Y) ?
                HORIZONTAL : (double) (pt2.X - pt1.X) / (pt2.Y - pt1.Y);
    }
    //---------------------------------------------------------------------------

    inline static void SetDx(TEdge &e) {
        cInt dy = (e.Top.Y - e.Bot.Y);
        if (dy == 0) e.Dx = HORIZONTAL;
        else e.Dx = (double) (e.Top.X - e.Bot.X) / dy;
    }
    //---------------------------------------------------------------------------

    inline static void SwapSides(TEdge &Edge1, TEdge &Edge2) {
        EdgeSide Side = Edge1.Side;
        Edge1.Side = Edge2.Side;
        Edge2.Side = Side;
    }
    //------------------------------------------------------------------------------

    inline static void SwapPolyIndexes(TEdge &Edge1, TEdge &Edge2) {
        int OutIdx = Edge1.OutIdx;
        Edge1.OutIdx = Edge2.OutIdx;
        Edge2.OutIdx = OutIdx;
    }
    //------------------------------------------------------------------------------

    inline static cInt TopX(TEdge &edge, const cInt currentY) {
        return ( currentY == edge.Top.Y) ?
                edge.Top.X : edge.Bot.X + Round(edge.Dx * (currentY - edge.Bot.Y));
    }
    //------------------------------------------------------------------------------

    static void IntersectPoint(TEdge &Edge1, TEdge &Edge2, IntPoint &ip) {
#ifdef use_xyz  
        ip.Z = 0;
#endif

        double b1, b2;
        if (Edge1.Dx == Edge2.Dx) {
            ip.Y = Edge1.Curr.Y;
            ip.X = TopX(Edge1, ip.Y);
            return;
        } else if (Edge1.Dx == 0) {
            ip.X = Edge1.Bot.X;
            if (IsHorizontal(Edge2))
                ip.Y = Edge2.Bot.Y;
            else {
                b2 = Edge2.Bot.Y - (Edge2.Bot.X / Edge2.Dx);
                ip.Y = Round(ip.X / Edge2.Dx + b2);
            }
        } else if (Edge2.Dx == 0) {
            ip.X = Edge2.Bot.X;
            if (IsHorizontal(Edge1))
                ip.Y = Edge1.Bot.Y;
            else {
                b1 = Edge1.Bot.Y - (Edge1.Bot.X / Edge1.Dx);
                ip.Y = Round(ip.X / Edge1.Dx + b1);
            }
        }
        else {
            b1 = Edge1.Bot.X - Edge1.Bot.Y * Edge1.Dx;
            b2 = Edge2.Bot.X - Edge2.Bot.Y * Edge2.Dx;
            double q = (b2 - b1) / (Edge1.Dx - Edge2.Dx);
            ip.Y = Round(q);
            if (std::fabs(Edge1.Dx) < std::fabs(Edge2.Dx))
                ip.X = Round(Edge1.Dx * q + b1);
            else
                ip.X = Round(Edge2.Dx * q + b2);
        }

        if (ip.Y < Edge1.Top.Y || ip.Y < Edge2.Top.Y) {
            if (Edge1.Top.Y > Edge2.Top.Y)
                ip.Y = Edge1.Top.Y;
            else
                ip.Y = Edge2.Top.Y;
            if (std::fabs(Edge1.Dx) < std::fabs(Edge2.Dx))
                ip.X = TopX(Edge1, ip.Y);
            else
                ip.X = TopX(Edge2, ip.Y);
        }
        //finally, don't allow 'ip' to be BELOW curr.Y (ie bottom of scanbeam) ...
        if (ip.Y > Edge1.Curr.Y) {
            ip.Y = Edge1.Curr.Y;
            //use the more vertical edge to derive X ...
            if (std::fabs(Edge1.Dx) > std::fabs(Edge2.Dx))
                ip.X = TopX(Edge2, ip.Y);
            else
                ip.X = TopX(Edge1, ip.Y);
        }
    }
    //------------------------------------------------------------------------------

    static void ReversePolyPtLinks(OutPt *pp) {
        if (!pp) return;
        OutPt *pp1, *pp2;
        pp1 = pp;
        do {
            pp2 = pp1->Next;
            pp1->Next = pp1->Prev;
            pp1->Prev = pp2;
            pp1 = pp2;
        } while (pp1 != pp);
    }
    //------------------------------------------------------------------------------

    static void DisposeOutPts(OutPt*& pp) {
        if (pp == 0) return;
        pp->Prev->Next = 0;
        while (pp) {
            OutPt *tmpPp = pp;
            pp = pp->Next;
            delete tmpPp;
        }
    }
    //------------------------------------------------------------------------------

    inline static  void InitEdge(TEdge* e, TEdge* eNext, TEdge* ePrev, const IntPoint& Pt) {
        std::memset(e, 0, sizeof (TEdge));
        e->Next = eNext;
        e->Prev = ePrev;
        e->Curr = Pt;
        e->OutIdx = Unassigned;
    }
    //------------------------------------------------------------------------------

    static void InitEdge2(TEdge& e, PolyType Pt) {
        if (e.Curr.Y >= e.Next->Curr.Y) {
            e.Bot = e.Curr;
            e.Top = e.Next->Curr;
        } else {
            e.Top = e.Curr;
            e.Bot = e.Next->Curr;
        }
        SetDx(e);
        e.PolyTyp = Pt;
    }
    //------------------------------------------------------------------------------

    static TEdge* RemoveEdge(TEdge* e) {
        //removes e from double_linked_list (but without removing from memory)
        e->Prev->Next = e->Next;
        e->Next->Prev = e->Prev;
        TEdge* result = e->Next;
        e->Prev = 0; //flag as removed (see ClipperBase.Clear)
        return result;
    }
    //------------------------------------------------------------------------------

    inline static void ReverseHorizontal(TEdge &e) {
        //swap horizontal edges' Top and Bottom x's so they follow the natural
        //progression of the bounds - ie so their xbots will align with the
        //adjoining lower edge. [Helpful in the ProcessHorizontal() method.]
        std::swap(e.Top.X, e.Bot.X);
#ifdef use_xyz  
        std::swap(e.Top.Z, e.Bot.Z);
#endif
    }
    //------------------------------------------------------------------------------

    static void SwapPoints(IntPoint &pt1, IntPoint &pt2) {
        IntPoint tmp = pt1;
        pt1 = pt2;
        pt2 = tmp;
    }
    //------------------------------------------------------------------------------

    static bool GetOverlapSegment(IntPoint pt1a, IntPoint pt1b, IntPoint pt2a,
            IntPoint pt2b, IntPoint &pt1, IntPoint &pt2) {
        //precondition: segments are Collinear.
        if (Abs(pt1a.X - pt1b.X) > Abs(pt1a.Y - pt1b.Y)) {
            if (pt1a.X > pt1b.X) SwapPoints(pt1a, pt1b);
            if (pt2a.X > pt2b.X) SwapPoints(pt2a, pt2b);
            if (pt1a.X > pt2a.X) pt1 = pt1a;
            else pt1 = pt2a;
            if (pt1b.X < pt2b.X) pt2 = pt1b;
            else pt2 = pt2b;
            return pt1.X < pt2.X;
        } else {
            if (pt1a.Y < pt1b.Y) SwapPoints(pt1a, pt1b);
            if (pt2a.Y < pt2b.Y) SwapPoints(pt2a, pt2b);
            if (pt1a.Y < pt2a.Y) pt1 = pt1a;
            else pt1 = pt2a;
            if (pt1b.Y > pt2b.Y) pt2 = pt1b;
            else pt2 = pt2b;
            return pt1.Y > pt2.Y;
        }
    }
    //------------------------------------------------------------------------------

    static bool FirstIsBottomPt(const OutPt* btmPt1, const OutPt* btmPt2) {
        OutPt *p = btmPt1->Prev;
        while ((p->Pt == btmPt1->Pt) && (p != btmPt1)) p = p->Prev;
        double dx1p = std::fabs(GetDx(btmPt1->Pt, p->Pt));
        p = btmPt1->Next;
        while ((p->Pt == btmPt1->Pt) && (p != btmPt1)) p = p->Next;
        double dx1n = std::fabs(GetDx(btmPt1->Pt, p->Pt));

        p = btmPt2->Prev;
        while ((p->Pt == btmPt2->Pt) && (p != btmPt2)) p = p->Prev;
        double dx2p = std::fabs(GetDx(btmPt2->Pt, p->Pt));
        p = btmPt2->Next;
        while ((p->Pt == btmPt2->Pt) && (p != btmPt2)) p = p->Next;
        double dx2n = std::fabs(GetDx(btmPt2->Pt, p->Pt));

        if (std::max(dx1p, dx1n) == std::max(dx2p, dx2n) &&
                std::min(dx1p, dx1n) == std::min(dx2p, dx2n))
            return Area(btmPt1) > 0; //if otherwise identical use orientation
        else
            return (dx1p >= dx2p && dx1p >= dx2n) || (dx1n >= dx2p && dx1n >= dx2n);
    }
    //------------------------------------------------------------------------------

    static OutPt* GetBottomPt(OutPt *pp) {
        OutPt* dups = 0;
        OutPt* p = pp->Next;
        while (p != pp) {
            if (p->Pt.Y > pp->Pt.Y) {
                pp = p;
                dups = 0;
            } else if (p->Pt.Y == pp->Pt.Y && p->Pt.X <= pp->Pt.X) {
                if (p->Pt.X < pp->Pt.X) {
                    dups = 0;
                    pp = p;
                } else {
                    if (p->Next != pp && p->Prev != pp) dups = p;
                }
            }
            p = p->Next;
        }
        if (dups) {
            //there appears to be at least 2 vertices at BottomPt so ...
            while (dups != p) {
                if (!FirstIsBottomPt(p, dups)) pp = dups;
                dups = dups->Next;
                while (dups->Pt != pp->Pt) dups = dups->Next;
            }
        }
        return pp;
    }
    //------------------------------------------------------------------------------

    static bool Pt2IsBetweenPt1AndPt3(const IntPoint pt1,
            const IntPoint pt2, const IntPoint pt3) {
        if ((pt1 == pt3) || (pt1 == pt2) || (pt3 == pt2))
            return false;
        else if (pt1.X != pt3.X)
            return (pt2.X > pt1.X) == (pt2.X < pt3.X);
        else
            return (pt2.Y > pt1.Y) == (pt2.Y < pt3.Y);
    }
    //------------------------------------------------------------------------------

    static bool HorzSegmentsOverlap(cInt seg1a, cInt seg1b, cInt seg2a, cInt seg2b) {
        if (seg1a > seg1b) std::swap(seg1a, seg1b);
        if (seg2a > seg2b) std::swap(seg2a, seg2b);
        return (seg1a < seg2b) && (seg2a < seg1b);
    }

    inline bool IsMinima(TEdge *e) {
        return e && (e->Prev->NextInLML != e) && (e->Next->NextInLML != e);
    }
    //------------------------------------------------------------------------------

    inline bool IsMaxima(TEdge *e, const cInt Y) {
        return e && e->Top.Y == Y && !e->NextInLML;
    }
    //------------------------------------------------------------------------------

    inline bool IsIntermediate(TEdge *e, const cInt Y) {
        return e->Top.Y == Y && e->NextInLML;
    }
    //------------------------------------------------------------------------------

    TEdge *GetMaximaPair(TEdge *e) {
        if ((e->Next->Top == e->Top) && !e->Next->NextInLML)
            return e->Next;
        else if ((e->Prev->Top == e->Top) && !e->Prev->NextInLML)
            return e->Prev;
        else return 0;
    }
    //------------------------------------------------------------------------------

    TEdge *GetMaximaPairEx(TEdge *e) {
        //as GetMaximaPair() but returns 0 if MaxPair isn't in AEL (unless it's horizontal)
        TEdge* result = GetMaximaPair(e);
        if (result && (result->OutIdx == Skip ||
                (result->NextInAEL == result->PrevInAEL && !IsHorizontal(*result)))) return 0;
        return result;
    }
    //------------------------------------------------------------------------------

    void Clipper::SwapPositionsInSEL(TEdge *Edge1, TEdge *Edge2) {
        if (!(Edge1->NextInSEL) && !(Edge1->PrevInSEL)) return;
        if (!(Edge2->NextInSEL) && !(Edge2->PrevInSEL)) return;

        if (Edge1->NextInSEL == Edge2) {
            TEdge* Next = Edge2->NextInSEL;
            if (Next) Next->PrevInSEL = Edge1;
            TEdge* Prev = Edge1->PrevInSEL;
            if (Prev) Prev->NextInSEL = Edge2;
            Edge2->PrevInSEL = Prev;
            Edge2->NextInSEL = Edge1;
            Edge1->PrevInSEL = Edge2;
            Edge1->NextInSEL = Next;
        } else if (Edge2->NextInSEL == Edge1) {
            TEdge* Next = Edge1->NextInSEL;
            if (Next) Next->PrevInSEL = Edge2;
            TEdge* Prev = Edge2->PrevInSEL;
            if (Prev) Prev->NextInSEL = Edge1;
            Edge1->PrevInSEL = Prev;
            Edge1->NextInSEL = Edge2;
            Edge2->PrevInSEL = Edge1;
            Edge2->NextInSEL = Next;
        } else {
            TEdge* Next = Edge1->NextInSEL;
            TEdge* Prev = Edge1->PrevInSEL;
            Edge1->NextInSEL = Edge2->NextInSEL;
            if (Edge1->NextInSEL) Edge1->NextInSEL->PrevInSEL = Edge1;
            Edge1->PrevInSEL = Edge2->PrevInSEL;
            if (Edge1->PrevInSEL) Edge1->PrevInSEL->NextInSEL = Edge1;
            Edge2->NextInSEL = Next;
            if (Edge2->NextInSEL) Edge2->NextInSEL->PrevInSEL = Edge2;
            Edge2->PrevInSEL = Prev;
            if (Edge2->PrevInSEL) Edge2->PrevInSEL->NextInSEL = Edge2;
        }

        if (!Edge1->PrevInSEL) m_SortedEdges = Edge1;
        else if (!Edge2->PrevInSEL) m_SortedEdges = Edge2;
    }
    //------------------------------------------------------------------------------

    TEdge* GetNextInAEL(TEdge *e, Direction dir) {
        return dir == dLeftToRight ? e->NextInAEL : e->PrevInAEL;
    }
    //------------------------------------------------------------------------------

    void GetHorzDirection(TEdge& HorzEdge, Direction& Dir, cInt& Left, cInt& Right) {
        if (HorzEdge.Bot.X < HorzEdge.Top.X) {
            Left = HorzEdge.Bot.X;
            Right = HorzEdge.Top.X;
            Dir = dLeftToRight;
        } else {
            Left = HorzEdge.Top.X;
            Right = HorzEdge.Bot.X;
            Dir = dRightToLeft;
        }
    }
    //------------------------------------------------------------------------

    /*******************************************************************************
     * Notes: Horizontal edges (HEs) at scanline intersections (ie at the Top or    *
     * Bottom of a scanbeam) are processed as if layered. The order in which HEs    *
     * are processed doesn't matter. HEs intersect with other HE Bot.Xs only [#]    *
     * (or they could intersect with Top.Xs only, ie EITHER Bot.Xs OR Top.Xs),      *
     * and with other non-horizontal edges [*]. Once these intersections are        *
     * processed, intermediate HEs then 'promote' the Edge above (NextInLML) into   *
     * the AEL. These 'promoted' edges may in turn intersect [%] with other HEs.    *
     *******************************************************************************/

    void Clipper::ProcessHorizontal(TEdge *horzEdge) {
        Direction dir;
        cInt horzLeft, horzRight;
        bool IsOpen = (horzEdge->WindDelta == 0);

        GetHorzDirection(*horzEdge, dir, horzLeft, horzRight);

        TEdge* eLastHorz = horzEdge, *eMaxPair = 0;
        while (eLastHorz->NextInLML && IsHorizontal(*eLastHorz->NextInLML))
            eLastHorz = eLastHorz->NextInLML;
        if (!eLastHorz->NextInLML)
            eMaxPair = GetMaximaPair(eLastHorz);

        MaximaList::const_iterator maxIt;
        MaximaList::const_reverse_iterator maxRit;
        if (m_Maxima.size() > 0) {
            //get the first maxima in range (X) ...
            if (dir == dLeftToRight) {
                maxIt = m_Maxima.begin();
                while (maxIt != m_Maxima.end() && *maxIt <= horzEdge->Bot.X) maxIt++;
                if (maxIt != m_Maxima.end() && *maxIt >= eLastHorz->Top.X)
                    maxIt = m_Maxima.end();
            } else {
                maxRit = m_Maxima.rbegin();
                while (maxRit != m_Maxima.rend() && *maxRit > horzEdge->Bot.X) maxRit++;
                if (maxRit != m_Maxima.rend() && *maxRit <= eLastHorz->Top.X)
                    maxRit = m_Maxima.rend();
            }
        }

        OutPt* op1 = 0;

        for (;;) //loop through consec. horizontal edges
        {

            bool IsLastHorz = (horzEdge == eLastHorz);
            TEdge* e = GetNextInAEL(horzEdge, dir);
            while (e) {

                //this code block inserts extra coords into horizontal edges (in output
                //polygons) whereever maxima touch these horizontal edges. This helps
                //'simplifying' polygons (ie if the Simplify property is set).
                if (m_Maxima.size() > 0) {
                    if (dir == dLeftToRight) {
                        while (maxIt != m_Maxima.end() && *maxIt < e->Curr.X) {
                            if (horzEdge->OutIdx >= 0 && !IsOpen)
                                AddOutPt(horzEdge, IntPoint(*maxIt, horzEdge->Bot.Y));
                            maxIt++;
                        }
                    } else {
                        while (maxRit != m_Maxima.rend() && *maxRit > e->Curr.X) {
                            if (horzEdge->OutIdx >= 0 && !IsOpen)
                                AddOutPt(horzEdge, IntPoint(*maxRit, horzEdge->Bot.Y));
                            maxRit++;
                        }
                    }
                };

                if ((dir == dLeftToRight && e->Curr.X > horzRight) ||
                        (dir == dRightToLeft && e->Curr.X < horzLeft)) break;

                //Also break if we've got to the end of an intermediate horizontal edge ...
                //nb: Smaller Dx's are to the right of larger Dx's ABOVE the horizontal.
                if (e->Curr.X == horzEdge->Top.X && horzEdge->NextInLML &&
                        e->Dx < horzEdge->NextInLML->Dx) break;

                if (horzEdge->OutIdx >= 0 && !IsOpen) //note: may be done multiple times
                {
#ifdef use_xyz
                    if (dir == dLeftToRight) SetZ(e->Curr, *horzEdge, *e);
                    else SetZ(e->Curr, *e, *horzEdge);
#endif      
                    op1 = AddOutPt(horzEdge, e->Curr);
                    TEdge* eNextHorz = m_SortedEdges;
                    while (eNextHorz) {
                        if (eNextHorz->OutIdx >= 0 &&
                                HorzSegmentsOverlap(horzEdge->Bot.X,
                                horzEdge->Top.X, eNextHorz->Bot.X, eNextHorz->Top.X)) {
                            OutPt* op2 = GetLastOutPt(eNextHorz);
                            AddJoin(op2, op1, eNextHorz->Top);
                        }
                        eNextHorz = eNextHorz->NextInSEL;
                    }
                    AddGhostJoin(op1, horzEdge->Bot);
                }

                //OK, so far we're still in range of the horizontal Edge  but make sure
                //we're at the last of consec. horizontals when matching with eMaxPair
                if (e == eMaxPair && IsLastHorz) {
                    if (horzEdge->OutIdx >= 0)
                        AddLocalMaxPoly(horzEdge, eMaxPair, horzEdge->Top);
                    DeleteFromAEL(horzEdge);
                    DeleteFromAEL(eMaxPair);
                    return;
                }

                if (dir == dLeftToRight) {
                    IntPoint Pt = IntPoint(e->Curr.X, horzEdge->Curr.Y);
                    IntersectEdges(horzEdge, e, Pt);
                } else {
                    IntPoint Pt = IntPoint(e->Curr.X, horzEdge->Curr.Y);
                    IntersectEdges(e, horzEdge, Pt);
                }
                TEdge* eNext = GetNextInAEL(e, dir);
                SwapPositionsInAEL(horzEdge, e);
                e = eNext;
            } //end while(e)

            //Break out of loop if HorzEdge.NextInLML is not also horizontal ...
            if (!horzEdge->NextInLML || !IsHorizontal(*horzEdge->NextInLML)) break;

            UpdateEdgeIntoAEL(horzEdge);
            if (horzEdge->OutIdx >= 0) AddOutPt(horzEdge, horzEdge->Bot);
            GetHorzDirection(*horzEdge, dir, horzLeft, horzRight);

        } //end for (;;)

        if (horzEdge->OutIdx >= 0 && !op1) {
            op1 = GetLastOutPt(horzEdge);
            TEdge* eNextHorz = m_SortedEdges;
            while (eNextHorz) {
                if (eNextHorz->OutIdx >= 0 &&
                        HorzSegmentsOverlap(horzEdge->Bot.X,
                        horzEdge->Top.X, eNextHorz->Bot.X, eNextHorz->Top.X)) {
                    OutPt* op2 = GetLastOutPt(eNextHorz);
                    AddJoin(op2, op1, eNextHorz->Top);
                }
                eNextHorz = eNextHorz->NextInSEL;
            }
            AddGhostJoin(op1, horzEdge->Top);
        }

        if (horzEdge->NextInLML) {
            if (horzEdge->OutIdx >= 0) {
                op1 = AddOutPt(horzEdge, horzEdge->Top);
                UpdateEdgeIntoAEL(horzEdge);
                if (horzEdge->WindDelta == 0) return;
                //nb: HorzEdge is no longer horizontal here
                TEdge* ePrev = horzEdge->PrevInAEL;
                TEdge* eNext = horzEdge->NextInAEL;
                if (ePrev && ePrev->Curr.X == horzEdge->Bot.X &&
                        ePrev->Curr.Y == horzEdge->Bot.Y && ePrev->WindDelta != 0 &&
                        (ePrev->OutIdx >= 0 && ePrev->Curr.Y > ePrev->Top.Y &&
                        SlopesEqual(*horzEdge, *ePrev, m_UseFullRange))) {
                    OutPt* op2 = AddOutPt(ePrev, horzEdge->Bot);
                    AddJoin(op1, op2, horzEdge->Top);
                } else if (eNext && eNext->Curr.X == horzEdge->Bot.X &&
                        eNext->Curr.Y == horzEdge->Bot.Y && eNext->WindDelta != 0 &&
                        eNext->OutIdx >= 0 && eNext->Curr.Y > eNext->Top.Y &&
                        SlopesEqual(*horzEdge, *eNext, m_UseFullRange)) {
                    OutPt* op2 = AddOutPt(eNext, horzEdge->Bot);
                    AddJoin(op1, op2, horzEdge->Top);
                }
            } else
                UpdateEdgeIntoAEL(horzEdge);
        } else {
            if (horzEdge->OutIdx >= 0) AddOutPt(horzEdge, horzEdge->Top);
            DeleteFromAEL(horzEdge);
        }
    }
    //------------------------------------------------------------------------------

    bool Clipper::ProcessIntersections(const cInt topY) {
        if (!m_ActiveEdges) return true;
        try {
            BuildIntersectList(topY);
            size_t IlSize = m_IntersectList.size();
            if (IlSize == 0) return true;
            if (IlSize == 1 || FixupIntersectionOrder()) ProcessIntersectList();
            else return false;
        }        catch (...) {
            m_SortedEdges = 0;
            DisposeIntersectNodes();
            throw clipperException("ProcessIntersections error");
        }
        m_SortedEdges = 0;
        return true;
    }
    //------------------------------------------------------------------------------

    void Clipper::DisposeIntersectNodes() {
        for (size_t i = 0; i < m_IntersectList.size(); ++i)
            delete m_IntersectList[i];
        m_IntersectList.clear();
    }
    //------------------------------------------------------------------------------

    void Clipper::BuildIntersectList(const cInt topY) {
        if (!m_ActiveEdges) return;

        //prepare for sorting ...
        TEdge* e = m_ActiveEdges;
        m_SortedEdges = e;
        while (e) {
            e->PrevInSEL = e->PrevInAEL;
            e->NextInSEL = e->NextInAEL;
            e->Curr.X = TopX(*e, topY);
            e = e->NextInAEL;
        }

        //bubblesort ...
        bool isModified;
        do {
            isModified = false;
            e = m_SortedEdges;
            while (e->NextInSEL) {
                TEdge *eNext = e->NextInSEL;
                IntPoint Pt;
                if (e->Curr.X > eNext->Curr.X) {
                    IntersectPoint(*e, *eNext, Pt);
                    if (Pt.Y < topY) Pt = IntPoint(TopX(*e, topY), topY);
                    IntersectNode * newNode = new IntersectNode;
                    newNode->Edge1 = e;
                    newNode->Edge2 = eNext;
                    newNode->Pt = Pt;
                    m_IntersectList.push_back(newNode);

                    SwapPositionsInSEL(e, eNext);
                    isModified = true;
                } else
                    e = eNext;
            }
            if (e->PrevInSEL) e->PrevInSEL->NextInSEL = 0;
            else break;
        }        while (isModified);
        m_SortedEdges = 0; //important
    }
    //------------------------------------------------------------------------------

    void Clipper::ProcessIntersectList() {
        for (size_t i = 0; i < m_IntersectList.size(); ++i) {
            IntersectNode* iNode = m_IntersectList[i];
            {
                IntersectEdges(iNode->Edge1, iNode->Edge2, iNode->Pt);
                SwapPositionsInAEL(iNode->Edge1, iNode->Edge2);
            }
            delete iNode;
        }
        m_IntersectList.clear();
    }
    //------------------------------------------------------------------------------

    bool IntersectListSort(IntersectNode* node1, IntersectNode* node2) {
        return node2->Pt.Y < node1->Pt.Y;
    }
    //------------------------------------------------------------------------------

    inline bool EdgesAdjacent(const IntersectNode &inode) {
        return (inode.Edge1->NextInSEL == inode.Edge2) ||
                (inode.Edge1->PrevInSEL == inode.Edge2);
    }
    //------------------------------------------------------------------------------

    bool Clipper::FixupIntersectionOrder() {
        //pre-condition: intersections are sorted Bottom-most first.
        //Now it's crucial that intersections are made only between adjacent edges,
        //so to ensure this the order of intersections may need adjusting ...
        CopyAELToSEL();
        std::sort(m_IntersectList.begin(), m_IntersectList.end(), IntersectListSort);
        size_t cnt = m_IntersectList.size();
        for (size_t i = 0; i < cnt; ++i) {
            if (!EdgesAdjacent(*m_IntersectList[i])) {
                size_t j = i + 1;
                while (j < cnt && !EdgesAdjacent(*m_IntersectList[j])) j++;
                if (j == cnt) return false;
                std::swap(m_IntersectList[i], m_IntersectList[j]);
            }
            SwapPositionsInSEL(m_IntersectList[i]->Edge1, m_IntersectList[i]->Edge2);
        }
        return true;
    }
    //------------------------------------------------------------------------------

    void Clipper::DoMaxima(TEdge *e) {
        TEdge* eMaxPair = GetMaximaPairEx(e);
        if (!eMaxPair) {
            if (e->OutIdx >= 0)
                AddOutPt(e, e->Top);
            DeleteFromAEL(e);
            return;
        }

        TEdge* eNext = e->NextInAEL;
        while (eNext && eNext != eMaxPair) {
            IntersectEdges(e, eNext, e->Top);
            SwapPositionsInAEL(e, eNext);
            eNext = e->NextInAEL;
        }

        if (e->OutIdx == Unassigned && eMaxPair->OutIdx == Unassigned) {
            DeleteFromAEL(e);
            DeleteFromAEL(eMaxPair);
        } else if (e->OutIdx >= 0 && eMaxPair->OutIdx >= 0) {
            if (e->OutIdx >= 0) AddLocalMaxPoly(e, eMaxPair, e->Top);
            DeleteFromAEL(e);
            DeleteFromAEL(eMaxPair);
        }
#ifdef use_lines
        else if (e->WindDelta == 0) {
            if (e->OutIdx >= 0) {
                AddOutPt(e, e->Top);
                e->OutIdx = Unassigned;
            }
            DeleteFromAEL(e);

            if (eMaxPair->OutIdx >= 0) {
                AddOutPt(eMaxPair, e->Top);
                eMaxPair->OutIdx = Unassigned;
            }
            DeleteFromAEL(eMaxPair);
        }
#endif
        else throw clipperException("DoMaxima error");
    }
    //------------------------------------------------------------------------------

    void Clipper::ProcessEdgesAtTopOfScanbeam(const cInt topY) {
        TEdge* e = m_ActiveEdges;
        while (e) {
            //1. process maxima, treating them as if they're 'bent' horizontal edges,
            //   but exclude maxima with horizontal edges. nb: e can't be a horizontal.
            bool IsMaximaEdge = IsMaxima(e, topY);

            if (IsMaximaEdge) {
                TEdge* eMaxPair = GetMaximaPairEx(e);
                IsMaximaEdge = (!eMaxPair || !IsHorizontal(*eMaxPair));
            }

            if (IsMaximaEdge) {
                if (m_StrictSimple) m_Maxima.push_back(e->Top.X);
                TEdge* ePrev = e->PrevInAEL;
                DoMaxima(e);
                if (!ePrev) e = m_ActiveEdges;
                else e = ePrev->NextInAEL;
            } else {
                //2. promote horizontal edges, otherwise update Curr.X and Curr.Y ...
                if (IsIntermediate(e, topY) && IsHorizontal(*e->NextInLML)) {
                    UpdateEdgeIntoAEL(e);
                    if (e->OutIdx >= 0)
                        AddOutPt(e, e->Bot);
                    AddEdgeToSEL(e);
                }
                else {
                    e->Curr.X = TopX(*e, topY);
                    e->Curr.Y = topY;
#ifdef use_xyz
                    e->Curr.Z = topY == e->Top.Y ? e->Top.Z : (topY == e->Bot.Y ? e->Bot.Z : 0);
#endif
                }

                //When StrictlySimple and 'e' is being touched by another edge, then
                //make sure both edges have a vertex here ...
                if (m_StrictSimple) {
                    TEdge* ePrev = e->PrevInAEL;
                    if ((e->OutIdx >= 0) && (e->WindDelta != 0) && ePrev && (ePrev->OutIdx >= 0) &&
                            (ePrev->Curr.X == e->Curr.X) && (ePrev->WindDelta != 0)) {
                        IntPoint pt = e->Curr;
#ifdef use_xyz
                        SetZ(pt, *ePrev, *e);
#endif
                        OutPt* op = AddOutPt(ePrev, pt);
                        OutPt* op2 = AddOutPt(e, pt);
                        AddJoin(op, op2, pt); //StrictlySimple (type-3) join
                    }
                }

                e = e->NextInAEL;
            }
        }

        //3. Process horizontals at the Top of the scanbeam ...
        m_Maxima.sort();
        ProcessHorizontals();
        m_Maxima.clear();

        //4. Promote intermediate vertices ...
        e = m_ActiveEdges;
        while (e) {
            if (IsIntermediate(e, topY)) {
                OutPt* op = 0;
                if (e->OutIdx >= 0)
                    op = AddOutPt(e, e->Top);
                UpdateEdgeIntoAEL(e);

                //if output polygons share an edge, they'll need joining later ...
                TEdge* ePrev = e->PrevInAEL;
                TEdge* eNext = e->NextInAEL;
                if (ePrev && ePrev->Curr.X == e->Bot.X &&
                        ePrev->Curr.Y == e->Bot.Y && op &&
                        ePrev->OutIdx >= 0 && ePrev->Curr.Y > ePrev->Top.Y &&
                        SlopesEqual(e->Curr, e->Top, ePrev->Curr, ePrev->Top, m_UseFullRange) &&
                        (e->WindDelta != 0) && (ePrev->WindDelta != 0)) {
                    OutPt* op2 = AddOutPt(ePrev, e->Bot);
                    AddJoin(op, op2, e->Top);
                } else if (eNext && eNext->Curr.X == e->Bot.X &&
                        eNext->Curr.Y == e->Bot.Y && op &&
                        eNext->OutIdx >= 0 && eNext->Curr.Y > eNext->Top.Y &&
                        SlopesEqual(e->Curr, e->Top, eNext->Curr, eNext->Top, m_UseFullRange) &&
                        (e->WindDelta != 0) && (eNext->WindDelta != 0)) {
                    OutPt* op2 = AddOutPt(eNext, e->Bot);
                    AddJoin(op, op2, e->Top);
                }
            }
            e = e->NextInAEL;
        }
    }
    //------------------------------------------------------------------------------

    void Clipper::FixupOutPolyline(OutRec &outrec) {
        OutPt *pp = outrec.Pts;
        OutPt *lastPP = pp->Prev;
        while (pp != lastPP) {
            pp = pp->Next;
            if (pp->Pt == pp->Prev->Pt) {
                if (pp == lastPP) lastPP = pp->Prev;
                OutPt *tmpPP = pp->Prev;
                tmpPP->Next = pp->Next;
                pp->Next->Prev = tmpPP;
                delete pp;
                pp = tmpPP;
            }
        }

        if (pp == pp->Prev) {
            DisposeOutPts(pp);
            outrec.Pts = 0;
            return;
        }
    }
    //------------------------------------------------------------------------------

    void Clipper::FixupOutPolygon(OutRec &outrec) {
        //FixupOutPolygon() - removes duplicate points and simplifies consecutive
        //parallel edges by removing the middle vertex.
        OutPt *lastOK = 0;
        outrec.BottomPt = 0;
        OutPt *pp = outrec.Pts;
        bool preserveCol = m_PreserveCollinear || m_StrictSimple;

        for (;;) {
            if (pp->Prev == pp || pp->Prev == pp->Next) {
                DisposeOutPts(pp);
                outrec.Pts = 0;
                return;
            }

            //test for duplicate points and collinear edges ...
            if ((pp->Pt == pp->Next->Pt) || (pp->Pt == pp->Prev->Pt) ||
                    (SlopesEqual(pp->Prev->Pt, pp->Pt, pp->Next->Pt, m_UseFullRange) &&
                    (!preserveCol || !Pt2IsBetweenPt1AndPt3(pp->Prev->Pt, pp->Pt, pp->Next->Pt)))) {
                lastOK = 0;
                OutPt *tmp = pp;
                pp->Prev->Next = pp->Next;
                pp->Next->Prev = pp->Prev;
                pp = pp->Prev;
                delete tmp;
            } else if (pp == lastOK) break;
            else {
                if (!lastOK) lastOK = pp;
                pp = pp->Next;
            }
        }
        outrec.Pts = pp;
    }
    //------------------------------------------------------------------------------

    int PointCount(OutPt *Pts) {
        if (!Pts) return 0;
        int result = 0;
        OutPt* p = Pts;
        do {
            result++;
            p = p->Next;
        }        while (p != Pts);
        return result;
    }
    //------------------------------------------------------------------------------

    void Clipper::BuildResult(Paths &polys) {
        polys.reserve(m_PolyOuts.size());
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
            if (!m_PolyOuts[i]->Pts) continue;
            Path pg;
            OutPt* p = m_PolyOuts[i]->Pts->Prev;
            int cnt = PointCount(p);
            if (cnt < 2) continue;
            pg.reserve(cnt);
            for (int i2 = 0; i2 < cnt; ++i2) {
                pg.push_back(p->Pt);
                p = p->Prev;
            }
            polys.push_back(pg);
        }
    }
    //------------------------------------------------------------------------------

    void Clipper::BuildResult2(PolyTree& polytree) {
        polytree.Clear();
        polytree.AllNodes.reserve(m_PolyOuts.size());
        //add each output polygon/contour to polytree ...
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); i++) {
            OutRec* outRec = m_PolyOuts[i];
            int cnt = PointCount(outRec->Pts);
            if ((outRec->IsOpen && cnt < 2) || (!outRec->IsOpen && cnt < 3)) continue;
            FixHoleLinkage(*outRec);
            PolyNode* pn = new PolyNode();
            //nb: polytree takes ownership of all the PolyNodes
            polytree.AllNodes.push_back(pn);
            outRec->PolyNd = pn;
            pn->Parent = 0;
            pn->Index = 0;
            pn->Contour.reserve(cnt);
            OutPt *op = outRec->Pts->Prev;
            for (int j = 0; j < cnt; j++) {
                pn->Contour.push_back(op->Pt);
                op = op->Prev;
            }
        }

        //fixup PolyNode links etc ...
        polytree.Childs.reserve(m_PolyOuts.size());
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); i++) {
            OutRec* outRec = m_PolyOuts[i];
            if (!outRec->PolyNd) continue;
            if (outRec->IsOpen) {
                outRec->PolyNd->m_IsOpen = true;
                polytree.AddChild(*outRec->PolyNd);
            } else if (outRec->FirstLeft && outRec->FirstLeft->PolyNd)
                outRec->FirstLeft->PolyNd->AddChild(*outRec->PolyNd);
            else
                polytree.AddChild(*outRec->PolyNd);
        }
    }
    //------------------------------------------------------------------------------

    void SwapIntersectNodes(IntersectNode &int1, IntersectNode &int2) {
        //just swap the contents (because fIntersectNodes is a single-linked-list)
        IntersectNode inode = int1; //gets a copy of Int1
        int1.Edge1 = int2.Edge1;
        int1.Edge2 = int2.Edge2;
        int1.Pt = int2.Pt;
        int2.Edge1 = inode.Edge1;
        int2.Edge2 = inode.Edge2;
        int2.Pt = inode.Pt;
    }
    //------------------------------------------------------------------------------

    inline bool E2InsertsBeforeE1(TEdge &e1, TEdge &e2) {
        if (e2.Curr.X == e1.Curr.X) {
            if (e2.Top.Y > e1.Top.Y)
                return e2.Top.X < TopX(e1, e2.Top.Y);
            else return e1.Top.X > TopX(e2, e1.Top.Y);
        }
        else return e2.Curr.X < e1.Curr.X;
    }
    //------------------------------------------------------------------------------

    bool GetOverlap(const cInt a1, const cInt a2, const cInt b1, const cInt b2,
            cInt& Left, cInt& Right) {
        if (a1 < a2) {
            if (b1 < b2) {
                Left = std::max(a1, b1);
                Right = std::min(a2, b2);
            } else {
                Left = std::max(a1, b2);
                Right = std::min(a2, b1);
            }
        }
        else {
            if (b1 < b2) {
                Left = std::max(a2, b1);
                Right = std::min(a1, b2);
            } else {
                Left = std::max(a2, b2);
                Right = std::min(a1, b1);
            }
        }
        return Left < Right;
    }
    //------------------------------------------------------------------------------

    inline void UpdateOutPtIdxs(OutRec& outrec) {
        OutPt* op = outrec.Pts;
        do {
            op->Idx = outrec.Idx;
            op = op->Prev;
        }        while (op != outrec.Pts);
    }
    //------------------------------------------------------------------------------

    void Clipper::InsertEdgeIntoAEL(TEdge *edge, TEdge* startEdge) {
        if (!m_ActiveEdges) {
            edge->PrevInAEL = 0;
            edge->NextInAEL = 0;
            m_ActiveEdges = edge;
        } else if (!startEdge && E2InsertsBeforeE1(*m_ActiveEdges, *edge)) {
            edge->PrevInAEL = 0;
            edge->NextInAEL = m_ActiveEdges;
            m_ActiveEdges->PrevInAEL = edge;
            m_ActiveEdges = edge;
        }
        else {
            if (!startEdge) startEdge = m_ActiveEdges;
            while (startEdge->NextInAEL &&
                    !E2InsertsBeforeE1(*startEdge->NextInAEL, *edge))
                startEdge = startEdge->NextInAEL;
            edge->NextInAEL = startEdge->NextInAEL;
            if (startEdge->NextInAEL) startEdge->NextInAEL->PrevInAEL = edge;
            edge->PrevInAEL = startEdge;
            startEdge->NextInAEL = edge;
        }
    }
    //----------------------------------------------------------------------

    OutPt* DupOutPt(OutPt* outPt, bool InsertAfter) {
        OutPt* result = new OutPt;
        result->Pt = outPt->Pt;
        result->Idx = outPt->Idx;
        if (InsertAfter) {
            result->Next = outPt->Next;
            result->Prev = outPt;
            outPt->Next->Prev = result;
            outPt->Next = result;
        }
        else {
            result->Prev = outPt->Prev;
            result->Next = outPt;
            outPt->Prev->Next = result;
            outPt->Prev = result;
        }
        return result;
    }
    //------------------------------------------------------------------------------

    bool JoinHorz(OutPt* op1, OutPt* op1b, OutPt* op2, OutPt* op2b,
            const IntPoint Pt, bool DiscardLeft) {
        Direction Dir1 = (op1->Pt.X > op1b->Pt.X ? dRightToLeft : dLeftToRight);
        Direction Dir2 = (op2->Pt.X > op2b->Pt.X ? dRightToLeft : dLeftToRight);
        if (Dir1 == Dir2) return false;

        //When DiscardLeft, we want Op1b to be on the Left of Op1, otherwise we
        //want Op1b to be on the Right. (And likewise with Op2 and Op2b.)
        //So, to facilitate this while inserting Op1b and Op2b ...
        //when DiscardLeft, make sure we're AT or RIGHT of Pt before adding Op1b,
        //otherwise make sure we're AT or LEFT of Pt. (Likewise with Op2b.)
        if (Dir1 == dLeftToRight) {
            while (op1->Next->Pt.X <= Pt.X &&
                    op1->Next->Pt.X >= op1->Pt.X && op1->Next->Pt.Y == Pt.Y)
                op1 = op1->Next;
            if (DiscardLeft && (op1->Pt.X != Pt.X)) op1 = op1->Next;
            op1b = DupOutPt(op1, !DiscardLeft);
            if (op1b->Pt != Pt) {
                op1 = op1b;
                op1->Pt = Pt;
                op1b = DupOutPt(op1, !DiscardLeft);
            }
        }
        else {
            while (op1->Next->Pt.X >= Pt.X &&
                    op1->Next->Pt.X <= op1->Pt.X && op1->Next->Pt.Y == Pt.Y)
                op1 = op1->Next;
            if (!DiscardLeft && (op1->Pt.X != Pt.X)) op1 = op1->Next;
            op1b = DupOutPt(op1, DiscardLeft);
            if (op1b->Pt != Pt) {
                op1 = op1b;
                op1->Pt = Pt;
                op1b = DupOutPt(op1, DiscardLeft);
            }
        }

        if (Dir2 == dLeftToRight) {
            while (op2->Next->Pt.X <= Pt.X &&
                    op2->Next->Pt.X >= op2->Pt.X && op2->Next->Pt.Y == Pt.Y)
                op2 = op2->Next;
            if (DiscardLeft && (op2->Pt.X != Pt.X)) op2 = op2->Next;
            op2b = DupOutPt(op2, !DiscardLeft);
            if (op2b->Pt != Pt) {
                op2 = op2b;
                op2->Pt = Pt;
                op2b = DupOutPt(op2, !DiscardLeft);
            };
        } else {
            while (op2->Next->Pt.X >= Pt.X &&
                    op2->Next->Pt.X <= op2->Pt.X && op2->Next->Pt.Y == Pt.Y)
                op2 = op2->Next;
            if (!DiscardLeft && (op2->Pt.X != Pt.X)) op2 = op2->Next;
            op2b = DupOutPt(op2, DiscardLeft);
            if (op2b->Pt != Pt) {
                op2 = op2b;
                op2->Pt = Pt;
                op2b = DupOutPt(op2, DiscardLeft);
            };
        };

        if ((Dir1 == dLeftToRight) == DiscardLeft) {
            op1->Prev = op2;
            op2->Next = op1;
            op1b->Next = op2b;
            op2b->Prev = op1b;
        } else {
            op1->Next = op2;
            op2->Prev = op1;
            op1b->Prev = op2b;
            op2b->Next = op1b;
        }
        return true;
    }
    //------------------------------------------------------------------------------

    bool Clipper::JoinPoints(Join *j, OutRec* outRec1, OutRec* outRec2) {
        OutPt *op1 = j->OutPt1, *op1b;
        OutPt *op2 = j->OutPt2, *op2b;

        //There are 3 kinds of joins for output polygons ...
        //1. Horizontal joins where Join.OutPt1 & Join.OutPt2 are vertices anywhere
        //along (horizontal) collinear edges (& Join.OffPt is on the same horizontal).
        //2. Non-horizontal joins where Join.OutPt1 & Join.OutPt2 are at the same
        //location at the Bottom of the overlapping segment (& Join.OffPt is above).
        //3. StrictSimple joins where edges touch but are not collinear and where
        //Join.OutPt1, Join.OutPt2 & Join.OffPt all share the same point.
        bool isHorizontal = (j->OutPt1->Pt.Y == j->OffPt.Y);

        if (isHorizontal && (j->OffPt == j->OutPt1->Pt) &&
                (j->OffPt == j->OutPt2->Pt)) {
            //Strictly Simple join ...
            if (outRec1 != outRec2) return false;
            op1b = j->OutPt1->Next;
            while (op1b != op1 && (op1b->Pt == j->OffPt))
                op1b = op1b->Next;
            bool reverse1 = (op1b->Pt.Y > j->OffPt.Y);
            op2b = j->OutPt2->Next;
            while (op2b != op2 && (op2b->Pt == j->OffPt))
                op2b = op2b->Next;
            bool reverse2 = (op2b->Pt.Y > j->OffPt.Y);
            if (reverse1 == reverse2) return false;
            if (reverse1) {
                op1b = DupOutPt(op1, false);
                op2b = DupOutPt(op2, true);
                op1->Prev = op2;
                op2->Next = op1;
                op1b->Next = op2b;
                op2b->Prev = op1b;
                j->OutPt1 = op1;
                j->OutPt2 = op1b;
                return true;
            } else {
                op1b = DupOutPt(op1, true);
                op2b = DupOutPt(op2, false);
                op1->Next = op2;
                op2->Prev = op1;
                op1b->Prev = op2b;
                op2b->Next = op1b;
                j->OutPt1 = op1;
                j->OutPt2 = op1b;
                return true;
            }
        }
        else if (isHorizontal) {
            //treat horizontal joins differently to non-horizontal joins since with
            //them we're not yet sure where the overlapping is. OutPt1.Pt & OutPt2.Pt
            //may be anywhere along the horizontal edge.
            op1b = op1;
            while (op1->Prev->Pt.Y == op1->Pt.Y && op1->Prev != op1b && op1->Prev != op2)
                op1 = op1->Prev;
            while (op1b->Next->Pt.Y == op1b->Pt.Y && op1b->Next != op1 && op1b->Next != op2)
                op1b = op1b->Next;
            if (op1b->Next == op1 || op1b->Next == op2) return false; //a flat 'polygon'

            op2b = op2;
            while (op2->Prev->Pt.Y == op2->Pt.Y && op2->Prev != op2b && op2->Prev != op1b)
                op2 = op2->Prev;
            while (op2b->Next->Pt.Y == op2b->Pt.Y && op2b->Next != op2 && op2b->Next != op1)
                op2b = op2b->Next;
            if (op2b->Next == op2 || op2b->Next == op1) return false; //a flat 'polygon'

            cInt Left, Right;
            //Op1 --> Op1b & Op2 --> Op2b are the extremites of the horizontal edges
            if (!GetOverlap(op1->Pt.X, op1b->Pt.X, op2->Pt.X, op2b->Pt.X, Left, Right))
                return false;

            //DiscardLeftSide: when overlapping edges are joined, a spike will created
            //which needs to be cleaned up. However, we don't want Op1 or Op2 caught up
            //on the discard Side as either may still be needed for other joins ...
            IntPoint Pt;
            bool DiscardLeftSide;
            if (op1->Pt.X >= Left && op1->Pt.X <= Right) {
                Pt = op1->Pt;
                DiscardLeftSide = (op1->Pt.X > op1b->Pt.X);
            }
            else if (op2->Pt.X >= Left && op2->Pt.X <= Right) {
                Pt = op2->Pt;
                DiscardLeftSide = (op2->Pt.X > op2b->Pt.X);
            }
            else if (op1b->Pt.X >= Left && op1b->Pt.X <= Right) {
                Pt = op1b->Pt;
                DiscardLeftSide = op1b->Pt.X > op1->Pt.X;
            }
            else {
                Pt = op2b->Pt;
                DiscardLeftSide = (op2b->Pt.X > op2->Pt.X);
            }
            j->OutPt1 = op1;
            j->OutPt2 = op2;
            return JoinHorz(op1, op1b, op2, op2b, Pt, DiscardLeftSide);
        } else {
            //nb: For non-horizontal joins ...
            //    1. Jr.OutPt1.Pt.Y == Jr.OutPt2.Pt.Y
            //    2. Jr.OutPt1.Pt > Jr.OffPt.Y

            //make sure the polygons are correctly oriented ...
            op1b = op1->Next;
            while ((op1b->Pt == op1->Pt) && (op1b != op1)) op1b = op1b->Next;
            bool Reverse1 = ((op1b->Pt.Y > op1->Pt.Y) ||
                    !SlopesEqual(op1->Pt, op1b->Pt, j->OffPt, m_UseFullRange));
            if (Reverse1) {
                op1b = op1->Prev;
                while ((op1b->Pt == op1->Pt) && (op1b != op1)) op1b = op1b->Prev;
                if ((op1b->Pt.Y > op1->Pt.Y) ||
                        !SlopesEqual(op1->Pt, op1b->Pt, j->OffPt, m_UseFullRange)) return false;
            };
            op2b = op2->Next;
            while ((op2b->Pt == op2->Pt) && (op2b != op2))op2b = op2b->Next;
            bool Reverse2 = ((op2b->Pt.Y > op2->Pt.Y) ||
                    !SlopesEqual(op2->Pt, op2b->Pt, j->OffPt, m_UseFullRange));
            if (Reverse2) {
                op2b = op2->Prev;
                while ((op2b->Pt == op2->Pt) && (op2b != op2)) op2b = op2b->Prev;
                if ((op2b->Pt.Y > op2->Pt.Y) ||
                        !SlopesEqual(op2->Pt, op2b->Pt, j->OffPt, m_UseFullRange)) return false;
            }

            if ((op1b == op1) || (op2b == op2) || (op1b == op2b) ||
                    ((outRec1 == outRec2) && (Reverse1 == Reverse2))) return false;

            if (Reverse1) {
                op1b = DupOutPt(op1, false);
                op2b = DupOutPt(op2, true);
                op1->Prev = op2;
                op2->Next = op1;
                op1b->Next = op2b;
                op2b->Prev = op1b;
                j->OutPt1 = op1;
                j->OutPt2 = op1b;
                return true;
            } else {
                op1b = DupOutPt(op1, true);
                op2b = DupOutPt(op2, false);
                op1->Next = op2;
                op2->Prev = op1;
                op1b->Prev = op2b;
                op2b->Next = op1b;
                j->OutPt1 = op1;
                j->OutPt2 = op1b;
                return true;
            }
        }
    }
    //----------------------------------------------------------------------

    static OutRec* ParseFirstLeft(OutRec* FirstLeft) {
        while (FirstLeft && !FirstLeft->Pts)
            FirstLeft = FirstLeft->FirstLeft;
        return FirstLeft;
    }
    //------------------------------------------------------------------------------

    void Clipper::FixupFirstLefts1(OutRec* OldOutRec, OutRec* NewOutRec) {
        //tests if NewOutRec contains the polygon before reassigning FirstLeft
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
            OutRec* outRec = m_PolyOuts[i];
            OutRec* firstLeft = ParseFirstLeft(outRec->FirstLeft);
            if (outRec->Pts && firstLeft == OldOutRec) {
                if (Poly2ContainsPoly1(outRec->Pts, NewOutRec->Pts))
                    outRec->FirstLeft = NewOutRec;
            }
        }
    }
    //----------------------------------------------------------------------

    void Clipper::FixupFirstLefts2(OutRec* InnerOutRec, OutRec* OuterOutRec) {
        //A polygon has split into two such that one is now the inner of the other.
        //It's possible that these polygons now wrap around other polygons, so check
        //every polygon that's also contained by OuterOutRec's FirstLeft container
        //(including 0) to see if they've become inner to the new inner polygon ...
        OutRec* orfl = OuterOutRec->FirstLeft;
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
            OutRec* outRec = m_PolyOuts[i];

            if (!outRec->Pts || outRec == OuterOutRec || outRec == InnerOutRec)
                continue;
            OutRec* firstLeft = ParseFirstLeft(outRec->FirstLeft);
            if (firstLeft != orfl && firstLeft != InnerOutRec && firstLeft != OuterOutRec)
                continue;
            if (Poly2ContainsPoly1(outRec->Pts, InnerOutRec->Pts))
                outRec->FirstLeft = InnerOutRec;
            else if (Poly2ContainsPoly1(outRec->Pts, OuterOutRec->Pts))
                outRec->FirstLeft = OuterOutRec;
            else if (outRec->FirstLeft == InnerOutRec || outRec->FirstLeft == OuterOutRec)
                outRec->FirstLeft = orfl;
        }
    }
    //----------------------------------------------------------------------

    void Clipper::FixupFirstLefts3(OutRec* OldOutRec, OutRec* NewOutRec) {
        //reassigns FirstLeft WITHOUT testing if NewOutRec contains the polygon
        for (PolyOutList::size_type i = 0; i < m_PolyOuts.size(); ++i) {
            OutRec* outRec = m_PolyOuts[i];
            OutRec* firstLeft = ParseFirstLeft(outRec->FirstLeft);
            if (outRec->Pts && firstLeft == OldOutRec)
                outRec->FirstLeft = NewOutRec;
        }
    }
    //----------------------------------------------------------------------

    void Clipper::JoinCommonEdges() {
        for (JoinList::size_type i = 0; i < m_Joins.size(); i++) {
            Join* join = m_Joins[i];

            OutRec *outRec1 = GetOutRec(join->OutPt1->Idx);
            OutRec *outRec2 = GetOutRec(join->OutPt2->Idx);

            if (!outRec1->Pts || !outRec2->Pts) continue;
            if (outRec1->IsOpen || outRec2->IsOpen) continue;

            //get the polygon fragment with the correct hole state (FirstLeft)
            //before calling JoinPoints() ...
            OutRec *holeStateRec;
            if (outRec1 == outRec2) holeStateRec = outRec1;
            else if (OutRec1RightOfOutRec2(outRec1, outRec2)) holeStateRec = outRec2;
            else if (OutRec1RightOfOutRec2(outRec2, outRec1)) holeStateRec = outRec1;
            else holeStateRec = GetLowermostRec(outRec1, outRec2);

            if (!JoinPoints(join, outRec1, outRec2)) continue;

            if (outRec1 == outRec2) {
                //instead of joining two polygons, we've just created a new one by
                //splitting one polygon into two.
                outRec1->Pts = join->OutPt1;
                outRec1->BottomPt = 0;
                outRec2 = CreateOutRec();
                outRec2->Pts = join->OutPt2;

                //update all OutRec2.Pts Idx's ...
                UpdateOutPtIdxs(*outRec2);

                if (Poly2ContainsPoly1(outRec2->Pts, outRec1->Pts)) {
                    //outRec1 contains outRec2 ...
                    outRec2->IsHole = !outRec1->IsHole;
                    outRec2->FirstLeft = outRec1;

                    if (m_UsingPolyTree) FixupFirstLefts2(outRec2, outRec1);

                    if ((outRec2->IsHole ^ m_ReverseOutput) == (Area(*outRec2) > 0))
                        ReversePolyPtLinks(outRec2->Pts);

                } else if (Poly2ContainsPoly1(outRec1->Pts, outRec2->Pts)) {
                    //outRec2 contains outRec1 ...
                    outRec2->IsHole = outRec1->IsHole;
                    outRec1->IsHole = !outRec2->IsHole;
                    outRec2->FirstLeft = outRec1->FirstLeft;
                    outRec1->FirstLeft = outRec2;

                    if (m_UsingPolyTree) FixupFirstLefts2(outRec1, outRec2);

                    if ((outRec1->IsHole ^ m_ReverseOutput) == (Area(*outRec1) > 0))
                        ReversePolyPtLinks(outRec1->Pts);
                }
                else {
                    //the 2 polygons are completely separate ...
                    outRec2->IsHole = outRec1->IsHole;
                    outRec2->FirstLeft = outRec1->FirstLeft;

                    //fixup FirstLeft pointers that may need reassigning to OutRec2
                    if (m_UsingPolyTree) FixupFirstLefts1(outRec1, outRec2);
                }

            } else {
                //joined 2 polygons together ...

                outRec2->Pts = 0;
                outRec2->BottomPt = 0;
                outRec2->Idx = outRec1->Idx;

                outRec1->IsHole = holeStateRec->IsHole;
                if (holeStateRec == outRec2)
                    outRec1->FirstLeft = outRec2->FirstLeft;
                outRec2->FirstLeft = outRec1;

                if (m_UsingPolyTree) FixupFirstLefts3(outRec2, outRec1);
            }
        }
    }

    //------------------------------------------------------------------------------
    // ClipperOffset support functions ...
    //------------------------------------------------------------------------------

    DoublePoint GetUnitNormal(const IntPoint &pt1, const IntPoint &pt2) {
        if (pt2.X == pt1.X && pt2.Y == pt1.Y)
            return DoublePoint(0, 0);

        double Dx = (double) (pt2.X - pt1.X);
        double dy = (double) (pt2.Y - pt1.Y);
        double f = 1 * 1.0 / std::sqrt(Dx * Dx + dy * dy);
        Dx *= f;
        dy *= f;
        return DoublePoint(dy, -Dx);
    }

    //------------------------------------------------------------------------------
    // ClipperOffset class
    //------------------------------------------------------------------------------

    ClipperOffset::ClipperOffset(double miterLimit, double arcTolerance) {
        this->MiterLimit = miterLimit;
        this->ArcTolerance = arcTolerance;
        m_lowest.X = -1;
    }
    //------------------------------------------------------------------------------

    ClipperOffset::~ClipperOffset() {
        Clear();
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::Clear() {
        for (int i = 0; i < m_polyNodes.ChildCount(); ++i)
            delete m_polyNodes.Childs[i];
        m_polyNodes.Childs.clear();
        m_lowest.X = -1;
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::AddPath(const Path& path, JoinType joinType, EndType endType) {
        int highI = (int) path.size() - 1;
        if (highI < 0) return;
        PolyNode* newNode = new PolyNode();
        newNode->m_jointype = joinType;
        newNode->m_endtype = endType;

        //strip duplicate points from path and also get index to the lowest point ...
        if (endType == etClosedLine || endType == etClosedPolygon)
            while (highI > 0 && path[0] == path[highI]) highI--;
        newNode->Contour.reserve(highI + 1);
        newNode->Contour.push_back(path[0]);
        int j = 0, k = 0;
        for (int i = 1; i <= highI; i++)
            if (newNode->Contour[j] != path[i]) {
                j++;
                newNode->Contour.push_back(path[i]);
                if (path[i].Y > newNode->Contour[k].Y ||
                        (path[i].Y == newNode->Contour[k].Y &&
                        path[i].X < newNode->Contour[k].X)) k = j;
            }
        if (endType == etClosedPolygon && j < 2) {
            delete newNode;
            return;
        }
        m_polyNodes.AddChild(*newNode);

        //if this path's lowest pt is lower than all the others then update m_lowest
        if (endType != etClosedPolygon) return;
        if (m_lowest.X < 0)
            m_lowest = IntPoint(m_polyNodes.ChildCount() - 1, k);
        else {
            IntPoint ip = m_polyNodes.Childs[(int) m_lowest.X]->Contour[(int) m_lowest.Y];
            if (newNode->Contour[k].Y > ip.Y ||
                    (newNode->Contour[k].Y == ip.Y &&
                    newNode->Contour[k].X < ip.X))
                m_lowest = IntPoint(m_polyNodes.ChildCount() - 1, k);
        }
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::AddPaths(const Paths& paths, JoinType joinType, EndType endType) {
        for (typename Paths::size_type i = 0; i < paths.size(); ++i)
            AddPath(paths[i], joinType, endType);
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::FixOrientations() {
        //fixup orientations of all closed paths if the orientation of the
        //closed path with the lowermost vertex is wrong ...
        if (m_lowest.X >= 0 &&
                !Orientation(m_polyNodes.Childs[(int) m_lowest.X]->Contour)) {
            for (int i = 0; i < m_polyNodes.ChildCount(); ++i) {
                PolyNode& node = *m_polyNodes.Childs[i];
                if (node.m_endtype == etClosedPolygon ||
                        (node.m_endtype == etClosedLine && Orientation(node.Contour)))
                    ReversePath(node.Contour);
            }
        } else {
            for (int i = 0; i < m_polyNodes.ChildCount(); ++i) {
                PolyNode& node = *m_polyNodes.Childs[i];
                if (node.m_endtype == etClosedLine && !Orientation(node.Contour))
                    ReversePath(node.Contour);
            }
        }
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::Execute(Paths& solution, double delta) {
        solution.clear();
        FixOrientations();
        DoOffset(delta);

        //now clean up 'corners' ...
        Clipper clpr;
        clpr.AddPaths(m_destPolys, ptSubject, true);
        if (delta > 0) {
            clpr.Execute(ctUnion, solution, pftPositive, pftPositive);
        } else {
            IntRect r = clpr.GetBounds();
            Path outer(4);
            outer[0] = IntPoint(r.left - 10, r.bottom + 10);
            outer[1] = IntPoint(r.right + 10, r.bottom + 10);
            outer[2] = IntPoint(r.right + 10, r.top - 10);
            outer[3] = IntPoint(r.left - 10, r.top - 10);

            clpr.AddPath(outer, ptSubject, true);
            clpr.ReverseSolution(true);
            clpr.Execute(ctUnion, solution, pftNegative, pftNegative);
            if (solution.size() > 0) solution.erase(solution.begin());
        }
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::Execute(PolyTree& solution, double delta) {
        solution.Clear();
        FixOrientations();
        DoOffset(delta);

        //now clean up 'corners' ...
        Clipper clpr;
        clpr.AddPaths(m_destPolys, ptSubject, true);
        if (delta > 0) {
            clpr.Execute(ctUnion, solution, pftPositive, pftPositive);
        } else {
            IntRect r = clpr.GetBounds();
            Path outer(4);
            outer[0] = IntPoint(r.left - 10, r.bottom + 10);
            outer[1] = IntPoint(r.right + 10, r.bottom + 10);
            outer[2] = IntPoint(r.right + 10, r.top - 10);
            outer[3] = IntPoint(r.left - 10, r.top - 10);

            clpr.AddPath(outer, ptSubject, true);
            clpr.ReverseSolution(true);
            clpr.Execute(ctUnion, solution, pftNegative, pftNegative);
            //remove the outer PolyNode rectangle ...
            if (solution.ChildCount() == 1 && solution.Childs[0]->ChildCount() > 0) {
                PolyNode* outerNode = solution.Childs[0];
                solution.Childs.reserve(outerNode->ChildCount());
                solution.Childs[0] = outerNode->Childs[0];
                solution.Childs[0]->Parent = outerNode->Parent;
                for (int i = 1; i < outerNode->ChildCount(); ++i)
                    solution.AddChild(*outerNode->Childs[i]);
            } else
                solution.Clear();
        }
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::DoOffset(double delta) {
        m_destPolys.clear();
        m_delta = delta;

        //if Zero offset, just copy any CLOSED polygons to m_p and return ...
        if (NEAR_ZERO(delta)) {
            m_destPolys.reserve(m_polyNodes.ChildCount());
            for (int i = 0; i < m_polyNodes.ChildCount(); i++) {
                PolyNode& node = *m_polyNodes.Childs[i];
                if (node.m_endtype == etClosedPolygon)
                    m_destPolys.push_back(node.Contour);
            }
            return;
        }

        //see offset_triginometry3.svg in the documentation folder ...
        if (MiterLimit > 2) m_miterLim = 2 / (MiterLimit * MiterLimit);
        else m_miterLim = 0.5;

        double y;
        if (ArcTolerance <= 0.0) y = def_arc_tolerance;
        else if (ArcTolerance > std::fabs(delta) * def_arc_tolerance)
            y = std::fabs(delta) * def_arc_tolerance;
        else y = ArcTolerance;
        //see offset_triginometry2.svg in the documentation folder ...
        double steps = pi / std::acos(1 - y / std::fabs(delta));
        if (steps > std::fabs(delta) * pi)
            steps = std::fabs(delta) * pi; //ie excessive precision check
        m_sin = std::sin(two_pi / steps);
        m_cos = std::cos(two_pi / steps);
        m_StepsPerRad = steps / two_pi;
        if (delta < 0.0) m_sin = -m_sin;

        m_destPolys.reserve(m_polyNodes.ChildCount() * 2);
        for (int i = 0; i < m_polyNodes.ChildCount(); i++) {
            PolyNode& node = *m_polyNodes.Childs[i];
            m_srcPoly = node.Contour;

            int len = (int) m_srcPoly.size();
            if (len == 0 || (delta <= 0 && (len < 3 || node.m_endtype != etClosedPolygon)))
                continue;

            m_destPoly.clear();
            if (len == 1) {
                if (node.m_jointype == jtRound) {
                    double X = 1.0, Y = 0.0;
                    for (cInt j = 1; j <= steps; j++) {
                        m_destPoly.push_back(IntPoint(
                                Round(m_srcPoly[0].X + X * delta),
                                Round(m_srcPoly[0].Y + Y * delta)));
                        double X2 = X;
                        X = X * m_cos - m_sin * Y;
                        Y = X2 * m_sin + Y * m_cos;
                    }
                } else {
                    double X = -1.0, Y = -1.0;
                    for (int j = 0; j < 4; ++j) {
                        m_destPoly.push_back(IntPoint(
                                Round(m_srcPoly[0].X + X * delta),
                                Round(m_srcPoly[0].Y + Y * delta)));
                        if (X < 0) X = 1;
                        else if (Y < 0) Y = 1;
                        else X = -1;
                    }
                }
                m_destPolys.push_back(m_destPoly);
                continue;
            }
            //build m_normals ...
            m_normals.clear();
            m_normals.reserve(len);
            for (int j = 0; j < len - 1; ++j)
                m_normals.push_back(GetUnitNormal(m_srcPoly[j], m_srcPoly[j + 1]));
            if (node.m_endtype == etClosedLine || node.m_endtype == etClosedPolygon)
                m_normals.push_back(GetUnitNormal(m_srcPoly[len - 1], m_srcPoly[0]));
            else
                m_normals.push_back(DoublePoint(m_normals[len - 2]));

            if (node.m_endtype == etClosedPolygon) {
                int k = len - 1;
                for (int j = 0; j < len; ++j)
                    OffsetPoint(j, k, node.m_jointype);
                m_destPolys.push_back(m_destPoly);
            } else if (node.m_endtype == etClosedLine) {
                int k = len - 1;
                for (int j = 0; j < len; ++j)
                    OffsetPoint(j, k, node.m_jointype);
                m_destPolys.push_back(m_destPoly);
                m_destPoly.clear();
                //re-build m_normals ...
                DoublePoint n = m_normals[len - 1];
                for (int j = len - 1; j > 0; j--)
                    m_normals[j] = DoublePoint(-m_normals[j - 1].X, -m_normals[j - 1].Y);
                m_normals[0] = DoublePoint(-n.X, -n.Y);
                k = 0;
                for (int j = len - 1; j >= 0; j--)
                    OffsetPoint(j, k, node.m_jointype);
                m_destPolys.push_back(m_destPoly);
            } else {
                int k = 0;
                for (int j = 1; j < len - 1; ++j)
                    OffsetPoint(j, k, node.m_jointype);

                IntPoint pt1;
                if (node.m_endtype == etOpenButt) {
                    int j = len - 1;
                    pt1 = IntPoint((cInt) Round(m_srcPoly[j].X + m_normals[j].X *
                            delta), (cInt) Round(m_srcPoly[j].Y + m_normals[j].Y * delta));
                    m_destPoly.push_back(pt1);
                    pt1 = IntPoint((cInt) Round(m_srcPoly[j].X - m_normals[j].X *
                            delta), (cInt) Round(m_srcPoly[j].Y - m_normals[j].Y * delta));
                    m_destPoly.push_back(pt1);
                } else {
                    int j = len - 1;
                    k = len - 2;
                    m_sinA = 0;
                    m_normals[j] = DoublePoint(-m_normals[j].X, -m_normals[j].Y);
                    if (node.m_endtype == etOpenSquare)
                        DoSquare(j, k);
                    else
                        DoRound(j, k);
                }

                //re-build m_normals ...
                for (int j = len - 1; j > 0; j--)
                    m_normals[j] = DoublePoint(-m_normals[j - 1].X, -m_normals[j - 1].Y);
                m_normals[0] = DoublePoint(-m_normals[1].X, -m_normals[1].Y);

                k = len - 1;
                for (int j = k - 1; j > 0; --j) OffsetPoint(j, k, node.m_jointype);

                if (node.m_endtype == etOpenButt) {
                    pt1 = IntPoint((cInt) Round(m_srcPoly[0].X - m_normals[0].X * delta),
                            (cInt) Round(m_srcPoly[0].Y - m_normals[0].Y * delta));
                    m_destPoly.push_back(pt1);
                    pt1 = IntPoint((cInt) Round(m_srcPoly[0].X + m_normals[0].X * delta),
                            (cInt) Round(m_srcPoly[0].Y + m_normals[0].Y * delta));
                    m_destPoly.push_back(pt1);
                } else {
                    k = 1;
                    m_sinA = 0;
                    if (node.m_endtype == etOpenSquare)
                        DoSquare(0, 1);
                    else
                        DoRound(0, 1);
                }
                m_destPolys.push_back(m_destPoly);
            }
        }
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::OffsetPoint(int j, int& k, JoinType jointype) {
        //cross product ...
        m_sinA = (m_normals[k].X * m_normals[j].Y - m_normals[j].X * m_normals[k].Y);
        if (std::fabs(m_sinA * m_delta) < 1.0) {
            //dot product ...
            double cosA = (m_normals[k].X * m_normals[j].X + m_normals[j].Y * m_normals[k].Y);
            if (cosA > 0) // angle => 0 degrees
            {
                m_destPoly.push_back(IntPoint(Round(m_srcPoly[j].X + m_normals[k].X * m_delta),
                        Round(m_srcPoly[j].Y + m_normals[k].Y * m_delta)));
                return;
            }
            //else angle => 180 degrees   
        } else if (m_sinA > 1.0) m_sinA = 1.0;
        else if (m_sinA < -1.0) m_sinA = -1.0;

        if (m_sinA * m_delta < 0) {
            m_destPoly.push_back(IntPoint(Round(m_srcPoly[j].X + m_normals[k].X * m_delta),
                    Round(m_srcPoly[j].Y + m_normals[k].Y * m_delta)));
            m_destPoly.push_back(m_srcPoly[j]);
            m_destPoly.push_back(IntPoint(Round(m_srcPoly[j].X + m_normals[j].X * m_delta),
                    Round(m_srcPoly[j].Y + m_normals[j].Y * m_delta)));
        } else
            switch (jointype) {
                case jtMiter:
                {
                    double r = 1 + (m_normals[j].X * m_normals[k].X +
                            m_normals[j].Y * m_normals[k].Y);
                    if (r >= m_miterLim) DoMiter(j, k, r);
                    else DoSquare(j, k);
                    break;
                }
                case jtSquare: DoSquare(j, k);
                    break;
                case jtRound: DoRound(j, k);
                    break;
            }
        k = j;
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::DoSquare(int j, int k) {
        double dx = std::tan(std::atan2(m_sinA,
                m_normals[k].X * m_normals[j].X + m_normals[k].Y * m_normals[j].Y) / 4);
        m_destPoly.push_back(IntPoint(
                Round(m_srcPoly[j].X + m_delta * (m_normals[k].X - m_normals[k].Y * dx)),
                Round(m_srcPoly[j].Y + m_delta * (m_normals[k].Y + m_normals[k].X * dx))));
        m_destPoly.push_back(IntPoint(
                Round(m_srcPoly[j].X + m_delta * (m_normals[j].X + m_normals[j].Y * dx)),
                Round(m_srcPoly[j].Y + m_delta * (m_normals[j].Y - m_normals[j].X * dx))));
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::DoMiter(int j, int k, double r) {
        double q = m_delta / r;
        m_destPoly.push_back(IntPoint(Round(m_srcPoly[j].X + (m_normals[k].X + m_normals[j].X) * q),
                Round(m_srcPoly[j].Y + (m_normals[k].Y + m_normals[j].Y) * q)));
    }
    //------------------------------------------------------------------------------

    void ClipperOffset::DoRound(int j, int k) {
        double a = std::atan2(m_sinA,
                m_normals[k].X * m_normals[j].X + m_normals[k].Y * m_normals[j].Y);
        int steps = std::max((int) Round(m_StepsPerRad * std::fabs(a)), 1);

        double X = m_normals[k].X, Y = m_normals[k].Y, X2;
        for (int i = 0; i < steps; ++i) {
            m_destPoly.push_back(IntPoint(
                    Round(m_srcPoly[j].X + X * m_delta),
                    Round(m_srcPoly[j].Y + Y * m_delta)));
            X2 = X;
            X = X * m_cos - m_sin * Y;
            Y = X2 * m_sin + Y * m_cos;
        }
        m_destPoly.push_back(IntPoint(
                Round(m_srcPoly[j].X + m_normals[j].X * m_delta),
                Round(m_srcPoly[j].Y + m_normals[j].Y * m_delta)));
    }

    //------------------------------------------------------------------------------
    // Miscellaneous public functions
    //------------------------------------------------------------------------------

    void Clipper::DoSimplePolygons() {
        PolyOutList::size_type i = 0;
        while (i < m_PolyOuts.size()) {
            OutRec* outrec = m_PolyOuts[i++];
            OutPt* op = outrec->Pts;
            if (!op || outrec->IsOpen) continue;
            do //for each Pt in Polygon until duplicate found do ...
            {
                OutPt* op2 = op->Next;
                while (op2 != outrec->Pts) {
                    if ((op->Pt == op2->Pt) && op2->Next != op && op2->Prev != op) {
                        //split the polygon into two ...
                        OutPt* op3 = op->Prev;
                        OutPt* op4 = op2->Prev;
                        op->Prev = op4;
                        op4->Next = op;
                        op2->Prev = op3;
                        op3->Next = op2;

                        outrec->Pts = op;
                        OutRec* outrec2 = CreateOutRec();
                        outrec2->Pts = op2;
                        UpdateOutPtIdxs(*outrec2);
                        if (Poly2ContainsPoly1(outrec2->Pts, outrec->Pts)) {
                            //OutRec2 is contained by OutRec1 ...
                            outrec2->IsHole = !outrec->IsHole;
                            outrec2->FirstLeft = outrec;
                            if (m_UsingPolyTree) FixupFirstLefts2(outrec2, outrec);
                        } else
                            if (Poly2ContainsPoly1(outrec->Pts, outrec2->Pts)) {
                            //OutRec1 is contained by OutRec2 ...
                            outrec2->IsHole = outrec->IsHole;
                            outrec->IsHole = !outrec2->IsHole;
                            outrec2->FirstLeft = outrec->FirstLeft;
                            outrec->FirstLeft = outrec2;
                            if (m_UsingPolyTree) FixupFirstLefts2(outrec, outrec2);
                        } else {
                            //the 2 polygons are separate ...
                            outrec2->IsHole = outrec->IsHole;
                            outrec2->FirstLeft = outrec->FirstLeft;
                            if (m_UsingPolyTree) FixupFirstLefts1(outrec, outrec2);
                        }
                        op2 = op; //ie get ready for the Next iteration
                    }
                    op2 = op2->Next;
                }
                op = op->Next;
            }            while (op != outrec->Pts);
        }
    }
    //------------------------------------------------------------------------------

    static void ReversePath(Path& p) {
        std::reverse(p.begin(), p.end());
    }
    //------------------------------------------------------------------------------

    static void ReversePaths(Paths& p) {
        for (typename Paths::size_type i = 0; i < p.size(); ++i)
            ReversePath(p[i]);
    }
    //------------------------------------------------------------------------------

    static void SimplifyPolygon(const Path &in_poly, Paths &out_polys, PolyFillType fillType) {
        Clipper c;
        c.StrictlySimple(true);
        c.AddPath(in_poly, ptSubject, true);
        c.Execute(ctUnion, out_polys, fillType, fillType);
    }
    //------------------------------------------------------------------------------

    static void SimplifyPolygons(const Paths &in_polys, Paths &out_polys, PolyFillType fillType) {
        Clipper c;
        c.StrictlySimple(true);
        c.AddPaths(in_polys, ptSubject, true);
        c.Execute(ctUnion, out_polys, fillType, fillType);
    }
    //------------------------------------------------------------------------------

    static void SimplifyPolygons(Paths &polys, PolyFillType fillType) {
        SimplifyPolygons(polys, polys, fillType);
    }
    //------------------------------------------------------------------------------

    inline static double DistanceSqrd(const IntPoint& pt1, const IntPoint& pt2) {
        double Dx = ((double) pt1.X - pt2.X);
        double dy = ((double) pt1.Y - pt2.Y);
        return (Dx * Dx + dy * dy);
    }
    //------------------------------------------------------------------------------

    static double DistanceFromLineSqrd(
            const IntPoint& pt, const IntPoint& ln1, const IntPoint& ln2) {
        //The equation of a line in general form (Ax + By + C = 0)
        //given 2 points (x¹,y¹) & (x²,y²) is ...
        //(y¹ - y²)x + (x² - x¹)y + (y² - y¹)x¹ - (x² - x¹)y¹ = 0
        //A = (y¹ - y²); B = (x² - x¹); C = (y² - y¹)x¹ - (x² - x¹)y¹
        //perpendicular distance of point (x³,y³) = (Ax³ + By³ + C)/Sqrt(A² + B²)
        //see http://en.wikipedia.org/wiki/Perpendicular_distance
        double A = double(ln1.Y - ln2.Y);
        double B = double(ln2.X - ln1.X);
        double C = A * ln1.X + B * ln1.Y;
        C = A * pt.X + B * pt.Y - C;
        return (C * C) / (A * A + B * B);
    }
    //---------------------------------------------------------------------------

    static bool SlopesNearCollinear(const IntPoint& pt1,
            const IntPoint& pt2, const IntPoint& pt3, double distSqrd) {
        //this function is more accurate when the point that's geometrically
        //between the other 2 points is the one that's tested for distance.
        //ie makes it more likely to pick up 'spikes' ...
        if (Abs(pt1.X - pt2.X) > Abs(pt1.Y - pt2.Y)) {
            if ((pt1.X > pt2.X) == (pt1.X < pt3.X))
                return DistanceFromLineSqrd(pt1, pt2, pt3) < distSqrd;
            else if ((pt2.X > pt1.X) == (pt2.X < pt3.X))
                return DistanceFromLineSqrd(pt2, pt1, pt3) < distSqrd;
            else
                return DistanceFromLineSqrd(pt3, pt1, pt2) < distSqrd;
        } else {
            if ((pt1.Y > pt2.Y) == (pt1.Y < pt3.Y))
                return DistanceFromLineSqrd(pt1, pt2, pt3) < distSqrd;
            else if ((pt2.Y > pt1.Y) == (pt2.Y < pt3.Y))
                return DistanceFromLineSqrd(pt2, pt1, pt3) < distSqrd;
            else
                return DistanceFromLineSqrd(pt3, pt1, pt2) < distSqrd;
        }
    }
    //------------------------------------------------------------------------------

    static bool PointsAreClose(IntPoint pt1, IntPoint pt2, double distSqrd) {
        double Dx = (double) pt1.X - pt2.X;
        double dy = (double) pt1.Y - pt2.Y;
        return ((Dx * Dx) + (dy * dy) <= distSqrd);
    }
    //------------------------------------------------------------------------------

    static OutPt* ExcludeOp(OutPt* op) {
        OutPt* result = op->Prev;
        result->Next = op->Next;
        op->Next->Prev = result;
        result->Idx = 0;
        return result;
    }
    //------------------------------------------------------------------------------

    static void CleanPolygon(const Path& in_poly, Path& out_poly, double distance) {
        //distance = proximity in units/pixels below which vertices
        //will be stripped. Default ~= sqrt(2).

        size_t size = in_poly.size();

        if (size == 0) {
            out_poly.clear();
            return;
        }

        OutPt* outPts = new OutPt[size];
        for (size_t i = 0; i < size; ++i) {
            outPts[i].Pt = in_poly[i];
            outPts[i].Next = &outPts[(i + 1) % size];
            outPts[i].Next->Prev = &outPts[i];
            outPts[i].Idx = 0;
        }

        double distSqrd = distance * distance;
        OutPt* op = &outPts[0];
        while (op->Idx == 0 && op->Next != op->Prev) {
            if (PointsAreClose(op->Pt, op->Prev->Pt, distSqrd)) {
                op = ExcludeOp(op);
                size--;
            }
            else if (PointsAreClose(op->Prev->Pt, op->Next->Pt, distSqrd)) {
                ExcludeOp(op->Next);
                op = ExcludeOp(op);
                size -= 2;
            } else if (SlopesNearCollinear(op->Prev->Pt, op->Pt, op->Next->Pt, distSqrd)) {
                op = ExcludeOp(op);
                size--;
            } else {
                op->Idx = 1;
                op = op->Next;
            }
        }

        if (size < 3) size = 0;
        out_poly.resize(size);
        for (size_t i = 0; i < size; ++i) {
            out_poly[i] = op->Pt;
            op = op->Next;
        }
        delete [] outPts;
    }
    //------------------------------------------------------------------------------

    static void CleanPolygon(Path& poly, double distance) {
        CleanPolygon(poly, poly, distance);
    }
    //------------------------------------------------------------------------------

    static void CleanPolygons(const Paths& in_polys, Paths& out_polys, double distance) {
        out_polys.resize(in_polys.size());
        for (typename Paths::size_type i = 0; i < in_polys.size(); ++i)
            CleanPolygon(in_polys[i], out_polys[i], distance);
    }
    //------------------------------------------------------------------------------

    static void CleanPolygons(Paths& polys, double distance) {
        CleanPolygons(polys, polys, distance);
    }
    //------------------------------------------------------------------------------

    static void Minkowski(const Path& poly, const Path& path,
            Paths& solution, bool isSum, bool isClosed) {
        int delta = (isClosed ? 1 : 0);
        size_t polyCnt = poly.size();
        size_t pathCnt = path.size();
        Paths pp;
        pp.reserve(pathCnt);
        if (isSum)
            for (size_t i = 0; i < pathCnt; ++i) {
                Path p;
                p.reserve(polyCnt);
                for (size_t j = 0; j < poly.size(); ++j)
                    p.push_back(IntPoint(path[i].X + poly[j].X, path[i].Y + poly[j].Y));
                pp.push_back(p);
            } else
            for (size_t i = 0; i < pathCnt; ++i) {
                Path p;
                p.reserve(polyCnt);
                for (size_t j = 0; j < poly.size(); ++j)
                    p.push_back(IntPoint(path[i].X - poly[j].X, path[i].Y - poly[j].Y));
                pp.push_back(p);
            }

        solution.clear();
        solution.reserve((pathCnt + delta) * (polyCnt + 1));
        for (size_t i = 0; i < pathCnt - 1 + delta; ++i)
            for (size_t j = 0; j < polyCnt; ++j) {
                Path quad;
                quad.reserve(4);
                quad.push_back(pp[i % pathCnt][j % polyCnt]);
                quad.push_back(pp[(i + 1) % pathCnt][j % polyCnt]);
                quad.push_back(pp[(i + 1) % pathCnt][(j + 1) % polyCnt]);
                quad.push_back(pp[i % pathCnt][(j + 1) % polyCnt]);
                if (!Orientation(quad)) ReversePath(quad);
                solution.push_back(quad);
            }
    }
    //------------------------------------------------------------------------------

    static void MinkowskiSum(const Path& pattern, const Path& path, Paths& solution, bool pathIsClosed) {
        Minkowski(pattern, path, solution, true, pathIsClosed);
        Clipper c;
        c.AddPaths(solution, ptSubject, true);
        c.Execute(ctUnion, solution, pftNonZero, pftNonZero);
    }
    //------------------------------------------------------------------------------

    static void TranslatePath(const Path& input, Path& output, const IntPoint delta) {
        //precondition: input != output
        output.resize(input.size());
        for (size_t i = 0; i < input.size(); ++i)
            output[i] = IntPoint(input[i].X + delta.X, input[i].Y + delta.Y);
    }
    //------------------------------------------------------------------------------

    static void MinkowskiSum(const Path& pattern, const Paths& paths, Paths& solution, bool pathIsClosed) {
        Clipper c;
        for (size_t i = 0; i < paths.size(); ++i) {
            Paths tmp;
            Minkowski(pattern, paths[i], tmp, true, pathIsClosed);
            c.AddPaths(tmp, ptSubject, true);
            if (pathIsClosed) {
                Path tmp2;
                TranslatePath(paths[i], tmp2, pattern[0]);
                c.AddPath(tmp2, ptClip, true);
            }
        }
        c.Execute(ctUnion, solution, pftNonZero, pftNonZero);
    }
    //------------------------------------------------------------------------------

    static void MinkowskiDiff(const Path& poly1, const Path& poly2, Paths& solution) {
        Minkowski(poly1, poly2, solution, false, true);
        Clipper c;
        c.AddPaths(solution, ptSubject, true);
        c.Execute(ctUnion, solution, pftNonZero, pftNonZero);
    }
    //------------------------------------------------------------------------------

    enum NodeType {
        ntAny, ntOpen, ntClosed
    };

    static void AddPolyNodeToPaths(const PolyNode& polynode, NodeType nodetype, Paths& paths) {
        bool match = true;
        if (nodetype == ntClosed) match = !polynode.IsOpen();
        else if (nodetype == ntOpen) return;

        if (!polynode.Contour.empty() && match)
            paths.push_back(polynode.Contour);
        for (int i = 0; i < polynode.ChildCount(); ++i)
            AddPolyNodeToPaths(*polynode.Childs[i], nodetype, paths);
    }
    //------------------------------------------------------------------------------

    static void PolyTreeToPaths(const PolyTree& polytree, Paths& paths) {
        paths.resize(0);
        paths.reserve(polytree.Total());
        AddPolyNodeToPaths(polytree, ntAny, paths);
    }
    //------------------------------------------------------------------------------

    static void ClosedPathsFromPolyTree(const PolyTree& polytree, Paths& paths) {
        paths.resize(0);
        paths.reserve(polytree.Total());
        AddPolyNodeToPaths(polytree, ntClosed, paths);
    }
    //------------------------------------------------------------------------------

    static void OpenPathsFromPolyTree(PolyTree& polytree, Paths& paths) {
        paths.resize(0);
        paths.reserve(polytree.Total());
        //Open paths are top level only, so ...
        for (int i = 0; i < polytree.ChildCount(); ++i)
            if (polytree.Childs[i]->IsOpen())
                paths.push_back(polytree.Childs[i]->Contour);
    }
    /*
    //------------------------------------------------------------------------------

    std::ostream& operator<<(std::ostream &s, const IntPoint &p) {
        s << "(" << p.X << "," << p.Y << ")";
        return s;
    }
    //------------------------------------------------------------------------------

    std::ostream& operator<<(std::ostream &s, const Path &p) {
        if (p.empty()) return s;
        Path::size_type last = p.size() - 1;
        for (Path::size_type i = 0; i < last; i++)
            s << "(" << p[i].X << "," << p[i].Y << "), ";
        s << "(" << p[last].X << "," << p[last].Y << ")\n";
        return s;
    }
    //------------------------------------------------------------------------------

    std::ostream& operator<<(std::ostream &s, const Paths &p) {
        for (Paths::size_type i = 0; i < p.size(); i++)
            s << p[i];
        s << "\n";
        return s;
    }
    //------------------------------------------------------------------------------
    */
//} //ClipperLib namespace

//#endif /* NPOLYGON_CLIP_IMPL_H */

